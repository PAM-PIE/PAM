function Mia(varargin)
% MIA  Microtime Image Analysis 2.0
% Graphical User Interface to analyse multicolor image series using
% fluctuation / correlation analysis.
% The program can also be run from the command line by providing the full path to a file as an argument when calling Mia.


global UserValues MIAData PathToApp
h.Mia=findobj('Tag','Mia');

% add functions folder to matlab path
addpath(genpath(['.' filesep 'functions']));

if ~isempty(h.Mia)
    %Mia figure exists already
    if numel(varargin)~=1
        % Mia called from Matlab command without argument
        % Mia called from Launcher or PAM
        figure(h.Mia); % Gives focus to Mia figure
    else
        % Mia called from Matlab command with a single argument
        % f.e. path to a .json metadata file
        Mia_Command(varargin{1});
    end
    return; %do not re-initialize UI
end

% get path to Mia.m or Mia.exe
if isempty(PathToApp)
    GetAppFolder();
end

if isempty(varargin)
    %%% show splash screen:
    %%% if you load Mia from Matlab command 
    %%% if you load Mia from a compiled Mia.m main function
    s = SplashScreen( 'Splashscreen', [PathToApp filesep 'images' filesep 'PAM' filesep 'logo.png'], ...
        'ProgressBar', 'on', ...
        'ProgressPosition', 5, ...
        'ProgressRatio', 0 );
    s.addText( 30, 50, 'MIA - Microtime Image Analysis', 'FontSize', 30, 'Color', [1 1 1] );
    s.addText( 30, 80, 'v2.0', 'FontSize', 20, 'Color', [1 1 1] );
    s.addText( 375, 395, 'Loading...', 'FontSize', 25, 'Color', 'white' );
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Figure generation %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Loads user profile
LST = evalc('LSUserValues(0);');

if numel(varargin)~=1 % Normal MIA UI run in matlab
    disp(LST);
else
    fprintf('SPECTRAL_RICS: Initializing analysis...\n')
end

%%% To save typing
Look=UserValues.Look;
%%% Generates the Mia figure
h.Mia = figure(...
    'Units','normalized',...
    'Tag','Mia',...
    'Name','MIA: Microtime image analysis',...
    'NumberTitle','off',...
    'Menu','none',...
    'defaultUicontrolFontName',Look.Font,...
    'defaultAxesFontName',Look.Font,...
    'defaultTextFontName',Look.Font,...
    'Toolbar','figure',...
    'UserData',[],...
    'OuterPosition',[0.01 0.1 0.98 0.9],...
    'CloseRequestFcn',@CloseWindow,...
    'Visible','off');
%h.Mia.Visible='off';
h.Text=[];

%%% Sets background of axes and other things
whitebg(Look.Axes);
%%% Changes Pam background; must be called after whitebg
h.Mia.Color=Look.Back;
%%% Remove unneeded items from toolbar
toolbar = findall(h.Mia,'Type','uitoolbar');
toolbar_items = findall(toolbar);
if verLessThan('matlab','9.5') %%% toolbar behavior changed in MATLAB 2018b
    delete(toolbar_items([2:7 13:17]));
else %%% 2018b and upward
    %%% just remove the tool bar since the options are now in the axis
    %%% (e.g. axis zoom etc)
    delete(toolbar_items);
end
%%% Menu to load mia data
h.Mia_Load = uimenu(...
    'Parent',h.Mia,...
    'Label','Load...',...
    'Tag','Load_Mia');
%%% Load TIFF
h.Mia_Load_TIFF_Single = uimenu(...
    'Parent',h.Mia_Load,...
    'Label','...single color TIFFs',...
    'Callback',{@Mia_Load,1},...
    'Tag','Load_Mia_TIFF_SIngle');
%%% Load TIFF
h.Mia_Load_TIFF_RGB = uimenu(...
    'Parent',h.Mia_Load,...
    'Label','...RGB TIFFs');
%%% Load TIFF
h.Mia_Load_TIFF_RGB_GR = uimenu(...
    'Parent',h.Mia_Load_TIFF_RGB,...
    'Label','...Green-Red',...
    'Callback',{@Mia_Load,4},...
    'Tag','Load_Mia_TIFF_SIngle');
%%% Load TIFF
h.Mia_Load_TIFF_RGB_GR = uimenu(...
    'Parent',h.Mia_Load_TIFF_RGB,...
    'Label','...Blue-Green',...
    'Callback',{@Mia_Load,5},...
    'Tag','Load_Mia_TIFF_Single');
%%% Load TIFF
h.Mia_Load_TIFF_RGB_GR = uimenu(...
    'Parent',h.Mia_Load_TIFF_RGB,...
    'Label','...Blue-Red',...
    'Callback',{@Mia_Load,6},...
    'Tag','Load_Mia_TIFF_Single');

%%% Load TIFF
h.Mia_Load_TIFF_RLICS = uimenu(...
    'Parent',h.Mia_Load,...
    'Label','...weighted TIFFs',...
    'Callback',{@Mia_Load,1.5},...
    'Tag','Load_Mia_TIFF_RLICS');
%%% Load Data from Pam
h.Mia_Load_Pam = uimenu(...
    'Parent',h.Mia_Load,...
    'Label','...data from PAM...');
%%% Load selected channels
h.Mia_Load_Pam_Sel = uimenu(...
    'Parent',h.Mia_Load_Pam,...
    'Label','...selected PIE channels on the "Channels" tab',...
    'Callback',{@Mia_Load,2},...
    'Tag','Mia_Load_Pam_Sel');
%%% Load all PIE channels
h.Mia_Load_Pam_All = uimenu(...
    'Parent',h.Mia_Load_Pam,...
    'Label','...all available PIE channels',...
    'Callback',{@Mia_Load,2},...
    'Tag','Mia_Load_Pam_All');

%%% Load custom data format
h.Mia_Load_Custom = uimenu(...
    'Parent',h.Mia_Load,...
    'Label','...custom data format',...
    'Callback',{@Mia_Load,3},...
    'Tag','Load_Mia_Custom');

%%% Load lifetime data format
h.Mia_Load_LT = uimenu(...
    'Parent',h.Mia_Load,...
    'Label','...mean lifetime data',...
    'Callback',{@Mia_Load,7},...
    'Tag','Load_Mia_LT');

%%% Menu to open MIAFit and Spectral
h.Mia_Open = uimenu(...
    'Parent',h.Mia,...
    'Label','Advanced Analyses...',...
    'Tag','Mia_Open');
%%% Load and analyse calibration
h.Mia_Open_Calibration = uimenu(...
    'Parent',h.Mia_Open,...
    'Label','RICS focal volume (.czi)',...
    'Callback',{@Calibration},...
    'Tag','Mia_Open_Calibration');
%%% Open MIAFit
h.Mia_Open_MIAFit = uimenu(...
    'Parent',h.Mia_Open,...
    'Label','MIAFit',...
    'Callback',@MIAFit,...
    'Tag','Mia_Open_MIAFit');
%%% Open RICSPE
h.Mia_Open_RICSPE = uimenu(...
    'Parent',h.Mia_Open,...
    'Label','RICSPE',...
    'Callback',@RICSPE_GUI,...
    'Tag','Mia_Open_RICSPE');
%%% Open Spectral
h.Mia_Open_Spectral = uimenu(...
    'Parent',h.Mia_Open,...
    'Label','Spectral',...
    'Callback',@Spectral,...
    'Tag','Mia_Open_Spectral');

%%% Menu to Import ROI and other data
h.Mia_Import = uimenu(...
    'Parent',h.Mia,...
    'Label','Import...',...
    'Tag','Mia_Import');
%%% Import Cellpose ROIs
h.Mia_Import_Cellpose = uimenu(...
    'Parent',h.Mia_Import,...
    'Label','Cellpose ROIs (_seg.npy)',...
    'Callback', {@Mia_Import_Cellpose},...
    'Tag','Import_Cellpose');
%%% Import FLIM Analysis
h.Mia_Import_FLIM = uimenu(...
    'Parent',h.Mia_Import,...
    'Label','FLIM Analysis (.mat)',...
    'Callback', {@Mia_Import_FLIM},...
    'Tag','Import_FLIM');

%%% Menu to Export images
h.Mia_Export = uimenu(...
    'Parent',h.Mia,...
    'Label','Export...',...
    'Tag','Save_Mia');
%%% Load TIFF
h.Mia_Export_TwoColorImage = uimenu(...
    'Parent',h.Mia_Export,...
    'Label','Dual color image',...
    'Tag','Export_TwoColorImage');
%%% Load TIFF
h.Mia_Export_TwoColorImage_unc = uimenu(...
    'Parent',h.Mia_Export_TwoColorImage,...
    'Label','uncorrected',...
    'Callback',{@Mia_Export,2},...
    'Tag','Export_TwoColorImage_unc');
%%% Load TIFF
h.Mia_Export_TwoColorImage_cor = uimenu(...
    'Parent',h.Mia_Export_TwoColorImage,...
    'Label','corrected',...
    'Callback',{@Mia_Export,3},...
    'Tag','Export_TwoColorImage_cor');
%%% Load TIFF
h.Mia_Export_Video = uimenu(...
    'Parent',h.Mia_Export,...
    'Label','MP4 video',...
    'Tag','Export_Video');
%%% Load TIFF
h.Mia_Export_Video1 = uimenu(...
    'Parent',h.Mia_Export_Video,...
    'Label','channel 1 corrected',...
    'Callback',{@Mia_Export,4},...
    'Tag','Export_Video1');
%%% Load TIFF
h.Mia_Export_Video2 = uimenu(...
    'Parent',h.Mia_Export_Video,...
    'Label','channel 2 corrected',...
    'Callback',{@Mia_Export,5},...
    'Tag','Export_Video2');

h.Mia_Extras = uimenu(...
    'Parent',h.Mia,...
    'Tag','Extras',...
    'Label','Extras');
% h.Mia_ParallelProcessing = uimenu(...
%     'Parent',h.Mia_Extras,...
%     'Tag','ParallelProcessing',...
%     'Label','Parallel Processing');
% if UserValues.Settings.Pam.ParallelProcessing == 0
%     checked = 'off';
% end
% if UserValues.Settings.Pam.ParallelProcessing == Inf
%     checked = 'on';
% end
% h.Mia_UseParfor = uimenu(...
%     'Parent',h.Mia_ParallelProcessing,...
%     'Tag','UseParfor',...
%     'Label','Enable Multicore',...
%     'Checked', checked,...
%     'Callback',{@MIA_Various,14});
% h.Mia_NumberOfCores = uimenu(...
%     'Parent',h.Mia_ParallelProcessing,...
%     'Tag','UseParfor',...
%     'Label',['Number of Cores: ' num2str(UserValues.Settings.Pam.NumberOfCores)],...
%     'Callback',{@MIA_Various,14});
h.Mia_Sim = uimenu(...
    'Parent', h.Mia_Extras,...
    'Tag','Sim_Menu',...
    'Label','Simulate photon data',...
    'Callback',@Sim);
h.Mia_Look = uimenu(...
    'Parent', h.Mia_Extras,...
    'Tag','Look_Menu',...
    'Label','Adjust Mia Look',...
    'Separator','on',...
    'Callback',@LookSetup);
h.Mia_Manual = uimenu(...
    'Parent', h.Mia_Extras,...
    'Tag','Manual_Menu',...
    'Separator','on',...
    'Label','Mia Manual',...
    'Callback',@Open_Doc);
h.Mia_NotePad = uimenu(...
    'Parent',h.Mia_Extras,...
    'Label','Notepad',...
    'Separator','on',...
    'Callback',@Open_Notepad);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Progressbar and file names %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Panel for progressbar
h.Mia_Progress_Panel = uibuttongroup(...
    'Parent',h.Mia,...
    'Units','normalized',...
    'BackgroundColor', Look.Back,...
    'ForegroundColor', Look.Fore,...
    'HighlightColor', Look.Control,...
    'ShadowColor', Look.Shadow,...
    'Position',[0.001 0.98 0.998 0.02]);
%%% Axes for progressbar
h.Mia_Progress_Axes = axes(...
    'Parent',h.Mia_Progress_Panel,...
    'Units','normalized',...
    'Color',Look.Control,...
    'Position',[0 0 1 1]);
h.Mia_Progress_Axes.XTick=[]; h.Mia_Progress_Axes.YTick=[];
%%% Progress and filename text
h.Mia_Progress_Text=text(...
    'Parent',h.Mia_Progress_Axes,...
    'Units','normalized',...
    'FontSize',12,...
    'FontWeight','bold',...
    'String','Nothing loaded',...
    'Interpreter','none',...
    'HorizontalAlignment','center',...
    'BackgroundColor','none',...
    'Color',Look.Fore,...
    'Position',[0.5 0.5]);
%% Main tab container %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
h.Mia_Main_Tabs = uitabgroup(...
    'Parent',h.Mia,...
    'Tag','Mia_Main_Tabs',...
    'Units','normalized',...
    'Position',[0 0 1 0.98]);
%% Image Tab
h.Mia_Image.Tab = uitab(...
    'Parent',h.Mia_Main_Tabs,...
    'Title','Image',...
    'Tag','Mia_Main_Tabs',...
    'Units','normalized');
h.Mia_Image.Panel = uibuttongroup(...
    'Parent',h.Mia_Image.Tab,...
    'Tag','Mia_Main_Panel',...
    'Units','normalized',...
    'BackgroundColor', Look.Back,...
    'ForegroundColor', Look.Fore,...
    'HighlightColor', Look.Control,...
    'ShadowColor', Look.Shadow,...
    'Position',[0 0 0.8 1]);

%% Contextmenu for freehand drawing an ROI
h.Mia_Image.Menu = uicontextmenu;
h.Mia_Image.Select_Manual_ROI = uimenu(...
    'Parent',h.Mia_Image.Menu,...
    'Label','Select manual ROI',...
    'Callback',{@Mia_Freehand,1},...
    'Visible', 'off');
h.Mia_Image.Unselect_Manual_ROI = uimenu(...
    'Parent',h.Mia_Image.Menu,...
    'Label','Unselect manual ROI',...
    'Callback',{@Mia_Freehand,2},...
    'Visible', 'off');
h.Mia_Image.Clear_Manual_ROI = uimenu(...
    'Parent',h.Mia_Image.Menu,...
    'Label','Clear manual ROI',...
    'Callback',{@Mia_Freehand,3},...
    'Visible', 'off');
h.Mia_Image.FRAP_ROI_Menu = uimenu(...
    'Parent',h.Mia_Image.Menu,...
    'Label','Draw ROI for FRAP',...
    'Callback',{@Mia_Freehand,7},...
    'Visible', 'off');

%% 4 images on the main tab
for i=1:2
    %%% Axes to display images
    % raw image i - Keep the tag!
    h.Mia_Image.Axes(i,1)= axes(...
        'Parent',h.Mia_Image.Panel,...
        'Tag', ['ImRaw', num2str(i)],...
        'Units','normalized',...
        'NextPlot','Add',...
        'Position',[0.28 0.99-0.49*i 0.35 0.48]);
    %colormap(h.Mia_Image.Axes(i,1),gray(64));
    % corrected image i - Keep the tag!
    h.Mia_Image.Axes(i,2)= axes(...
        'Parent',h.Mia_Image.Panel,...
        'Tag', ['ImCorr', num2str(i)],...
        'Units','normalized',...
        'NextPlot','Add',...
        'Position',[0.64 0.99-0.49*i 0.35 0.48]);
   % h.Mia_Image.Axes(i,2).Colormap=,gray(64));
    %%% Initializes empty plots
    %raw plot i
    h.Plots.Image(i,1)=imagesc(zeros(1,1,3)+0.75,...
        'Parent',h.Mia_Image.Axes(i,1),...
        'ButtonDownFcn',{@Mia_ROI,2},...
        'Tag',['im' num2str(i)]);
    h.Mia_Image.Axes(i,1).DataAspectRatio=[1 1 1];
    h.Mia_Image.Axes(i,1).XTick=[];
    h.Mia_Image.Axes(i,1).YTick=[];
    h.Plots.Image(i,2)=imagesc(zeros(1,1,3)+0.75,...
        'Parent',h.Mia_Image.Axes(i,2),...
        'ButtonDownFcn',{@Mia_Export,1},...
        'UIContextMenu', h.Mia_Image.Menu);
    %corrected plot i
    h.Mia_Image.Axes(i,2).DataAspectRatio=[1 1 1];
    h.Mia_Image.Axes(i,2).XTick=[];
    h.Mia_Image.Axes(i,2).YTick=[];
    %%% Initializes a rectangle ROI
    h.Plots.ROI(i)=rectangle(...
        'Parent',h.Mia_Image.Axes(i,1),...
        'Position',[0.5 0.5 1 1],...
        'EdgeColor',[1 1 1],...
        'HitTest','off');
    %%% Set the colormap of the axis (and the colorbar) to the preset value
    h.Mia_Image.Colorbar(i) = colorbar(h.Mia_Image.Axes(i,1));
    h.Mia_Image.Axes(i,1).Colormap = Image_Colormap([],[],i);
    h.Mia_Image.Colorbar(i).Color = [1 1 1];
    if i == 1
        h.Mia_Image.Colorbar(i).Title.String = '[kHz]';
        h.Mia_Image.Colorbar(i).Title.FontSize = 11;
    end
    h.Mia_Image.Colorbar(i).Title.Color = [1 1 1];
end

%% Intensity traces
h.Mia_Image.Intensity_Axes = axes(...
    'Parent',h.Mia_Image.Panel,...
    'Units','normalized',...ots.Int(1,2)
    'XColor',Look.Fore,...
    'YColor',Look.Fore,...
    'Position',[0.05 0.55 0.21 0.38]);
% raw channel 1
h.Plots.Int(1,1) = line(...
    'Parent',h.Mia_Image.Intensity_Axes,...
    'XData',[0 1],...
    'YData', [0 0],...
    'LineStyle','--',...
    'Color',[0 0.6 0]);
% raw channel 2
h.Plots.Int(2,1) = line(...
    'Parent',h.Mia_Image.Intensity_Axes,...
    'XData',[0 1],...
    'YData', [0 0],...
    'LineStyle','--',...
    'Visible','off',...
    'Color',[1 0 0]);
% corrected channel 1
h.Plots.Int(1,2) = line(...
    'Parent',h.Mia_Image.Intensity_Axes,...
    'XData',[0 1],...
    'YData', [0 0],...
    'Color',[0 0.6 0]);
% corrected channel 2
h.Plots.Int(2,2) = line(...
    'Parent',h.Mia_Image.Intensity_Axes,...
    'XData',[0 1],...
    'YData', [0 0],...
    'Visible','off',...
    'Color',[1 0 0]);
h.Mia_Image.Intensity_Axes.XLabel.String = 'Frame';
h.Mia_Image.Intensity_Axes.YLabel.String = 'Average Count rate [kHz]';
h.Mia_Image.Intensity_Axes.XLabel.Color = Look.Fore;
h.Mia_Image.Intensity_Axes.YLabel.Color = Look.Fore;
h.Mia_Image.Intensity_Axes.YLabel.UserData = 1;
h.Mia_Image.Intensity_Axes.YLabel.Interactions = []; %prevents that clicking the label triggers editing the label
h.Mia_Image.Intensity_Axes.YLabel.ButtonDownFcn = {@MIA_Various,[1 2]};
h.Mia_Image.Intensity_Axes.XLabel.UserData = 1;
h.Mia_Image.Intensity_Axes.XLabel.Interactions = []; %prevents that clicking the label triggers editing the label
h.Mia_Image.Intensity_Axes.XLabel.ButtonDownFcn = {@MIA_Various,[1 2]};

%% Bottom left intensity or PCH histogram
%%% popupmenu to choose between mean intensity histogram or PCH
h.Mia_Image.IntHist = uicontrol(...
    'Parent', h.Mia_Image.Panel,...
    'Style','popupmenu',...
    'Units','normalized',...
    'FontSize',12,...
    'BackgroundColor', Look.Control,...
    'ForegroundColor', Look.Fore,...
    'Position',[0.02 0.47, 0.12 0.02],...
    'Value', 1,...
    'Callback',{@Update_Plots,4},...
    'String',{'Intensity','PCH'});
if ismac
    h.Mia_Image.IntHist.ForegroundColor = [0 0 0];
    h.Mia_Image.IntHist.BackgroundColor = [1 1 1];
end

h.Mia_Image.IntHist_Axes = axes(...
    'Parent',h.Mia_Image.Panel,...
    'Units','normalized',...
    'XColor',Look.Fore,...
    'YColor',Look.Fore,...
    'Position',[0.05 0.07 0.21 0.38]);
%%% first channel raw data
h.Plots.IntHist(1,1) = line(...
    'Parent',h.Mia_Image.IntHist_Axes,...
    'XData',[0 1],...
    'YData', [0 0],...
    'LineStyle','--',...
    'Color',[0 0.6 0]);
%%% second channel raw data
h.Plots.IntHist(2,1) = line(...
    'Parent',h.Mia_Image.IntHist_Axes,...
    'XData',[0 1],...
    'YData', [0 0],...
    'LineStyle','--',...
    'Visible','off',...
    'Color',[1 0 0]);
%%% first channel data within ROI, corrected
h.Plots.IntHist(1,2) = line(...
    'Parent',h.Mia_Image.IntHist_Axes,...
    'XData',[0 1],...
    'YData', [0 0],...
    'Color',[0 0.6 0]);
%%% second channel data within ROI, corrected
h.Plots.IntHist(2,2) = line(...
    'Parent',h.Mia_Image.IntHist_Axes,...
    'XData',[0 1],...
    'YData', [0 0],...
    'Visible','off',...
    'Color',[1 0 0]);
h.Mia_Image.IntHist_Axes.XLabel.String = 'Average Count rate [kHz]';
h.Mia_Image.IntHist_Axes.YLabel.String = 'Frequency';
h.Mia_Image.IntHist_Axes.XLabel.Color = Look.Fore;
h.Mia_Image.IntHist_Axes.YLabel.Color = Look.Fore;
h.Mia_Image.IntHist_Axes.YLabel.UserData = 1;
h.Mia_Image.IntHist_Axes.YLabel.Interactions = []; %prevents that clicking the label triggers editing the label
h.Mia_Image.IntHist_Axes.YLabel.ButtonDownFcn = {@MIA_Various,[1 2]};
h.Mia_Image.IntHist_Axes.XLabel.UserData = 1; 
h.Mia_Image.IntHist_Axes.XLabel.Interactions = []; %prevents that clicking the label triggers editing the label
h.Mia_Image.IntHist_Axes.XLabel.ButtonDownFcn = {@MIA_Various,[1 2]};


%% Settings Tab container
h.Mia_Image.Settings.Panel = uibuttongroup(...
    'Parent',h.Mia_Image.Tab,...
    'Tag','Mia_Settings_Panel',...
    'Units','normalized',...
    'BackgroundColor', Look.Back,...
    'ForegroundColor', Look.Fore,...
    'HighlightColor', Look.Control,...
    'ShadowColor', Look.Shadow,...
    'Position',[0.8 0.5 0.2 0.5]);
h.Mia_Image.Settings.Tabs = uitabgroup(...
    'Parent',h.Mia_Image.Settings.Panel,...
    'Tag','Mia_Settings_Tabs',...
    'Units','normalized',...
    'Position',[0 0 1 1]);
%% Mia Channel setting container
%%% Tab and panel for Mia image settings UIs
h.Mia_Image.Settings.Channel_Tab= uitab(...
    'Parent',h.Mia_Image.Settings.Tabs,...
    'Title','Channels');
h.Mia_Image.Settings.Channel_Panel = uibuttongroup(...
    'Parent',h.Mia_Image.Settings.Channel_Tab,...
    'Units','normalized',...
    'BackgroundColor', Look.Back,...
    'ForegroundColor', Look.Fore,...
    'HighlightColor', Look.Control,...
    'ShadowColor', Look.Shadow,...
    'Position',[0 0 1 1]);

%%% Text
h.Text{end+1} = handle(uicontrol(...
    'Parent',h.Mia_Image.Settings.Channel_Panel,...
    'Style','text',...
    'Units','normalized',...
    'FontSize',14,...
    'HorizontalAlignment','center',...
    'BackgroundColor', Look.Back,...
    'ForegroundColor', Look.Fore,...
    'Position',[0.01 0.92 0.48 0.06],...
    'String','Top Channel' ));
%%% Text
h.Text{end+1} = handle(uicontrol(...
    'Parent',h.Mia_Image.Settings.Channel_Panel,...
    'Style','text',...
    'Units','normalized',...
    'FontSize',14,...
    'HorizontalAlignment','center',...
    'BackgroundColor', Look.Back,...
    'ForegroundColor', Look.Fore,...
    'Position',[0.51 0.92 0.48 0.06],...
    'String','Bottom Channel' ));
%%% Text
h.Text{end+1} = handle(uicontrol(...
    'Parent',h.Mia_Image.Settings.Channel_Panel,...
    'Style','text',...
    'Units','normalized',...
    'FontSize',12,...
    'HorizontalAlignment','center',...
    'BackgroundColor', Look.Back,...
    'ForegroundColor', Look.Fore,...
    'Position',[0.01 0.86 0.98 0.05],...
    'String','Image Colormap:' ));

%%% Text
h.Text{end+1} = uicontrol(...
    'Parent',h.Mia_Image.Settings.Channel_Panel,...
    'Style','text',...
    'Units','normalized',...
    'FontSize',12,...
    'HorizontalAlignment','center',...
    'BackgroundColor', Look.Back,...
    'ForegroundColor', Look.Fore,...
    'Position',[0.01 0.74 0.98 0.05],...
    'String','Show in left images:');
%%% Select which image to show at the top
% by default: take the list of PIE channels
pielist = [UserValues.PIE.Name,{'Nothing'}]';
for i = 1:(numel(pielist)-1)
    pielist{i} = ['PIE channel: ' pielist{i}];
end
%%% BEWARE, this list is used also for the initialization of other
%%% popupmenus!
h.Mia_Image.Settings.Channel_PIE(1) = uicontrol(...
    'Parent',h.Mia_Image.Settings.Channel_Panel,...
    'Style','popupmenu',...
    'Units','normalized',...
    'FontSize',12,...
    'BackgroundColor', Look.Control,...
    'ForegroundColor', Look.Fore,...
    'CallBack', {@ImageDropDown,1},...
    'Value',1,...
    'Position',[0.01 0.68, 0.48 0.06],...
    'String',pielist);
%%% Select which image to show at the bottom
h.Mia_Image.Settings.Channel_PIE(2) = uicontrol(...
    'Parent',h.Mia_Image.Settings.Channel_Panel,...
    'Style','popupmenu',...
    'Units','normalized',...
    'FontSize',12,...
    'BackgroundColor', Look.Control,...
    'ForegroundColor', Look.Fore,...
    'CallBack', {@ImageDropDown,2},...
    'Value',numel(pielist),...
    'Position',[0.51 0.68, 0.48 0.06],...
    'String',pielist); %Value = nothing by default

%%% Text
h.Text{end+1} = uicontrol(...
    'Parent',h.Mia_Image.Settings.Channel_Panel,...
    'Style','text',...
    'Units','normalized',...
    'FontSize',12,...
    'HorizontalAlignment','center',...
    'BackgroundColor', Look.Back,...
    'ForegroundColor', Look.Fore,...
    'Position',[0.01 0.62, 0.98 0.05],...
    'String','Show in right images:');

%%% Text
h.Text{end+1} = uicontrol(...
    'Parent',h.Mia_Image.Settings.Channel_Panel,...
    'Style','text',...
    'Units','normalized',...
    'FontSize',12,...
    'HorizontalAlignment','center',...
    'BackgroundColor', Look.Back,...
    'ForegroundColor', Look.Fore,...
    'Position',[0.01 0.5, 0.98 0.05],...
    'String','Scale image:');

%%% Popupmenu to select how to scale images
h.Mia_Image.Settings.AutoScale = uicontrol(...
    'Parent',h.Mia_Image.Settings.Channel_Panel,...
    'Style','popupmenu',...
    'Units','normalized',...
    'FontSize',12,...
    'BackgroundColor', Look.Control,...
    'ForegroundColor', Look.Fore,...
    'Position',[0.01 0.44, 0.98 0.06],...
    'Callback',{@Update_Plots,1},...
    'Value',1,...
    'String',{'Autoscale Frame','Autoscale Total','Manual Scale'});
if ismac
    h.Mia_Image.Settings.AutoScale.ForegroundColor = [0 0 0];
    h.Mia_Image.Settings.AutoScale.BackgroundColor = [1 1 1];
    
end
%%% Text
h.Mia_Image.Settings.Scale_Text = uicontrol(...
    'Parent',h.Mia_Image.Settings.Channel_Panel,...
    'Style','text',...
    'Units','normalized',...
    'FontSize',12,...
    'HorizontalAlignment','center',...
    'BackgroundColor', Look.Back,...
    'ForegroundColor', Look.Fore,...
    'Visible', 'off',...
    'Position',[0.01 0.38, 0.98 0.05],...
    'String','Scale min/max [Counts]:');

%%% Checkbox to link channels
h.Mia_Image.Settings.Channel_Link = uicontrol(...
    'Parent',h.Mia_Image.Settings.Channel_Panel,...
    'Style','checkbox',...
    'Units','normalized',...
    'FontSize',12,...
    'Callback',{@Mia_Frame,2,1},...
    'BackgroundColor', Look.Back,...
    'ForegroundColor', Look.Fore,...
    'Position',[0.01 0.17, 0.98 0.05],...
    'Value',1,...
    'String','Link channels');

%%% Text
h.Text{end+1} = uicontrol(...
    'Parent',h.Mia_Image.Settings.Channel_Panel,...
    'Style','text',...
    'Units','normalized',...
    'FontSize',12,...
    'BackgroundColor', Look.Back,...
    'ForegroundColor', Look.Fore,...
    'Position',[0.02 0.11, 0.98 0.05],...
    'String','Show Frame:');

%%% Selection per channel
for i = 1:2
    h.Mia_Image.Settings.Channel_Colormap(i) = uicontrol(...
        'Parent',h.Mia_Image.Settings.Channel_Panel,...
        'Style','popupmenu',...
        'Units','normalized',...
        'FontSize',12,...
        'Value', UserValues.MIA.ColorMap_Main(i),...
        'UserData',UserValues.MIA.CustomColor(i,:),...
        'Callback',{@Update_Plots,1},...
        'ButtonDownFcn',@Mia_Color,...
        'BackgroundColor', Look.Control,...
        'ForegroundColor', Look.Fore,...
        'Position',[0.01+0.5*(i-1) 0.80 0.48 0.06],...
        'String',{'Gray','Jet','Hot','HSV','Custom', 'HiLo', 'Inverse Gray','Parula'});
    if ismac
        h.Mia_Image.Settings.Channel_Colormap(i).ForegroundColor = [0 0 0];
        h.Mia_Image.Settings.Channel_Colormap(i).BackgroundColor = [1 1 1];
    end
    if ismac
        h.Mia_Image.Settings.Channel_PIE(i).ForegroundColor = [0 0 0];
        h.Mia_Image.Settings.Channel_PIE(i).BackgroundColor = [1 1 1];
    end
    %%% Popupmenu to select what to plot in second plot
    h.Mia_Image.Settings.Channel_Second(i) = uicontrol(...
        'Parent',h.Mia_Image.Settings.Channel_Panel,...
        'Style','popupmenu',...
        'Units','normalized',...
        'FontSize',12,...
        'BackgroundColor', Look.Control,...
        'ForegroundColor', Look.Fore,...
        'Position',[0.01+0.5*(i-1) 0.56, 0.48 0.06],...
        'Callback',{@Update_Plots,1},...
        'Value',2,...
        'String',{'Original','Corrected','Original-Corrected'});
    if ismac
        h.Mia_Image.Settings.Channel_Second(i).ForegroundColor = [0 0 0];
        h.Mia_Image.Settings.Channel_Second(i).BackgroundColor = [1 1 1];
    end
    %%% Editboxes for scaling
    h.Mia_Image.Settings.Scale(i,1) = uicontrol(...
        'Parent',h.Mia_Image.Settings.Channel_Panel,...
        'Style','edit',...
        'Units','normalized',...
        'FontSize',12,...
        'BackgroundColor', Look.Control,...
        'ForegroundColor', Look.Fore,...
        'Visible', 'off',...
        'Callback',{@Update_Plots,1},...
        'Position',[0.01+0.5*(i-1) 0.32, 0.2 0.05],...
        'String','0');
    h.Mia_Image.Settings.Scale(i,2) = uicontrol(...
        'Parent',h.Mia_Image.Settings.Channel_Panel,...
        'Style','edit',...
        'Units','normalized',...
        'FontSize',12,...
        'BackgroundColor', Look.Control,...
        'ForegroundColor', Look.Fore,...
        'Visible', 'off',...
        'Callback',{@Update_Plots,1},...
        'Position',[0.23+0.5*(i-1) 0.32, 0.2 0.05],...
        'String','100');
    h.Mia_Image.Settings.Scale(i,3) = uicontrol(...
        'Parent',h.Mia_Image.Settings.Channel_Panel,...
        'Style','edit',...
        'Units','normalized',...
        'FontSize',12,...
        'BackgroundColor', Look.Control,...
        'ForegroundColor', Look.Fore,...
        'Visible', 'off',...
        'Callback',{@Update_Plots,1},...
        'Position',[0.01+0.5*(i-1) 0.26, 0.2 0.05],...
        'String','0');
    h.Mia_Image.Settings.Scale(i,4) = uicontrol(...
        'Parent',h.Mia_Image.Settings.Channel_Panel,...
        'Style','edit',...
        'Units','normalized',...
        'FontSize',12,...
        'BackgroundColor', Look.Control,...
        'ForegroundColor', Look.Fore,...
        'Visible', 'off',...
        'Callback',{@Update_Plots,1},...
        'Position',[0.23+0.5*(i-1) 0.26, 0.2 0.05],...
        'String','100');
    %%% Editbox for frame
    h.Mia_Image.Settings.Channel_Frame(i) = uicontrol(...
        'Parent',h.Mia_Image.Settings.Channel_Panel,...
        'Style','edit',...
        'Units','normalized',...
        'FontSize',12,...
        'BackgroundColor', Look.Control,...
        'ForegroundColor', Look.Fore,...
        'Callback',{@Mia_Frame,1,i},...
        'Position',[0.01+0.5*(i-1) 0.06, 0.2 0.05],...
        'String','1');
    %%% Slider for frame
    h.Mia_Image.Settings.Channel_Frame_Slider(i) = uicontrol(...
        'Parent',h.Mia_Image.Settings.Channel_Panel,...
        'Style','slider',...
        'Units','normalized',...
        'FontSize',12,...
        'BackgroundColor', Look.Control,...
        'ForegroundColor', Look.Fore,...
        'UserData',[2,1],...
        'Position',[0.01+0.5*(i-1) 0.01, 0.48 0.04]);
    h.Mia_Image.Settings.Channel_Frame_Listener(i)=addlistener(h.Mia_Image.Settings.Channel_Frame_Slider(i),'Value','PostSet',@Mia_Frame);
end

%%% Checkbox for use frame
h.Mia_Image.Settings.Channel_FrameUse = uicontrol(...
    'Parent',h.Mia_Image.Settings.Channel_Panel,...
    'Style','checkbox',...
    'Units','normalized',...
    'FontSize',12,...
    'BackgroundColor', Look.Back,...
    'ForegroundColor', Look.Fore,...
    'Callback',@Mia_CheckFrame,...
    'Value',1,...
    'Position',[0.23 0.06, 0.25 0.05],...
    'String','Use');

%% Mia image settings tab
%%% Tab and panel for Mia image settings UIs
h.Mia_Image.Settings.Image_Tab= uitab(...
    'Parent',h.Mia_Image.Settings.Tabs,...
    'Tag','MI_Image_Tab',...
    'Title','Image');
h.Mia_Image.Settings.Image_Panel = uibuttongroup(...
    'Parent',h.Mia_Image.Settings.Image_Tab,...
    'Tag','Mia_Image_Settings_Panel',...
    'Units','normalized',...
    'BackgroundColor', Look.Back,...
    'ForegroundColor', Look.Fore,...
    'HighlightColor', Look.Control,...
    'ShadowColor', Look.Shadow,...
    'Position',[0 0 1 1]);

%%% Text
h.Text{end+1} = uicontrol(...
    'Parent',h.Mia_Image.Settings.Image_Panel,...
    'Style','text',...
    'Units','normalized',...
    'FontSize',12,...
    'HorizontalAlignment','left',...
    'BackgroundColor', Look.Back,...
    'ForegroundColor', Look.Fore,...
    'Position',[0.02 0.92, 0.4 0.06],...
    'String','Frame time [s]:');
%%% Editbox to set frame time
h.Mia_Image.Settings.Image_Frame = uicontrol(...
    'Parent',h.Mia_Image.Settings.Image_Panel,...
    'Style','edit',...
    'Units','normalized',...
    'FontSize',12,...
    'BackgroundColor', Look.Control,...
    'ForegroundColor', Look.Fore,...
    'Callback',{@Update_Plots, [4 6]},...
    'Position',[0.45 0.92, 0.2 0.06],...
    'String','1');
%%% Text
h.Mia_Image.Settings.Image_Line_Text = uicontrol(...
    'Parent',h.Mia_Image.Settings.Image_Panel,...
    'Style','text',...
    'Units','normalized',...
    'FontSize',12,...
    'HorizontalAlignment','left',...
    'BackgroundColor', Look.Back,...
    'ForegroundColor', Look.Fore,...
    'Position',[0.02 0.84, 0.4 0.06],...
    'String','Line time [ms]:');
%%% Editbox to set line time
h.Mia_Image.Settings.Image_Line = uicontrol(...
    'Parent',h.Mia_Image.Settings.Image_Panel,...
    'Style','edit',...
    'Units','normalized',...
    'FontSize',12,...
    'BackgroundColor', Look.Control,...
    'ForegroundColor', Look.Fore,...
    'Position',[0.45 0.84, 0.2 0.06],...
    'String','3.333');
%%% Text
h.Mia_Image.Settings.Image_Pixel_text = uicontrol(...
    'Parent',h.Mia_Image.Settings.Image_Panel,...
    'Style','text',...
    'Units','normalized',...
    'FontSize',12,...
    'HorizontalAlignment','left',...
    'BackgroundColor', Look.Back,...
    'ForegroundColor', Look.Fore,...
    'Position',[0.02 0.76, 0.4 0.06],...
    'String','Pixel time [us]:');
%%% Editbox to set pixel time
h.Mia_Image.Settings.Image_Pixel = uicontrol(...
    'Parent',h.Mia_Image.Settings.Image_Panel,...
    'Style','edit',...
    'Units','normalized',...
    'FontSize',12,...
    'BackgroundColor', Look.Control,...
    'ForegroundColor', Look.Fore,...
    'Callback',{@Update_Plots,[1,4]},...
    'Position',[0.45 0.76, 0.2 0.06],...
    'String',num2str(UserValues.MIA.FitParams.PixTime));
%%% Text
h.Text{end+1} = uicontrol(...
    'Parent',h.Mia_Image.Settings.Image_Panel,...
    'Style','text',...
    'Units','normalized',...
    'FontSize',12,...
    'HorizontalAlignment','left',...
    'BackgroundColor', Look.Back,...
    'ForegroundColor', Look.Fore,...
    'Position',[0.02 0.68, 0.4 0.06],...
    'String','Pixel size [nm]:');
%%% Editbox to set pixel size
h.Mia_Image.Settings.Pixel_Size = uicontrol(...
    'Parent',h.Mia_Image.Settings.Image_Panel,...
    'Style','edit',...
    'Units','normalized',...
    'FontSize',12,...
    'BackgroundColor', Look.Control,...
    'ForegroundColor', Look.Fore,...
    'Position',[0.45 0.68, 0.2 0.06],...
    'Callback',{@MIA_Various, 8},...
    'String','40');

%%% Text
h.Mia_Image.Settings.Image_Mean_CR = uicontrol(...
    'Parent',h.Mia_Image.Settings.Image_Panel,...
    'Style','text',...
    'Units','normalized',...
    'FontSize',12,...
    'HorizontalAlignment','left',...
    'BackgroundColor', Look.Back,...
    'ForegroundColor', Look.Fore,...
    'Position',[0.02 0.52, 0.96 0.1],...
    'String','Mean Countrate [kHz]:');

%%% Text
h.Text{end+1} = uicontrol(...
    'Parent',h.Mia_Image.Settings.Image_Panel,...
    'Style','text',...
    'Units','normalized',...
    'FontSize',12,...
    'HorizontalAlignment','left',...
    'BackgroundColor', Look.Back,...
    'ForegroundColor', Look.Fore,...
    'Position',[0.02 0.42, 0.4 0.06],...
    'String','Scale bar [um]:');
%%% Editbox to set scale bar in um
h.Mia_Image.Settings.ScaleBar = uicontrol(...
    'Parent',h.Mia_Image.Settings.Image_Panel,...
    'Style','edit',...
    'Units','normalized',...
    'FontSize',12,...
    'BackgroundColor', Look.Control,...
    'ForegroundColor', Look.Fore,...
    'Position',[0.45 0.42, 0.2 0.06],...
    'Callback',{@Update_Plots, 1},...
    'String','');

%% Mia ROI setting tab
%%% Tab and panel for Mia ROI settings UIs
h.Mia_Image.Settings.ROI_Tab= uitab(...
    'Parent',h.Mia_Image.Settings.Tabs,...
    'Tag','MI_ROI_Settings_Tab',...
    'Title','ROI');
h.Mia_Image.Settings.ROI_Panel = uibuttongroup(...
    'Parent',h.Mia_Image.Settings.ROI_Tab,...
    'Tag','Mia_ROI_Settings_Panel',...
    'Units','normalized',...
    'BackgroundColor', Look.Back,...
    'ForegroundColor', Look.Fore,...
    'HighlightColor', Look.Control,...
    'ShadowColor', Look.Shadow,...
    'Position',[0 0 1 1]);
%%% Text
h.Text{end+1} = uicontrol(...
    'Parent',h.Mia_Image.Settings.ROI_Panel,...
    'Style','text',...
    'Units','normalized',...
    'FontSize',12,...
    'HorizontalAlignment','left',...
    'BackgroundColor', Look.Back,...
    'ForegroundColor', Look.Fore,...
    'Position',[0.02 0.92, 0.35 0.06],...
    'String','ROI size:');
%%% Popupmenu to select ROI Size
h.Mia_Image.Settings.ROI_SizeX = uicontrol(...
    'Parent',h.Mia_Image.Settings.ROI_Panel,...
    'Style','edit',...
    'Units','normalized',...
    'FontSize',12,...
    'BackgroundColor', Look.Control,...
    'ForegroundColor', Look.Fore,...
    'Position',[0.43 0.92, 0.25 0.06],...
    'Callback',{@Mia_ROI,1},...
    'String','200');
%%% Popupmenu to select ROI Size
h.Mia_Image.Settings.ROI_SizeY = uicontrol(...
    'Parent',h.Mia_Image.Settings.ROI_Panel,...
    'Style','edit',...
    'Units','normalized',...
    'FontSize',12,...
    'BackgroundColor', Look.Control,...
    'ForegroundColor', Look.Fore,...
    'Position',[0.7 0.92, 0.25 0.06],...
    'Callback',{@Mia_ROI,1},...
    'String','200');
%%% Text
h.Text{end+1} = uicontrol(...
    'Parent',h.Mia_Image.Settings.ROI_Panel,...
    'Style','text',...
    'Units','normalized',...
    'FontSize',12,...
    'HorizontalAlignment','left',...
    'BackgroundColor', Look.Back,...
    'ForegroundColor', Look.Fore,...
    'Position',[0.02 0.85, 0.35 0.06],...
    'String','ROI pos:');
%%% Popupmenu to select ROI Position
h.Mia_Image.Settings.ROI_PosX = uicontrol(...
    'Parent',h.Mia_Image.Settings.ROI_Panel,...
    'Style','edit',...
    'Units','normalized',...
    'FontSize',12,...
    'BackgroundColor', Look.Control,...
    'ForegroundColor', Look.Fore,...
    'Position',[0.43 0.85, 0.25 0.06],...
    'Callback',{@Mia_ROI,1},...
    'String','1');
%%% Popupmenu to select ROI Position
h.Mia_Image.Settings.ROI_PosY = uicontrol(...
    'Parent',h.Mia_Image.Settings.ROI_Panel,...
    'Style','edit',...
    'Units','normalized',...
    'FontSize',12,...
    'BackgroundColor', Look.Control,...
    'ForegroundColor', Look.Fore,...
    'Position',[0.7 0.85, 0.25 0.06],...
    'Callback',{@Mia_ROI,1},...
    'String','1');

%%% Text
h.Text{end+1} = uicontrol(...
    'Parent', h.Mia_Image.Settings.ROI_Panel,...
    'Style','text',...
    'Units','normalized',...
    'FontSize',12,...
    'HorizontalAlignment','left',...
    'BackgroundColor', Look.Back,...
    'ForegroundColor', Look.Fore,...
    'Position',[0.02 0.78, 0.5 0.06],...
    'String','Frame range:',...
    'Tooltipstring','Sets the frame range to use (applies to all analyses)');
%%% Editbox to select, which frames to correlate
h.Mia_Image.Settings.ROI_Frames = uicontrol(...
    'Parent', h.Mia_Image.Settings.ROI_Panel,...
    'Style','edit',...
    'Units','normalized',...
    'FontSize',12,...
    'BackgroundColor', Look.Control,...
    'ForegroundColor', Look.Fore,...
    'Position',[0.55 0.78, 0.3 0.06],...
    'String','1',...
    'Callback',{@Update_Plots,1},...
    'Tooltipstring','Sets the frame range to use (applies to all analyses)');

%%% Text
h.Text{end+1} = uicontrol(...
    'Parent', h.Mia_Image.Settings.ROI_Panel,...
    'Style','text',...
    'Units','normalized',...
    'FontSize',12,...
    'HorizontalAlignment','left',...
    'BackgroundColor', Look.Back,...
    'ForegroundColor', Look.Fore,...
    'Position',[0.02 0.71, 0.5 0.06],...
    'String','Special selection:');
%%% Select unselection criteria
h.Mia_Image.Settings.ROI_FramesUse = uicontrol(...
    'Parent', h.Mia_Image.Settings.ROI_Panel,...
    'Style','popupmenu',...
    'Units','normalized',...
    'FontSize',12,...
    'BackgroundColor', Look.Control,...
    'ForegroundColor', Look.Fore,...
    'Position',[0.45 0.71, 0.5 0.06],...
    'Callback',{@MIA_Various,3},...
    'String',{'None','Checked frames','Arbitrary ROI'});
if ismac
    h.Mia_Image.Settings.ROI_FramesUse.ForegroundColor = [0 0 0];
    h.Mia_Image.Settings.ROI_FramesUse.BackgroundColor = [1 1 1];
end

%%% Text
h.Mia_Image.Settings.ROI_AR_Text = {};
h.Mia_Image.Settings.ROI_AR_Text{end+1} = uicontrol(...
    'Parent', h.Mia_Image.Settings.ROI_Panel,...
    'Style','text',...
    'Units','normalized',...
    'FontSize',12,...
    'HorizontalAlignment','left',...
    'BackgroundColor', Look.Back,...
    'ForegroundColor', Look.Fore,...
    'TooltipString',['Specifies the sliding windows used' 10 'for intensity\variance thresholding' 10 'for arbitrary region ICS'],...
    'Position',[0.02 0.64, 0.5 0.06],...
    'Visible', 'off',...
    'String','Subregions [px]:');
%%% Smaller subregion
h.Mia_Image.Settings.ROI_AR_Sub1 = uicontrol(...
    'Parent', h.Mia_Image.Settings.ROI_Panel,...
    'Style','edit',...
    'Units','normalized',...
    'FontSize',12,...
    'BackgroundColor', Look.Control,...
    'ForegroundColor', Look.Fore,...
    'TooltipString',['Specifies the smaller sliding window used' 10 'for intensity\variance thresholding' 10 'for arbitrary region ICS'],...
    'Position',[0.43 0.64, 0.25 0.06],...
    'Visible', 'off',...
    'Callback',{@MIA_Various,6},...
    'String',num2str(UserValues.MIA.AR_Region(1)));
%%% Larger subregion
h.Mia_Image.Settings.ROI_AR_Sub2 = uicontrol(...
    'Parent', h.Mia_Image.Settings.ROI_Panel,...
    'Style','edit',...
    'Units','normalized',...
    'FontSize',12,...
    'BackgroundColor', Look.Control,...
    'ForegroundColor', Look.Fore,...
    'TooltipString',['Specifies the larger sliding window used' 10 'for intensity\variance thresholding' 10 'for arbitrary region ICS'],...
    'Position',[0.7 0.64, 0.25 0.06],...
    'Visible', 'off',...
    'Callback',{@MIA_Various,6},...
    'String',num2str(UserValues.MIA.AR_Region(2)));
%%%Button to reset AROI values
h.Mia_Image.Settings.ROI_AR_Reset = uicontrol(...
    'Parent', h.Mia_Image.Settings.ROI_Tab,...
    'Style','push',...
    'Units','normalized',...
    'FontSize',12,...
    'BackgroundColor', Look.Control,...
    'ForegroundColor', Look.Fore,...
    'Position',[0.05 0.56, 0.22 0.06],...
    'Callback',{@MIA_Various,7},...
    'String','Reset',...
    'Visible','off',...
    'Tooltipstring','Reset AROI values to default');
%%% Select unselection criteria
h.Mia_Image.Settings.ROI_AR_Channel = uicontrol(...
    'Parent', h.Mia_Image.Settings.ROI_Panel,...
    'Style','popupmenu',...
    'Units','normalized',...
    'FontSize',12,...
    'BackgroundColor', Look.Control,...
    'ForegroundColor', Look.Fore,...
    'Position',[0.45 0.57, 0.5 0.06],...
    'Callback',{@MIA_Various,5},...
    'Value',2,...
    'Visible','off',...
    'String',[{'All'}; pielist]);
if ismac
    h.Mia_Image.Settings.ROI_AR_Channel.ForegroundColor = [0 0 0];
    h.Mia_Image.Settings.ROI_AR_Channel.BackgroundColor = [1 1 1];
end
%%% Text
h.Mia_Image.Settings.ROI_AR_Text{end+1} = uicontrol(...
    'Parent', h.Mia_Image.Settings.ROI_Panel,...
    'Style','text',...
    'Units','normalized',...
    'FontSize',12,...
    'HorizontalAlignment','left',...
    'BackgroundColor', Look.Back,...
    'ForegroundColor', Look.Fore,...
    'Position',[0.51 0.50, 0.2 0.06],...
    'Visible', 'off',...
    'String','MIN');
%%% Text
h.Mia_Image.Settings.ROI_AR_Text{end+1} = uicontrol(...
    'Parent', h.Mia_Image.Settings.ROI_Panel,...
    'Style','text',...
    'Units','normalized',...
    'FontSize',12,...
    'HorizontalAlignment','left',...
    'BackgroundColor', Look.Back,...
    'ForegroundColor', Look.Fore,...
    'Position',[0.77 0.50, 0.2 0.06],...
    'Visible', 'off',...
    'String','MAX');
%%% Text
h.Mia_Image.Settings.ROI_AR_Text{end+1} = uicontrol(...
    'Parent', h.Mia_Image.Settings.ROI_Panel,...
    'Style','text',...
    'Units','normalized',...
    'FontSize',12,...
    'HorizontalAlignment','left',...
    'BackgroundColor', Look.Back,...
    'ForegroundColor', Look.Fore,...
    'TooltipString',['Specifies min\max pixel countrate' 10 'averaged over all frames'],...
    'Position',[0.02 0.45, 0.35 0.06],...
    'Visible', 'off',...
    'String','Intensity [kHz]:');
%%% Minimal average pixel countrate
h.Mia_Image.Settings.ROI_AR_Int_Min = uicontrol(...
    'Parent', h.Mia_Image.Settings.ROI_Panel,...
    'Style','edit',...
    'Units','normalized',...
    'FontSize',12,...
    'BackgroundColor', Look.Control,...
    'ForegroundColor', Look.Fore,...
    'TooltipString',['Specifies min pixel countrate' 10 'averaged over all frames'],...
    'Position',[0.43 0.45, 0.25 0.06],...
    'Visible', 'off',...
    'Callback',{@MIA_Various,6},...
    'String',num2str(UserValues.MIA.AR_Int(1)));
%%% Maximal average pixel countrate
h.Mia_Image.Settings.ROI_AR_Int_Max = uicontrol(...
    'Parent', h.Mia_Image.Settings.ROI_Panel,...
    'Style','edit',...
    'Units','normalized',...
    'FontSize',12,...
    'BackgroundColor', Look.Control,...
    'ForegroundColor', Look.Fore,...
    'TooltipString',['Specifies max pixel countrate' 10 'averaged over all frames'],...
    'Position',[0.7 0.45, 0.25 0.06],...
    'Visible', 'off',...
    'Callback',{@MIA_Various,6},...
    'String',num2str(UserValues.MIA.AR_Int(11)));
%%% Text
h.Mia_Image.Settings.ROI_AR_Text{end+1} = uicontrol(...
    'Parent', h.Mia_Image.Settings.ROI_Panel,...
    'Style','text',...
    'Units','normalized',...
    'FontSize',12,...
    'HorizontalAlignment','left',...
    'BackgroundColor', Look.Back,...
    'ForegroundColor', Look.Fore,...
    'TooltipString',['Specifies min\max intensity ratio' 10 'of subregions specified above'],...
    'Position',[0.02 0.38, 0.5 0.06],...
    'Visible', 'off',...
    'String','Intensity [Fold]:');
%%% Minimal intensity ratio of subregions
h.Mia_Image.Settings.ROI_AR_Int_Fold_Min = uicontrol(...
    'Parent', h.Mia_Image.Settings.ROI_Panel,...
    'Style','edit',...
    'Units','normalized',...
    'FontSize',12,...
    'BackgroundColor', Look.Control,...
    'ForegroundColor', Look.Fore,...
    'TooltipString',['Specifies min intensity ratio' 10 'of subregions specified above'],...
    'Position',[0.43 0.38, 0.25 0.06],...
    'Visible', 'off',...
    'Callback',{@MIA_Various,6},...
    'String',num2str(UserValues.MIA.AR_Int_Fold(1)));
%%% Maximal intensity ratio of subregions
h.Mia_Image.Settings.ROI_AR_Int_Fold_Max = uicontrol(...
    'Parent', h.Mia_Image.Settings.ROI_Panel,...
    'Style','edit',...
    'Units','normalized',...
    'FontSize',12,...
    'BackgroundColor', Look.Control,...
    'ForegroundColor', Look.Fore,...
    'TooltipString',['Specifies max intensity ratio' 10 'of subregions specified above'],...
    'Position',[0.7 0.38, 0.25 0.06],...
    'Visible', 'off',...
    'Callback',{@MIA_Various,6},...
    'String',num2str(UserValues.MIA.AR_Int_Fold(11)));
%%% Text
h.Mia_Image.Settings.ROI_AR_Text{end+1} = uicontrol(...
    'Parent', h.Mia_Image.Settings.ROI_Panel,...
    'Style','text',...
    'Units','normalized',...
    'FontSize',12,...
    'HorizontalAlignment','left',...
    'BackgroundColor', Look.Back,...
    'ForegroundColor', Look.Fore,...
    'TooltipString',['Specifies min\max variance ratio' 10 'of subregions specified above'],...
    'Position',[0.02 0.31, 0.5 0.06],...
    'Visible', 'off',...
    'String','Variance [Fold]:');
%%% Minimal variance ratio of subregions
h.Mia_Image.Settings.ROI_AR_Var_Fold_Min = uicontrol(...
    'Parent', h.Mia_Image.Settings.ROI_Panel,...
    'Style','edit',...
    'Units','normalized',...
    'FontSize',12,...
    'BackgroundColor', Look.Control,...
    'ForegroundColor', Look.Fore,...
    'TooltipString',['Specifies min variance ratio' 10 'of subregions specified above'],...
    'Position',[0.43 0.31, 0.25 0.06],...
    'Visible', 'off',...
    'Callback',{@MIA_Various,6},...
    'String',num2str(UserValues.MIA.AR_Var_Fold(1)));
%%% Maximal variance ratio of subregions
h.Mia_Image.Settings.ROI_AR_Var_Fold_Max = uicontrol(...
    'Parent', h.Mia_Image.Settings.ROI_Panel,...
    'Style','edit',...
    'Units','normalized',...
    'FontSize',12,...
    'BackgroundColor', Look.Control,...
    'ForegroundColor', Look.Fore,...
    'TooltipString',['Specifies max variance ratio' 10 'of subregions specified above'],...
    'Position',[0.7 0.31, 0.25 0.06],...
    'Visible', 'off',...
    'Callback',{@MIA_Various,6},...
    'String',num2str(UserValues.MIA.AR_Var_Fold(11)));
%%% Text
h.Mia_Image.Settings.ROI_AR_Text{end+1} = uicontrol(...
    'Parent', h.Mia_Image.Settings.ROI_Panel,...
    'Style','text',...
    'Units','normalized',...
    'FontSize',12,...
    'HorizontalAlignment','left',...
    'BackgroundColor', Look.Back,...
    'ForegroundColor', Look.Fore,...
    'TooltipString',['Specifies which mask is used' 10 'in further analysis'],...
    'Position',[0.02 0.24, 0.35 0.06],...
    'Visible', 'off',...
    'String','Which masks:');
%%% Same AR for both channels
h.Mia_Image.Settings.ROI_AR_Same = uicontrol(...
    'Parent', h.Mia_Image.Settings.ROI_Panel,...
    'Style','popupmenu',...
    'Units','normalized',...
    'FontSize',12,...
    'BackgroundColor', Look.Control,...
    'ForegroundColor', Look.Fore,...
    'Position',[0.43 0.24, 0.5 0.06],...
    'Visible', 'off',...
    'Callback',{@MIA_Various,6},...
    'Value', UserValues.MIA.AR_Same,...
    'String',[{'OR'}; pielist(1:end-1); {'AND'}],...
    'Tooltipstring',['"OR" means the mask in each channel can be different.' 10 '"Channel 1" means the mask in Channel 1 is mapped onto all channels.' 10 '"AND" means only pixels that are included in all channels are kept.' 10 'For cross-correlation analyses, use "AND".']);

%%% Checkbox to toggle right-click context menu for left plot
h.Mia_Image.Settings.ROI_AR_LeftContextMenu = uicontrol(...
    'Parent', h.Mia_Image.Settings.ROI_Panel,...
    'Style','checkbox',...
    'Units','normalized',...
    'FontSize',12,...
    'Value',UserValues.MIA.Options.LeftContextMenu,...
    'BackgroundColor', Look.Back,...
    'ForegroundColor', Look.Fore,...
    'Position',[0.02 0.18, 0.5 0.06],...
    'Callback',{@MIA_Various,10},...
    'Visible','off',...
    'String', 'Left Image Context Menu',...
    'Tooltip',['Enable right-click context menu on left image' 10 ...
    'to draw an arbitrary ROI.']);

%%% Checkbox to toggle right-click context menu for left plot
h.Mia_Image.Settings.ROI_AR_ThresholdCorr = uicontrol(...
    'Parent', h.Mia_Image.Settings.ROI_Panel,...
    'Style','checkbox',...
    'Units','normalized',...
    'FontSize',12,...
    'Value',0,...
    'BackgroundColor', Look.Back,...
    'ForegroundColor', Look.Fore,...
    'Position',[0.55 0.18, 0.5 0.06],...
    'Callback',{@MIA_Various,6},...
    'Visible','off',...
    'String', 'Threshold Corrected Image',...
    'Tooltip',['Additionally threshold the corrected' 10 ...
    'image with intensity fold relative to the' 10 ...
    'mean of the complete corrected image.']);

h.Mia_Image.Settings.ROI_AR_Spatial_Int = uicontrol(...
    'Parent', h.Mia_Image.Settings.ROI_Panel,...
    'Style','checkbox',...
    'Units','normalized',...
    'FontSize',12,...
    'Value',UserValues.MIA.AR_Framewise,...
    'Callback', {@MIA_Various,6},...
    'Visible', 'off',...
    'BackgroundColor', Look.Back,...
    'ForegroundColor', Look.Fore,...
    'Position',[0.02 0.11, 0.5 0.06],...
    'TooltipString',['Do spatial absolute intensity' 10 'thresholding in the small sub-ROI' 10 'instead of over all frames.' 10 'the code considers photobleaching'],...
    'String','Framewise Int. Threshold');
h.Mia_Image.Settings.ROI_AR_median = uicontrol(...
    'Parent', h.Mia_Image.Settings.ROI_Panel,...
    'Style','checkbox',...
    'Units','normalized',...
    'FontSize',12,...
    'Value',UserValues.MIA.AR_Median,...
    'Callback', {@MIA_Various,6},...
    'Visible', 'off',...
    'BackgroundColor', Look.Back,...
    'ForegroundColor', Look.Fore,...
    'Position',[0.55 0.11, 0.4 0.06],...
    'TooltipString',['median filter the ROI' 10, 'in a square region' 10 'with small subROI size'],...
    'String','Median Filter');

%%%Button to Import ROI from file
h.Mia_Image.Settings.Load_ROI = uicontrol(...
    'Parent', h.Mia_Image.Settings.ROI_Tab,...
    'Style','push',...
    'Units','normalized',...
    'FontSize',12,...
    'BackgroundColor', Look.Control,...
    'ForegroundColor', Look.Fore,...
    'Position',[0.02 0.02, 0.35 0.06],...
    'Callback',{@Import_ROI},...
    'String','Import ROI',...
    'Tooltipstring',['Import ROI from .mat file' 10 ...
                    'ROI = [sizeX sizeY posX PosY] and' 10 ...
                    'Mask = ROI-sized single frame mask']);

%% Mia correction tab
%%% Tab and panel for Mia image correction settings UIs
h.Mia_Image.Settings.Correction_Tab= uitab(...
    'Parent',h.Mia_Image.Settings.Tabs,...
    'Tag','MI_Correction_SettingsTab',...
    'Title','Correction');
h.Mia_Image.Settings.Correction_Panel = uibuttongroup(...
    'Parent',h.Mia_Image.Settings.Correction_Tab,...
    'Tag','Mia_Correction_Settings_Panel',...
    'Units','normalized',...
    'BackgroundColor', Look.Back,...
    'ForegroundColor', Look.Fore,...
    'HighlightColor', Look.Control,...
    'ShadowColor', Look.Shadow,...
    'Position',[0 0 1 1]);
%%% Text
h.Text{end+1} = uicontrol(...
    'Parent',h.Mia_Image.Settings.Correction_Panel,...
    'Style','text',...
    'Units','normalized',...
    'FontSize',14,...
    'HorizontalAlignment','left',...
    'BackgroundColor', Look.Back,...
    'ForegroundColor', Look.Fore,...
    'Position',[0.02 0.92, 0.3 0.06],...
    'String','Subtract:');
%%% Popupmenu to select what to subtract
h.Mia_Image.Settings.Correction_Subtract = uicontrol(...
    'Parent',h.Mia_Image.Settings.Correction_Panel,...
    'Style','popupmenu',...
    'Units','normalized',...
    'FontSize',12,...
    'Value',UserValues.MIA.Correct_Type(1),...
    'BackgroundColor', Look.Control,...
    'ForegroundColor', Look.Fore,...
    'Position',[0.34 0.92, 0.64 0.06],...
    'Callback',{@Mia_Correct,1},...
    'String',{'Nothing','Frame mean','Pixel mean','Moving average'});
if ismac
    h.Mia_Image.Settings.Correction_Subtract.ForegroundColor = [0 0 0];
    h.Mia_Image.Settings.Correction_Subtract.BackgroundColor = [1 1 1];
end
%%% Text
h.Mia_Image.Settings.Correction_Subtract_Pixel_Text = uicontrol(...
    'Parent',h.Mia_Image.Settings.Correction_Panel,...
    'Style','text',...
    'Units','normalized',...
    'FontSize',14,...
    'HorizontalAlignment','left',...
    'BackgroundColor', Look.Back,...
    'ForegroundColor', Look.Fore,...
    'Position',[0.02 0.84, 0.25 0.06],...
    'Visible','off',...
    'String','Pixel:');
%%% Pixels to average for subtracting moving average
h.Mia_Image.Settings.Correction_Subtract_Pixel = uicontrol(...
    'Parent',h.Mia_Image.Settings.Correction_Panel,...
    'Style','edit',...
    'Units','normalized',...
    'FontSize',12,...
    'BackgroundColor', Look.Control,...
    'ForegroundColor', Look.Fore,...
    'Position',[0.27 0.84, 0.15 0.06],...
    'Callback',{@Mia_Correct,1},...
    'Visible','off',...
    'String',num2str(UserValues.MIA.Correct_Sub_Values(1)));
%%% Text
h.Mia_Image.Settings.Correction_Subtract_Frames_Text = uicontrol(...
    'Parent',h.Mia_Image.Settings.Correction_Panel,...
    'Style','text',...
    'Units','normalized',...
    'FontSize',14,...
    'HorizontalAlignment','left',...
    'BackgroundColor', Look.Back,...
    'ForegroundColor', Look.Fore,...
    'Position',[0.52 0.84, 0.28 0.06],...
    'Visible','off',...
    'String','Frames:');
%%% Pixels to average for subtracting moving average
h.Mia_Image.Settings.Correction_Subtract_Frames = uicontrol(...
    'Parent',h.Mia_Image.Settings.Correction_Panel,...
    'Style','edit',...
    'Units','normalized',...
    'FontSize',12,...
    'BackgroundColor', Look.Control,...
    'ForegroundColor', Look.Fore,...
    'Position',[0.83 0.84, 0.15 0.06],...
    'Callback',{@Mia_Correct,1},...
    'Visible','off',...
    'String',num2str(UserValues.MIA.Correct_Sub_Values(2)));

%%% Text
h.Text{end+1} = uicontrol(...
    'Parent',h.Mia_Image.Settings.Correction_Panel,...
    'Style','text',...
    'Units','normalized',...
    'FontSize',14,...
    'HorizontalAlignment','left',...
    'BackgroundColor', Look.Back,...
    'ForegroundColor', Look.Fore,...
    'Position',[0.02 0.7, 0.3 0.06],...
    'String','Add:');
%%% Popupmenu to select what to subtract
h.Mia_Image.Settings.Correction_Add = uicontrol(...
    'Parent',h.Mia_Image.Settings.Correction_Panel,...
    'Style','popupmenu',...
    'Units','normalized',...
    'FontSize',12,...
    'Value',UserValues.MIA.Correct_Type(2),...
    'BackgroundColor', Look.Control,...
    'ForegroundColor', Look.Fore,...
    'Position',[0.34 0.7, 0.64 0.06],...
    'Callback',{@Mia_Correct,1},...
    'String',{'Nothing','Total mean','Frame mean','Pixel mean','Moving average'});
if ismac
    h.Mia_Image.Settings.Correction_Add.ForegroundColor = [0 0 0];
    h.Mia_Image.Settings.Correction_Add.BackgroundColor = [1 1 1];
end
%%% Text
h.Mia_Image.Settings.Correction_Add_Pixel_Text = uicontrol(...
    'Parent',h.Mia_Image.Settings.Correction_Panel,...
    'Style','text',...
    'Units','normalized',...
    'FontSize',14,...
    'HorizontalAlignment','left',...
    'BackgroundColor', Look.Back,...
    'ForegroundColor', Look.Fore,...
    'Position',[0.02 0.62, 0.25 0.06],...
    'Visible','off',...
    'String','Pixel:');
%%% Pixels to average for subtracting moving average
h.Mia_Image.Settings.Correction_Add_Pixel = uicontrol(...
    'Parent',h.Mia_Image.Settings.Correction_Panel,...
    'Style','edit',...
    'Units','normalized',...
    'FontSize',12,...
    'BackgroundColor', Look.Control,...
    'ForegroundColor', Look.Fore,...
    'Position',[0.27 0.62, 0.15 0.06],...
    'Callback',{@Mia_Correct,1},...
    'Visible','off',...
    'String',num2str(UserValues.MIA.Correct_Add_Values(1)));
%%% Text
h.Mia_Image.Settings.Correction_Add_Frames_Text = uicontrol(...
    'Parent',h.Mia_Image.Settings.Correction_Panel,...
    'Style','text',...
    'Units','normalized',...
    'FontSize',14,...
    'HorizontalAlignment','left',...
    'BackgroundColor', Look.Back,...
    'ForegroundColor', Look.Fore,...
    'Position',[0.52 0.62, 0.28 0.06],...
    'Visible','off',...
    'String','Frames:');
%%% Pixels to average for subtracting moving average
h.Mia_Image.Settings.Correction_Add_Frames = uicontrol(...
    'Parent',h.Mia_Image.Settings.Correction_Panel,...
    'Style','edit',...
    'Units','normalized',...
    'FontSize',12,...
    'BackgroundColor', Look.Control,...
    'ForegroundColor', Look.Fore,...
    'Position',[0.83 0.62, 0.15 0.06],...
    'Callback',{@Mia_Correct,1},...
    'Visible','off',...
    'String',num2str(UserValues.MIA.Correct_Add_Values(2)));
h.Mia_Image.Settings.Correction_PlotDrift = uicontrol(...
    'Parent', h.Mia_Image.Settings.Correction_Panel,...
    'Style','checkbox',...
    'Units','normalized',...
    'FontSize',12,...
    'Value',1,...
    'BackgroundColor', Look.Back,...
    'ForegroundColor', Look.Fore,...
    'Position',[0.4 0.42, 0.48 0.06],...
    'String','Plot Drift',...
    'TooltipString',['Linearly shifts each frame to' 10 'overlay it on the first one.' 10 'Operates via cross-correlation.' 10 'Re-open data to undo drift correction.']);
h.Mia_Image.Settings.Correction_Drift = uicontrol(...
    'Parent', h.Mia_Image.Settings.Correction_Panel,...
    'Tag', 'PlotDriftButton',...
    'Style','pushbutton',...
    'Units','normalized',...
    'FontSize',12,...
    'Callback', @MIA_Drift,...
    'BackgroundColor', Look.Back,...
    'ForegroundColor', Look.Fore,...
    'Position',[0.02 0.42, 0.35 0.06],...
    'String','Correct for drift',...
    'TooltipString',['Linearly shifts each frame to' 10 'overlay it on the first one.' 10 'Operates via cross-correlation.']);

%%%Button to Store ROI count rate as background
h.Mia_Image.Settings.GetBG = uicontrol(...
    'Parent', h.Mia_Image.Settings.Correction_Panel,...
    'Style','push',...
    'Units','normalized',...
    'FontSize',12,...
    'BackgroundColor', Look.Control,...
    'ForegroundColor', Look.Fore,...
    'Position',[0.02 0.16, 0.35 0.06],...
    'Callback',{@MIA_Various,4},...
    'String','Background',...
    'Tooltipstring',['get background from the current ROI (in pixel counts).' 10 ...
    'This background is subtracted from all pixels prior to any image correction.']);

h.Text{end+1} = uicontrol(...
    'Parent',h.Mia_Image.Settings.Correction_Panel,...
    'Style','text',...
    'Units','normalized',...
    'FontSize',14,...
    'HorizontalAlignment','left',...
    'BackgroundColor', Look.Back,...
    'ForegroundColor', Look.Fore,...
    'Position',[0.02 0.09, 0.96 0.06],...
    'String','Chan 1-2');

%Set background to zero to avoid confusion
UserValues.MIA.BG = [0 0 0 0 0 0 0 0 0 0];
LSUserValues(1)

for i = 1:2
    %%% Editbox to select, which frames to correlate
    h.Mia_Image.Settings.Background(i) = uicontrol(...
        'Parent', h.Mia_Image.Settings.Correction_Panel,...
        'Style','edit',...
        'Units','normalized',...
        'FontSize',12,...
        'BackgroundColor', Look.Control,...
        'ForegroundColor', Look.Fore,...
        'Position',[0.02+(i-1)*0.18+i*0.01 0.02, 0.18 0.05] ,...
        'String','0',...
        'Callback',{@Mia_Correct,1},...
        'ToolTipString', ['Background of displayed channel ' num2str(i) 10 'in counts per dwell time.']);
end

%% Mia image orientation tab
%%% Tab and panel for Mia image orientation settings UIs
h.Mia_Image.Settings.Orientation_Tab= uitab(...
    'Parent',h.Mia_Image.Settings.Tabs,...
    'Tag','MI_Orientation_SettingsTab',...
    'Title','Options');
h.Mia_Image.Settings.Orientation_Panel = uibuttongroup(...
    'Parent',h.Mia_Image.Settings.Orientation_Tab,...
    'Tag','Mia_Orientation_Settings_Panel',...
    'Units','normalized',...
    'BackgroundColor', Look.Back,...
    'ForegroundColor', Look.Fore,...
    'HighlightColor', Look.Control,...
    'ShadowColor', Look.Shadow,...
    'Position',[0 0 1 1]);
%%% Text
h.Text{end+1} = uicontrol(...
    'Parent',h.Mia_Image.Settings.Orientation_Panel,...
    'Style','text',...
    'Units','normalized',...
    'FontSize',14,...
    'HorizontalAlignment','left',...
    'BackgroundColor', Look.Back,...
    'ForegroundColor', Look.Fore,...
    'Position',[0.02 0.93, 0.4 0.06],...
    'String','Ch2 orientation:');
%%% pushbutton for horizontal mirroring
h.Mia_Image.Settings.Orientation_Flip_Hor = uicontrol(...
    'Parent',h.Mia_Image.Settings.Orientation_Panel,...
    'Style','pushbutton',...
    'Units','normalized',...
    'FontSize',12,...
    'BackgroundColor', Look.Control,...
    'ForegroundColor', Look.Fore,...
    'Callback',{@Mia_Orientation,1},...
    'Value',0,...
    'Position',[0.02 0.86, 0.35 0.06],...
    'String','Flip Horizontally');
%%% pushbutton for vertical mirroring
h.Mia_Image.Settings.Orientation_Flip_Ver = uicontrol(...
    'Parent',h.Mia_Image.Settings.Orientation_Panel,...
    'Style','pushbutton',...
    'Units','normalized',...
    'FontSize',12,...
    'BackgroundColor', Look.Control,...
    'ForegroundColor', Look.Fore,...
    'Callback',{@Mia_Orientation,2},...
    'Value',0,...
    'Position',[0.39 0.86, 0.35 0.06],...
    'String','Flip Vertically');
%%% pushbutton for rotation
h.Mia_Image.Settings.Orientation_Rotate = uicontrol(...
    'Parent',h.Mia_Image.Settings.Orientation_Panel,...
    'Style','pushbutton',...
    'Units','normalized',...
    'FontSize',12,...
    'BackgroundColor', Look.Control,...
    'ForegroundColor', Look.Fore,...
    'Callback',{@Mia_Orientation,3},...
    'Value',0,...
    'Position',[0.02 0.78, 0.35 0.06],...
    'String','Rotate');

%%% Popupmenu for rotation direction
h.Mia_Image.Settings.Orientation_Rotate_Dir = uicontrol(...
    'Parent',h.Mia_Image.Settings.Orientation_Panel,...
    'Style','popupmenu',...
    'Units','normalized',...
    'FontSize',12,...
    'BackgroundColor', Look.Control,...
    'ForegroundColor', Look.Fore,...
    'String',{'clockwise','counterclockwise'},...
    'Value',1,...
    'Position',[0.39 0.78, 0.35 0.06] );
if ismac
    h.Mia_Image.Settings.Orientation_Rotate_Dir.ForegroundColor = [0 0 0];
    h.Mia_Image.Settings.Orientation_Rotate_Dir.BackgroundColor = [1 1 1];
end

%%% Checkbox to switch between automatic\manual filenames
h.Mia_Image.Settings.kHz = uicontrol(...
    'Parent', h.Mia_Image.Settings.Orientation_Panel,...
    'Style','checkbox',...
    'Units','normalized',...
    'FontSize',12,...
    'Value',UserValues.MIA.Options.kHz,...
    'BackgroundColor', Look.Back,...
    'ForegroundColor', Look.Fore,...
    'Position',[0.02 0.70, 0.5 0.06],...
    'Callback',{@MIA_Various,1.5},...
    'String','Photon counting');

%%% Checkbox to switch between automatic\manual filenames
h.Mia_Image.Settings.scanning = uicontrol(...
    'Parent', h.Mia_Image.Settings.Orientation_Panel,...
    'Style','checkbox',...
    'Units','normalized',...
    'FontSize',12,...
    'Value',UserValues.MIA.Options.scanning,...
    'BackgroundColor', Look.Back,...
    'ForegroundColor', Look.Fore,...
    'Position',[0.02 0.63, 0.5 0.06],...
    'Callback',{@MIA_Various,9},...
    'String','Raster scanning');

%%% Text
h.Text{end+1} = uicontrol(...
    'Parent',h.Mia_Image.Settings.Orientation_Panel,...
    'Style','text',...
    'Units','normalized',...
    'FontSize',14,...
    'HorizontalAlignment','left',...
    'BackgroundColor', Look.Back,...
    'ForegroundColor', Look.Fore,...
    'Position',[0.02 0.54, 0.5 0.06],...
    'String','Custom Filetype:');

%%% Allows custom Filetype selection
if ~isdeployed
    Customdir = [PathToApp filesep 'functions' filesep 'MIA' filesep 'ReadIn'];
    if ~(exist(Customdir,'dir') == 7)
        mkdir(Customdir);
    end
    %%% Finds all matlab files in profiles directory
    Custom_Methods = what(Customdir);
    Custom_Methods = ['none'; Custom_Methods.m(:)];
    Custom_Value = 1;
    for i=2:numel(Custom_Methods)
        Custom_Methods{i}=Custom_Methods{i}(1:end-2);
        if strcmp(Custom_Methods{i},UserValues.File.MIA_Custom_Filetype)
            Custom_Value = i;
        end
    end
else
    %%% compiled application
    %%% custom file types are embedded
    %%% names are in associated text file
    fid = fopen([PathToApp filesep 'functions' filesep 'MIA' filesep 'ReadIn' filesep 'Custom_Read_Ins_MIA.txt'],'rt');
    if fid == -1
        fprintf('No Custom Read-In routines defined. Missing file Custom_Read_Ins.txt\n');
        Custom_Methods = {'none'};
        Custom_Value = 1;
    else % read file
        % skip the first three lines (header)
        for i = 1:3
            tline = fgetl(fid);
        end
        Custom_Methods = {'none'};
        Custom_Value = 1;
        while ischar(tline)
            tline = fgetl(fid);
            if ischar(tline)
                Custom_Methods{end+1,1} = tline;
            end
        end
        for i=2:numel(Custom_Methods)
            if strcmp(Custom_Methods{i},UserValues.File.MIA_Custom_Filetype)
                Custom_Value = i;
            end
        end
    end
end

%%% Creates objects for custom filetype settings
h.Mia_Image.Settings.FileType = uicontrol(...
    'Parent',h.Mia_Image.Settings.Orientation_Panel,...
    'Style','popupmenu',...
    'Units','normalized',...
    'FontSize',12,...
    'BackgroundColor', Look.Control,...
    'ForegroundColor', Look.Fore,...
    'String', Custom_Methods,...
    'UserData',{[],[],[]},...
    'Value',Custom_Value,...
    'Callback',{@MIA_CustomFileType,1},...
    'Position',[0.52 0.55, 0.41 0.05] );
if ismac
    h.Mia_Image.Settings.FileType.ForegroundColor = [0 0 0];
    h.Mia_Image.Settings.FileType.BackgroundColor = [1 1 1];
end
MIA_CustomFileType(h.Mia_Image.Settings.FileType,[],1);
h.Mia_Image.Settings.Custom = h.Mia_Image.Settings.FileType.UserData{3};

%%% Text
h.Text{end+1} = uicontrol(...
    'Parent',h.Mia_Image.Settings.Orientation_Panel,...
    'Style','text',... 
    'Units','normalized',...
    'FontSize',12,...
    'HorizontalAlignment','left',...
    'BackgroundColor', Look.Back,...
    'ForegroundColor', Look.Fore,...
    'Position',[0.02 0.20, 0.15 0.06],...
    'String','S:');
% editbox for variance-intensity slope
S = '';
for i = 1:5
    S = [S num2str(UserValues.MIA.Options.S(i)) ';'];
end
S = S(1:end-1);

h.Mia_Image.Settings.S = uicontrol(...
    'Tag','h.Mia_Image.Settings.S',...
    'Parent',h.Mia_Image.Settings.Orientation_Panel,...
    'Style','edit',...
    'Units','normalized',...
    'FontSize',12,...
    'BackgroundColor', Look.Control,...
    'ForegroundColor', Look.Fore,...
    'String', S,...
    'Callback',{@Mia_Orientation,4},...
    'Position',[0.3 0.22, 0.2 0.05],...
    'Tooltipstring','Variance vs Intensity slope for a sample without concentration fluctuations');

%%% Text
h.Text{end+1} = uicontrol(...
    'Parent',h.Mia_Image.Settings.Orientation_Panel,...
    'Style','text',...
    'Units','normalized',...
    'FontSize',12,...
    'HorizontalAlignment','left',...
    'BackgroundColor', Look.Back,...
    'ForegroundColor', Look.Fore,...
    'Position',[0.02 0.12, 0.15 0.06],...
    'String','Offset:');
% editbox for offset ("what is the intensity if you extrapolate to variance zero")
h.Mia_Image.Settings.Offset = uicontrol(...
    'Tag','h.Mia_Image.Settings.Offset',...
    'Parent',h.Mia_Image.Settings.Orientation_Panel,...
    'Style','edit',...
    'Units','normalized',...
    'FontSize',12,...
    'BackgroundColor', Look.Control,...
    'ForegroundColor', Look.Fore,...
    'String',num2str(UserValues.MIA.Options.Offset),...
    'Callback',{@Mia_Orientation,4},...
    'Position',[0.3 0.14, 0.2 0.05],...
    'Tooltipstring','Extrapolated Intensity at zero variance for a sample without concentration fluctuations');
%%% Checkbox to save images to TIF
h.Mia_Image.Settings.SaveImage = uicontrol(...
    'Parent', h.Mia_Image.Settings.Orientation_Panel,...
    'Style','checkbox',...
    'Units','normalized',...
    'FontSize',12,...
    'Value',1,...
    'BackgroundColor', Look.Back,...
    'ForegroundColor', Look.Fore,...
    'Position',[0.02 0.05, 0.5 0.06],...
    'String','Save images',...
    'Tooltipstring',['Saves images to a file when image panels are clicked.' 10 'Unchecking just displays the image in a new figure.']);

%% Calculations tab container
h.Mia_Image.Calculations_Panel = uibuttongroup(...
    'Parent',h.Mia_Image.Tab,...
    'Tag','Mia_Calculations_Panel',...
    'Units','normalized',...
    'BackgroundColor', Look.Back,...
    'ForegroundColor', Look.Fore,...
    'HighlightColor', Look.Control,...
    'ShadowColor', Look.Shadow,...
    'Position',[0.8 0 0.2 0.5]);
h.Mia_Image.Calculations_Tabs = uitabgroup(...
    'Parent',h.Mia_Image.Calculations_Panel,...
    'Tag','Mia_Calculations_Tabs',...
    'Units','normalized',...
    'Position',[0 0 1 1]);

%% Perform correlation tab
%%% Tab and panel for perform correlation UIs
h.Mia_Image.Calculations.Cor_Tab= uitab(...
    'Parent',h.Mia_Image.Calculations_Tabs,...
    'Tag','MI_Image_Tab',...
    'Title','Correlate');
h.Mia_Image.Calculations.Cor_Panel = uibuttongroup(...
    'Parent',h.Mia_Image.Calculations.Cor_Tab,...
    'Tag','Mia_Calculations_Cor_Panel',...
    'Units','normalized',...
    'BackgroundColor', Look.Back,...
    'ForegroundColor', Look.Fore,...
    'HighlightColor', Look.Control,...
    'ShadowColor', Look.Shadow,...
    'Position',[0 0 1 1]);

%%% Select, what to correlate
h.Mia_Image.Calculations.Cor_Type = uicontrol(...
    'Parent', h.Mia_Image.Calculations.Cor_Panel,...
    'Style','popupmenu',...
    'Units','normalized',...
    'FontSize',12,...
    'BackgroundColor', Look.Control,...
    'ForegroundColor', Look.Fore,...
    'Position',[0.02 0.92, 0.96 0.06],...
    'String',{'ACF1','ACF2','ACFs+CCF'});
if ismac
    h.Mia_Image.Calculations.Cor_Type.ForegroundColor = [0 0 0];
    h.Mia_Image.Calculations.Cor_Type.BackgroundColor = [1 1 1];
end
%%% Button to start image correlation
h.Mia_Image.Calculations.Cor_Do_ICS = uicontrol(...
    'Parent', h.Mia_Image.Calculations.Cor_Panel,...
    'Style','push',...
    'Units','normalized',...
    'FontSize',12,...
    'BackgroundColor', Look.Control,...
    'ForegroundColor', Look.Fore,...
    'Position',[0.02 0.82, 0.47 0.06],...
    'Callback', @Do_RICS,...
    'String','Do (R)ICS');
%%% Selects data saving procedure
h.Mia_Image.Calculations.Cor_Save_ICS = uicontrol(...
    'Parent', h.Mia_Image.Calculations.Cor_Panel,...
    'Style','popupmenu',...
    'Units','normalized',...
    'FontSize',12,...
    'BackgroundColor', Look.Control,...
    'ForegroundColor', Look.Fore,...
    'Position',[0.51 0.82, 0.47 0.06],...
    'Callback',{@MIA_Various, 11},...
    'String',{'Do not save','Save as .miacor','Save as TIFF', 'Save blockwise analysis as .miacor', 'grid-based map', 'intensity-based map'});
if ismac
    h.Mia_Image.Settings.ROI_AR_Same.ForegroundColor = [0 0 0];
    h.Mia_Image.Settings.ROI_AR_Same.BackgroundColor = [1 1 1];
    h.Mia_Image.Calculations.Cor_Save_ICS.ForegroundColor = [0 0 0];
    h.Mia_Image.Calculations.Cor_Save_ICS.BackgroundColor = [1 1 1];
end
%%% Button to start temporal image correlation
h.Mia_Image.Calculations.Cor_Do_TICS = uicontrol(...
    'Parent', h.Mia_Image.Calculations.Cor_Panel,...
    'Style','push',...
    'Tag', 'DoTICS',...
    'Units','normalized',...
    'FontSize',12,...
    'BackgroundColor', Look.Control,...
    'ForegroundColor', Look.Fore,...
    'Position',[0.02 0.72, 0.47 0.06],...
    'Callback',@Do_1D_XCor,...
    'String','Do TICS');
%%% Selects data saving procedure
h.Mia_Image.Calculations.Cor_Save_TICS = uicontrol(...
    'Parent', h.Mia_Image.Calculations.Cor_Panel,...
    'Style','popupmenu',...
    'Units','normalized',...
    'FontSize',12,...
    'BackgroundColor', Look.Control,...
    'ForegroundColor', Look.Fore,...
    'Position',[0.51 0.72, 0.47 0.06],...
    'String',{'Do not save','Save as .mcor'});
if ismac
    h.Mia_Image.Calculations.Cor_Save_TICS.ForegroundColor = [0 0 0];
    h.Mia_Image.Calculations.Cor_Save_TICS.BackgroundColor = [1 1 1];
end
%%% Button to start spatio-temporal correlation
h.Mia_Image.Calculations.Cor_Do_STICS = uicontrol(...
    'Parent', h.Mia_Image.Calculations.Cor_Panel,...
    'Style','push',...
    'Units','normalized',...
    'FontSize',12,...
    'BackgroundColor', Look.Control,...
    'ForegroundColor', Look.Fore,...
    'Position',[0.02 0.62, 0.47 0.06],...
    'Callback',@Do_3D_XCor,...
    'String','Do STICS/iMSD');
%%% Selects data saving procedure
h.Mia_Image.Calculations.Cor_Save_STICS = uicontrol(...
    'Parent', h.Mia_Image.Calculations.Cor_Panel,...
    'Style','popupmenu',...
    'Units','normalized',...
    'FontSize',12,...
    'BackgroundColor', Look.Control,...
    'ForegroundColor', Look.Fore,...
    'Position',[0.51 0.62, 0.47 0.06],...
    'String',{'Do not save','Save as iMSD(.mcor)','Save as STICS (.stcor)','Both'});
if ismac
    h.Mia_Image.Calculations.Cor_Save_STICS.ForegroundColor = [0 0 0];
    h.Mia_Image.Calculations.Cor_Save_STICS.BackgroundColor = [1 1 1];
end
h.Text{end+1} = uicontrol(...
    'Parent', h.Mia_Image.Calculations.Cor_Panel,...
    'Style','text',...
    'Units','normalized',...
    'FontSize',12,...
    'HorizontalAlignment','left',...
    'BackgroundColor', Look.Back,...
    'ForegroundColor', Look.Fore,...
    'Position',[0.02 0.54, 0.76 0.06],...
    'String','Window:',...
    'TooltipString',['The sliding window size (blockwise RICS),' 10 'number of lags to be calculated (STICS)' 10 'or the grid element size (grid-based RICS)']);
%%% Edit box to choose the n.o. lags to calculate (STICS) or the window size (RICS)
h.Mia_Image.Calculations.Cor_ICS_Window = uicontrol(...
    'Parent', h.Mia_Image.Calculations.Cor_Panel,...
    'Style','edit',...
    'Units','normalized',...
    'FontSize',12,...
    'BackgroundColor', Look.Control,...
    'ForegroundColor', Look.Fore,...
    'Position',[0.3 0.54, 0.15 0.06],...
    'String','20');
h.Text{end+1} = uicontrol(...
    'Parent', h.Mia_Image.Calculations.Cor_Panel,...
    'Style','text',...
    'Units','normalized',...
    'FontSize',12,...
    'HorizontalAlignment','left',...
    'BackgroundColor', Look.Back,...
    'ForegroundColor', Look.Fore,...
    'Position',[0.02 0.47, 0.76 0.06],...
    'String','Offset:',...
    'Tooltipstring', ['The offset between the first' 10 'frame of each window (blockwise RICS),' 10 'or the offset between the first pixels' 10 'of the grid elements (grid-based RICS).' 10 'If W=O, then the blocks are' 10 'adjacent, non-overlapping.']);
%%% Edit box to choose the n.o. frames to slide from one window to the other (RICS)
h.Mia_Image.Calculations.Cor_ICS_Offset = uicontrol(...
    'Parent', h.Mia_Image.Calculations.Cor_Panel,...
    'Style','edit',...
    'Units','normalized',...
    'FontSize',12,...
    'BackgroundColor', Look.Control,...
    'ForegroundColor', Look.Fore,...
    'Position',[0.3 0.47, 0.15 0.06],...
    'String','20');
h.Mia_Image.Calculations.Cor_ICS_nAROI_txt = uicontrol(...
    'Parent', h.Mia_Image.Calculations.Cor_Panel,...
    'Style','text',...
    'Units','normalized',...
    'FontSize',12,...
    'HorizontalAlignment','left',...
    'BackgroundColor', Look.Back,...
    'ForegroundColor', Look.Fore,...
    'Position',[0.02 0.40, 0.76 0.06],...
    'String','n.o. AROIs:',...
    'Visible','off',...
    'Tooltipstring', ['number of ROIs used for intensity-thresholded' 10 'RICS segmentation and diffusion mapping.' 10 'Use a number between 3 and 6.']);
%%% Edit box to choose the n.o. frames to slide from one window to the other (RICS)
h.Mia_Image.Calculations.Cor_ICS_nAROI = uicontrol(...
    'Parent', h.Mia_Image.Calculations.Cor_Panel,...
    'Style','edit',...
    'Units','normalized',...
    'FontSize',12,...
    'BackgroundColor', Look.Control,...
    'ForegroundColor', Look.Fore,...
    'Position',[0.3 0.40, 0.15 0.06],...
    'Visible','off',...
    'String','4');
%%% Popupmenu to select averaging style
h.Mia_Image.Calculations.Cor_ICS_Scaling = uicontrol(...
    'Parent', h.Mia_Image.Calculations.Cor_Panel,...
    'Style','popupmenu',...
    'Units','normalized',...
    'FontSize',12,...
    'Value',1,...
    'BackgroundColor', Look.Control,...
    'ForegroundColor', Look.Fore,...
    'Position',[0.51 0.4, 0.4 0.06],...
    'Visible','off',...
    'String','0',...
    'Tooltipstring',['Besides the used colormap, the brightness of the pixels' 10 'in the maps can be set to the number of times' 10 'a pixel is sampled throughout the analysis' 10 'or with the local count rate.']);
if ismac
    h.Mia_Image.Calculations.Cor_ICS_Scaling.ForegroundColor = [0 0 0];
    h.Mia_Image.Calculations.Cor_ICS_Scaling.BackgroundColor = [1 1 1];
end
%%% Checkbox to switch between automatic\manual filenames
h.Mia_Image.Calculations.Save_Name = uicontrol(...
    'Parent', h.Mia_Image.Calculations.Cor_Panel,...
    'Style','checkbox',...
    'Units','normalized',...
    'FontSize',12,...
    'Value',UserValues.MIA.AutoNames,...
    'Callback', @(src,event) LSUserValues(1,src,{'Value','MIA','AutoNames'}),...
    'BackgroundColor', Look.Back,...
    'ForegroundColor', Look.Fore,...
    'Position',[0.02 0.02, 0.7 0.06],...
    'String','Use automatic filename');
%%% Checkbox to switch between automatic\manual filenames
h.Mia_Image.Calculations.Overwrite = uicontrol(...
    'Parent', h.Mia_Image.Calculations.Cor_Panel,...
    'Style','checkbox',...
    'Units','normalized',...
    'FontSize',12,...
    'Value',UserValues.MIA.Overwrite,...
    'Callback', @(src,event) LSUserValues(1,src,{'Value','MIA','Overwrite'}),...
    'BackgroundColor', Look.Back,...
    'ForegroundColor', Look.Fore,...
    'Position',[0.02 0.1, 0.7 0.06],...
    'String','Overwrite existing files');
%%% Popupmenu to select averaging style
h.Mia_Image.Calculations.Cor_TICS_SpaceAvg = uicontrol(...
    'Parent', h.Mia_Image.Calculations.Cor_Panel,...
    'Style','popupmenu',...
    'Units','normalized',...
    'FontSize',12,...
    'Value',2,...
    'BackgroundColor', Look.Control,...
    'ForegroundColor', Look.Fore,...
    'Position',[0.02 0.31, 0.4 0.06],...
    'String',{'None','Average','Disk','Gaussian'},...
    'Tooltipstring','Spatially xy average the TICS correlations');
if ismac
    h.Mia_Image.Calculations.Cor_TICS_SpaceAvg.ForegroundColor = [0 0 0];
    h.Mia_Image.Calculations.Cor_TICS_SpaceAvg.BackgroundColor = [1 1 1];
end
%%% Text
h.Text{end+1} = uicontrol(...
    'Parent', h.Mia_Image.Calculations.Cor_Panel,...
    'Style','text',...
    'Units','normalized',...
    'FontSize',12,...
    'HorizontalAlignment','left',...
    'BackgroundColor', Look.Back,...
    'ForegroundColor', Look.Fore,...
    'Position',[0.02 0.21, 0.76 0.06],...
    'String','Size:');
%%% Editbox for Averaging radius
h.Mia_Image.Calculations.Cor_TICS_AvgSize = uicontrol(...
    'Parent', h.Mia_Image.Calculations.Cor_Panel,...
    'Style','edit',...
    'Units','normalized',...
    'FontSize',12,...
    'BackgroundColor', Look.Control,...
    'ForegroundColor', Look.Fore,...
    'Position',[0.3 0.21, 0.15 0.06],...
    'String','3',...
    'Tooltipstring',['Radius for the spatial TICS xy averaging.' 10 'Size of the median filter on the TICS tab.']);


%% Perform N&B calculation tab
%%% Tab and panel for perform correlation UIs
h.Mia_Image.Calculations.NB_Tab= uitab(...
    'Parent',h.Mia_Image.Calculations_Tabs,...
    'Title','Do N&B');
h.Mia_Image.Calculations.NB_Panel = uibuttongroup(...
    'Parent',h.Mia_Image.Calculations.NB_Tab,...
    'Units','normalized',...
    'BackgroundColor', Look.Back,...
    'ForegroundColor', Look.Fore,...
    'HighlightColor', Look.Control,...
    'ShadowColor', Look.Shadow,...
    'Position',[0 0 1 1]);
%%% Button to start N&B calculation
h.Mia_Image.Calculations.NB_Do = uicontrol(...
    'Parent', h.Mia_Image.Calculations.NB_Panel,...
    'Style','push',...
    'Units','normalized',...
    'FontSize',12,...
    'BackgroundColor', Look.Control,...
    'ForegroundColor', Look.Fore,...
    'Position',[0.02 0.92, 0.96 0.06],...
    'Callback',@Do_NB,...
    'String','Calculate N&B');
%%% Popupmenu to select for which channels to calculate
h.Mia_Image.Calculations.NB_Type = uicontrol(...
    'Parent', h.Mia_Image.Calculations.NB_Panel,...
    'Style','popupmenu',...
    'Units','normalized',...
    'FontSize',12,...
    'BackgroundColor', Look.Control,...
    'ForegroundColor', Look.Fore,...
    'Position',[0.02 0.84, 0.4 0.06],...
    'String',{'Top Channel','Bottom Channel','Cross'});
if ismac
    h.Mia_Image.Calculations.NB_Type.ForegroundColor = [0 0 0];
    h.Mia_Image.Calculations.NB_Type.BackgroundColor = [1 1 1];
end
%%% Popupmenu to select averaging style
h.Mia_Image.Calculations.NB_Average = uicontrol(...
    'Parent', h.Mia_Image.Calculations.NB_Panel,...
    'Style','popupmenu',...
    'Units','normalized',...
    'FontSize',12,...
    'Value',2,...
    'BackgroundColor', Look.Control,...
    'ForegroundColor', Look.Fore,...
    'Position',[0.02 0.76, 0.4 0.06],...
    'String',{'None','Average','Disk','Gaussian'},...
    'Tooltipstring','apply an averaging filter to the intensity and variance images');
if ismac
    h.Mia_Image.Calculations.NB_Average.ForegroundColor = [0 0 0];
    h.Mia_Image.Calculations.NB_Average.BackgroundColor = [1 1 1];
end
%%% Text
h.Text{end+1} = uicontrol(...
    'Parent', h.Mia_Image.Calculations.NB_Panel,...
    'Style','text',...
    'Units','normalized',...
    'FontSize',12,...
    'BackgroundColor', Look.Back,...
    'ForegroundColor', Look.Fore,...
    'Position',[0.44 0.745, 0.26 0.06],...
    'String','Radius:');
%%% Editbox for Averaging radius
h.Mia_Image.Calculations.NB_Average_Radius = uicontrol(...
    'Parent', h.Mia_Image.Calculations.NB_Panel,...
    'Style','edit',...
    'Units','normalized',...
    'FontSize',12,...
    'BackgroundColor', Look.Control,...
    'ForegroundColor', Look.Fore,...
    'Position',[0.72 0.75, 0.15 0.06],...
    'String','3',...
    'Tooltipstring','sets the radius for the averaging and median filtering');
%%% Checkboxbox for Median filter
h.Mia_Image.Calculations.NB_Median = uicontrol(...
    'Parent', h.Mia_Image.Calculations.NB_Panel,...
    'Style','checkbox',...
    'Units','normalized',...
    'FontSize',12,...
    'BackgroundColor', Look.Back,...
    'ForegroundColor', Look.Fore,...
    'Position',[0.02 0.68, 0.35 0.06],...
    'String','Median filter',...
    'Tooltipstring','apply a median filter to the N and epsilon images');
%%% Text
h.Text{end+1} = uicontrol(...
    'Parent', h.Mia_Image.Calculations.NB_Panel,...
    'Style','text',...
    'Units','normalized',...
    'FontSize',12,...
    'BackgroundColor', Look.Back,...
    'ForegroundColor', Look.Fore,...
    'Position',[0.01 0.60, 0.8 0.06],...
    'String','Detector Dead time [ns]:');
%%% Editbox for Deadtime correction
h.Mia_Image.Calculations.NB_Detector_Deadtime = uicontrol(...
    'Parent', h.Mia_Image.Calculations.NB_Panel,...
    'Style','edit',...
    'Units','normalized',...
    'FontSize',12,...
    'BackgroundColor', Look.Control,...
    'ForegroundColor', Look.Fore,...
    'Position',[0.7 0.60, 0.22 0.06],...
    'String','100');

%% Perform FRET tab
%%% Tab and panel for perform correlation UIs
h.Mia_Image.Calculations.FRET_Tab= uitab(...
    'Parent',h.Mia_Image.Calculations_Tabs,...
    'Tag','MI_Image_Tab',...
    'Title','FRET');
h.Mia_Image.Calculations.FRET_Panel = uibuttongroup(...
    'Parent',h.Mia_Image.Calculations.FRET_Tab,...
    'Tag','Mia_Calculations_FRET_Panel',...
    'Units','normalized',...
    'BackgroundColor', Look.Back,...
    'ForegroundColor', Look.Fore,...
    'HighlightColor', Look.Control,...
    'ShadowColor', Look.Shadow,...
    'Position',[0 0 1 1]);

%%% Select, which FRET method to use
h.Mia_Image.Calculations.FRET_Type = uicontrol(...
    'Parent', h.Mia_Image.Calculations.FRET_Panel,...
    'Style','popupmenu',...
    'Units','normalized',...
    'FontSize',12,...
    'BackgroundColor', Look.Control,...
    'ForegroundColor', Look.Fore,...
    'Position',[0.02 0.92, 0.96 0.06],...
    'String',{'norm A/D','A/D','Fc (Youvan)', 'FRETN (Gordon)', 'N-FRET (Xia)'});
if ismac
    h.Mia_Image.Calculations.FRET_Type.ForegroundColor = [0 0 0];
    h.Mia_Image.Calculations.FRET_Type.BackgroundColor = [1 1 1];
end
%%% Button to start FRET calculation
h.Mia_Image.Calculations.DoFRET = uicontrol(...
    'Parent', h.Mia_Image.Calculations.FRET_Panel,...
    'Style','push',...
    'Units','normalized',...
    'FontSize',12,...
    'BackgroundColor', Look.Control,...
    'ForegroundColor', Look.Fore,...
    'Position',[0.02 0.72, 0.47 0.06],...
    'Callback',@Do_FRET,...
    'String','Do FRET');
%%% Selects data saving procedure
h.Mia_Image.Calculations.Save_FRET = uicontrol(...
    'Parent', h.Mia_Image.Calculations.FRET_Panel,...
    'Style','popupmenu',...
    'Units','normalized',...
    'FontSize',12,...
    'BackgroundColor', Look.Control,...
    'ForegroundColor', Look.Fore,...
    'Position',[0.51 0.72, 0.47 0.06],...
    'String',{'Do not save','Save'});
%%% Text
h.Text{end+1} = uicontrol(...
    'Parent', h.Mia_Image.Calculations.FRET_Panel,...
    'Style','text',...
    'Units','normalized',...
    'FontSize',12,...
    'BackgroundColor', Look.Back,...
    'ForegroundColor', Look.Fore,...
    'Position',[0.02 0.82, 0.4 0.06],...
    'String','Normalize on frames:');
%%% Editbox for Averaging radius
h.Mia_Image.Calculations.FRET_norm = uicontrol(...
    'Parent', h.Mia_Image.Calculations.FRET_Panel,...
    'Style','edit',...
    'Units','normalized',...
    'FontSize',12,...
    'BackgroundColor', Look.Control,...
    'ForegroundColor', Look.Fore,...
    'Position',[0.52 0.82, 0.15 0.06],...
    'String','1:5',...
    'Tooltipstring','normalizes the FRET data on frames n:m');
if ismac
    h.Mia_Image.Calculations.Save_FRET.ForegroundColor = [0 0 0];
    h.Mia_Image.Calculations.Save_FRET.BackgroundColor = [1 1 1];
end

%% Perform Colocalization tab
%%% Tab and panel for perform correlation UIs
h.Mia_Image.Calculations.Coloc_Tab= uitab(...
    'Parent',h.Mia_Image.Calculations_Tabs,...
    'Tag','MI_Image_Tab',...
    'Title','Colocalization');
h.Mia_Image.Calculations.Coloc_Panel = uibuttongroup(...
    'Parent',h.Mia_Image.Calculations.Coloc_Tab,...
    'Tag','Mia_Calculations_Coloc_Panel',...
    'Units','normalized',...
    'BackgroundColor', Look.Back,...
    'ForegroundColor', Look.Fore,...
    'HighlightColor', Look.Control,...
    'ShadowColor', Look.Shadow,...
    'Position',[0 0 1 1]);

%%% Select, which Coloc method to use
h.Mia_Image.Calculations.Coloc_Type = uicontrol(...
    'Parent', h.Mia_Image.Calculations.Coloc_Panel,...
    'Style','popupmenu',...
    'Units','normalized',...
    'FontSize',12,...
    'BackgroundColor', Look.Control,...
    'ForegroundColor', Look.Fore,...
    'Position',[0.02 0.92, 0.5 0.06],...
    'String',{'Pearson','Manders', 'Costes', 'Van Steensel', 'Li', 'object-based'});
if ismac
    h.Mia_Image.Calculations.Coloc_Type.ForegroundColor = [0 0 0];
    h.Mia_Image.Calculations.Coloc_Type.BackgroundColor = [1 1 1];
end

%%% Checkboxbox for intensity weighting
h.Mia_Image.Calculations.Coloc_weighting = uicontrol(...
    'Parent', h.Mia_Image.Calculations.Coloc_Panel,...
    'Style','checkbox',...
    'Units','normalized',...
    'FontSize',12,...
    'BackgroundColor', Look.Back,...
    'ForegroundColor', Look.Fore,...
    'Position',[0.02 0.82, 0.5 0.06],...
    'String','Intensity weighted',...
    'Tooltipstring','Calculate the colocalization using intensity-weighting. Option doesn`t work yet!');
h.Text{end+1} = uicontrol(...
    'Parent', h.Mia_Image.Calculations.Coloc_Panel,...
    'Style','text',...
    'Units','normalized',...
    'FontSize',12,...
    'BackgroundColor', Look.Back,...
    'ForegroundColor', Look.Fore,...
    'Position',[0.02 0.72, 0.3 0.06],...
    'String','Averaging diameter:');
%%% Editbox for Averaging radius
h.Mia_Image.Calculations.Coloc_avg = uicontrol(...
    'Parent', h.Mia_Image.Calculations.Coloc_Panel,...
    'Style','edit',...
    'Units','normalized',...
    'FontSize',12,...
    'BackgroundColor', Look.Control,...
    'ForegroundColor', Look.Fore,...
    'Position',[0.52 0.72, 0.15 0.06],...
    'String','3',...
    'Tooltipstring','size of the square subregion for calculating the square');

h.Mia_Image.Calculations.Coloc_avg.Visible = 'off';
h.Text{end}.Visible = 'off';

%%% Button to start Coloc calculation
h.Mia_Image.Calculations.DoColoc = uicontrol(...
    'Parent', h.Mia_Image.Calculations.Coloc_Panel,...
    'Style','push',...
    'Units','normalized',...
    'FontSize',12,...
    'BackgroundColor', Look.Control,...
    'ForegroundColor', Look.Fore,...
    'Position',[0.02 0.62, 0.3 0.06],...
    'Callback',@Do_Coloc,...
    'Tooltipstring','Calculate the colocalization using the top right AROI',...
    'String','Analyze');
%%% Selects data saving procedure
h.Mia_Image.Calculations.Save_Coloc = uicontrol(...
    'Parent', h.Mia_Image.Calculations.Coloc_Panel,...
    'Style','popupmenu',...
    'Units','normalized',...
    'FontSize',12,...
    'BackgroundColor', Look.Control,...
    'ForegroundColor', Look.Fore,...
    'Position',[0.35 0.62, 0.47 0.06],...
    'String',{'Do not save','Save as .miacor'});
%% mean Pearson's coefficient
h.Mia_Image.Calculations.Coloc_Pearsons = uicontrol(...
    'Parent', h.Mia_Image.Calculations.Coloc_Panel,...
    'Style','text',...
    'Units','normalized',...
    'FontSize',12,...
    'HorizontalAlignment','left',...
    'BackgroundColor', Look.Back,...
    'ForegroundColor', Look.Fore,...
    'Position',[0.02 0.47, 0.96 0.1],...
    'String','mean Pearson`s: ',...
    'ToolTipString', 'Pearsons correlation coefficient for the displayed rightmost images in Mia within the AROI');% %%% Text
if ismac
    h.Mia_Image.Calculations.Save_Coloc.ForegroundColor = [0 0 0];
    h.Mia_Image.Calculations.Save_Coloc.BackgroundColor = [1 1 1];
end

%% FRAP tab
%%% Tab and panel for performing FRAP
h.Mia_Image.Calculations.FRAP_Tab= uitab(...
    'Parent',h.Mia_Image.Calculations_Tabs,...
    'Tag','MI_Image_Tab',...
    'Title','FRAP');
h.Mia_Image.Calculations.FRAP_Panel = uibuttongroup(...
    'Parent',h.Mia_Image.Calculations.FRAP_Tab,...
    'Tag','Mia_Calculations_FRAP_Panel',...
    'Units','normalized',...
    'BackgroundColor', Look.Back,...
    'ForegroundColor', Look.Fore,...
    'HighlightColor', Look.Control,...
    'ShadowColor', Look.Shadow,...
    'Position',[0 0 1 1]);
%%% Button to start FRAP calculation
h.Mia_Image.Calculations.FRAP_Do = uicontrol(...
    'Parent', h.Mia_Image.Calculations.FRAP_Panel,...
    'Style','push',...
    'Units','normalized',...
    'FontSize',12,...
    'BackgroundColor', Look.Control,...
    'ForegroundColor', Look.Fore,...
    'Position',[0.02 0.91, 0.47 0.07],...
    'Callback',@Do_FRAP,...
    'String','Calculate FRAP');
%%% Select which channel to analyze
h.Mia_Image.Calculations.FRAP_Save = uicontrol(...
    'Parent', h.Mia_Image.Calculations.FRAP_Panel,...
    'Style','popupmenu',...
    'Units','normalized',...
    'FontSize',12,...
    'BackgroundColor', Look.Control,...
    'ForegroundColor', Look.Fore,...
    'Position',[0.51 0.92, 0.47 0.06],...
    'String',{'Do not save','Save as .mat and .fig'});
if ismac
    h.Mia_Image.Calculations.FRAP_Save.ForegroundColor = [0 0 0];
    h.Mia_Image.Calculations.FRAP_Save.BackgroundColor = [1 1 1];
end

%%% Select which channel to analyze
h.Mia_Image.Calculations.FRAP_Channel = uicontrol(...
    'Parent', h.Mia_Image.Calculations.FRAP_Panel,...
    'Style','popupmenu',...
    'Units','normalized',...
    'FontSize',12,...
    'BackgroundColor', Look.Control,...
    'ForegroundColor', Look.Fore,...
    'Position',[0.02 0.84, 0.47 0.06],...
    'Callback',{@MIA_Various,12},...
    'String',{'Top Channel','Bottom Channel', 'Both'},...
    'Tooltipstring','Select the FRAP Channel(s)');
if ismac
    h.Mia_Image.Calculations.FRAP_Channel.ForegroundColor = [0 0 0];
    h.Mia_Image.Calculations.FRAP_Channel.BackgroundColor = [1 1 1];
end

%%% Select type of bleaching spot
h.Mia_Image.Calculations.FRAP_ROI_Type = uicontrol(...
    'Parent', h.Mia_Image.Calculations.FRAP_Panel,...
    'Style','popupmenu',...
    'Units','normalized',...
    'FontSize',12,...
    'BackgroundColor', Look.Control,...
    'ForegroundColor', Look.Fore,...
    'Position',[0.51 0.84, 0.47 0.06],...
    'Callback',{@MIA_Various,12},...
    'String',{'Circle', 'Rectangle', 'Freehand'},...
    'Tooltipstring','Choose type of ROI to use for bleached spot');
if ismac
    h.Mia_Image.Calculations.FRAP_ROI_Type.ForegroundColor = [0 0 0];
    h.Mia_Image.Calculations.FRAP_ROI_Type.BackgroundColor = [1 1 1];
end

%%% Text for bleach position
h.Text{end+1} = uicontrol(...
    'Parent', h.Mia_Image.Calculations.FRAP_Panel,...
    'Tag', 'FRAP_Pos',...
    'Style','text',...
    'Units','normalized',...
    'FontSize',12,...
    'BackgroundColor', Look.Back,...
    'ForegroundColor', Look.Fore,...
    'Position',[0.02 0.70, 0.5 0.06],...
    'String','Bleach Position [px]:');
h.Text{end+1} = uicontrol(...
    'Parent', h.Mia_Image.Calculations.FRAP_Panel,...
    'Tag', 'FRAP_Pos',...
    'Style','text',...
    'Units','normalized',...
    'FontSize',12,...
    'BackgroundColor', Look.Back,...
    'ForegroundColor', Look.Fore,...
    'Position',[0.52 0.76, 0.2 0.06],...
    'String','X');
h.Text{end+1} = uicontrol(...
    'Parent', h.Mia_Image.Calculations.FRAP_Panel,...
    'Tag', 'FRAP_Pos',...
    'Style','text',...
    'Units','normalized',...
    'FontSize',12,...
    'BackgroundColor', Look.Back,...
    'ForegroundColor', Look.Fore,...
    'Position',[0.74 0.76, 0.2 0.06],...
    'String','Y');

%%% Editbox for bleaching ROI position
h.Mia_Image.Calculations.FRAP_Spot_X = uicontrol(...
    'Parent', h.Mia_Image.Calculations.FRAP_Panel,...
    'Tag', 'FRAP_Pos',...
    'Style','edit',...
    'Units','normalized',...
    'FontSize',12,...
    'BackgroundColor', Look.Control,...
    'ForegroundColor', Look.Fore,...
    'Position',[0.52 0.70, 0.2 0.06],...
    'Callback',{@MIA_Various,12},...
    'String','1',...
    'Tooltipstring','sets the X position of the bleached spot/area');
h.Mia_Image.Calculations.FRAP_Spot_Y = uicontrol(...
    'Parent', h.Mia_Image.Calculations.FRAP_Panel,...
    'Tag', 'FRAP_Pos',...
    'Style','edit',...
    'Units','normalized',...
    'FontSize',12,...
    'BackgroundColor', Look.Control,...
    'ForegroundColor', Look.Fore,...
    'Position',[0.74 0.70, 0.2 0.06],...
    'Callback',{@MIA_Various,12},...
    'String','1',...
    'Tooltipstring','sets the Y position of the bleached spot/area');

%%% Text
h.Text{end+1} = uicontrol(...
    'Parent', h.Mia_Image.Calculations.FRAP_Panel,...
    'Tag', 'FRAP_Pos_Circle',...
    'Style','text',...
    'Units','normalized',...
    'FontSize',12,...
    'BackgroundColor', Look.Back,...
    'ForegroundColor', Look.Fore,...
    'Position',[0.02 0.62, 0.5 0.06],...
    'String','Spot Radius [px]:');
%%% Editbox for bleaching spot radius
h.Mia_Image.Calculations.FRAP_Spot_Radius = uicontrol(...
    'Parent', h.Mia_Image.Calculations.FRAP_Panel,...
    'Tag', 'FRAP_Pos_Circle',...
    'Style','edit',...
    'Units','normalized',...
    'FontSize',12,...
    'BackgroundColor', Look.Control,...
    'ForegroundColor', Look.Fore,...
    'Position',[0.52 0.62, 0.2 0.06],...
    'Callback',{@MIA_Various,12},...
    'String','20',...
    'Tooltipstring','sets the radius of the bleached spot');

%%% Text
h.Text{end+1} = uicontrol(...
    'Parent', h.Mia_Image.Calculations.FRAP_Panel,...
    'Tag', 'FRAP_Pos_Rectangle',...
    'Style','text',...
    'Units','normalized',...
    'FontSize',12,...
    'BackgroundColor', Look.Back,...
    'ForegroundColor', Look.Fore,...
    'Position',[0.02 0.62, 0.5 0.06],...
    'String','Rectangle Size [px]:',...
    'Visible', 'off');
%%% Editbox for bleaching ROI size
h.Mia_Image.Calculations.FRAP_Rect_SizeX = uicontrol(...
    'Parent', h.Mia_Image.Calculations.FRAP_Panel,...
    'Tag', 'FRAP_Pos_Rectangle',...
    'Style','edit',...
    'Units','normalized',...
    'FontSize',12,...
    'BackgroundColor', Look.Control,...
    'ForegroundColor', Look.Fore,...
    'Position',[0.52 0.62, 0.2 0.06],...
    'Callback',{@MIA_Various,12},...
    'String','20',...
    'Tooltipstring','sets the size of the bleached area in X',...
    'Visible', 'off');
h.Mia_Image.Calculations.FRAP_Rect_SizeY = uicontrol(...
    'Parent', h.Mia_Image.Calculations.FRAP_Panel,...
    'Tag', 'FRAP_Pos_Rectangle',...
    'Style','edit',...
    'Units','normalized',...
    'FontSize',12,...
    'BackgroundColor', Look.Control,...
    'ForegroundColor', Look.Fore,...
    'Position',[0.74 0.62, 0.2 0.06],...
    'Callback',{@MIA_Various,12},...
    'String','20',...
    'Tooltipstring','sets the size of the bleached area in Y',...
    'Visible', 'off');

%%% Text
h.Text{end+1} = uicontrol(...
    'Parent', h.Mia_Image.Calculations.FRAP_Panel,...
    'Style','text',...
    'Units','normalized',...
    'FontSize',12,...
    'BackgroundColor', Look.Back,...
    'ForegroundColor', Look.Fore,...
    'Position',[0.02 0.54, 0.5 0.06],...
    'String','Frame Interval [s]:');
%%% Editbox for bleaching spot radius
h.Mia_Image.Calculations.FRAP_Frame_Interval = uicontrol(...
    'Parent', h.Mia_Image.Calculations.FRAP_Panel,...
    'Style','edit',...
    'Units','normalized',...
    'FontSize',12,...
    'BackgroundColor', Look.Control,...
    'ForegroundColor', Look.Fore,...
    'Position',[0.52 0.54, 0.2 0.06],...
    'String','1',...
    'Tooltipstring','sets the frame interval');

%%% Text
h.Text{end+1} = uicontrol(...
    'Parent', h.Mia_Image.Calculations.FRAP_Panel,...
    'Style','text',...
    'Units','normalized',...
    'FontSize',12,...
    'BackgroundColor', Look.Back,...
    'ForegroundColor', Look.Fore,...
    'Position',[0.02 0.46, 0.5 0.06],...
    'String','Pre-bleach range:');
%%% Set pre-bleach range
h.Mia_Image.Calculations.FRAP_Prebleach_Start = uicontrol(...
    'Parent', h.Mia_Image.Calculations.FRAP_Panel,...
    'Style','edit',...
    'Units','normalized',...
    'FontSize',12,...
    'BackgroundColor', Look.Control,...
    'ForegroundColor', Look.Fore,...
    'Position',[0.52 0.46, 0.2 0.06],...
    'String','0',...
    'Tooltipstring',['range of pre-bleach frames (used for normalization)' 10 'no normalization if either value is set to 0' 10 'or the range is invalid']);
h.Mia_Image.Calculations.FRAP_Prebleach_End = uicontrol(...
    'Parent', h.Mia_Image.Calculations.FRAP_Panel,...
    'Style','edit',...
    'Units','normalized',...
    'FontSize',12,...
    'BackgroundColor', Look.Control,...
    'ForegroundColor', Look.Fore,...
    'Position',[0.74 0.46, 0.2 0.06],...
    'String','0',...
    'Tooltipstring',['range of pre-bleach frames (used for normalization)' 10 'no normalization if either value is set to 0' 10 'or the range is invalid']);

%%% Text
h.Text{end+1} = uicontrol(...
    'Parent', h.Mia_Image.Calculations.FRAP_Panel,...
    'Style','text',...
    'Units','normalized',...
    'FontSize',12,...
    'BackgroundColor', Look.Back,...
    'ForegroundColor', Look.Fore,...
    'Position',[0.02 0.38, 0.5 0.06],...
    'String','1st bleached frame:');
h.Mia_Image.Calculations.FRAP_FirstRecoveryFrame = uicontrol(...
    'Parent', h.Mia_Image.Calculations.FRAP_Panel,...
    'Style','edit',...
    'Units','normalized',...
    'FontSize',12,...
    'BackgroundColor', Look.Control,...
    'ForegroundColor', Look.Fore,...
    'Position',[0.52 0.38, 0.2 0.06],...
    'String','1',...
    'Tooltipstring','specify the 1st frame where recovery begins');

%%% Text
h.Text{end+1} = uicontrol(...
    'Parent', h.Mia_Image.Calculations.FRAP_Panel,...
    'Style','text',...
    'Units','normalized',...
    'FontSize',12,...
    'BackgroundColor', Look.Back,...
    'ForegroundColor', Look.Fore,...
    'Position',[0.02 0.30, 0.25 0.06],...
    'String','Fit Model:');
%%% Select which fit model to use
h.Mia_Image.Calculations.FRAP_Fit_Model = uicontrol(...
    'Parent', h.Mia_Image.Calculations.FRAP_Panel,...
    'Style','popupmenu',...
    'Units','normalized',...
    'FontSize',12,...
    'BackgroundColor', Look.Control,...
    'ForegroundColor', Look.Fore,...
    'Position',[0.3 0.31, 0.68 0.06],...
    'Callback',{@MIA_Various,12},...
    'String',{'None','1 Comp Saturating Exponential', '2 Comp Saturating Exponential', ...
    '2-phase Exponential Decay', 'cFRAP'});

%%% Exponential fit options table
TableData_FRAP2ExpFit = {   'A0', 1, 0, Inf, false;...
                'Amax1', 0.5, 0, Inf, false;...
                'k1', 0.1, 0, Inf, false;...
                'Amax2', 0.5, 0, Inf, false;...
                'k2', 0.01, 0, Inf, false};
ColumnNames = {'Parameter', 'Initial Value', 'LB', 'UB', 'Fix'};
ColumnWidth = {80,80,60,60,40};
ColumnEditable = [false,true,true,true,true];
RowNames = [];

h.Mia_Image.Calculations.FRAP_FitOptions = uitable(...
    'Parent', h.Mia_Image.Calculations.FRAP_Panel,...
    'Units','normalized',...
    'FontSize',10,...
    'Position',[0.01 0.01 0.98 0.29],...
    'Data',TableData_FRAP2ExpFit,...
    'ColumnName',ColumnNames,...
    'RowName',RowNames,...
    'ColumnWidth',ColumnWidth,...
    'ColumnEditable',ColumnEditable,...
    'Visible', 'off');

%%% cFRAP options table
TableData = {   'Diffusion coeff. D [m^2/s]', 1;...
                'Bleaching parameter K0', 0.3;...
                'Mobile fraction k', 1;...
                'Average squared resolution r2av [m^2]', 3;...
                'No. of discretization of D-space', 30;...
                'D-space range [Dmin Dmax]', '[0.05 50]';...
                'Number of rings', 1;...
                'slope', 1;...
                'intercept', 0};
ColumnNames = {'Parameter', 'Value'};
ColumnWidth = {250,70};
ColumnEditable = [false,true];
RowNames = [];

h.Mia_Image.Calculations.cFRAP_Options = uitable(...
    'Parent', h.Mia_Image.Calculations.FRAP_Panel,...
    'Units','normalized',...
    'FontSize',10,...
    'Position',[0.01 0.01 0.98 0.29],...
    'Data',TableData,...
    'ColumnName',ColumnNames,...
    'RowName',RowNames,...
    'ColumnWidth',ColumnWidth,...
    'ColumnEditable',ColumnEditable,...
    'Visible', 'off');

%% FLIM Calculations tab
%%% Tab and panel for performing FLIM
h.Mia_Image.Calculations.FLIM_Tab= uitab(...
    'Parent',h.Mia_Image.Calculations_Tabs,...
    'Tag','MI_Image_Tab',...
    'Title','FLIM');
h.Mia_Image.Calculations.FLIM_Panel = uibuttongroup(...
    'Parent',h.Mia_Image.Calculations.FLIM_Tab,...
    'Tag','Mia_Calculations_FRAP_Panel',...
    'Units','normalized',...
    'BackgroundColor', Look.Back,...
    'ForegroundColor', Look.Fore,...
    'HighlightColor', Look.Control,...
    'ShadowColor', Look.Shadow,...
    'Position',[0 0 1 1]);
%%% Button to start FLIM fitting
h.Mia_Image.Calculations.FLIM_Do = uicontrol(...
    'Parent', h.Mia_Image.Calculations.FLIM_Panel,...
    'Style','push',...
    'Units','normalized',...
    'FontSize',12,...
    'BackgroundColor', Look.Control,...
    'ForegroundColor', Look.Fore,...
    'Position',[0.02 0.91, 0.47 0.07],...
    'Callback',{@Do_FLIM, 1:3, 1:2},...
    'String','Start FLIM Fitting');
%%% Button to export FLIM fit results
% h.Mia_Image.Calculations.FLIM_Export = uicontrol(...
%     'Parent', h.Mia_Image.Calculations.FLIM_Panel,...
%     'Style','push',...
%     'Units','normalized',...
%     'FontSize',12,...
%     'BackgroundColor', Look.Control,...
%     'ForegroundColor', Look.Fore,...
%     'Position',[0.51 0.91, 0.47 0.07],...
%     'Callback',@Export_FLIM,...
%     'String','Export');
%%% TACRange
h.Text{end+1} = uicontrol(...
    'Parent', h.Mia_Image.Calculations.FLIM_Panel,...
    'Tag', 'TACRange',...
    'Style','text',...
    'Units','normalized',...
    'FontSize',12,...
    'BackgroundColor', Look.Back,...
    'ForegroundColor', Look.Fore,...
    'Position',[0.02 0.70, 0.5 0.06],...
    'String','TACRange [ns]:');
h.Mia_Image.Calculations.TACRange = uicontrol(...
    'Parent', h.Mia_Image.Calculations.FLIM_Panel,...
    'Tag', 'TACRange',...
    'Style','edit',...
    'Units','normalized',...
    'FontSize',12,...
    'BackgroundColor', Look.Control,...
    'ForegroundColor', Look.Fore,...
    'Position',[0.52 0.70, 0.2 0.06],...
    'String','12.5');
%%% MI_Bins
h.Text{end+1} = uicontrol(...
    'Parent', h.Mia_Image.Calculations.FLIM_Panel,...
    'Tag', 'MI_Bins',...
    'Style','text',...
    'Units','normalized',...
    'FontSize',12,...
    'BackgroundColor', Look.Back,...
    'ForegroundColor', Look.Fore,...
    'Position',[0.02 0.63, 0.5 0.06],...
    'String','# Microtime Bins');
h.Mia_Image.Calculations.MI_Bins = uicontrol(...
    'Parent', h.Mia_Image.Calculations.FLIM_Panel,...
    'Tag', 'MI_Bins',...
    'Style','edit',...
    'Units','normalized',...
    'FontSize',12,...
    'BackgroundColor', Look.Control,...
    'ForegroundColor', Look.Fore,...
    'Position',[0.52 0.63, 0.2 0.06],...
    'String','4096');
h.Text{end+1} = uicontrol(...
    'Parent', h.Mia_Image.Calculations.FLIM_Panel,...
    'Tag', 'MI_Bins',...
    'Style','text',...
    'Units','normalized',...
    'FontSize',12,...
    'BackgroundColor', Look.Back,...
    'ForegroundColor', Look.Fore,...
    'Position',[0.02 0.56, 0.5 0.06],...
    'String','MI Rebinning Factor');
h.Mia_Image.Calculations.MI_Rebinning_Factor = uicontrol(...
    'Parent', h.Mia_Image.Calculations.FLIM_Panel,...
    'Tag', 'MI_Bins',...
    'Style','edit',...
    'Units','normalized',...
    'FontSize',12,...
    'BackgroundColor', Look.Control,...
    'ForegroundColor', Look.Fore,...
    'Position',[0.52 0.56, 0.2 0.06],...
    'String','1');
%%% IRF source
h.Text{end+1} = uicontrol(...
    'Parent', h.Mia_Image.Calculations.FLIM_Panel,...
    'Tag', 'IRF_Source',...
    'Style','text',...
    'Units','normalized',...
    'FontSize',12,...
    'BackgroundColor', Look.Back,...
    'ForegroundColor', Look.Fore,...
    'Position',[0.02 0.46, 0.5 0.06],...
    'String','IRF source (left ch):');
h.Text{end+1} = uicontrol(...
    'Parent', h.Mia_Image.Calculations.FLIM_Panel,...
    'Tag', 'IRF_Source',...
    'Style','text',...
    'Units','normalized',...
    'FontSize',12,...
    'BackgroundColor', Look.Back,...
    'ForegroundColor', Look.Fore,...
    'Position',[0.52 0.46, 0.5 0.06],...
    'String','IRF source (right ch):');
irflist = [UserValues.PIE.Name,{'Load from file'}]';
for i = 1:(numel(irflist)-1)
    irflist{i} = ['PIE channel: ' irflist{i}];
end
h.Mia_Image.Calculations.IRF_Source(1) = uicontrol(...
    'Parent',h.Mia_Image.Calculations.FLIM_Panel,...
    'Style','popupmenu',...
    'Units','normalized',...
    'FontSize',12,...
    'BackgroundColor', Look.Control,...
    'ForegroundColor', Look.Fore,...
    'Value',1,...
    'Position',[0.02 0.40, 0.5 0.06],...
    'String',irflist,...
    'Callback',{@Update_IRF, 1});
h.Mia_Image.Calculations.IRF_Source(2) = uicontrol(...
    'Parent',h.Mia_Image.Calculations.FLIM_Panel,...
    'Style','popupmenu',...
    'Units','normalized',...
    'FontSize',12,...
    'BackgroundColor', Look.Control,...
    'ForegroundColor', Look.Fore,...
    'Value',2,...
    'Position',[0.52 0.40, 0.5 0.06],...
    'String',irflist,...
    'Callback',{@Update_IRF, 2});

%% Calculate tab
%%% Tab and panel for performing calculations
h.Mia_Image.Calculations.Calc_Tab= uitab(...
    'Parent',h.Mia_Image.Calculations_Tabs,...
    'Tag','MI_Image_Tab',...
    'Title','Calculate');
h.Mia_Image.Calculations.Calc_Panel = uibuttongroup(...
    'Parent',h.Mia_Image.Calculations.Calc_Tab,...
    'Tag','Mia_Calculations_Calc_Panel',...
    'Units','normalized',...
    'BackgroundColor', Look.Back,...
    'ForegroundColor', Look.Fore,...
    'HighlightColor', Look.Control,...
    'ShadowColor', Look.Shadow,...
    'Position',[0 0 1 1]);
%%% Button to start calculation
h.Mia_Image.Calculations.Calc_Do = uicontrol(...
    'Parent', h.Mia_Image.Calculations.Calc_Panel,...
    'Style','push',...
    'Units','normalized',...
    'FontSize',12,...
    'BackgroundColor', Look.Control,...
    'ForegroundColor', Look.Fore,...
    'Position',[0.02 0.91, 0.47 0.07],...
    'Callback',{@MIA_Various,13},...
    'String','Calculate',...
    'Visible','on');
%%% Text for bleach position
h.Text{end+1} = uicontrol(...
    'Parent', h.Mia_Image.Calculations.Calc_Panel,...
    'Style','text',...
    'Units','normalized',...
    'FontSize',12,...
    'BackgroundColor', Look.Back,...
    'ForegroundColor', Look.Fore,...
    'Position',[0.02 0.70, 0.5 0.06],...
    'String','Divide frame (range):');
%%% Editbox for bleaching ROI position
h.Mia_Image.Calculations.Divide = uicontrol(...
    'Parent', h.Mia_Image.Calculations.Calc_Panel,...
    'Style','edit',...
    'Units','normalized',...
    'FontSize',12,...
    'BackgroundColor', Look.Control,...
    'ForegroundColor', Look.Fore,...
    'Position',[0.52 0.70, 0.2 0.06],...
    'String','1',...
    'Tooltipstring',['Specify "1" for a frame or "1:10" for' 10 'a frame range. Do not write the " "']);
%%% Text for bleach position
h.Text{end+1} = uicontrol(...
    'Parent', h.Mia_Image.Calculations.Calc_Panel,...
    'Style','text',...
    'Units','normalized',...
    'FontSize',12,...
    'BackgroundColor', Look.Back,...
    'ForegroundColor', Look.Fore,...
    'Position',[0.02 0.55, 0.5 0.06],...
    'String','By frame (range):');
%%% Editbox for bleaching ROI position
h.Mia_Image.Calculations.DivideBy = uicontrol(...
    'Parent', h.Mia_Image.Calculations.Calc_Panel,...
    'Style','edit',...
    'Units','normalized',...
    'FontSize',12,...
    'BackgroundColor', Look.Control,...
    'ForegroundColor', Look.Fore,...
    'Position',[0.52 0.55, 0.2 0.06],...
    'String','1',...
    'Tooltipstring',['Specify "1" for a frame or "1:10" for' 10 'a frame range. Do not write the " "']);
%%% Text for bleach position
h.Text{end+1} = uicontrol(...
    'Parent', h.Mia_Image.Calculations.Calc_Panel,...
    'Style','text',...
    'Units','normalized',...
    'FontSize',12,...
    'BackgroundColor', Look.Back,...
    'ForegroundColor', Look.Fore,...
    'Position',[0.02 0.45, 0.5 0.06],...
    'String','Remove ratio >');
%%% Editbox for bleaching ROI position
h.Mia_Image.Calculations.RatioThresholdHigh = uicontrol(...
    'Parent', h.Mia_Image.Calculations.Calc_Panel,...
    'Style','edit',...
    'Units','normalized',...
    'FontSize',12,...
    'BackgroundColor', Look.Control,...
    'ForegroundColor', Look.Fore,...
    'Position',[0.52 0.45, 0.2 0.06],...
    'String','2');
%%% Text for bleach position
h.Text{end+1} = uicontrol(...
    'Parent', h.Mia_Image.Calculations.Calc_Panel,...
    'Style','text',...
    'Units','normalized',...
    'FontSize',12,...
    'BackgroundColor', Look.Back,...
    'ForegroundColor', Look.Fore,...
    'Position',[0.02 0.35, 0.5 0.06],...
    'String','Remove ratio <');
%%% Editbox for bleaching ROI position
h.Mia_Image.Calculations.RatioThresholdLow = uicontrol(...
    'Parent', h.Mia_Image.Calculations.Calc_Panel,...
    'Style','edit',...
    'Units','normalized',...
    'FontSize',12,...
    'BackgroundColor', Look.Control,...
    'ForegroundColor', Look.Fore,...
    'Position',[0.52 0.35, 0.2 0.06],...
    'String','0');
%%% Checkbox to switch between automatic\manual filenames
h.Mia_Image.Calculations.MedFilt = uicontrol(...
    'Parent', h.Mia_Image.Calculations.Calc_Panel,...
    'Style','checkbox',...
    'Units','normalized',...
    'FontSize',12,...
    'Value',1,...
    'BackgroundColor', Look.Back,...
    'ForegroundColor', Look.Fore,...
    'Position',[0.02 0.25, 0.5 0.06],...
    'String','Median filter',...
    'Tooltipstring',['Applies a 3x3 median filter' 10 'to the ratio image and to the mask']);

% %%% Tex)t
% h.Text{end+1} = uicontrol(...
%     'Parent', h.Mia_Image.Calculations.Calc_Panel,...
%     'Style','text',...
%     'Units','normalized',...
%     'FontSize',12,...
%     'BackgroundColor', Look.Back,...
%     'ForegroundColor', Look.Fore,...
%     'Position',[0.02 0.62, 0.5 0.06],...
%     'String','Threshold [min/max]');
% %%% Editbox for minimal ratio
% h.Mia_Image.Calculations.Calc_Min = uicontrol(...
%     'Parent', h.Mia_Image.Calculations.FRAP_Panel,...
%     'Tag', 'FRAP_Pos_Circle',...
%     'Style','edit',...
%     'Units','normalized',...
%     'FontSize',12,...
%     'BackgroundColor', Look.Control,...
%     'ForegroundColor', Look.Fore,...
%     'Position',[0.52 0.62, 0.2 0.06],...
%     'Callback',{@MIA_Various,13},...
%     'String','0',...
%     'Tooltipstring','minimum value of ratio');
% %%% Editbox for maximal ratio
% h.Mia_Image.Calculations.Calc_Max = uicontrol(...
%     'Parent', h.Mia_Image.Calculations.FRAP_Panel,...
%     'Tag', 'FRAP_Pos_Circle',...
%     'Style','edit',...
%     'Units','normalized',...
%     'FontSize',12,...
%     'BackgroundColor', Look.Control,...
%     'ForegroundColor', Look.Fore,...
%     'Position',[0.74 0.62, 0.2 0.06],...
%     'Callback',{@MIA_Various,13},...
%     'String','100',...
%     'Tooltipstring','maximum value of ratio');

%% (R)ICS Tab
s.ProgressRatio = 0.35;
h.Mia_ICS.Tab = uitab(...
    'Parent',h.Mia_Main_Tabs,...
    'Title','(R)ICS',...
    'Tag','Mia_Cor_Tabs',...
    'Units','normalized');
h.Mia_ICS.Panel = uibuttongroup(...
    'Parent',h.Mia_ICS.Tab,...
    'Tag','Mia_Cor_Panel',...
    'Units','normalized',...
    'BackgroundColor', Look.Back,...
    'ForegroundColor', Look.Fore,...
    'HighlightColor', Look.Control,...
    'ShadowColor', Look.Shadow,...
    'Position',[0 0 1 1]);

%%% Text
h.Text{end+1} = uicontrol(...
    'Parent',h.Mia_ICS.Panel,...
    'Style','text',...
    'Units','normalized',...
    'FontSize',14,...
    'HorizontalAlignment','left',...
    'BackgroundColor', Look.Back,...
    'ForegroundColor', Look.Fore,...
    'Position',[0.01 0.95, 0.045 0.03],...
    'String','Size:');
%%% Editbox for correlation size
h.Mia_ICS.Size = uicontrol(...
    'Parent',h.Mia_ICS.Panel,...
    'Style','edit',...
    'Units','normalized',...
    'FontSize',12,...
    'BackgroundColor', Look.Control,...
    'ForegroundColor', Look.Fore,...
    'Callback',{@Update_Plots,2},...
    'Position',[0.06 0.95, 0.03 0.03],...
    'String','31');
%%% Colormap selection for correlations
h.Mia_ICS.Cor_Colormap = uicontrol(...
    'Parent',h.Mia_ICS.Panel,...
    'Style','popupmenu',...
    'Units','normalized',...
    'FontSize',12,...
    'UserData',[1 0 0],...
    'Callback',{@Update_Plots,2},...
    'ButtonDownFcn',@Mia_Color,...
    'BackgroundColor', Look.Control,...
    'ForegroundColor', Look.Fore,...
    'Value',2,...
    'Position',[0.1 0.95, 0.08 0.03],...
    'String',{'Gray','Jet','Hot','HSV','Custom'});
if ismac
    h.Mia_ICS.Cor_Colormap.ForegroundColor = [0 0 0];
    h.Mia_ICS.Cor_Colormap.BackgroundColor = [1 1 1];
end
%%% Text
h.Text{end+1} = uicontrol(...
    'Parent',h.Mia_ICS.Panel,...
    'Style','text',...
    'Units','normalized',...
    'FontSize',14,...
    'HorizontalAlignment','left',...
    'BackgroundColor', Look.Back,...
    'ForegroundColor', Look.Fore,...
    'Position',[0.01 0.91, 0.045 0.03],...
    'String','Frame:');
%%% Editbox for frame
h.Mia_ICS.Frame = uicontrol(...
    'Parent',h.Mia_ICS.Panel,...
    'Style','edit',...
    'Units','normalized',...
    'FontSize',12,...
    'BackgroundColor', Look.Control,...
    'ForegroundColor', Look.Fore,...
    'Callback',{@Mia_Frame,3,1:3},...
    'Position',[0.06 0.91, 0.03 0.03],...
    'String','0');
%%% Slider for frame
h.Mia_ICS.Frame_Slider = uicontrol(...
    'Parent',h.Mia_ICS.Panel,...
    'Style','slider',...
    'Units','normalized',...
    'FontSize',12,...
    'BackgroundColor', Look.Control,...
    'ForegroundColor', Look.Fore,...
    'UserData',[4,i],...
    'Position',[0.1 0.91, 0.08 0.03]);
h.Mia_ICS.Frame_Listener=addlistener(h.Mia_ICS.Frame_Slider,'Value','PostSet',@Mia_Frame);
%%% Text
h.Text{end+1} = uicontrol(...
    'Parent',h.Mia_ICS.Panel,...
    'Style','text',...
    'Units','normalized',...
    'FontSize',14,...
    'HorizontalAlignment','left',...
    'BackgroundColor', Look.Back,...
    'ForegroundColor', Look.Fore,...
    'Position',[0.01 0.87, 0.1 0.03],...
    'String','Frames to use:');
%%% Editbox for frames to use
h.Mia_ICS.Frames2Use = uicontrol(...
    'Parent',h.Mia_ICS.Panel,...
    'Style','edit',...
    'Units','normalized',...
    'FontSize',12,...
    'BackgroundColor', Look.Control,...
    'ForegroundColor', Look.Fore,...
    'Callback',{@Update_Plots,2},...
    'Position',[0.12 0.87, 0.03 0.03],...
    'String','0');

%%% RICS Fit table
h.Mia_ICS.Fit_Table = uitable(...
    'Parent',h.Mia_ICS.Panel,...
    'Units','normalized',...
    'FontSize',8,...
    'BackgroundColor', [Look.Table1;Look.Table2],...
    'ForegroundColor', Look.TableFore,...
    'ColumnName',{'ACF1','CCF','ACF2'},...
    'ColumnWidth',num2cell([40,40,40]),...
    'ColumnEditable',true,...
    'RowName',{'N';'Fix';'D [um2/s]';'Fix';'w_r [um]';'Fix';'w_z [um]';'Fix';'y0';'Fix';'P Size [nm]';'Fix';'P Time [us]';'Fix';'L Time [ms]';'Fix'},...
    'CellEditCallback',{@Update_Plots,2},...
    'Position',[0.01 0.46, 0.2 0.4]);
Data=cell(16,3);
Data(1,:)={'1'};
Data(3,:)={'10'};

% wr/wz
for i = [1, 3]
    Data(5,i)={num2str(UserValues.MIA.FitParams.wr(ceil(i/1.5)))};
    Data(7,i)={num2str(UserValues.MIA.FitParams.wz(ceil(i/1.5)))};
end
%recalculate w_r/w_z parameters for the CCF 
Data(5,2)={num2str(sqrt(0.5.*(str2double(Data(5,1))^2+str2double(Data(5,3))^2)))};
Data(7,2)={num2str(sqrt(0.5.*(str2double(Data(7,1))^2+str2double(Data(7,3))^2)))};


Data(9,:)={'0'};
Data(11,:)={'40'};
Data(13,:)={num2str(UserValues.MIA.FitParams.PixTime)};
Data(15,:)={'3.33'};
Data([2 4 6 10],:)={false};
Data([6 8 12 14 16],:)={true};
h.Mia_ICS.Fit_Table.Data=Data;

%%% Buttons to fit correlation
h.Mia_ICS.Fit(1) = uicontrol(...
    'Parent',h.Mia_ICS.Panel,...
    'Style','push',...
    'Units','normalized',...
    'FontSize',14,...
    'BackgroundColor', Look.Control,...
    'ForegroundColor', Look.Fore,...
    'String','Fit ACF1',...
    'Callback',{@Do_RICS_Fit,1},...
    'Position',[0.01 0.42, 0.06 0.03]);
h.Mia_ICS.Fit(2) = uicontrol(...
    'Parent',h.Mia_ICS.Panel,...
    'Style','push',...
    'Units','normalized',...
    'FontSize',14,...
    'BackgroundColor', Look.Control,...
    'ForegroundColor', Look.Fore,...
    'String','Fit CCF',...
    'Callback',{@Do_RICS_Fit,2},...
    'Position',[0.08 0.42, 0.06 0.03]);
h.Mia_ICS.Fit(3) = uicontrol(...
    'Parent',h.Mia_ICS.Panel,...
    'Style','push',...
    'Units','normalized',...
    'FontSize',14,...
    'BackgroundColor', Look.Control,...
    'ForegroundColor', Look.Fore,...
    'String','Fit ACF2',...
    'Callback',{@Do_RICS_Fit,3},...
    'Position',[0.15 0.42, 0.06 0.03]);
%%% Pupupmenu, to select, what fit to plot
h.Mia_ICS.Fit_Type = uicontrol(...
    'Parent',h.Mia_ICS.Panel,...
    'Style','popupmenu',...
    'Units','normalized',...
    'FontSize',12,...
    'BackgroundColor', Look.Control,...
    'ForegroundColor', Look.Fore,...
    'String',{'Fit surf','Residuals surf','Fit/Residuals','On Axes plot'},...
    'Callback',{@Update_Plots,2},...
    'Position',[0.01 0.38, 0.1 0.03]);
if ismac
    h.Mia_ICS.Fit_Type.ForegroundColor = [0 0 0];
    h.Mia_ICS.Fit_Type.BackgroundColor = [1 1 1];
end
for i=1:3
    %%% Axes to display correlation images
    h.Mia_ICS.Axes(i,1)= axes(...
        'Parent',h.Mia_ICS.Panel,...
        'Units','normalized',...
        'Position',[0.02+0.25*i 0.67 0.22 0.32]);
    %%% Axes to display correlation surface
    h.Mia_ICS.Axes(i,2)= axes(...
        'Parent',h.Mia_ICS.Panel,...
        'Units','normalized',...
        'Position',[0.02+0.25*i 0.34 0.22 0.32]);
    %%% Axes to display correlation 0 lines
    h.Mia_ICS.Axes(i,4)= axes(...
        'Parent',h.Mia_ICS.Panel,...
        'Units','normalized',...
        'Position',[0.02+0.25*i 0.05 0.22 0.28],...
        'NextPlot','add');
    %%% Axes to display correlation fit surface
    h.Mia_ICS.Axes(i,3)= axes(...
        'Parent',h.Mia_ICS.Panel,...
        'Units','normalized',...
        'Position',[0.02+0.25*i 0.01 0.22 0.32]);
    linkaxes(h.Mia_ICS.Axes(i,1:3), 'xy');
    
    
    %%% Initializes empty plots
    h.Plots.Cor(i,1)=image(zeros(1,1,3),...
        'Parent',h.Mia_ICS.Axes(i,1));
    h.Mia_ICS.Axes(i,1).Color=[0 0 0];
    h.Mia_ICS.Axes(i,1).Visible='off';
    h.Plots.Cor(i,1).Visible='off';
    h.Mia_ICS.Axes(i,1).DataAspectRatio=[1 1 1];
    h.Mia_ICS.Axes(i,1).XTick=[];
    h.Mia_ICS.Axes(i,1).YTick=[];
    h.Plots.Cor(i,2)=surf(zeros(2),zeros(2,2,3),...
        'Parent',h.Mia_ICS.Axes(i,2));
    h.Mia_ICS.Axes(i,2).Visible='off';
    h.Plots.Cor(i,2).Visible='off';
    h.Mia_ICS.Axes(i,2).Color=(Look.Back+0.1)/1.1;
    h.Mia_ICS.Axes(i,2).XColor = Look.Fore;
    h.Mia_ICS.Axes(i,2).YColor = Look.Fore;
    h.Mia_ICS.Axes(i,2).ZColor = Look.Fore;
    h.Mia_ICS.Axes(i,2).XTick=[];
    h.Mia_ICS.Axes(i,2).YTick=[];
    h.Plots.Cor(i,3)=surf(zeros(2),zeros(2,2,3),...
        'Parent',h.Mia_ICS.Axes(i,3));
    h.Mia_ICS.Axes(i,3).Visible='off';
    h.Plots.Cor(i,3).Visible='off';
    h.Mia_ICS.Axes(i,3).XColor = Look.Fore;
    h.Mia_ICS.Axes(i,3).YColor = Look.Fore;
    h.Mia_ICS.Axes(i,3).ZColor = Look.Fore;
    h.Mia_ICS.Axes(i,3).Color=(Look.Back+0.1)/1.1;
    h.Mia_ICS.Axes(i,3).XTick=[];
    h.Mia_ICS.Axes(i,3).YTick=[];
    h.Plots.Cor(i,4)=plot(0,0,...
        'Parent',h.Mia_ICS.Axes(i,4),...
        'Color','b',...
        'LineStyle','none',...
        'Marker','o');
    h.Plots.Cor(i,5)=plot(0,0,...
        'Parent',h.Mia_ICS.Axes(i,4),...
        'Color','b');
    h.Plots.Cor(i,6)=plot(0,0,...
        'Parent',h.Mia_ICS.Axes(i,4),...
        'Color','r',...
        'LineStyle','none',...
        'Marker','o');
    h.Plots.Cor(i,7)=plot(0,0,...
        'Parent',h.Mia_ICS.Axes(i,4),...
        'Color','r');
    h.Mia_ICS.Axes(i,4).Visible='off';
    h.Plots.Cor(i,4).Visible='off';
    h.Plots.Cor(i,5).Visible='off';
    h.Plots.Cor(i,6).Visible='off';
    h.Plots.Cor(i,7).Visible='off';
    h.Mia_ICS.Axes(i,4).XColor = Look.Fore;
    h.Mia_ICS.Axes(i,4).YColor = Look.Fore;
end


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% TICS Tab
s.ProgressRatio = 0.65;
h.Mia_TICS.Tab = uitab(...
    'Parent',h.Mia_Main_Tabs,...
    'Title','TICS',...
    'Units','normalized');
h.Mia_TICS.Panel = uibuttongroup(...
    'Parent',h.Mia_TICS.Tab,...
    'Units','normalized',...
    'BackgroundColor', Look.Back,...
    'ForegroundColor', Look.Fore,...
    'HighlightColor', Look.Control,...
    'ShadowColor', Look.Shadow,...
    'Position',[0 0 1 1]);

%%% TICS Fit table
h.Mia_TICS.Fit_Table = uitable(...
    'Parent',h.Mia_TICS.Panel,...
    'Units','normalized',...
    'FontSize',8,...
    'BackgroundColor', [Look.Table1;Look.Table2],...
    'ForegroundColor', Look.TableFore,...
    'ColumnName',{'ACF1','CCF','ACF2'},...
    'ColumnWidth',num2cell([40,40,40]),...
    'ColumnEditable',true,...
    'RowName',{'N';'Fix';'D [um2/s]';'Fix';'w_r [um]';'Fix';'w_z [um]';'Fix';'y0';'Fix';},...
    'CellEditCallback',{@Calc_TICS_Fit,1:3},...
    'Position',[0.01 0.71, 0.18 0.27]);
Data=cell(10,3);
Data(1,:)={'1'};
Data(3,:)={'0.01'};
Data(5,:)={'0.2'};
Data(7,:)={'1'};
Data(9,:)={'0'};
Data([2 4 10],:)={false};
Data([6 8],:)={true};
h.Mia_TICS.Fit_Table.Data=Data;

%%% Buttons to fit correlation
h.Mia_TICS.Fit = uicontrol(...
    'Parent',h.Mia_TICS.Panel,...
    'Style','push',...
    'Tag','Fit',...
    'Units','normalized',...
    'FontSize',14,...
    'BackgroundColor', Look.Control,...
    'ForegroundColor', Look.Fore,...
    'String','Fit',...
    'Callback',@Do_TICS_Fit,...
    'Position',[0.01 0.67, 0.04 0.03]);
%%% Button to Save the correlation data to .mcor
h.Mia_TICS.Save = uicontrol(...
    'Parent',h.Mia_TICS.Panel,...
    'Style','push',...
    'Units','normalized',...
    'Tag','Save',...
    'FontSize',14,...
    'BackgroundColor', Look.Control,...
    'ForegroundColor', Look.Fore,...
    'String','Save',...
    'Callback',{@Update_Plots,5},...
    'Position',[0.01 0.63, 0.04 0.03],...
    'ToolTipString', 'Save currently displayed data as .mcor');
%%% Button to Reset thresholds
h.Mia_TICS.Reset = uicontrol(...
    'Parent',h.Mia_TICS.Panel,...
    'Style','push',...
    'Tag','Reset',...
    'Units','normalized',...
    'FontSize',14,...
    'BackgroundColor', Look.Control,...
    'ForegroundColor', Look.Fore,...
    'String','Reset',...
    'Callback',{@Update_Plots,5},...
    'Position',[0.01 0.59, 0.04 0.03],...
    'ToolTipString', 'Reset thresholds to their initial values');

h.Text{end+1} = uicontrol(...
        'Parent',h.Mia_TICS.Panel,...
        'Style','text',...
        'Units','normalized',...
        'FontSize',12,...
        'BackgroundColor', Look.Back,...
        'ForegroundColor', Look.Fore,...
        'String', 'Image to display:',...
        'HorizontalAlignment', 'Left',...
        'Position',[0.01 0.555 0.08 0.03]);
%%% Popup to select what to image
h.Mia_TICS.SelectImage = uicontrol(...
    'Parent',h.Mia_TICS.Panel,...
    'Tag','SelectImage',...
    'Style','popup',...
    'Units','normalized',...
    'FontSize',12,...
    'BackgroundColor', Look.Control,...
    'ForegroundColor', Look.Fore,...
    'String',{'G(1)-G(end)','Brightness','Counts','Half-Life', 'Samples'},...
    'Callback',{@Update_Plots,5},...
    'Position',[0.01 0.53, 0.07 0.03]);
if ismac
    h.Mia_TICS.SelectImage.ForegroundColor = [0 0 0];
    h.Mia_TICS.SelectImage.BackgroundColor = [1 1 1];
end

h.Mia_TICS.Median = uicontrol(...
    'Parent', h.Mia_TICS.Panel,...
    'Style','checkbox',...
    'Units','normalized',...
    'FontSize',12,...
    'BackgroundColor', Look.Back,...
    'ForegroundColor', Look.Fore,...
    'Position',[0.01 0.50, 0.08 0.03],...
    'String','Median filter',...
    'Callback',{@Update_Plots,5},...
    'Value', 0,...
    'Tag','Median',...
    'Tooltipstring',['Median filter the G1, brightness, counts,' 10 'halflife and sample images and the TICS mask.' 10 'Size is defined on the Correlate tab "Size:".']);

h.Mia_TICS.Normalize = uicontrol(...
    'Parent', h.Mia_TICS.Panel,...
    'Style','checkbox',...
    'Units','normalized',...
    'FontSize',12,...
    'BackgroundColor', Look.Back,...
    'ForegroundColor', Look.Fore,...
    'Position',[0.07 0.50, 0.08 0.03],...
    'String','Normalize',...
    'Callback',{@Update_Plots,5},...
    'Value', 0,...
    'Tag','Normalize 1D TICS',...
    'Tooltipstring','Normalize the average TICS correlation (CF-offset)/(CF(0)-offset)');

%%% Editboxes for different thresholds for species selection
    h.Mia_TICS.ThresholdsContainer = uigridcontainer(...
        'v0',...
        'GridSize',[6,3],...
        'HorizontalWeight',[0.3,0.2,0.2],... 
        'Parent',h.Mia_TICS.Panel,...
        'Units','norm',...
        'Position',[.08,0.54,0.13,0.16],...
        'BackgroundColor',Look.Back);
    h.Mia_TICS.Threshold_Text = uicontrol('style','text',...
        'Parent',h.Mia_TICS.ThresholdsContainer,...
        'String','',...
        'Units','normalized',...
        'FontSize',12,...
        'BackgroundColor', Look.Back,...
        'ForegroundColor', Look.Fore);
     h.Mia_TICS.Threshold_MinText = uicontrol('style','text',...
        'Parent',h.Mia_TICS.ThresholdsContainer,...
        'String','Min',...
        'Units','normalized',...
        'FontSize',12,...
        'BackgroundColor', Look.Back,...
        'ForegroundColor', Look.Fore);
    h.Mia_TICS.Threshold_MaxText = uicontrol('style','text',...
        'Parent',h.Mia_TICS.ThresholdsContainer,...
        'String','Max',...
        'Units','normalized',...
        'FontSize',12,...
        'BackgroundColor', Look.Back,...
        'ForegroundColor', Look.Fore);
    h.Mia_TICS.Threshold_G1_Text = uicontrol('style','text',...
        'Parent',h.Mia_TICS.ThresholdsContainer,...
        'String','G(1)-G(end)',...
        'Units','normalized',...
        'FontSize',12,...
        'BackgroundColor', Look.Back,...
        'ForegroundColor', Look.Fore,...
        'Tooltipstring', 'G1-Gend is the decaying correlation amplitude');
    h.Mia_TICS.Threshold_G1_Min_Edit = uicontrol('style','edit',...
        'Parent',h.Mia_TICS.ThresholdsContainer,...
        'Tag', 'thresholds',...
        'String',0,...
        'BackgroundColor', Look.Control,...
        'ForegroundColor', Look.Fore,...
        'Units','normalized',...
        'FontSize',12,...
        'Callback',{@Update_Plots,5},...
    'Tooltipstring', 'G1-Gend is the decaying correlation amplitude');
    h.Mia_TICS.Threshold_G1_Max_Edit = uicontrol('style','edit',...
        'Parent',h.Mia_TICS.ThresholdsContainer,...
        'Tag', 'thresholds',...
        'String',100,...
        'BackgroundColor', Look.Control,...
        'ForegroundColor', Look.Fore,...
        'Units','normalized',...
        'FontSize',12,...
        'Callback',{@Update_Plots,5},...
    'Tooltipstring', 'G1-Gend is the decaying correlation amplitude');
    h.Mia_TICS.Threshold_brightness_Text = uicontrol('style','text',...
        'Parent',h.Mia_TICS.ThresholdsContainer,...
        'String','Brightness',...
        'Units','normalized',...
        'FontSize',12,...
        'BackgroundColor', Look.Back,...
        'ForegroundColor', Look.Fore,...
    'Tooltipstring', 'Brightness is Counts*(G1-Gend)');
    h.Mia_TICS.Threshold_brightness_Min_Edit = uicontrol('style','edit',...
        'Parent',h.Mia_TICS.ThresholdsContainer,...
        'Tag', 'thresholds',...
        'String',0,...
        'BackgroundColor', Look.Control,...
        'ForegroundColor', Look.Fore,...
        'Units','normalized',...
        'FontSize',12,...
        'Callback',{@Update_Plots,5},...
    'Tooltipstring', 'Brightness is Counts*(G1-Gend)');
    h.Mia_TICS.Threshold_brightness_Max_Edit = uicontrol('style','edit',...
        'Parent',h.Mia_TICS.ThresholdsContainer,...
        'Tag', 'thresholds',...
        'String',100,...
        'BackgroundColor', Look.Control,...
        'ForegroundColor', Look.Fore,...
        'Units','normalized',...
        'FontSize',12,...
        'Callback',{@Update_Plots,5},...
    'Tooltipstring', 'Brightness is Counts*(G1-Gend)');
    h.Mia_TICS.Threshold_counts_Text = uicontrol('style','text',...
        'Parent',h.Mia_TICS.ThresholdsContainer,...
        'String','Counts',...
        'Units','normalized',...
        'FontSize',12,...
        'BackgroundColor', Look.Back,...
        'ForegroundColor', Look.Fore,...
    'Tooltipstring', 'Counts is the avg. intensity of the kept pixels');
    h.Mia_TICS.Threshold_counts_Min_Edit = uicontrol('style','edit',...
        'Parent',h.Mia_TICS.ThresholdsContainer,...
        'Tag', 'thresholds',...
        'String',0,...
        'BackgroundColor', Look.Control,...
        'ForegroundColor', Look.Fore,...
        'Units','normalized',...
        'FontSize',12,...
        'Callback',{@Update_Plots,5},...
    'Tooltipstring', 'Counts is the avg. intensity of the kept pixels');
    h.Mia_TICS.Threshold_counts_Max_Edit = uicontrol('style','edit',...
        'Parent',h.Mia_TICS.ThresholdsContainer,...
        'Tag', 'thresholds',...
        'String',100,...
        'BackgroundColor', Look.Control,...
        'ForegroundColor', Look.Fore,...
        'Units','normalized',...
        'FontSize',12,...
        'Callback',{@Update_Plots,5},...
    'Tooltipstring', 'Counts is the avg. intensity of the kept pixels');
    h.Mia_TICS.Threshold_halflife_Text = uicontrol('style','text',...
        'Parent',h.Mia_TICS.ThresholdsContainer,...
        'String','Half-life',...
        'Units','normalized',...
        'FontSize',12,...
        'BackgroundColor', Look.Back,...
        'ForegroundColor', Look.Fore,...
    'Tooltipstring', 'Half-life is the lag time at half-amplitude');
    h.Mia_TICS.Threshold_halflife_Min_Edit = uicontrol('style','edit',...
        'Parent',h.Mia_TICS.ThresholdsContainer,...
        'Tag', 'thresholds',...
        'String',0,...
        'BackgroundColor', Look.Control,...
        'ForegroundColor', Look.Fore,...
        'Units','normalized',...
        'FontSize',12,...
        'Callback',{@Update_Plots,5},...
    'Tooltipstring', 'Half-life is the lag time at half-amplitude');
    h.Mia_TICS.Threshold_halflife_Max_Edit = uicontrol('style','edit',...
        'Parent',h.Mia_TICS.ThresholdsContainer,...
        'Tag', 'thresholds',...
        'String',100,...
        'BackgroundColor', Look.Control,...
        'ForegroundColor', Look.Fore,...
        'Units','normalized',...
        'FontSize',12,...
        'Callback',{@Update_Plots,5},...
    'Tooltipstring', 'Half-life is the lag time at half-amplitude');
    h.Mia_TICS.Threshold_samples_Text = uicontrol('style','text',...
        'Parent',h.Mia_TICS.ThresholdsContainer,...
        'String','Samples',...
        'Units','normalized',...
        'FontSize',12,...
        'BackgroundColor', Look.Back,...
        'ForegroundColor', Look.Fore,...
    'Tooltipstring', 'Samples is the number of samples per kept pixel');
    h.Mia_TICS.Threshold_samples_Min_Edit = uicontrol('style','edit',...
        'Parent',h.Mia_TICS.ThresholdsContainer,...
        'Tag', 'thresholds',...
        'String',50,...
        'BackgroundColor', Look.Control,...
        'ForegroundColor', Look.Fore,...
        'Units','normalized',...
        'FontSize',12,...
        'Callback',{@Update_Plots,5},...
    'Tooltipstring', 'Samples is the number of samples per kept pixel');
    h.Mia_TICS.Threshold_samples_Max_Edit = uicontrol('style','edit',...
        'Parent',h.Mia_TICS.ThresholdsContainer,...
        'Tag', 'thresholds',...
        'String',1000,...
        'BackgroundColor', Look.Control,...
        'ForegroundColor', Look.Fore,...
        'Units','normalized',...
        'FontSize',12,...
        'Callback',{@Update_Plots,5},...
    'Tooltipstring', 'Samples is the number of samples per kept pixel');
    %%% Popup to select what to which image the thresholds apply
h.Mia_TICS.SelectCor = uicontrol(...
    'Parent',h.Mia_TICS.Panel,...
    'Style','popup',...
    'Tag','SelectCor',...
    'Units','normalized',...
    'FontSize',12,...
    'BackgroundColor', Look.Control,...
    'ForegroundColor', Look.Fore,...
    'String',{'ACF1:','ACF2:','CCF:'},...
    'Callback',{@Update_Plots,5},...
    'Position',[0.08 0.67, 0.06 0.03]);
if ismac
    h.Mia_TICS.SelectCor.ForegroundColor = [0 0 0];
    h.Mia_TICS.SelectCor.BackgroundColor = [1 1 1];
end

%%% Axes to display correlation
h.Mia_TICS.Axes = axes(...
    'Parent',h.Mia_TICS.Panel,...
    'Units','normalized',...
    'NextPlot','Add',...
    'Position',[0.24 0.59 0.74 0.38]);

%%% UIContextMenus for TICS freehand selection
h.Mia_TICS.Menu = uicontextmenu;
h.Mia_TICS.Select_Manual_ROI = uimenu(...
    'Parent',h.Mia_TICS.Menu,...
    'Label','Select manual ROI',...
    'Callback',{@Mia_Freehand,4});
h.Mia_TICS.Unselect_Manual_ROI = uimenu(...
    'Parent',h.Mia_TICS.Menu,...
    'Label','Unselect manual ROI',...
    'Callback',{@Mia_Freehand,5});
h.Mia_TICS.Clear_Manual_ROI = uimenu(...
    'Parent',h.Mia_TICS.Menu,...
    'Label','Clear manual ROI',...
    'Callback',{@Mia_Freehand,6});
for i=1:3
    color = [0 1 0;1 0 0;1 1 0];
    h.Plots.TICS(i,1) = errorbar(...
        [0.1 1],...
        [0 0],...
        [0 0],...
        [0 0],...
        'Parent',h.Mia_TICS.Axes,...
        'LineStyle','none',...
        'Marker','.',...
        'MarkerSize',8,...
        'Color',color(i,:));
    h.Plots.TICS(i,2) = line(...
        'Parent',h.Mia_TICS.Axes,...
        'XData',[0.1 1],...
        'YData',[0 0],...
        'LineStyle','-',...
        'Marker','none',...
        'MarkerSize',8,...
        'Color',color(i,:));
    switch i
        case 1
            pos = 0;
        case 2
            pos = 0.66;
        case 3
            pos = 0.33;
    end
    h.Text{end+1} = uicontrol(...
        'Parent',h.Mia_TICS.Panel,...
        'Style','text',...
        'Units','normalized',...
        'FontSize',14,...
        'FontWeight','bold',...
        'BackgroundColor', Look.Back,...
        'ForegroundColor', Look.Fore,...
        'Position',[0.02+pos 0.48 0.3 0.03]);
    switch i
        case 1
            h.Text{end}.String = 'ACF1';
        case 2
            h.Text{end}.String = 'ACF2';
        case 3
            h.Text{end}.String = 'CCF';
    end
    
    %%% Axes to display correlation images
    h.Mia_TICS.Image(i,1) = axes(...
        'Parent',h.Mia_TICS.Panel,...
        'Units','normalized',...
        'Box','off',...
        'Nextplot','Add',...
        'DataAspectRatio',[1 1 1],...
        'PlotBoxAspectRatio', [1 1 1],...
        'UIContextMenu',h.Mia_TICS.Menu,...
        'Position',[0.02+pos 0.02 0.3 0.45]);
    
    h.Plots.TICSImage(i,1) = imagesc(...
        zeros(2),...
        'Parent',h.Mia_TICS.Image(i),...
        'Tag','imTICS',...
        'UIContextMenu',h.Mia_TICS.Menu,...
        'ButtonDownFcn',{@Mia_Export,1},...
        'Visible','off');
    h.Mia_TICS.Image(i,1).XTick = [];
    h.Mia_TICS.Image(i,1).YTick = [];
    h.Mia_TICS.Image(i,1).Visible = 'off';
    h.Mia_TICS.Image(i,1).Colormap = jet;
    h.Mia_TICS.Image(i,2) = colorbar(...
        'Peer',h.Mia_TICS.Image(i,1),...
        'YColor',Look.Fore,...
        'Visible','off');
    % switch index again to undo index switch
end
h.Mia_TICS.Axes.XColor = Look.Fore;
h.Mia_TICS.Axes.YColor = Look.Fore;
h.Mia_TICS.Axes.XLabel.String = 'Time Lag {\it\tau{}} [s]';
h.Mia_TICS.Axes.XLabel.Color = Look.Fore;
h.Mia_TICS.Axes.YLabel.String = 'G({\it\tau{}})';
h.Mia_TICS.Axes.YLabel.Color = Look.Fore;
%h.Mia_TICS.Axes.XScale = 'log';

%% iMSD/STICS Tab
s.ProgressRatio = 0.75;
h.Mia_STICS.Tab = uitab(...
    'Parent',h.Mia_Main_Tabs,...
    'Title','STICS/iMDS',...
    'Units','normalized');
h.Mia_STICS.Panel = uibuttongroup(...
    'Parent',h.Mia_STICS.Tab,...
    'Units','normalized',...
    'BackgroundColor', Look.Back,...
    'ForegroundColor', Look.Fore,...
    'HighlightColor', Look.Control,...
    'ShadowColor', Look.Shadow,...
    'Position',[0 0 1 1]);
%%% Text
h.Text{end+1} = uicontrol(...
    'Parent',h.Mia_STICS.Panel,...
    'Style','text',...
    'Units','normalized',...
    'FontSize',14,...
    'HorizontalAlignment','left',...
    'BackgroundColor', Look.Back,...
    'ForegroundColor', Look.Fore,...
    'Position',[0.01 0.95, 0.045 0.03],...
    'String','Size');
%%% Editbox for correlation size
h.Mia_STICS.Size = uicontrol(...
    'Parent',h.Mia_STICS.Panel,...
    'Style','edit',...
    'Units','normalized',...
    'FontSize',12,...
    'BackgroundColor', Look.Control,...
    'ForegroundColor', Look.Fore,...
    'Callback',{@Update_Plots,6},...
    'Position',[0.06 0.95, 0.03 0.03],...
    'String','31');
h.Mia_STICS.Do_Gaussian = uicontrol(...
    'Parent',h.Mia_STICS.Panel,...
    'Style','push',...
    'Units','normalized',...
    'FontSize',12,...
    'BackgroundColor', Look.Control,...
    'ForegroundColor', Look.Fore,...
    'Callback',{@Do_Gaussian},...
    'String','Calc iMSD',...
    'Position',[0.1 0.95, 0.08 0.03]);
h.Text{end+1} = uicontrol(...
    'Parent',h.Mia_STICS.Panel,...
    'Style','text',...
    'Units','normalized',...
    'FontSize',14,...
    'HorizontalAlignment','left',...
    'BackgroundColor', Look.Back,...
    'ForegroundColor', Look.Fore,...
    'Position',[0.01 0.91, 0.045 0.03],...
    'String','Lag:');
%%% Editbox for frame
h.Mia_STICS.Lag = uicontrol(...
    'Parent',h.Mia_STICS.Panel,...
    'Style','edit',...
    'Units','normalized',...
    'FontSize',12,...
    'BackgroundColor', Look.Control,...
    'ForegroundColor', Look.Fore,...
    'Callback',{@Mia_Frame,5,1:3},...
    'Position',[0.06 0.91, 0.03 0.03],...
    'String','0');
h.Mia_STICS.Lag_Slider = uicontrol(...
    'Parent',h.Mia_STICS.Panel,...
    'Style','slider',...
    'Units','normalized',...
    'FontSize',12,...
    'Min',0,...
    'Max',1,...
    'SliderStep',[1 1],...
    'BackgroundColor', Look.Control,...
    'ForegroundColor', Look.Fore,...
    'Callback',{@Mia_Frame,6,1:3},...
    'Position',[0.1 0.91, 0.08 0.03]);
%%% STICS Fit table
h.Mia_STICS.Fit_Table = uitable(...
    'Parent',h.Mia_STICS.Panel,...
    'Units','normalized',...
    'FontSize',8,...
    'BackgroundColor', [Look.Table1;Look.Table2],...
    'ForegroundColor', Look.TableFore,...
    'ColumnName',{'ACF1','CCF','ACF2'},...
    'ColumnWidth',num2cell([40,40,40]),...
    'ColumnEditable',true,...
    'RowName',{'w_r [um]';'Fix';'D [um2/s]';'Fix';'Alpha';'Fix';},...
    'CellEditCallback',{@Update_Plots,6},...
    'Position',[0.01 0.73, 0.18 0.17]);
Data = cell(6,3);
Data(1,:)={'0.2'};
Data(3,:)={'0.01'};
Data(5,:)={'1'};
Data([2 4],:)={false};
Data(6,:)={true};
h.Mia_STICS.Fit_Table.Data=Data;

h.Mia_STICS.Do_iMSD = uicontrol(...
    'Parent',h.Mia_STICS.Panel,...
    'Style','push',...
    'Units','normalized',...
    'FontSize',12,...
    'BackgroundColor', Look.Control,...
    'ForegroundColor', Look.Fore,...
    'Callback',{@Do_iMSD},...
    'String','Fit iMSD',...
    'Position',[0.01 0.69, 0.08 0.03]);

%%% Axes to display correlation
h.Mia_STICS.Axes = axes(...
    'Parent',h.Mia_STICS.Panel,...
    'Units','normalized',...
    'NextPlot','Add',...
    'Position',[0.24 0.59 0.74 0.38]);
for i=1:3
    h.Plots.STICS(i,1) = errorbar(...
        [0.1 1],...
        [0 0],...
        [0 0],...
        [0 0],...
        'Parent',h.Mia_STICS.Axes,...
        'LineStyle','none',...
        'Marker','.',...
        'MarkerSize',8,...
        'Visible','off',...
        'Color',ceil([mod(i-1,3)/2 mod(3-i,3)/2 0]));
    h.Plots.STICS(i,2) = line(...
        'Parent',h.Mia_STICS.Axes,...
        'XData',[0.1 1],...
        'YData',[0 0],...
        'LineStyle','-',...
        'Marker','none',...
        'MarkerSize',8,...
        'Visible','off',...
        'Color',ceil([mod(i-1,3)/2 mod(3-i,3)/2 0]));
    h.Mia_STICS.Axes.XColor = Look.Fore;
    h.Mia_STICS.Axes.YColor = Look.Fore;
    h.Mia_STICS.Axes.XLabel.String = 'Time Lag {\it\tau{}} [s]';
    h.Mia_STICS.Axes.XLabel.Color = Look.Fore;
    h.Mia_STICS.Axes.YLabel.String = 'iMSD [um2/s]';
    h.Mia_STICS.Axes.YLabel.Color = Look.Fore;
    
    
    h.Text{end+1} = uicontrol(...
        'Parent',h.Mia_STICS.Panel,...
        'Style','text',...
        'Units','normalized',...
        'FontSize',14,...
        'FontWeight','bold',...
        'BackgroundColor', Look.Back,...
        'ForegroundColor', Look.Fore,...
        'Position',[0.02+(i-1)*0.33 0.48 0.3 0.03]);
    switch i
        case 1
            h.Text{end}.String = 'ACF1';
        case 2
            h.Text{end}.String = 'CCF';
        case 3
            h.Text{end}.String = 'ACF2';
    end
    %%% Axes to display correlation images
    h.Mia_STICS.Image(i,1) = axes(...
        'Parent',h.Mia_STICS.Panel,...
        'Units','normalized',...
        'Box','off',...
        'Nextplot','Add',...
        'DataAspectRatio',[1 1 1],...
        'PlotBoxAspectRatio', [1 1 1],...
        'UIContextMenu',h.Mia_TICS.Menu,...
        'Position',[0.02+(i-1)*0.33 0.02 0.3 0.45]);
    
    h.Plots.STICSImage(i,1) = imagesc(...
        zeros(2),...
        'Visible','off',...
        'Parent',h.Mia_STICS.Image(i));
    h.Mia_STICS.Image(i,1).XTick = [];
    h.Mia_STICS.Image(i,1).YTick = [];
    h.Mia_STICS.Image(i,1).Visible = 'off';
    h.Mia_STICS.Image(i,1).Colormap = jet;
    h.Mia_STICS.Image(i,2) = colorbar(...
        'Peer',h.Mia_STICS.Image(i,1),...
        'Visible','off',...
        'YColor',Look.Fore);
end

%% N&B Tab
s.ProgressRatio = 0.85;
h.Mia_NB.Tab = uitab(...
    'Parent',h.Mia_Main_Tabs,...
    'Title','N&B',...
    'Units','normalized');
h.Mia_NB.Panel = uibuttongroup(...
    'Parent',h.Mia_NB.Tab,...
    'Units','normalized',...
    'BackgroundColor', Look.Back,...
    'ForegroundColor', Look.Fore,...
    'HighlightColor', Look.Control,...
    'ShadowColor', Look.Shadow,...
    'Position',[0 0 1 1]);

%%% Text
h.Text{end+1} = uicontrol(...
    'Parent',h.Mia_NB.Panel,...
    'Style','text',...
    'Units','normalized',...
    'FontSize',14,...
    'BackgroundColor', Look.Back,...
    'ForegroundColor', Look.Fore,...
    'Position',[0.04 0.96, 0.3 0.03],...
    'String','Intensity');
%%% Axes to display intensity images
h.Mia_NB.Axes(1)= axes(...
    'Parent',h.Mia_NB.Panel,...
    'Units','normalized',...
    'Position',[0.04 0.55 0.3 0.4]);
h.Mia_NB.Axes(1).Colormap = jet;
%%% Text
h.Text{end+1} = uicontrol(...
    'Parent',h.Mia_NB.Panel,...
    'Style','text',...
    'Units','normalized',...
    'FontSize',14,...
    'BackgroundColor', Look.Back,...
    'ForegroundColor', Look.Fore,...
    'Position',[0.36 0.96, 0.3 0.03],...
    'String','Number n');
%%% Axes to display number images
h.Mia_NB.Axes(2)= axes(...
    'Parent',h.Mia_NB.Panel,...
    'Units','normalized',...
    'Position',[0.36 0.55 0.3 0.4]);
h.Mia_NB.Axes(2).Colormap = jet;
%%% Text
h.Text{end+1} = uicontrol(...
    'Parent',h.Mia_NB.Panel,...
    'Style','text',...
    'Units','normalized',...
    'FontSize',14,...
    'BackgroundColor', Look.Back,...
    'ForegroundColor', Look.Fore,...
    'Position',[0.68 0.96, 0.3 0.03],...
    'String','Brightness epsilon');
%%% Axes to display brightness images
h.Mia_NB.Axes(3)= axes(...
    'Parent',h.Mia_NB.Panel,...
    'Units','normalized',...
    'Position',[0.68 0.55 0.3 0.4]);
h.Mia_NB.Axes(3).Colormap = jet;

%%% Popupmenu to select histogram to plot
h.Mia_NB.Hist1D = uicontrol(...
    'Parent',h.Mia_NB.Panel,...
    'Style','popupmenu',...
    'Units','normalized',...
    'FontSize',12,...
    'BackgroundColor', Look.Control,...
    'ForegroundColor', Look.Fore,...
    'Position',[0.04 0.49, 0.06 0.03],...
    'Callback',{@Update_Plots,3},...
    'String',{'PCH';'Intensity';'Number';'Brightness'});
if ismac
    h.Mia_NB.Hist1D.ForegroundColor = [0 0 0];
    h.Mia_NB.Hist1D.BackgroundColor = [1 1 1];
end
%%% Axes to display 2D histograms
h.Mia_NB.Axes(4)= axes(...
    'Parent',h.Mia_NB.Panel,...
    'Units','normalized',...
    'Position',[0.04 0.06 0.26 0.4]);

%%% Popupmenu to select 2D histogram Y axes
h.Mia_NB.Hist2D(1) = uicontrol(...
    'Parent',h.Mia_NB.Panel,...
    'Style','popupmenu',...
    'Units','normalized',...
    'FontSize',12,...
    'BackgroundColor', Look.Control,...
    'ForegroundColor', Look.Fore,...
    'Value',3,...
    'Position',[0.36 0.49, 0.08 0.03],...
    'Callback',{@Update_Plots,3},...
    'String',{'Intensity';'Number';'Brightness'});
%%% Popupmenu to select 2D histogram X axes
h.Mia_NB.Hist2D(2) = uicontrol(...
    'Parent',h.Mia_NB.Panel,...
    'Style','popupmenu',...
    'Units','normalized',...
    'FontSize',12,...
    'BackgroundColor', Look.Control,...
    'ForegroundColor', Look.Fore,...
    'Value',1,...
    'Position',[0.45 0.49, 0.08 0.03],...
    'Callback',{@Update_Plots,3},...
    'String',{'Intensity';'Number';'Brightness'});
%%% Popupmenu to select 2D histogram Color
h.Mia_NB.Hist2D(3) = uicontrol(...
    'Parent',h.Mia_NB.Panel,...
    'Style','popupmenu',...
    'Units','normalized',...
    'FontSize',12,...
    'BackgroundColor', [1 1 1],...
    'ForegroundColor', [0 0 0],...
    'Value',1,...
    'Position',[0.54 0.49, 0.05 0.03],...
    'Callback',{@Update_Plots,3},...
    'String',{'Jet';'Parula';'Hot';'HSV';'Gray'});
if ismac
    for i = 1:3
        h.Mia_NB.Hist2D(i).ForegroundColor = [0 0 0];
        h.Mia_NB.Hist2D(i).BackgroundColor = [1 1 1];
    end
end
%%% Axes to display various histograms
h.Mia_NB.Axes(5)= axes(...
    'Parent',h.Mia_NB.Panel,...
    'Units','normalized',...
    'Position',[0.36 0.06 0.3 0.4]);
h.Mia_NB.Axes(5).Colormap = jet;

%%% Initializes empty plots
h.Plots.NB(1)=imagesc(zeros(1,1),...
    'Parent',h.Mia_NB.Axes(1));
h.Mia_NB.Axes(1).Color=[0 0 0];
h.Mia_NB.Axes(1).DataAspectRatio=[1 1 1];
h.Mia_NB.Axes(1).XTick=[];
h.Mia_NB.Axes(1).YTick=[];
a=colorbar(h.Mia_NB.Axes(1));
a.YColor=Look.Fore;
h.Plots.NB(2)=imagesc(zeros(1,1),...
    'Parent',h.Mia_NB.Axes(2));
h.Mia_NB.Axes(2).Color=[0 0 0];
h.Mia_NB.Axes(2).DataAspectRatio=[1 1 1];
h.Mia_NB.Axes(2).XTick=[];
h.Mia_NB.Axes(2).YTick=[];
a=colorbar(h.Mia_NB.Axes(2));
a.YColor=Look.Fore;
h.Plots.NB(3)=imagesc(zeros(1,1),...
    'Parent',h.Mia_NB.Axes(3));
h.Mia_NB.Axes(3).Color=[0 0 0];
h.Mia_NB.Axes(3).DataAspectRatio=[1 1 1];
h.Mia_NB.Axes(3).XTick=[];
h.Mia_NB.Axes(3).YTick=[];
a=colorbar(h.Mia_NB.Axes(3));
a.YColor=Look.Fore;
h.Plots.NB(5)=imagesc(zeros(1,1,3),...
    'Parent',h.Mia_NB.Axes(5),...
    'ButtonDownFcn',@NB_2DHist_BG);
h.Mia_NB.Axes(5).Color=[0 0 0];
h.Mia_NB.Axes(5).XColor = Look.Fore;
h.Mia_NB.Axes(5).YColor = Look.Fore;
h.Mia_NB.Axes(5).XLabel.String='Intensity [kHz]';
h.Mia_NB.Axes(5).XLabel.Color=Look.Fore;
h.Mia_NB.Axes(5).YLabel.String='Brightness [kHz]';
h.Mia_NB.Axes(5).YLabel.Color=Look.Fore;
h.Mia_NB.Axes(5).YDir='normal';
%h.Mia_NB.Axes(5).ButtonDownFcn = 

h.Plots.NB(4)=stairs(0,0,...
    'Parent',h.Mia_NB.Axes(4),...
    'Color','b');
h.Mia_NB.Axes(4).XColor = Look.Fore;
h.Mia_NB.Axes(4).YColor = Look.Fore;
h.Mia_NB.Axes(4).XLabel.String='Counts per pixel';
h.Mia_NB.Axes(4).XLabel.Color=Look.Fore;
h.Mia_NB.Axes(4).YLabel.String='Frequency';
h.Mia_NB.Axes(4).YLabel.Color=Look.Fore;
%%% Text to show mean
h.Mia_NB.Hist1D_Text = text(....
    'Parent',h.Mia_NB.Axes(4),...
    'Units','normalized',...
    'FontSize',14,...
    'Color', 'b',...
    'Position',[0.7 0.93],...
    'String','');

%%% Tabs for N%B Settings
h.Mia_NB.Settings.Tabs = uitabgroup(...
    'Parent',h.Mia_NB.Panel,...
    'Tag','Mia_NB_Settings_Tabs',...
    'Units','normalized',...
    'Position',[0.68 0.01 0.31 0.45]);
%% Tab for N%B Image and Plot Settings
h.Mia_NB.Image.Tab = uitab(...
    'Parent',h.Mia_NB.Settings.Tabs,...
    'Title','Image & Plot');
h.Mia_NB.Image.Panel = uibuttongroup(...
    'Parent',h.Mia_NB.Image.Tab,...
    'Units','normalized',...
    'BackgroundColor', Look.Back,...
    'ForegroundColor', Look.Fore,...
    'HighlightColor', Look.Control,...
    'ShadowColor', Look.Shadow,...
    'Position',[0 0 1 1]);

%%% Text
h.Text{end+1} = uicontrol(...
    'Parent',h.Mia_NB.Image.Panel,...
    'Style','text',...
    'Units','normalized',...
    'FontSize',12,...
    'HorizontalAlignment','left',...
    'BackgroundColor', Look.Back,...
    'ForegroundColor', Look.Fore,...
    'Position',[0.02 0.92, 0.26 0.06],...
    'String','Frame time [s]:');
%%% Editbox to set frame time
h.Mia_NB.Image.Frame = uicontrol(...
    'Parent',h.Mia_NB.Image.Panel,...
    'Style','edit',...
    'Units','normalized',...
    'FontSize',12,...
    'BackgroundColor', Look.Control,...
    'ForegroundColor', Look.Fore,...
    'Position',[0.29 0.92, 0.14 0.06],...
    'String','1');
h.FT_Linker=linkprop([h.Mia_NB.Image.Frame,h.Mia_Image.Settings.Image_Frame],'String');
%%% Text
h.Text{end+1} = uicontrol(...
    'Parent',h.Mia_NB.Image.Panel,...
    'Style','text',...
    'Units','normalized',...
    'FontSize',12,...
    'HorizontalAlignment','left',...
    'BackgroundColor', Look.Back,...
    'ForegroundColor', Look.Fore,...
    'Position',[0.02 0.84, 0.26 0.06],...
    'String','Line time [ms]:');
%%% Editbox to set line time
h.Mia_NB.Image.Line = uicontrol(...
    'Parent',h.Mia_NB.Image.Panel,...
    'Style','edit',...
    'Units','normalized',...
    'FontSize',12,...
    'BackgroundColor', Look.Control,...
    'ForegroundColor', Look.Fore,...
    'Position',[0.29 0.84, 0.14 0.06],...
    'String','3.333');
h.LT_Linker=linkprop([h.Mia_NB.Image.Line,h.Mia_Image.Settings.Image_Line],'String');
%%% Text
h.Text{end+1} = uicontrol(...
    'Parent',h.Mia_NB.Image.Panel,...
    'Style','text',...
    'Units','normalized',...
    'FontSize',12,...
    'HorizontalAlignment','left',...
    'BackgroundColor', Look.Back,...
    'ForegroundColor', Look.Fore,...
    'Position',[0.02 0.76, 0.26 0.06],...
    'String','Pixel time [us]:');
%%% Editbox to set pixel time
h.Mia_NB.Image.Pixel = uicontrol(...
    'Parent',h.Mia_NB.Image.Panel,...
    'Style','edit',...
    'Units','normalized',...
    'FontSize',12,...
    'BackgroundColor', Look.Control,...
    'ForegroundColor', Look.Fore,...
    'Callback',{@Update_Plots,3},...
    'Position',[0.29 0.76, 0.14 0.06],...
    'String',num2str(UserValues.MIA.FitParams.PixTime));
h.PT_Linker=linkprop([h.Mia_NB.Image.Pixel,h.Mia_Image.Settings.Image_Pixel],'String');

%%% Popupmenu to select N&B channel
h.Mia_NB.Image.Channel = uicontrol(...
    'Parent',h.Mia_NB.Image.Panel,...
    'Style','popupmenu',...
    'Units','normalized',...
    'FontSize',12,...
    'BackgroundColor', Look.Control,...
    'ForegroundColor', Look.Fore,...
    'Callback',{@Update_Plots,3},...
    'Position',[0.55 0.92, 0.25 0.06],...
    'String',{'Top Channel','Cross','Bottom Channel'});
if ismac
    h.Mia_NB.Image.Channel.ForegroundColor = [0 0 0];
    h.Mia_NB.Image.Channel.BackgroundColor = [1 1 1];
end
  h.Mia_NB.Image.logZ = uicontrol(...
        'Parent',h.Mia_NB.Image.Panel,...
        'Style','checkbox',...
        'Units','normalized',...
        'FontSize',12,...
        'BackgroundColor', Look.Back,...
        'ForegroundColor', Look.Fore,...
        'Value',0,...
        'Callback',{@Update_Plots,3},...
        'Position',[0.55 0.82, 0.25 0.06],...
        'String','log10',...
        'Tooltipstring', 'display the 2D histogram in a logaritmic Z scaling');
%%% Text
h.Text{end+1} = uicontrol(...
    'Parent',h.Mia_NB.Image.Panel,...
    'Style','text',...
    'Units','normalized',...
    'FontSize',12,...
    'BackgroundColor', Look.Back,...
    'ForegroundColor', Look.Fore,...
    'Position',[0.22 0.66, 0.12 0.06],...
    'String','Min');
h.Text{end+1} = uicontrol(...
    'Parent',h.Mia_NB.Image.Panel,...
    'Style','text',...
    'Units','normalized',...
    'FontSize',12,...
    'BackgroundColor', Look.Back,...
    'ForegroundColor', Look.Fore,...
    'Position',[0.36 0.66, 0.12 0.06],...
    'String','Max');
h.Text{end+1} = uicontrol(...
    'Parent',h.Mia_NB.Image.Panel,...
    'Style','text',...
    'Units','normalized',...
    'FontSize',12,...
    'BackgroundColor', Look.Back,...
    'ForegroundColor', Look.Fore,...
    'Position',[0.5 0.66, 0.12 0.06],...
    'String','Bins');
h.Text{end+1} = uicontrol(...
    'Parent',h.Mia_NB.Image.Panel,...
    'Style','text',...
    'Units','normalized',...
    'FontSize',12,...
    'HorizontalAlignment','left',...
    'BackgroundColor', Look.Back,...
    'ForegroundColor', Look.Fore,...
    'Position',[0.02 0.58, 0.18 0.06],...
    'String','Intensity:');
h.Text{end+1} = uicontrol(...
    'Parent',h.Mia_NB.Image.Panel,...
    'Style','text',...
    'Units','normalized',...
    'FontSize',12,...
    'HorizontalAlignment','left',...
    'BackgroundColor', Look.Back,...
    'ForegroundColor', Look.Fore,...
    'Position',[0.02 0.5, 0.18 0.06],...
    'String','Number:');
h.Text{end+1} = uicontrol(...
    'Parent',h.Mia_NB.Image.Panel,...
    'Style','text',...
    'Units','normalized',...
    'FontSize',12,...
    'HorizontalAlignment','left',...
    'BackgroundColor', Look.Back,...
    'ForegroundColor', Look.Fore,...
    'Position',[0.02 0.42, 0.18 0.06],...
    'String','Brightness:');

%%% Editboxes for Min, Max and Bin number
%%% Checkboxes for threshold use
for i=1:3
    for j=1:3
        h.Mia_NB.Image.Hist(i,j) = uicontrol(...
            'Parent',h.Mia_NB.Image.Panel,...
            'Style','edit',...
            'Units','normalized',...
            'FontSize',12,...
            'BackgroundColor', Look.Control,...
            'ForegroundColor', Look.Fore,...
            'Callback',{@Update_Plots,3},...
            'Position',[0.08+0.14*i 0.66-0.08*j, 0.12 0.06],...
            'String','1');
    end
    h.Mia_NB.Image.UseTH(i) = uicontrol(...
        'Parent',h.Mia_NB.Image.Panel,...
        'Style','checkbox',...
        'Units','normalized',...
        'FontSize',12,...
        'BackgroundColor', Look.Back,...
        'ForegroundColor', Look.Fore,...
        'Value',0,...
        'Callback',{@Update_Plots,3},...
        'Position',[0.63 0.66-0.08*i, 0.18 0.06],...
        'String','Use TH');
end

%%Button for ROI mask generation
h.Mia_NB.Image.Mask = uicontrol(...
    'Parent',h.Mia_NB.Image.Panel,...
    'Style','push',...
    'Units','normalized',...
    'FontSize',12,...
    'BackgroundColor', Look.Control,...
    'ForegroundColor', Look.Fore,...
    'Position',[0.2 0.2, 0.6 0.10],...
    'Callback',{@Export_ROI, 1},...
    'String','Export Threshold as ROI');

%% FLIM Tab
s.ProgressRatio = 0.95;
h.Mia_FLIM.Tab = uitab(...
    'Parent',h.Mia_Main_Tabs,...
    'Title','FLIM',...
    'Units','normalized');
h.Mia_FLIM.Panel = uibuttongroup(...
    'Parent',h.Mia_FLIM.Tab,...
    'Units','normalized',...
    'BackgroundColor', Look.Back,...
    'ForegroundColor', Look.Fore,...
    'HighlightColor', Look.Control,...
    'ShadowColor', Look.Shadow,...
    'Position',[0 0 1 1]);

%%% Context menu to draw ROI shapes on image
h.Mia_FLIM.Menu = uicontextmenu;
h.Mia_FLIM.Assisted_Freehand = uimenu(...
    'Parent',h.Mia_FLIM.Menu,...
    'Label','Assisted Freehand',...
    'Callback',{@FLIM_Draw_ROI},...
    'Visible', 'on');
h.Mia_FLIM.Freehand = uimenu(...
    'Parent',h.Mia_FLIM.Menu,...
    'Label','Freehand',...
    'Callback',{@FLIM_Draw_ROI},...
    'Visible', 'on');
h.Mia_FLIM.Circle = uimenu(...
    'Parent',h.Mia_FLIM.Menu,...
    'Label','Circle',...
    'Callback',{@FLIM_Draw_ROI},...
    'Visible', 'on');
h.Mia_FLIM.Ellipse = uimenu(...
    'Parent',h.Mia_FLIM.Menu,...
    'Label','Ellipse',...
    'Callback',{@FLIM_Draw_ROI},...
    'Visible', 'on');
h.Mia_FLIM.Rectangle = uimenu(...
    'Parent',h.Mia_FLIM.Menu,...
    'Label','Rectangle',...
    'Callback',{@FLIM_Draw_ROI},...
    'Visible', 'on');
h.Mia_FLIM.Polygon = uimenu(...
    'Parent',h.Mia_FLIM.Menu,...
    'Label','Polygon',...
    'Callback',{@FLIM_Draw_ROI},...
    'Visible', 'on');

%%% Button to calculate decay matrix
h.Mia_FLIM.Calc_Decay_Matrix = uicontrol(...
    'Parent', h.Mia_FLIM.Panel,...
    'Style','push',...
    'Units','normalized',...
    'FontSize',10,...
    'BackgroundColor', Look.Control,...
    'ForegroundColor', Look.Fore,...
    'Position',[0.04 0.97, 0.1 0.03],...
    'Callback', {@FLIM_Decay_Matrix, 1:2},...
    'String','Calc. Decay Matrix');

%%% Button to export results
h.Mia_FLIM.Export = uicontrol(...
    'Parent', h.Mia_FLIM.Panel,...
    'Style','push',...
    'Units','normalized',...
    'FontSize',10,...
    'BackgroundColor', Look.Control,...
    'ForegroundColor', Look.Fore,...
    'Position',[0.15 0.97, 0.1 0.03],...
    'Callback', @FLIM_Export,...
    'String','Export');

%%% Axes to display images
h.Mia_FLIM.Axes(1,1)= axes(...
    'Parent',h.Mia_FLIM.Panel,...
    'Units','normalized',...
    'Position',[0.02 0.51 0.28 0.43]);
h.Plots.FLIM.Image(1)=imagesc(zeros(1,1),...
    'Parent',h.Mia_FLIM.Axes(1,1),...
    'UIContextMenu', h.Mia_FLIM.Menu);
h.Mia_FLIM.Axes(1,1).Colormap = jet;
h.Mia_FLIM.Axes(1,1).Color=[0 0 0];
h.Mia_FLIM.Axes(1,1).DataAspectRatio=[1 1 1];
h.Mia_FLIM.Axes(1,1).XTick=[];
h.Mia_FLIM.Axes(1,1).YTick=[];
h.Mia_FLIM.Axes(1,1).YDir = 'normal';
a=colorbar(h.Mia_FLIM.Axes(1,1));
a.YColor=Look.Fore;

h.Mia_FLIM.Axes(1,2)= axes(...
    'Parent',h.Mia_FLIM.Panel,...
    'Units','normalized',...
    'Position',[0.52 0.51 0.28 0.43]);
h.Plots.FLIM.Image(2)=imagesc(zeros(1,1),...
    'Parent',h.Mia_FLIM.Axes(1,2),...
    'UIContextMenu', h.Mia_FLIM.Menu);
h.Mia_FLIM.Axes(1,2).Colormap = jet;
h.Mia_FLIM.Axes(1,2).Color=[0 0 0];
h.Mia_FLIM.Axes(1,2).DataAspectRatio=[1 1 1];
h.Mia_FLIM.Axes(1,2).XTick=[];
h.Mia_FLIM.Axes(1,2).YTick=[];
h.Mia_FLIM.Axes(1,2).YDir = 'normal';
a=colorbar(h.Mia_FLIM.Axes(1,2));
a.YColor=Look.Fore;

%%% Axes to display microtime histograms
h.Mia_FLIM.Axes(2,1)= axes(...
    'Parent',h.Mia_FLIM.Panel,...
    'Units','normalized',...
    'Position',[0.04 0.18 0.25 0.28]);
h.Plots.FLIM.IRF(1)=line(0,0,...
    'Parent',h.Mia_FLIM.Axes(2,1),...
    'Color', 'k',...
    'LineStyle', ':');
h.Plots.FLIM.Decay(1)=line(0,0,...
    'Parent',h.Mia_FLIM.Axes(2,1),...
    'Color', 'b',...
    'LineStyle', '-');
h.Plots.FLIM.Fit(1)=line(0,0,...
    'Parent',h.Mia_FLIM.Axes(2,1),...
    'Color', 'k',...
    'LineStyle', '-');
h.Mia_FLIM.Axes(2,1).XColor = Look.Fore;
h.Mia_FLIM.Axes(2,1).YColor = Look.Fore;
h.Mia_FLIM.Axes(2,1).YScale = 'log';
%h.Mia_FLIM.Axes(2,1).XLabel.String='TCSPC channel';
%h.Mia_FLIM.Axes(2,1).XLabel.Color=Look.Fore;
h.Mia_FLIM.Axes(2,1).XTickLabel = '';
h.Mia_FLIM.Axes(2,1).YLabel.String='Counts';
h.Mia_FLIM.Axes(2,1).YLabel.Color=Look.Fore;

h.Plots.FLIM.Text(1) = text(...
    0,0,'',...
    'Parent',h.Mia_FLIM.Axes(2,1),...
    'FontSize',10,...
    'FontWeight','bold',...
    'BackgroundColor','none',...
    'Units','normalized',...
    'Color',Look.AxesFore,...
    'BackgroundColor',Look.Axes,...
    'VerticalAlignment','top',...
    'HorizontalAlignment','right',...
    'Position',[0.95 0.95]);

h.Mia_FLIM.Axes(2,2)= axes(...
    'Parent',h.Mia_FLIM.Panel,...
    'Units','normalized',...
    'Position',[0.54 0.18 0.25 0.28]);
h.Plots.FLIM.IRF(2)=line(0,0,...
    'Parent',h.Mia_FLIM.Axes(2,2),...
    'Color', 'k',...
    'LineStyle', ':');
h.Plots.FLIM.Decay(2)=line(0,0,...
    'Parent',h.Mia_FLIM.Axes(2,2),...
    'Color', 'b',...
    'LineStyle', '-');
h.Plots.FLIM.Fit(2)=line(0,0,...
    'Parent',h.Mia_FLIM.Axes(2,2),...
    'Color', 'k',...
    'LineStyle', '-');
h.Mia_FLIM.Axes(2,2).XColor = Look.Fore;
h.Mia_FLIM.Axes(2,2).YColor = Look.Fore;
h.Mia_FLIM.Axes(2,2).YScale = 'log';
%h.Mia_FLIM.Axes(2,2).XLabel.String='TCSPC channel';
%h.Mia_FLIM.Axes(2,2).XLabel.Color=Look.Fore;
h.Mia_FLIM.Axes(2,2).XTickLabel = '';
h.Mia_FLIM.Axes(2,2).YLabel.String='Counts';
h.Mia_FLIM.Axes(2,2).YLabel.Color=Look.Fore;

h.Plots.FLIM.Text(2) = text(...
    0,0,'',...
    'Parent',h.Mia_FLIM.Axes(2,2),...
    'FontSize',10,...
    'FontWeight','bold',...
    'BackgroundColor','none',...
    'Units','normalized',...
    'Color',Look.AxesFore,...
    'BackgroundColor',Look.Axes,...
    'VerticalAlignment','top',...
    'HorizontalAlignment','right',...
    'Position',[0.95 0.95]);

%%% Axes to display residuals
h.Mia_FLIM.Axes(3,1)= axes(...
    'Parent',h.Mia_FLIM.Panel,...
    'Units','normalized',...
    'Position',[0.04 0.06 0.25 0.1]);
h.Plots.FLIM.WRes(1)=line(0,0,...
    'Parent',h.Mia_FLIM.Axes(3,1),...
    'Color', 'k',...
    'LineStyle', '-');
h.Mia_FLIM.Axes(3,1).XColor = Look.Fore;
h.Mia_FLIM.Axes(3,1).YColor = Look.Fore;
h.Mia_FLIM.Axes(3,1).XLabel.String='Time [ns]';
h.Mia_FLIM.Axes(3,1).XLabel.Color=Look.Fore;
h.Mia_FLIM.Axes(3,1).YLabel.String='Residuals';
h.Mia_FLIM.Axes(3,1).YLabel.Color=Look.Fore;
linkaxes([h.Mia_FLIM.Axes(2,1) h.Mia_FLIM.Axes(3,1)],'x');

h.Mia_FLIM.Axes(3,2)= axes(...
    'Parent',h.Mia_FLIM.Panel,...
    'Units','normalized',...
    'Position',[0.54 0.06 0.25 0.1]);
h.Plots.FLIM.WRes(2)=line(0,0,...
    'Parent',h.Mia_FLIM.Axes(3,2),...
    'Color', 'k',...
    'LineStyle', '-');
h.Mia_FLIM.Axes(3,2).XColor = Look.Fore;
h.Mia_FLIM.Axes(3,2).YColor = Look.Fore;
h.Mia_FLIM.Axes(3,2).XLabel.String='Time [ns]';
h.Mia_FLIM.Axes(3,2).XLabel.Color=Look.Fore;
h.Mia_FLIM.Axes(3,2).YLabel.String='Residuals';
h.Mia_FLIM.Axes(3,2).YLabel.Color=Look.Fore;
linkaxes([h.Mia_FLIM.Axes(2,2) h.Mia_FLIM.Axes(3,2)],'x');

%%% Image scaling options
h.Mia_FLIM.Ch(1).AutoScale = uicontrol(...
        'Parent',h.Mia_FLIM.Panel,...
        'Style','checkbox',...
        'Units','normalized',...
        'FontSize',10,...
        'BackgroundColor', Look.Back,...
        'ForegroundColor', Look.Fore,...
        'Position',[0.06 0.47, 0.06 0.03],...
        'String','Autoscale',...
        'Value',1,...
        'Callback',{@Do_FLIM, 1:3, 1});
h.Mia_FLIM.Ch(1).ScaleMin = uicontrol(...
        'Parent',h.Mia_FLIM.Panel,...
        'Style','edit',...
        'Units','normalized',...
        'FontSize',10,...
        'BackgroundColor', Look.Back,...
        'ForegroundColor', Look.Fore,...
        'Position',[0.13 0.47, 0.04 0.03],...
        'String','0',...
        'Callback',{@Do_FLIM, 1:3, 1});
h.Mia_FLIM.Ch(1).ScaleMax = uicontrol(...
        'Parent',h.Mia_FLIM.Panel,...
        'Style','edit',...
        'Units','normalized',...
        'FontSize',10,...
        'BackgroundColor', Look.Back,...
        'ForegroundColor', Look.Fore,...
        'Position',[0.18 0.47, 0.04 0.03],...
        'String','100',...
        'Callback',{@Do_FLIM, 1:3, 1});

h.Mia_FLIM.Ch(2).AutoScale = uicontrol(...
        'Parent',h.Mia_FLIM.Panel,...
        'Style','checkbox',...
        'Units','normalized',...
        'FontSize',10,...
        'BackgroundColor', Look.Back,...
        'ForegroundColor', Look.Fore,...
        'Position',[0.56 0.47, 0.06 0.03],...
        'String','Autoscale',...
        'Value',1,...
        'Callback',{@Do_FLIM, 1:3, 2});
h.Mia_FLIM.Ch(2).ScaleMin = uicontrol(...
        'Parent',h.Mia_FLIM.Panel,...
        'Style','edit',...
        'Units','normalized',...
        'FontSize',10,...
        'BackgroundColor', Look.Back,...
        'ForegroundColor', Look.Fore,...
        'Position',[0.63 0.47, 0.04 0.03],...
        'String','0',...
        'Callback',{@Do_FLIM, 1:3, 2});
h.Mia_FLIM.Ch(2).ScaleMax = uicontrol(...
        'Parent',h.Mia_FLIM.Panel,...
        'Style','edit',...
        'Units','normalized',...
        'FontSize',10,...
        'BackgroundColor', Look.Back,...
        'ForegroundColor', Look.Fore,...
        'Position',[0.68 0.47, 0.04 0.03],...
        'String','100',...
        'Callback',{@Do_FLIM, 1:3, 2});

%%% FLIM fit options
%%% Channel Selection
h.Mia_FLIM.Ch(1).Channel = uicontrol(...
        'Parent',h.Mia_FLIM.Panel,...
        'Style','Popupmenu',...
        'Tag','FLIMChannel_Popupmenu',...
        'Units','normalized',...
        'FontSize',10,...
        'BackgroundColor', Look.Control,...
        'ForegroundColor', Look.Fore,...
        'Position',[0.3 0.95 0.1 0.03],...
        'String',{'Channel 1', 'Channel 2'},...
        'Value', 1, ...
        'Callback',{@Do_FLIM,1:3, 1});
h.Mia_FLIM.Ch(2).Channel = uicontrol(...
        'Parent',h.Mia_FLIM.Panel,...
        'Style','Popupmenu',...
        'Tag','FLIMChannel_Popupmenu',...
        'Units','normalized',...
        'FontSize',10,...
        'BackgroundColor', Look.Control,...
        'ForegroundColor', Look.Fore,...
        'Position',[0.8 0.95 0.1 0.03],...
        'String',{'Channel 1', 'Channel 2'},...
        'Value', 2,...
        'Callback',{@Do_FLIM,1:3, 2});

%%% Image Type
h.Mia_FLIM.ImageType = {'Intensity', 'Mean Arrival Time', 'a1', 'Tau1', 'a2', 'Tau2', 'a3', 'Tau3', 'Chi2', 'Mean Tau'};
h.Mia_FLIM.Ch(1).ImageType = uicontrol(...
        'Parent',h.Mia_FLIM.Panel,...
        'Style','Popupmenu',...
        'Tag','FLIMChannel_Popupmenu',...
        'Units','normalized',...
        'FontSize',10,...
        'BackgroundColor', Look.Control,...
        'ForegroundColor', Look.Fore,...
        'Position',[0.4 0.95 0.1 0.03],...
        'String', h.Mia_FLIM.ImageType,...
        'Value', 1, ...
        'Callback',{@Do_FLIM,1, 1});
h.Mia_FLIM.Ch(2).ImageType = uicontrol(...
        'Parent',h.Mia_FLIM.Panel,...
        'Style','Popupmenu',...
        'Tag','FLIMChannel_Popupmenu',...
        'Units','normalized',...
        'FontSize',10,...
        'BackgroundColor', Look.Control,...
        'ForegroundColor', Look.Fore,...
        'Position',[0.9 0.95 0.1 0.03],...
        'String', h.Mia_FLIM.ImageType,...
        'Value', 1,...
        'Callback',{@Do_FLIM,1, 2});

%%% TCSPC Range
h.Text{end+1} = uicontrol(...
    'Parent',h.Mia_FLIM.Panel,...
    'Style','text',...
    'Units','normalized',...
    'FontSize',10,...
    'HorizontalAlignment','left',...
    'BackgroundColor', Look.Back,...
    'ForegroundColor', Look.Fore,...
    'Position',[0.3 0.91, 0.1 0.03],...
    'String','TCSPC Range:');
h.Mia_FLIM.Ch(1).Fit_Start = uicontrol(...
    'Parent',h.Mia_FLIM.Panel,...
    'Style','edit',...
    'Units','normalized',...
    'FontSize',10,...
    'BackgroundColor', Look.Control,...
    'ForegroundColor', Look.Fore,...
    'Position',[0.4 0.91, 0.04 0.03],...
    'String','1',...
    'Callback',{@Do_FLIM,1:3, 1});
h.Mia_FLIM.Ch(1).Fit_End = uicontrol(...
    'Parent',h.Mia_FLIM.Panel,...
    'Style','edit',...
    'Units','normalized',...
    'FontSize',10,...
    'BackgroundColor', Look.Control,...
    'ForegroundColor', Look.Fore,...
    'Position',[0.45 0.91, 0.04 0.03],...
    'String','4096',...
    'Callback',{@Do_FLIM,1:3, 1});
h.Text{end+1} = uicontrol(...
    'Parent',h.Mia_FLIM.Panel,...
    'Style','text',...
    'Units','normalized',...
    'FontSize',10,...
    'HorizontalAlignment','left',...
    'BackgroundColor', Look.Back,...
    'ForegroundColor', Look.Fore,...
    'Position',[0.8 0.91, 0.1 0.03],...
    'String','TCSPC Range:');
h.Mia_FLIM.Ch(2).Fit_Start = uicontrol(...
    'Parent',h.Mia_FLIM.Panel,...
    'Style','edit',...
    'Units','normalized',...
    'FontSize',10,...
    'BackgroundColor', Look.Control,...
    'ForegroundColor', Look.Fore,...
    'Position',[0.9 0.91, 0.04 0.03],...
    'String','1',...
    'Callback',{@Do_FLIM,1:3, 2});
h.Mia_FLIM.Ch(2).Fit_End = uicontrol(...
    'Parent',h.Mia_FLIM.Panel,...
    'Style','edit',...
    'Units','normalized',...
    'FontSize',10,...
    'BackgroundColor', Look.Control,...
    'ForegroundColor', Look.Fore,...
    'Position',[0.95 0.91, 0.04 0.03],...
    'String','4096',...
    'Callback',{@Do_FLIM,1:3, 2});

%%% IRF Length
h.Text{end+1} = uicontrol(...
    'Parent',h.Mia_FLIM.Panel,...
    'Style','text',...
    'Units','normalized',...
    'FontSize',10,...
    'HorizontalAlignment','left',...
    'BackgroundColor', Look.Back,...
    'ForegroundColor', Look.Fore,...
    'Position',[0.3 0.88, 0.1 0.03],...
    'String','IRF Length:');
h.Mia_FLIM.Ch(1).IRF_Length = uicontrol(...
    'Parent',h.Mia_FLIM.Panel,...
    'Style','edit',...
    'Units','normalized',...
    'FontSize',10,...
    'BackgroundColor', Look.Control,...
    'ForegroundColor', Look.Fore,...
    'Position',[0.4 0.88, 0.04 0.03],...
    'String','1500',...
    'Callback',{@Do_FLIM,2:3, 1});
h.Text{end+1} = uicontrol(...
    'Parent',h.Mia_FLIM.Panel,...
    'Style','text',...
    'Units','normalized',...
    'FontSize',10,...
    'HorizontalAlignment','left',...
    'BackgroundColor', Look.Back,...
    'ForegroundColor', Look.Fore,...
    'Position',[0.8 0.88, 0.1 0.03],...
    'String','IRF Length:');
h.Mia_FLIM.Ch(2).IRF_Length = uicontrol(...
    'Parent',h.Mia_FLIM.Panel,...
    'Style','edit',...
    'Units','normalized',...
    'FontSize',10,...
    'BackgroundColor', Look.Control,...
    'ForegroundColor', Look.Fore,...
    'Position',[0.9 0.88, 0.04 0.03],...
    'String','1500',...
    'Callback',{@Do_FLIM,2:3, 2});

%%% Ignore length
h.Text{end+1} = uicontrol(...
    'Parent',h.Mia_FLIM.Panel,...
    'Style','text',...
    'Units','normalized',...
    'FontSize',10,...
    'HorizontalAlignment','left',...
    'BackgroundColor', Look.Back,...
    'ForegroundColor', Look.Fore,...
    'Position',[0.3 0.85, 0.1 0.03],...
    'String','Ignore Length:');
h.Mia_FLIM.Ch(1).Ignore_Length = uicontrol(...
    'Parent',h.Mia_FLIM.Panel,...
    'Style','edit',...
    'Units','normalized',...
    'FontSize',10,...
    'BackgroundColor', Look.Control,...
    'ForegroundColor', Look.Fore,...
    'Position',[0.4 0.85, 0.04 0.03],...
    'String','1',...
    'Callback',{@Do_FLIM,2:3, 1});
h.Text{end+1} = uicontrol(...
    'Parent',h.Mia_FLIM.Panel,...
    'Style','text',...
    'Units','normalized',...
    'FontSize',10,...
    'HorizontalAlignment','left',...
    'BackgroundColor', Look.Back,...
    'ForegroundColor', Look.Fore,...
    'Position',[0.8 0.85, 0.1 0.03],...
    'String','Ignore Length:');
h.Mia_FLIM.Ch(2).Ignore_Length = uicontrol(...
    'Parent',h.Mia_FLIM.Panel,...
    'Style','edit',...
    'Units','normalized',...
    'FontSize',10,...
    'BackgroundColor', Look.Control,...
    'ForegroundColor', Look.Fore,...
    'Position',[0.9 0.85, 0.04 0.03],...
    'String','1',...
    'Callback',{@Do_FLIM,2:3, 2});

%%% Model Function
h.Mia_FLIM.FitMethods = {'1-Exp [reconv.]','2-Exp [reconv.]','3-Exp [reconv.]',...
    '1-Exp [tail]','2-Exp [tail]','3-Exp [tail]'};
h.Text{end+1} = uicontrol(...
    'Parent',h.Mia_FLIM.Panel,...
    'Style','text',...
    'Units','normalized',...
    'FontSize',10,...
    'HorizontalAlignment','left',...
    'BackgroundColor', Look.Back,...
    'ForegroundColor', Look.Fore,...
    'Position',[0.3 0.82, 0.1 0.03],...
    'String','Model Function:');
h.Mia_FLIM.Ch(1).Model_Function = uicontrol(...
    'Parent',h.Mia_FLIM.Panel,...
    'Style','popupmenu',...
    'Units','normalized',...
    'FontSize',10,...
    'BackgroundColor', Look.Control,...
    'ForegroundColor', Look.Fore,...
    'Position',[0.4 0.82, 0.1 0.03],...
    'String',h.Mia_FLIM.FitMethods,...
    'Value', 1,...
    'Callback',{@Update_FLIM_ModelFun, 1});
h.Text{end+1} = uicontrol(...
    'Parent',h.Mia_FLIM.Panel,...
    'Style','text',...
    'Units','normalized',...
    'FontSize',10,...
    'HorizontalAlignment','left',...
    'BackgroundColor', Look.Back,...
    'ForegroundColor', Look.Fore,...
    'Position',[0.8 0.82, 0.1 0.03],...
    'String','Model Function:');
h.Mia_FLIM.Ch(2).Model_Function = uicontrol(...
    'Parent',h.Mia_FLIM.Panel,...
    'Style','popupmenu',...
    'Units','normalized',...
    'FontSize',10,...
    'BackgroundColor', Look.Control,...
    'ForegroundColor', Look.Fore,...
    'Position',[0.9 0.82, 0.1 0.03],...
    'String',h.Mia_FLIM.FitMethods,...
    'Value', 1,...
    'Callback',{@Update_FLIM_ModelFun, 2});

%%% Optimization method
h.Mia_FLIM.OptimMethod = {'Nelder-Mead Simplex','Trust-Region-Reflective'};
h.Text{end+1} = uicontrol(...
    'Parent',h.Mia_FLIM.Panel,...
    'Style','text',...
    'Units','normalized',...
    'FontSize',10,...
    'HorizontalAlignment','left',...
    'BackgroundColor', Look.Back,...
    'ForegroundColor', Look.Fore,...
    'Position',[0.3 0.79, 0.1 0.03],...
    'String','Optimization Method:');
h.Mia_FLIM.Ch(1).OptimMethod = uicontrol(...
    'Parent',h.Mia_FLIM.Panel,...
    'Style','popupmenu',...
    'Units','normalized',...
    'FontSize',10,...
    'BackgroundColor', Look.Control,...
    'ForegroundColor', Look.Fore,...
    'Position',[0.4 0.79, 0.1 0.03],...
    'String',h.Mia_FLIM.OptimMethod,...
    'Value', 1,...
    'Callback',{@Do_FLIM,2:3, 1});
h.Text{end+1} = uicontrol(...
    'Parent',h.Mia_FLIM.Panel,...
    'Style','text',...
    'Units','normalized',...
    'FontSize',10,...
    'HorizontalAlignment','left',...
    'BackgroundColor', Look.Back,...
    'ForegroundColor', Look.Fore,...
    'Position',[0.8 0.79, 0.1 0.03],...
    'String','Optimization Method:');
h.Mia_FLIM.Ch(2).OptimMethod = uicontrol(...
    'Parent',h.Mia_FLIM.Panel,...
    'Style','popupmenu',...
    'Units','normalized',...
    'FontSize',10,...
    'BackgroundColor', Look.Control,...
    'ForegroundColor', Look.Fore,...
    'Position',[0.9 0.79, 0.1 0.03],...
    'String',h.Mia_FLIM.OptimMethod,...
    'Value', 1,...
    'Callback',{@Do_FLIM,2:3, 2});

%%% Weighted residuals
h.Mia_FLIM.ResidualsType = {'MLE - Poisson','LSQ - Pearson','LSQ - Neyman','LSQ - Equal Wt'};
h.Text{end+1} = uicontrol(...
    'Parent',h.Mia_FLIM.Panel,...
    'Style','text',...
    'Units','normalized',...
    'FontSize',10,...
    'HorizontalAlignment','left',...
    'BackgroundColor', Look.Back,...
    'ForegroundColor', Look.Fore,...
    'Position',[0.3 0.76, 0.1 0.03],...
    'String','Residuals:');
h.Mia_FLIM.Ch(1).ResidualsType = uicontrol(...
    'Parent',h.Mia_FLIM.Panel,...
    'Style','popupmenu',...
    'Units','normalized',...
    'FontSize',10,...
    'BackgroundColor', Look.Control,...
    'ForegroundColor', Look.Fore,...
    'Position',[0.4 0.76, 0.1 0.03],...
    'String',h.Mia_FLIM.ResidualsType,...
    'Value', 1,...
    'Callback',{@Do_FLIM,2:3, 1});
h.Text{end+1} = uicontrol(...
    'Parent',h.Mia_FLIM.Panel,...
    'Style','text',...
    'Units','normalized',...
    'FontSize',10,...
    'HorizontalAlignment','left',...
    'BackgroundColor', Look.Back,...
    'ForegroundColor', Look.Fore,...
    'Position',[0.8 0.76, 0.1 0.03],...
    'String','Residuals:');
h.Mia_FLIM.Ch(2).ResidualsType = uicontrol(...
    'Parent',h.Mia_FLIM.Panel,...
    'Style','popupmenu',...
    'Units','normalized',...
    'FontSize',10,...
    'BackgroundColor', Look.Control,...
    'ForegroundColor', Look.Fore,...
    'Position',[0.9 0.76, 0.1 0.03],...
    'String',h.Mia_FLIM.ResidualsType,...
    'Value', 1,...
    'Callback',{@Do_FLIM,2:3, 2});

%%% ROI Type
h.Mia_FLIM.Ch(1).ROI_Type = uicontrol(...
    'Parent',h.Mia_FLIM.Panel,...
    'Style','popupmenu',...
    'Units','normalized',...
    'FontSize',10,...
    'BackgroundColor', Look.Back,...
    'ForegroundColor', Look.Fore,...
    'Position',[0.4 0.73, 0.1 0.03],...
    'String',{'Cursor','Selected ROIs'},...
    'Value',1,...
    'Callback',{@Do_FLIM,1:3, 1});
h.Mia_FLIM.Ch(2).ROI_Type = uicontrol(...
    'Parent',h.Mia_FLIM.Panel,...
    'Style','popupmenu',...
    'Units','normalized',...
    'FontSize',10,...
    'BackgroundColor', Look.Back,...
    'ForegroundColor', Look.Fore,...
    'Position',[0.9 0.73, 0.1 0.03],...
    'String',{'Cursor','Selected ROIs'},...
    'Value',1,...
    'Callback',{@Do_FLIM,1:3, 2});

%%% Pixel Binning
h.Mia_FLIM.Pixel_Binning_Method = {'Square','Circular'};
h.Text{end+1} = uicontrol(...
    'Parent',h.Mia_FLIM.Panel,...
    'Style','text',...
    'Units','normalized',...
    'FontSize',10,...
    'HorizontalAlignment','left',...
    'BackgroundColor', Look.Back,...
    'ForegroundColor', Look.Fore,...
    'Position',[0.3 0.7, 0.1 0.03],...
    'String','Pixel Binning:');
h.Mia_FLIM.Ch(1).Pixel_Binning = uicontrol(...
    'Parent',h.Mia_FLIM.Panel,...
    'Style','edit',...
    'Units','normalized',...
    'FontSize',10,...
    'BackgroundColor', Look.Back,...
    'ForegroundColor', Look.Fore,...
    'Position',[0.4 0.7, 0.04 0.03],...
    'String','1',...
    'Callback',{@Do_FLIM,1:3, 1});
h.Mia_FLIM.Ch(1).Pixel_Binning_Method = uicontrol(...
    'Parent',h.Mia_FLIM.Panel,...
    'Style','popupmenu',...
    'Units','normalized',...
    'FontSize',10,...
    'BackgroundColor', Look.Back,...
    'ForegroundColor', Look.Fore,...
    'Position',[0.45 0.7, 0.05 0.03],...
    'String',h.Mia_FLIM.Pixel_Binning_Method, ...
    'Value',1,...
    'Callback',{@Do_FLIM,1:3, 1});
h.Text{end+1} = uicontrol(...
    'Parent',h.Mia_FLIM.Panel,...
    'Style','text',...
    'Units','normalized',...
    'FontSize',10,...
    'HorizontalAlignment','left',...
    'BackgroundColor', Look.Back,...
    'ForegroundColor', Look.Fore,...
    'Position',[0.8 0.7, 0.1 0.03],...
    'String','Pixel Binning:');
h.Mia_FLIM.Ch(2).Pixel_Binning = uicontrol(...
    'Parent',h.Mia_FLIM.Panel,...
    'Style','edit',...
    'Units','normalized',...
    'FontSize',10,...
    'BackgroundColor', Look.Back,...
    'ForegroundColor', Look.Fore,...
    'Position',[0.9 0.7, 0.04 0.03],...
    'String','1',...
    'Callback',{@Do_FLIM,1:3, 2});
h.Mia_FLIM.Ch(2).Pixel_Binning_Method = uicontrol(...
    'Parent',h.Mia_FLIM.Panel,...
    'Style','popupmenu',...
    'Units','normalized',...
    'FontSize',10,...
    'BackgroundColor', Look.Back,...
    'ForegroundColor', Look.Fore,...
    'Position',[0.95 0.7, 0.05 0.03],...
    'String',h.Mia_FLIM.Pixel_Binning_Method, ...
    'Value',1,...
    'Callback',{@Do_FLIM,1:3, 2});

%%% FLIM ROI selection table
FLIM_ROI_TableData = {'All', true, false; 'MIA AROI', false, false}; 
h.Mia_FLIM.Ch(1).ROI_Table = uitable(...
    'Parent',h.Mia_FLIM.Panel,...
    'Units','normalized',...
    'FontSize',8,...
    'BackgroundColor', [Look.Table1;Look.Table2],...
    'ForegroundColor', Look.TableFore,...
    'ColumnName',{'ROI Name','Select', 'Delete'},...
    'ColumnWidth',num2cell([140 60 60]),...
    'ColumnEditable',[false, true, true],...
    'CellEditCallback',{@FLIM_Update_ROI_Table, 1},...
    'Position',[0.3 0.5, 0.2 0.19]);
h.Mia_FLIM.Ch(2).ROI_Table = uitable(...
    'Parent',h.Mia_FLIM.Panel,...
    'Units','normalized',...
    'FontSize',8,...
    'BackgroundColor', [Look.Table1;Look.Table2],...
    'ForegroundColor', Look.TableFore,...
    'ColumnName',{'ROI Name','Select', 'Delete'},...
    'ColumnWidth',num2cell([140 60 60]),...
    'ColumnEditable',[false, true, true],...
    'CellEditCallback',{@FLIM_Update_ROI_Table, 2},...
    'Position',[0.8 0.5, 0.2 0.19]);
h.Mia_FLIM.Ch(1).ROI_Table.Data = FLIM_ROI_TableData;
h.Mia_FLIM.Ch(2).ROI_Table.Data = FLIM_ROI_TableData;

%%% FLIM fit table
h.Mia_FLIM.Ch(1).Fit_Table = uitable(...
    'Parent',h.Mia_FLIM.Panel,...
    'Units','normalized',...
    'FontSize',8,...
    'BackgroundColor', [Look.Table1;Look.Table2],...
    'ForegroundColor', Look.TableFore,...
    'ColumnName',{'Value', 'LB', 'UB', 'Fix'},...
    'ColumnWidth',num2cell([70,40,40,30]),...
    'ColumnEditable',true,...
    'RowName',{'Tau [ns]', 'Scatter', 'Background', 'IRF Shift'},...
    'CellEditCallback',{@FLIM_Update_Fit_Table,1},...
    'Position',[0.3 0.16, 0.2 0.3]);
h.Mia_FLIM.Ch(2).Fit_Table= uitable(...
    'Parent',h.Mia_FLIM.Panel,...
    'Units','normalized',...
    'FontSize',8,...
    'BackgroundColor', [Look.Table1;Look.Table2],...
    'ForegroundColor', Look.TableFore,...
    'ColumnName',{'Value', 'LB', 'UB', 'Fix'},...
    'ColumnWidth',num2cell([70,40,40,30]),...
    'ColumnEditable',true,...
    'RowName',{'Tau [ns]', 'Scatter', 'Background', 'IRF Shift'},...
    'CellEditCallback',{@FLIM_Update_Fit_Table,2},...
    'Position',[0.8 0.16, 0.2 0.3]);
FLIM_FitData={4, 0, Inf, false;...
    0, 0, Inf, true;...
    0, 0, Inf, false;...
    0, -Inf, Inf, false};
h.Mia_FLIM.Ch(1).Fit_Table.Data = FLIM_FitData;
h.Mia_FLIM.Ch(2).Fit_Table.Data = FLIM_FitData;

%%% Checkbox for automatic fit
h.Mia_FLIM.Ch(1).AutoFit = uicontrol(...
    'Parent',h.Mia_FLIM.Panel,...
    'Style','checkbox',...
    'Units','normalized',...
    'FontSize',10,...
    'BackgroundColor', Look.Back,...
    'ForegroundColor', Look.Fore,...
    'Position',[0.3 0.12, 0.1 0.03],...
    'String','Automatic Fit',...
    'Value',1,...
    'Callback',{@Do_FLIM,1:3, 1});
h.Mia_FLIM.Ch(2).AutoFit = uicontrol(...
    'Parent',h.Mia_FLIM.Panel,...
    'Style','checkbox',...
    'Units','normalized',...
    'FontSize',10,...
    'BackgroundColor', Look.Back,...
    'ForegroundColor', Look.Fore,...
    'Position',[0.8 0.12, 0.1 0.03],...
    'String','Automatic Fit',...
    'Value',1,...
    'Callback',{@Do_FLIM,1:3, 2});
%%% Button to trigger fit routine
h.MIA_FLIM.Ch(1).FitBtn = uicontrol(...
    'Parent',h.Mia_FLIM.Panel,...
    'Style','pushbutton',...
    'Units','normalized',...
    'FontSize',10,...
    'BackgroundColor', Look.Control,...
    'ForegroundColor', Look.Fore,...
    'Position',[0.3 0.08, 0.05 0.03],...
    'String','Fit',...
    'Value',1,...
    'Callback',{@Do_FLIM,1:3, 1});
h.MIA_FLIM.Ch(2).FitBtn = uicontrol(...
    'Parent',h.Mia_FLIM.Panel,...
    'Style','pushbutton',...
    'Units','normalized',...
    'FontSize',10,...
    'BackgroundColor', Look.Control,...
    'ForegroundColor', Look.Fore,...
    'Position',[0.8 0.08, 0.05 0.03],...
    'String','Fit',...
    'Value',1,...
    'Callback',{@Do_FLIM,1:3, 2});

%%% Checkbox for initial guess
h.Mia_FLIM.Ch(1).Guess = uicontrol(...
    'Parent',h.Mia_FLIM.Panel,...
    'Style','checkbox',...
    'Units','normalized',...
    'FontSize',10,...
    'BackgroundColor', Look.Back,...
    'ForegroundColor', Look.Fore,...
    'Position',[0.4 0.12, 0.1 0.03],...
    'String','Guess Lifetime',...
    'Value',0,...
    'Tooltip', 'Make an initial guess of lifetimes with a distributed fit routine',...
    'Callback',{@Do_FLIM,1:3, 1});
h.Mia_FLIM.Ch(2).Guess = uicontrol(...
    'Parent',h.Mia_FLIM.Panel,...
    'Style','checkbox',...
    'Units','normalized',...
    'FontSize',10,...
    'BackgroundColor', Look.Back,...
    'ForegroundColor', Look.Fore,...
    'Position',[0.9 0.12, 0.1 0.03],...
    'String','Guess Lifetime',...
    'Value',0,...
    'Tooltip', 'Make an initial guess of lifetimes with a distributed fit routine',...
    'Callback',{@Do_FLIM,1:3, 2});


%% Initializes global variables %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
MIAData=[];
MIAData.Data=[];
MIAData.Cor=cell(3,2);
MIAData.FileName=cell(0);
MIAData.Use=ones(1,1);
MIAData.AR = [];
MIAData.MS = cell(2,2);
MIAData.TICS.Data = [];
MIAData.TICS.Data = [];
MIAData.TICS.Data.Int = [];
MIAData.TICS.Data.MS = [];
MIAData.IntHist = [];
MIAData.STICS = [];
MIAData.STICS_SEM = [];
MIAData.iMSD = [];
MIAData.FRAP = [];
MIAData.FLIM = [];
MIAData.FLIM.Ch = cell([2 1]);

if isempty(varargin)
    %%% delete splash screen:
    %%% if you loaded Mia from Matlab command 
    %%% if you loaded Mia from a compiled Mia.m main function
    delete(s);
end

if numel(varargin)~=1
    % make Mia visible if loaded from Matlab command without argument, from
    % PAM or from Launcher
    h.Mia.Visible = 'on';  
    guidata(h.Mia,h);
else 
    % leave Mia invisible if loaded from Matlab command with a single argument (or
    % from compiled version with a single argument. The argument is the
    % complete path (folder + filename) to a file.
    guidata(h.Mia,h);
    Mia_Command(varargin{1})
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Functions to load different data types %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function Mia_Load(obj,~,mode)
% mode
% 1 normal TIFFs
% 1.5 RLICS/RSICS
% 2 pam
% 3 custom filetypes
% 4 5 6 RGB TIFFs
% 7 phf file from Phasor

global MIAData UserValues FileInfo TcspcData
h = guidata(findobj('Tag','Mia'));

switch mode
    case {1, 1.5} %%% Loads single color TIFFs
        % case 1 is normal data
        % case 1.5 is RSICS/RLICS data - the first half of the frames is
        % the raw data, the second half of the data is the filtered data
        for f = 1:5
            % the 5 possible imaging channels in Mia
            [FileName{f},Path{f}] = uigetfile({'*.tif'}, ['Load TIFFs for channel ' num2str(f)], UserValues.File.MIAPath, 'MultiSelect', 'on');
            % if user selects 1 file, the output is a char per channel
            % if user selects >1 files, the output is a cell of chars per channel
            if all(Path{f}==0)
                % stop loading if user presses cancel without loading any file
                FileName(f)=[];
                Path(f)=[];
                break
            else
                UserValues.File.MIAPath = Path{f};
                LSUserValues(1)
            end
        end
        if isempty(Path)
            % stop loading at all if user presses cancel without loading any file
            return
        end
        UserValues.File.MIAPath = Path{1};
        LSUserValues(1);
        %%% Transforms a char FileName into a cell
        for f = 1:numel(FileName)
            % loop through the number of channels
            if ~iscell(FileName{f})
                %user selected a single file
                FileName{f}={FileName{f}};
            end
        end
           
        %%% Empty all global variables
        MIAData.Data = {};
        MIAData.AR = [];
        MIAData.MS = cell(2,2);
        MIAData.Type = mode;
        MIAData.FileName = [];
        MIAData.IntHist = [];
        % Clears correlation data and plots
        MIAData.Cor=cell(3,2);
        MIAData.TICS.Data.MS = [];
        MIAData.TICS.Data = [];
        MIAData.TICS.Data.Int = [];
        MIAData.STICS = [];
        MIAData.STICS_SEM = [];
        MIAData.RLICS = [];
        for i=1:3
            h.Plots.Cor(i,1).CData=zeros(1,1,3);
            h.Plots.Cor(i,2).ZData=zeros(1);
            h.Plots.Cor(i,2).CData=zeros(1,1,3);
            h.Mia_ICS.Axes(i,1).Visible='off';
            h.Mia_ICS.Axes(i,2).Visible='off';
            h.Mia_ICS.Axes(i,3).Visible='off';
            h.Mia_ICS.Axes(i,4).Visible='off';
            h.Plots.Cor(i,1).Visible='off';
            h.Plots.Cor(i,2).Visible='off';
            h.Plots.Cor(i,3).Visible='off';
            h.Plots.Cor(i,4).Visible='off';
            h.Plots.Cor(i,5).Visible='off';
            h.Plots.Cor(i,6).Visible='off';
            h.Plots.Cor(i,7).Visible='off';
            h.Plots.TICS(i,1).Visible = 'off';
            h.Plots.TICS(i,2).Visible = 'off';
            h.Plots.STICS(i,1).Visible = 'off';
            h.Plots.STICS(i,2).Visible = 'off';
            h.Plots.TICSImage(i).Visible = 'off';
            h.Plots.STICSImage(i,1).Visible = 'off';
            h.Mia_TICS.Image(i,1).Visible = 'off';
            h.Mia_STICS.Image(i,1).Visible = 'off';
            h.Mia_STICS.Image(i,2).Visible = 'off';
        end
        h.Mia_ICS.Frame_Slider.Min=0;
        h.Mia_ICS.Frame_Slider.Max=0;
        h.Mia_ICS.Frame_Slider.SliderStep=[1 1];
        h.Mia_ICS.Frame_Slider.Value=0;
        h.Mia_STICS.Lag_Slider.Min=0;
        h.Mia_STICS.Lag_Slider.Max=1;
        h.Mia_STICS.Lag_Slider.SliderStep=[1 1];
        h.Mia_STICS.Lag_Slider.Value=0;
        %% Clears N&B data and plots
        MIAData.NB=[];
        h.Plots.NB(1).CData=zeros(1,1);
        h.Plots.NB(2).CData=zeros(1,1);
        h.Plots.NB(3).CData=zeros(1,1);
        h.Plots.NB(4).YData=0;
        h.Plots.NB(4).XData=0;
        h.Plots.NB(5).CData=zeros(1,1);  
        %% Clears FLIM data and plots
        if isfield(MIAData.FLIM, 'IRF') % preserves loaded IRF
            IRF = MIAData.FLIM.IRF;
        else
            IRF = [];
        end
        if isfield(MIAData.FLIM, 'ROI') && ~isempty(MIAData.FLIM.ROI) && all(ishandle(MIAData.FLIM.ROI), "all")
            delete(MIAData.FLIM.ROI);
        end
        MIAData.FLIM = [];
        MIAData.FLIM.Ch = cell([2 1]);
        MIAData.FLIM.IRF = IRF;
        for i = 1:2
            % f = allchild(h.Mia_FLIM.Axes(1,i));
            % todelete = arrayfun(@(x) ~isa(x, 'matlab.graphics.primitive.Image'), f);
            % delete(f(todelete));
            h.Plots.FLIM.Image(i).CData = zeros(1,1);
            h.Plots.FLIM.Decay(i).XData = 0;
            h.Plots.FLIM.Decay(i).YData = 0;
            h.Plots.FLIM.Fit(i).XData = 0;
            h.Plots.FLIM.Fit(i).YData = 0;
            h.Mia_FLIM.Axes(1,i).Color = [0 0 0];
            h.Mia_FLIM.Ch(i).ROI_Table.Data = h.Mia_FLIM.Ch(i).ROI_Table.Data(1:2, :);
        end

        %% Loads all frames for all channels
        %jelle: somewhere check that the number of frames is the same in all channels

        for f = 1:numel(FileName)
            for i=1:numel(FileName{f})
                MIAData.FileName{f}{i}=FileName{f}{i};
                Info=imfinfo(fullfile(Path{f},FileName{f}{i}));

                %%% Automatically updates image properties
                if isfield(Info(1), 'ImageDescription') && ~isempty(Info(1).ImageDescription)
                    Mia_Update_Image_Props(h, Info(1).ImageDescription, f);
                    % Parse the following variables from the TIFF metadata
                    % 'FrameTime [s]': store in Mia Image tab
                    % 'LineTime [ms]': store in Mia Image tab
                    % 'PixelTime [us]': store in Mia Image tab
                    % 'PixelSize [nm]': store in Mia Image tab
                    % 'Frame Interval [s]': store in Mia FRAP tab
                    % 'RLICS_Scale': store in MIAData.RLICS(ch,1)
                    % 'RLICS_Offset': store in MIAData.RLICS(ch,2)

                end
                H = Info.Height;
                W = Info.Width;

                warning('off', 'MATLAB:imagesci:tiffmexutils:libtiffWarning');
                warning('off', 'imageio:tiffmexutils:libtiffWarning'); % for Matlab 2021 and later
                TIFF_Handle = Tiff(fullfile(Path{f},FileName{f}{i}),'r'); % Open tif reference

                %%% If RLICS or RSICS was used, the data contains the
                %%% unfiltered data first. MIA can load the unfiltere or the
                %%% filtered data
                if isempty(MIAData.RLICS) && mode==1
                    % normal TIFFs
                    Frames = 1:numel(Info);
                    Data = zeros([H, W, numel(Frames)], 'uint16');
                elseif ~isempty(MIAData.RLICS) && mode==1
                    % TIFFs generated via RLICS or spectral and user wants to
                    % load the raw data that is stored in the first half of the
                    % frames
                    Frames = 1:numel(Info)/2;
                    Data = zeros([H, W, numel(Frames)], 'uint16');
                else %mode = 1.5 
                    % TIFFs generated via RLICS or spectral and user wants to load the weighted data
                    % that is stored in the last half of the frames
                    Frames = (numel(Info)/2+1):numel(Info);
                    Data = zeros([H, W, numel(Frames)], 'single');
                end

                for j=Frames
                    if mod(j,10)==0
                        %%% Updates progress bar
                        Progress(((j-1)+numel(Info)*numel(FileName)*(i-1))/(numel(Info)*numel(FileName)),...
                            h.Mia_Progress_Axes,...
                            h.Mia_Progress_Text,...
                            ['Loading Frame ' num2str(j) ' of ' num2str(numel(Info)) ' in File ' num2str(i) ' of ' num2str(numel(FileName{f})) ' for Channel ' num2str(f)]);
                    end
                    %%% Reads the actual data
                    TIFF_Handle.setDirectory(j);

                    %%% Adjusts range for RLICS and RSICS data
                    if ~isempty(MIAData.RLICS) && mode==1.5
                        % TIFFs generated via RLICS or spectral and user wants to load the weighted data
                        % that is stored in the last half of the frames
                        NoF = numel(Frames);
                        Data(:,:,j-NoF) = single(TIFF_Handle.read());
                        Data(:,:,j-NoF)= Data(:,:,j-NoF)/2^16*MIAData.RLICS(f,1)+MIAData.RLICS(f,2);
                    else
                        data = TIFF_Handle.read();
                        if size(data,3) > 1
                            % 3 color TIFFs, code takes the sum
                            if i ==1
                                msgbox('3 color TIFFs, code takes the sum')
                            end
                            data = sum(data, 3);
                        end
                        Data(:,:,j) = data;
                    end
                end

                % Concatenate to existing data if available
                if i > 1
                    MIAData.Data{f,1} = cat(3, MIAData.Data{f1,1}, Data);
                else
                    MIAData.Data{f,1} = Data;
                end
                TIFF_Handle.close(); % Close tif reference
                warning('on', 'MATLAB:imagesci:tiffmexutils:libtiffWarning');
                warning('on', 'imageio:tiffmexutils:libtiffWarning'); % for Matlab 2021 and later
            end

        end

        if numel(FileName) == 1
            h.Mia_Image.Settings.Channel_Link.Value = 0;
            h.Mia_Image.Settings.Channel_Link.Visible = 'off';
            h.Mia_Image.Settings.Channel_Frame(2).Visible = 'off';
            h.Mia_Image.Settings.Channel_Frame_Slider(2).Visible = 'off';
            h.Mia_Image.Axes(2,1).Visible = 'off';
            h.Mia_Image.Axes(2,2).Visible = 'off';
            h.Plots.Image(2,1).Visible = 'off';
            h.Plots.Image(2,2).Visible = 'off';
            h.Plots.ROI(2).Visible = 'off';
            h.Mia_Image.Colorbar(2).Visible = 'off';
            %%% Clears images for display channel 2
            h.Plots.Image(2,1).CData=zeros(1,1,3);
            h.Mia_Image.Axes(2,1).XLim=[0 1]+0.5;
            h.Mia_Image.Axes(2,1).YLim=[0 1]+0.5;
            h.Plots.Image(2,2).CData=zeros(1,1,3);
            h.Mia_Image.Axes(2,2).XLim=[0 1]+0.5;
            h.Mia_Image.Axes(2,2).YLim=[0 1]+0.5;
            %%% Resets slider
            h.Mia_Image.Settings.Channel_Frame_Slider(2).SliderStep=[1 1];
            h.Mia_Image.Settings.Channel_Frame_Slider(2).Max=1;
            h.Mia_Image.Settings.Channel_Frame_Slider(2).Value=1;
            h.Mia_Image.Settings.Channel_Frame_Slider(2).Min=1;
            h.Mia_Image.Settings.Channel_Frame(2).String='1';
            h.Mia_Image.Settings.Channel_PIE(2).Visible = 'off';
            h.Mia_Image.Settings.Channel_Colormap(2).Visible = 'off';
            h.Mia_Image.Settings.Channel_Second(2).Visible = 'off';
        else
            %%% Links frames
            h.Mia_Image.Settings.Channel_Link.Value = 1;
            h.Mia_Image.Settings.Channel_Link.Visible = 'on';
            h.Mia_Image.Settings.Channel_Frame(2).Visible = 'on';
            h.Mia_Image.Settings.Channel_Frame_Slider(2).Visible = 'on';
            h.Mia_Image.Axes(2,1).Visible = 'on';
            h.Mia_Image.Axes(2,2).Visible = 'on';
            h.Plots.Image(2,1).Visible = 'on';
            h.Plots.Image(2,2).Visible = 'on';
            h.Plots.ROI(2).Visible = 'on';
            h.Mia_Image.Colorbar(2).Visible = 'on';
            h.Mia_Image.Settings.Channel_PIE(2).Visible = 'on';
            h.Mia_Image.Settings.Channel_Colormap(2).Visible = 'on';
            h.Mia_Image.Settings.Channel_Second(2).Visible = 'on';
        end

        % a checkbox to in/exclude whole frames
        MIAData.Use=ones(size(MIAData.Data{f,1},3),1);

        for i = 1:2
            % set the sliders and frame range to the values of channel 1
            h.Mia_Image.Settings.Channel_Frame_Slider(i).SliderStep=[1./size(MIAData.Data{1,1},3),10/size(MIAData.Data{1,1},3)];
            h.Mia_Image.Settings.Channel_Frame_Slider(i).Max=size(MIAData.Data{1,1},3);
            h.Mia_Image.Settings.ROI_Frames.String=['1:' num2str(size(MIAData.Data{1,1},3))];
            h.Mia_Image.Settings.Channel_Frame_Slider(i).Value=0;
            h.Mia_Image.Settings.Channel_Frame_Slider(i).Min=0;
            h.Plots.ROI(i).Position=[str2double(h.Mia_Image.Settings.ROI_PosX.String)-0.5 str2double(h.Mia_Image.Settings.ROI_PosY.String)-0.5 str2double(h.Mia_Image.Settings.ROI_SizeX.String) str2double(h.Mia_Image.Settings.ROI_SizeY.String)];
        end

        %%% Update the left image popupmenu on the "Channel" tab
        chlist = cell(0);
        for i = 1:size(MIAData.Data,1)
            chlist = [chlist, ['channel ' num2str(i)]];
        end
        chlist(end+1) = {'Nothing'};
        h.Mia_Image.Settings.Channel_PIE(1).String = chlist(1:end-1);
        h.Mia_Image.Settings.Channel_PIE(2).String = chlist;
        h.Mia_Image.Settings.Channel_PIE(1).Value = 1;
        if size(MIAData.Data,1)==1
            % the last entry is 'Nothing', but if diffusion or other maps
            % are generated, it is no longer the last entry.
            % Only here, the following code is ok, cause we start from
            % scratch.
            h.Mia_Image.Settings.Channel_PIE(2).Value = size(h.Mia_Image.Settings.Channel_PIE(2).String,1);            
        else
            h.Mia_Image.Settings.Channel_PIE(2).Value = 2;
        end
        %Update the AR channel popupmenu
        h.Mia_Image.Settings.ROI_AR_Channel.String = [{'All'}; h.Mia_Image.Settings.Channel_PIE(1).String];
        h.Mia_Image.Settings.ROI_AR_Channel.Value = 2;
        %Update the "which masks" popupmenu
        h.Mia_Image.Settings.ROI_AR_Same.String = [{'OR'}; h.Mia_Image.Settings.Channel_PIE(1).String; {'AND'}];
        h.Mia_Image.Settings.ROI_AR_Same.Value = 1;

        %%% Updates plots
        Mia_ROI([],[],1)

        MIA_Various([],[],7) %reset the AR settings

        % convert data using S and offset parameter
        Mia_Orientation([],[],5)

        Progress(1);
    case {4,5,6} %%% Loads RGB TIFFs
        % case 4 green red
        % case 5 blue green
        % case 6 blue red
        
        [FileName1,Path1] = uigetfile({'*.tif'}, 'Load TIFF', UserValues.File.MIAPath, 'MultiSelect', 'on');
        
        msgbox('Mia can handle > 2 channels but this is not implemented yet. See code for suggestions.')
        % Mia can handle > 2 channels but this is not implemented yet when openin RGB data.
        % This loading function should be edited to load all three RGB channels.

        if all(Path1==0)
            return
        end
        UserValues.File.MIAPath = Path1;
        Path2 = Path1;
        LSUserValues(1);
        %%% Transforms FileName into cell array
        if ~iscell(FileName1)
            FileName1={FileName1};
        end
        FileName2 = FileName1;      
        MIAData.Data = {};
        MIAData.Type = mode;
        MIAData.FileName = [];
        MIAData.IntHist = [];
        %% Clears correlation data and plots
        MIAData.Cor=cell(3,2);
        MIAData.TICS.Data.MS = [];
        MIAData.TICS.Data = [];
        MIAData.TICS.Data.Int = [];
        MIAData.STICS = [];
        MIAData.STICS_SEM = [];
        MIAData.RLICS = [];
        for i=1:3
            h.Plots.Cor(i,1).CData=zeros(1,1,3);
            h.Plots.Cor(i,2).ZData=zeros(1);
            h.Plots.Cor(i,2).CData=zeros(1,1,3);
            h.Mia_ICS.Axes(i,1).Visible='off';
            h.Mia_ICS.Axes(i,2).Visible='off';
            h.Mia_ICS.Axes(i,3).Visible='off';
            h.Mia_ICS.Axes(i,4).Visible='off';
            h.Plots.Cor(i,1).Visible='off';
            h.Plots.Cor(i,2).Visible='off';
            h.Plots.Cor(i,3).Visible='off';
            h.Plots.Cor(i,4).Visible='off';
            h.Plots.Cor(i,5).Visible='off';
            h.Plots.Cor(i,6).Visible='off';
            h.Plots.Cor(i,7).Visible='off';
            h.Plots.TICS(i,1).Visible = 'off';
            h.Plots.TICS(i,2).Visible = 'off';
            h.Plots.STICS(i,1).Visible = 'off';
            h.Plots.STICS(i,2).Visible = 'off';
            h.Plots.TICSImage(i).Visible = 'off';
            h.Plots.STICSImage(i,1).Visible = 'off';
            h.Mia_TICS.Image(i,1).Visible = 'off';
            h.Mia_STICS.Image(i,1).Visible = 'off';
            h.Mia_STICS.Image(i,2).Visible = 'off';
        end
        h.Mia_ICS.Frame_Slider.Min=0;
        h.Mia_ICS.Frame_Slider.Max=0;
        h.Mia_ICS.Frame_Slider.SliderStep=[1 1];
        h.Mia_ICS.Frame_Slider.Value=0;
        h.Mia_STICS.Lag_Slider.Min=0;
        h.Mia_STICS.Lag_Slider.Max=1;
        h.Mia_STICS.Lag_Slider.SliderStep=[1 1];
        h.Mia_STICS.Lag_Slider.Value=0;
        %% Clears N&B data and plots
        MIAData.NB=[];
        h.Plots.NB(1).CData=zeros(1,1);
        h.Plots.NB(2).CData=zeros(1,1);
        h.Plots.NB(3).CData=zeros(1,1);
        h.Plots.NB(4).YData=0;
        h.Plots.NB(4).XData=0;
        h.Plots.NB(5).CData=zeros(1,1);        
        %% Loads all frames for channel 1
        for i=1:numel(FileName1)  
            MIAData.FileName{1}{i}=FileName1{i};
            Info=imfinfo(fullfile(Path1,FileName1{i}));
            
            %%% Automatically updates image properties
            if isfield(Info(1), 'ImageDescription') && ~isempty(Info(1).ImageDescription)
                Mia_Update_Image_Props(h, Info(1).ImageDescription, 1)
            end
            H = Info.Height;
            W = Info.Width;
            
            warning('off', 'MATLAB:imagesci:tiffmexutils:libtiffWarning');
            warning('off', 'imageio:tiffmexutils:libtiffWarning'); % for Matlab 2021 and later
            TIFF_Handle = Tiff(fullfile(Path1,FileName1{i}),'r'); % Open tif reference
            
                Frames = 1:numel(Info);
                Data = zeros([H, W, numel(Frames)], 'uint16');
            for j=Frames
                if mod(j,10)==0
                    %%% Updates progress bar
                    Progress(((j-1)+numel(Info)*(i-1))/(numel(Info)*numel(FileName1)),...
                        h.Mia_Progress_Axes,...
                        h.Mia_Progress_Text,...
                        ['Loading Frame ' num2str(j) ' of ' num2str(numel(Info)) ' in File ' num2str(i) ' of ' num2str(numel(FileName1)) ' for Channel 1']);
                end
                %%% Reads the actual data
                TIFF_Handle.setDirectory(j);
               
                data = TIFF_Handle.read();
                if mode == 4
                    data = data(:,:,2); %green
                else %mode == 5 or 6
                    data = data(:,:,3); %blue
                    
                end
                Data(:,:,j) = data;
            end
            
            % Concatenate to existing data if available
            if i > 1
                MIAData.Data{1,1} = cat(3, MIAData.Data{1,1}, Data);
            else
                MIAData.Data{1,1} = Data;
            end
            TIFF_Handle.close(); % Close tif reference
            warning('on', 'MATLAB:imagesci:tiffmexutils:libtiffWarning');
            warning('on', 'imageio:tiffmexutils:libtiffWarning'); % for Matlab 2021 and later
        end
        %% Updates frame settings for channel 1
        %%% Unlinks frames
        h.Mia_Image.Settings.Channel_Link.Value = 0;
        h.Mia_Image.Settings.Channel_Link.Visible = 'off';
        h.Mia_Image.Settings.Channel_Frame(2).Visible = 'off';
        h.Mia_Image.Settings.Channel_Frame_Slider(2).Visible = 'off';
        h.Mia_Image.Axes(2,1).Visible = 'off';
        h.Mia_Image.Axes(2,2).Visible = 'off';
        h.Plots.Image(2,1).Visible = 'off';
        h.Plots.Image(2,2).Visible = 'off';
        h.Plots.ROI(2).Visible = 'off';
        h.Mia_Image.Colorbar(2).Visible = 'off';
        h.Mia_Image.Settings.Channel_Frame_Slider(1).SliderStep=[1./size(MIAData.Data{1,1},3),10/size(MIAData.Data{1,1},3)];
        h.Mia_Image.Settings.Channel_Frame_Slider(1).Max=size(MIAData.Data{1,1},3);
        h.Mia_Image.Settings.ROI_Frames.String=['1:' num2str(size(MIAData.Data{1,1},3))];
        h.Mia_Image.Settings.Channel_Frame_Slider(1).Value=0;  
        h.Mia_Image.Settings.Channel_Frame_Slider(1).Min=0;
        MIAData.Use=ones(size(MIAData.Data{1,1},3),1);
        %% Stops function, if only one channel was loaded and clear channel 2
        if all(Path2==0) 
            %%% Clears images
            h.Plots.Image(2,1).CData=zeros(1,1,3);
            h.Mia_Image.Axes(2,1).XLim=[0 1]+0.5;
            h.Mia_Image.Axes(2,1).YLim=[0 1]+0.5;
            h.Plots.Image(2,2).CData=zeros(1,1,3);
            h.Mia_Image.Axes(2,2).XLim=[0 1]+0.5;
            h.Mia_Image.Axes(2,2).YLim=[0 1]+0.5;
            %%% Resets slider
            h.Mia_Image.Settings.Channel_Frame_Slider(2).SliderStep=[1 1];
            h.Mia_Image.Settings.Channel_Frame_Slider(2).Max=1;
            h.Mia_Image.Settings.Channel_Frame_Slider(2).Value=1;
            h.Mia_Image.Settings.Channel_Frame_Slider(2).Min=1;
            h.Mia_Image.Settings.Channel_Frame(2).String='1';
            Progress(1);
            %%% Updates plot
            Mia_ROI([],[],1)  
            return
        end
        %% Loads all frames for channel 2
        for i=1:numel(FileName2)
            MIAData.FileName{2}{i}=FileName2{i};
            Info=imfinfo(fullfile(Path2,FileName2{i}));
                        
            %%% Automatically updates image properties
            if isfield(Info(1), 'ImageDescription') && ~isempty(Info(1).ImageDescription)
                Mia_Update_Image_Props(h, Info(1).ImageDescription, 2)
            end
            H = Info.Height;
            W = Info.Width;
            
            warning('off', 'MATLAB:imagesci:tiffmexutils:libtiffWarning');
            warning('off', 'imageio:tiffmexutils:libtiffWarning'); % for Matlab 2021 and later
            TIFF_Handle = Tiff(fullfile(Path2,FileName2{i}),'r'); % Open tif reference
            
            Frames = 1:numel(Info);
            Data = zeros([H, W, numel(Frames)], 'uint16');
            
            
            for j=Frames
                if mod(j,10)==0
                    %%% Updates progress bar
                    Progress(((j-1)+numel(Info)*(i-1))/(numel(Info)*numel(FileName2)),...
                        h.Mia_Progress_Axes,...
                        h.Mia_Progress_Text,...
                        ['Loading Frame ' num2str(j) ' of ' num2str(numel(Info)) ' in File ' num2str(i) ' of ' num2str(numel(FileName2)) ' for Channel 2']);
                end
                TIFF_Handle.setDirectory(j);
                
                
                data = TIFF_Handle.read();
                if mode == 5
                    data = data(:,:,2); % green
                else
                    data = data(:,:,1);
                end
                Data(:,:,j) = data;
            end
            
            % Concatenate to existing data if available
            if i>1
                MIAData.Data{2,1} = cat(3, MIAData.Data{2,1}, Data);
            else
                MIAData.Data{2,1} = Data;
            end
            TIFF_Handle.close(); % Close tif reference
            warning('on', 'MATLAB:imagesci:tiffmexutils:libtiffWarning');
            warning('on', 'imageio:tiffmexutils:libtiffWarning'); % for Matlab 2021 and later
        end
        % convert data using S and offset parameter
        Mia_Orientation([],[],5)

        %%% Updates frame settings for channel 2
        h.Mia_Image.Settings.Channel_Frame_Slider(2).SliderStep=[1./size(MIAData.Data{2,1},3),10/size(MIAData.Data{2,1},3)];
        h.Mia_Image.Settings.Channel_Frame_Slider(2).Max=size(MIAData.Data{2,1},3);
        h.Mia_Image.Settings.Channel_Frame_Slider(2).Value=0;
        h.Mia_Image.Settings.Channel_Frame_Slider(2).Min=0;
        h.Plots.ROI(2).Position=[str2double(h.Mia_Image.Settings.ROI_PosX.String)-0.5 str2double(h.Mia_Image.Settings.ROI_PosY.String)-0.5 str2double(h.Mia_Image.Settings.ROI_SizeX.String) str2double(h.Mia_Image.Settings.ROI_SizeY.String)];
        h.Plots.ROI(4).Position=[str2double(h.Mia_Image.Settings.ROI_PosX.String)-0.5 str2double(h.Mia_Image.Settings.ROI_PosY.String)-0.5 str2double(h.Mia_Image.Settings.ROI_SizeX.String) str2double(h.Mia_Image.Settings.ROI_SizeY.String)];
        %%% Links frames
        h.Mia_Image.Settings.Channel_Link.Value = 1;
        h.Mia_Image.Settings.Channel_Link.Visible = 'on';
        h.Mia_Image.Settings.Channel_Frame(2).Visible = 'on';
        h.Mia_Image.Settings.Channel_Frame_Slider(2).Visible = 'on';
        h.Mia_Image.Axes(2,1).Visible = 'on';
        h.Mia_Image.Axes(2,2).Visible = 'on';
        h.Plots.Image(2,1).Visible = 'on';
        h.Plots.Image(2,2).Visible = 'on';
        h.Plots.ROI(2).Visible = 'on';
        h.Mia_Image.Colorbar(2).Visible = 'on';
        
        %%% Updates plots
        Mia_ROI([],[],1)

        MIA_Various([],[],7) %reset the AR settings

        % convert data using S and offset parameter
        Mia_Orientation([],[],5)

        Progress(1);
    case 2 %%% Loads data from Pam
        %% Aborts, if not Data is loaded or Pam is closed
        if isempty(findobj('Tag','Pam'))
            return;
        end
        
        if strcmp(obj.Tag, 'Mia_Load_Pam_All')
            msgbox('Mia can handle > 2 channels but this is not implemented yet for PAM data. See code for suggestions.')
            % Mia can handle > 2 channels but this is not implemented yet when transferring data from PAM.
            % This loading function should be edited to transform all available PIE channels.
            %take all PIE channels
        end

        Pam = guidata(findobj('Tag','Pam'));        
        if isempty(Pam) || all(all(cellfun(@isempty,TcspcData.MT)))
           return 
        end
        
        Sel = h.Mia_Image.Settings.Channel_PIE(1).Value;

        %% Clear current Data
        MIAData.Data=[];
        MIAData.Data{Sel,1} = single.empty(0,0,0);
        %% Clears correlation data and plots
        MIAData.Cor=cell(3,2);
        MIAData.TICS.Data.MS = [];
        MIAData.TICS.Data = [];
        MIAData.TICS.Data.Int = [];
        for i=1:3
            h.Plots.Cor(i,1).CData=zeros(1,1,3);
            h.Plots.Cor(i,2).ZData=zeros(1);
            h.Plots.Cor(i,2).CData=zeros(1,1,3);
            h.Mia_ICS.Axes(i,1).Visible='off';
            h.Mia_ICS.Axes(i,2).Visible='off';
            h.Mia_ICS.Axes(i,3).Visible='off';
            h.Mia_ICS.Axes(i,4).Visible='off';
            h.Plots.Cor(i,1).Visible='off';
            h.Plots.Cor(i,2).Visible='off';
            h.Plots.Cor(i,3).Visible='off';
            h.Plots.Cor(i,4).Visible='off';
            h.Plots.Cor(i,5).Visible='off';
            h.Plots.Cor(i,6).Visible='off';
            h.Plots.Cor(i,7).Visible='off';
        end
        h.Mia_ICS.Frame_Slider.Min=0;
        h.Mia_ICS.Frame_Slider.Max=0;
        h.Mia_ICS.Frame_Slider.SliderStep=[1 1];
        h.Mia_ICS.Frame_Slider.Value=0;        
        %% Clears N&B data and plots
        MIAData.NB=[];
        h.Plots.NB(1).CData=zeros(1,1);
        h.Plots.NB(2).CData=zeros(1,1);
        h.Plots.NB(3).CData=zeros(1,1);
        h.Plots.NB(4).YData=0;
        h.Plots.NB(4).XData=0;
        h.Plots.NB(5).CData=zeros(1,1);                 
        %% Extracts data from Pam for channel 1
        %%% Automatically updates image properties
        UserValues.File.MIAPath = FileInfo.Path;
        LSUserValues(1);
        MIAData.Type = mode;
        MIAData.FileName{1} = FileInfo.FileName;
        h.Mia_Image.Settings.Image_Frame.String = num2str(mean(diff(FileInfo.ImageTimes)));
        
        if ~isempty(FileInfo.LineStops)
            h.Mia_Image.Settings.Image_Pixel.String = num2str(mean(mean(FileInfo.LineStops-FileInfo.LineTimes))./FileInfo.Lines*1000000);
            h.Mia_Image.Settings.Image_Pixel.Stringh.Mia_Image.Settings.Image_Line.String = num2str(mean(mean(diff(FileInfo.LineTimes,1,2)))*1000);
        else
            h.Mia_Image.Settings.Image_Pixel.String = num2str(mean(diff(FileInfo.ImageTimes))./FileInfo.Lines^2*1000000);
            h.Mia_Image.Settings.Image_Line.String = num2str(mean(diff(FileInfo.ImageTimes))./FileInfo.Lines*1000);
        end
        UserValues.MIA.FitParams.PixTime = str2double(h.Mia_Image.Settings.Image_Pixel.String);

        h.Mia_ICS.Fit_Table.Data(15,:) = {num2str(mean(diff(FileInfo.ImageTimes))./FileInfo.Lines*1000)};
        h.Mia_ICS.Fit_Table.Data(13,:) = {num2str(mean(diff(FileInfo.ImageTimes))./FileInfo.Lines^2*1000000)};
        
        if isfield(FileInfo, 'Fabsurf') && ~isempty(FileInfo.Fabsurf)
            h.Mia_Image.Settings.Pixel_Size.String = num2str(FileInfo.Fabsurf.Imagesize/FileInfo.Lines*1000);
            h.Mia_ICS.Fit_Table.Data(11,:) = {num2str(FileInfo.Fabsurf.Imagesize/FileInfo.Lines*1000)};
        else
            h.Mia_Image.Settings.Pixel_Size.String = '50';
            h.Mia_ICS.Fit_Table.Data(11,:) = {'50'};
        end
            
        if UserValues.PIE.Detector(Sel)~=0
            if ~isempty(TcspcData.MT{UserValues.PIE.Detector(Sel),UserValues.PIE.Router(Sel)}(...
                    TcspcData.MI{UserValues.PIE.Detector(Sel),UserValues.PIE.Router(Sel)}>=UserValues.PIE.From(Sel) &...
                    TcspcData.MI{UserValues.PIE.Detector(Sel),UserValues.PIE.Router(Sel)}<=UserValues.PIE.To(Sel)))
                
                [MIAData.Data{Sel,1},~] = CalculateImage(TcspcData.MT{UserValues.PIE.Detector(Sel),UserValues.PIE.Router(Sel)}(...
                    TcspcData.MI{UserValues.PIE.Detector(Sel),UserValues.PIE.Router(Sel)}>=UserValues.PIE.From(Sel) &...
                    TcspcData.MI{UserValues.PIE.Detector(Sel),UserValues.PIE.Router(Sel)}<=UserValues.PIE.To(Sel))*FileInfo.ClockPeriod, 3);
            else
                MIAData.Data{Sel,1} = 0;
                msgbox('Empty PIE channel');
            end
            
        else
            PIE_MT=[];
            for i=UserValues.PIE.Combined{Sel}
                PIE_MT = [PIE_MT; TcspcData.MT{UserValues.PIE.Detector(i),UserValues.PIE.Router(i)}(...
                    TcspcData.MI{UserValues.PIE.Detector(i),UserValues.PIE.Router(i)}>=UserValues.PIE.From(i) &...
                    TcspcData.MI{UserValues.PIE.Detector(i),UserValues.PIE.Router(i)}<=UserValues.PIE.To(i))];
            end
            if ~isempty(PIE_MT)
                [MIAData.Data{Sel,1}, ~] = CalculateImage(PIE_MT*FileInfo.ClockPeriod, 3);
            else
                MIAData.Data{Sel,1} = 0;
                msgbox('Empty PIE channel');
            end
            clear PIE_MT;
            
        end
        
        %% Updates frame settings for channel 1
        %%% Unlinks framses
        h.Mia_Image.Settings.Channel_Link.Value = 0;
        h.Mia_Image.Settings.Channel_Link.Visible = 'off';
        h.Mia_Image.Settings.Channel_Frame(2).Visible = 'off';
        h.Mia_Image.Settings.Channel_Frame_Slider(2).Visible = 'off';
        h.Mia_Image.Axes(2,1).Visible = 'off';
        h.Mia_Image.Axes(2,2).Visible = 'off';
        h.Plots.Image(2,1).Visible = 'off';
        h.Plots.Image(2,2).Visible = 'off';
        h.Plots.ROI(2).Visible = 'off';
        h.Mia_Image.Colorbar(2).Visible = 'off';
        h.Mia_Image.Settings.Channel_Frame_Slider(1).SliderStep=[1./size(MIAData.Data{Sel,1},3),10/size(MIAData.Data{Sel,1},3)];
        h.Mia_Image.Settings.Channel_Frame_Slider(1).Max=size(MIAData.Data{Sel,1},3);
        h.Mia_Image.Settings.ROI_Frames.String=['1:' num2str(size(MIAData.Data{Sel,1},3))];
        h.Mia_Image.Settings.Channel_Frame_Slider(1).Value=0;
        h.Mia_Image.Settings.Channel_Frame_Slider(1).Min=0;
        MIAData.Use=ones(size(MIAData.Data{Sel,1},3),1);
        %% Stops function, if only one channel was loaded and clear channel 2
        if h.Mia_Image.Settings.Channel_PIE(2).Value == numel(h.Mia_Image.Settings.Channel_PIE(2).String)
            %%% Clears images
            h.Plots.Image(2,1).CData=zeros(1,1,3);
            h.Mia_Image.Axes(2,1).XLim=[0 1]+0.5;
            h.Mia_Image.Axes(2,1).YLim=[0 1]+0.5;
            h.Plots.Image(2,2).CData=zeros(1,1,3);
            h.Mia_Image.Axes(2,2).XLim=[0 1]+0.5;
            h.Mia_Image.Axes(2,2).YLim=[0 1]+0.5;
            %%% Resets slider
            h.Mia_Image.Settings.Channel_Frame_Slider(2).SliderStep=[1 1];
            h.Mia_Image.Settings.Channel_Frame_Slider(2).Max=1;
            h.Mia_Image.Settings.Channel_Frame_Slider(2).Value=1;
            h.Mia_Image.Settings.Channel_Frame_Slider(2).Min=1;
            h.Mia_Image.Settings.Channel_Frame(2).String='1';
            Progress(1);
            %%% Updates plot
            Mia_ROI([],[],1)
            return;
        end
        %% Extracts data from Pam for channel 2
        MIAData.FileName{2} = FileInfo.FileName;
        Sel = h.Mia_Image.Settings.Channel_PIE(2).Value;
        %%% Gets the photons        
        if UserValues.PIE.Detector(Sel)~=0
            if ~isempty(TcspcData.MT{UserValues.PIE.Detector(Sel),UserValues.PIE.Router(Sel)}(...
                    TcspcData.MI{UserValues.PIE.Detector(Sel),UserValues.PIE.Router(Sel)}>=UserValues.PIE.From(Sel) &...
                    TcspcData.MI{UserValues.PIE.Detector(Sel),UserValues.PIE.Router(Sel)}<=UserValues.PIE.To(Sel)))
                
                [MIAData.Data{Sel,1},~] = CalculateImage(TcspcData.MT{UserValues.PIE.Detector(Sel),UserValues.PIE.Router(Sel)}(...
                    TcspcData.MI{UserValues.PIE.Detector(Sel),UserValues.PIE.Router(Sel)}>=UserValues.PIE.From(Sel) &...
                    TcspcData.MI{UserValues.PIE.Detector(Sel),UserValues.PIE.Router(Sel)}<=UserValues.PIE.To(Sel))*FileInfo.ClockPeriod, 3);
            else
                MIAData.Data{Sel,1} = 0;
                msgbox('Empty PIE channel');
            end
        else
            PIE_MT=[];
            for i=UserValues.PIE.Combined{Sel}
                PIE_MT = [PIE_MT; TcspcData.MT{UserValues.PIE.Detector(i),UserValues.PIE.Router(i)}(...
                    TcspcData.MI{UserValues.PIE.Detector(i),UserValues.PIE.Router(i)}>=UserValues.PIE.From(i) &...
                    TcspcData.MI{UserValues.PIE.Detector(i),UserValues.PIE.Router(i)}<=UserValues.PIE.To(i))];
            end
            if ~isempty(PIE_MT)
                [MIAData.Data{Sel,1}, ~] = CalculateImage(PIE_MT*FileInfo.ClockPeriod, 3);
            else
                MIAData.Data{Sel,1} = 0;
                msgbox('Empty PIE channel');
            end
            clear PiE_MT;
        end
        %% Updates frame settings for channel 2
        h.Mia_Image.Settings.Channel_Frame_Slider(2).SliderStep=[1./size(MIAData.Data{Sel,1},3),10/size(MIAData.Data{Sel,1},3)];
        h.Mia_Image.Settings.Channel_Frame_Slider(2).Max=size(MIAData.Data{Sel,1},3);
        h.Mia_Image.Settings.Channel_Frame_Slider(2).Value=0;
        h.Mia_Image.Settings.Channel_Frame_Slider(2).Min=0;
        h.Plots.ROI(2).Position=[str2double(h.Mia_Image.Settings.ROI_PosX.String)-0.5 str2double(h.Mia_Image.Settings.ROI_PosY.String)-0.5 str2double(h.Mia_Image.Settings.ROI_SizeX.String) str2double(h.Mia_Image.Settings.ROI_SizeY.String)];       
        
        %%% Links frames
        h.Mia_Image.Settings.Channel_Link.Value = 1;
        h.Mia_Image.Settings.Channel_Link.Visible = 'on';
        h.Mia_Image.Settings.Channel_Frame(2).Visible = 'on';
        h.Mia_Image.Settings.Channel_Frame_Slider(2).Visible = 'on';
        h.Mia_Image.Axes(2,1).Visible = 'on';
        h.Mia_Image.Axes(2,2).Visible = 'on';
        h.Plots.Image(2,1).Visible = 'on';
        h.Plots.Image(2,2).Visible = 'on';
        h.Plots.ROI(2).Visible = 'on';
        h.Mia_Image.Colorbar(2).Visible = 'on';
        %%% Updates plots
        Mia_ROI([],[],1)

        MIA_Various([],[],7) %reset the AR settings

        % convert data using S and offset parameter
        Mia_Orientation([],[],5)

        Progress(1);
    case 3 %%% Loads custom data formats
        MIAData.RLICS = []; %clear this here, not inside the custom read-in functions
        MIA_CustomFileType([],[],2);
        if isempty(MIAData.Data) %user pressed cancel
            return
        end
        if mode == 3 && numel(str2num(h.Mia_Image.Settings.Custom(9).String))>1
            % user loaded .czi file and specified a range of z-planes
            % start the frame range from 2 to ensure proper moving average
            % correction
            h.Mia_Image.Settings.ROI_Frames.String=['2:' num2str(size(MIAData.Data{1,1},3))];
        end
        %%% Updates plots
        Mia_ROI([],[],1)

        MIA_Various([],[],[7, 1.5, 9]) %reset the AR settings, toggle display of axes, raster or not

        % convert data using S and offset parameter
        Mia_Orientation([],[],5)

        Progress(1);
    case 7 %%% Load .phf file generated from phasor tab
        % could be better just generated from PAM itself
        [FileName,Path] = uigetfile({'*.phf'}, 'Choose a phasor file', UserValues.File.PhasorPath, 'MultiSelect', 'off');
        UserValues.File.MIAPath = Path;

        LSUserValues(1);
    
        MIAData.Data = {};
        MIAData.Type = mode;
        MIAData.FileName = [];
        MIAData.IntHist = [];
        %% Clears correlation data and plots
        MIAData.Cor=cell(3,2);
        MIAData.TICS.Data.MS = [];
        MIAData.TICS.Data = [];
        MIAData.TICS.Data.Int = [];
        MIAData.STICS = [];
        MIAData.STICS_SEM = [];
        MIAData.RLICS = [];
        for i=1:3
            h.Plots.Cor(i,1).CData=zeros(1,1,3);
            h.Plots.Cor(i,2).ZData=zeros(1);
            h.Plots.Cor(i,2).CData=zeros(1,1,3);
            h.Mia_ICS.Axes(i,1).Visible='off';
            h.Mia_ICS.Axes(i,2).Visible='off';
            h.Mia_ICS.Axes(i,3).Visible='off';
            h.Mia_ICS.Axes(i,4).Visible='off';
            h.Plots.Cor(i,1).Visible='off';
            h.Plots.Cor(i,2).Visible='off';
            h.Plots.Cor(i,3).Visible='off';
            h.Plots.Cor(i,4).Visible='off';
            h.Plots.Cor(i,5).Visible='off';
            h.Plots.Cor(i,6).Visible='off';
            h.Plots.Cor(i,7).Visible='off';
            h.Plots.TICS(i,1).Visible = 'off';
            h.Plots.TICS(i,2).Visible = 'off';
            h.Plots.STICS(i,1).Visible = 'off';
            h.Plots.STICS(i,2).Visible = 'off';
            h.Plots.TICSImage(i).Visible = 'off';
            h.Plots.STICSImage(i,1).Visible = 'off';
            h.Mia_TICS.Image(i,1).Visible = 'off';
            h.Mia_STICS.Image(i,1).Visible = 'off';
            h.Mia_STICS.Image(i,2).Visible = 'off';
        end
        h.Mia_ICS.Frame_Slider.Min=0;
        h.Mia_ICS.Frame_Slider.Max=0;
        h.Mia_ICS.Frame_Slider.SliderStep=[1 1];
        h.Mia_ICS.Frame_Slider.Value=0;
        h.Mia_STICS.Lag_Slider.Min=0;
        h.Mia_STICS.Lag_Slider.Max=1;
        h.Mia_STICS.Lag_Slider.SliderStep=[1 1];
        h.Mia_STICS.Lag_Slider.Value=0;
        %% Clears N&B data and plots
        MIAData.NB=[];
        h.Plots.NB(1).CData=zeros(1,1);
        h.Plots.NB(2).CData=zeros(1,1);
        h.Plots.NB(3).CData=zeros(1,1);
        h.Plots.NB(4).YData=0;
        h.Plots.NB(4).XData=0;
        h.Plots.NB(5).CData=zeros(1,1);
        %% Loads all frames for channel 1
        
        data=load(fullfile(Path, FileName),'-mat');
        % hardcoded rescaling step to convert mean lifetime in TCSPC
        % channels to nanoseconds
        data.Mean_LT(isnan(data.Mean_LT)) = 0;
        data.Mean_LT = (data.Mean_LT)/4096*16.67;
        MIAData.Data{1,1} = single(data.Mean_LT);
        delete data
        
        %% Updates frame settings for channel 1
        %%% Unlinks framses
        h.Mia_Image.Settings.Channel_Link.Value = 0;
        h.Mia_Image.Settings.Channel_Link.Visible = 'off';
        h.Mia_Image.Settings.Channel_Frame(2).Visible = 'off';
        h.Mia_Image.Settings.Channel_Frame_Slider(2).Visible = 'off';
        h.Mia_Image.Axes(2,1).Visible = 'off';
        h.Mia_Image.Axes(2,2).Visible = 'off';
        h.Plots.Image(2,1).Visible = 'off';
        h.Plots.Image(2,2).Visible = 'off';
        h.Plots.ROI(2).Visible = 'off';
        h.Mia_Image.Colorbar(2).Visible = 'off';
        h.Mia_Image.Settings.Channel_Frame_Slider(1).SliderStep=[1./size(MIAData.Data{1,1},3),10/size(MIAData.Data{1,1},3)];
        h.Mia_Image.Settings.Channel_Frame_Slider(1).Max=size(MIAData.Data{1,1},3);
        h.Mia_Image.Settings.ROI_Frames.String=['1:' num2str(size(MIAData.Data{1,1},3))];
        
        h.Mia_Image.Settings.Channel_Frame_Slider(1).Value=0;  
        h.Mia_Image.Settings.Channel_Frame_Slider(1).Min=0;
        MIAData.Use=ones(size(MIAData.Data{1,1},3),1);

        %%% Clears images
        h.Plots.Image(2,1).CData=zeros(1,1,3);
        h.Mia_Image.Axes(2,1).XLim=[0 1]+0.5;
        h.Mia_Image.Axes(2,1).YLim=[0 1]+0.5;
        h.Plots.Image(2,2).CData=zeros(1,1,3);
        h.Mia_Image.Axes(2,2).XLim=[0 1]+0.5;
        h.Mia_Image.Axes(2,2).YLim=[0 1]+0.5;
        %%% Resets slider
        h.Mia_Image.Settings.Channel_Frame_Slider(2).SliderStep=[1 1];
        h.Mia_Image.Settings.Channel_Frame_Slider(2).Max=1;
        h.Mia_Image.Settings.Channel_Frame_Slider(2).Value=1;
        h.Mia_Image.Settings.Channel_Frame_Slider(2).Min=1;
        h.Mia_Image.Settings.Channel_Frame(2).String='1';
        Progress(1);
        %%% Updates plot
        Mia_ROI([],[],1)
        Progress(1);
end


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Updates mia plots %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function Update_Plots(obj,~,mode,fast)
%%% Mode
%%% 1: Intensity images
%%% 2: ICS
%%% 3: N&B 
%%% 4: Intensity traces/histograms/PCH
%%% 5: TICS
%%% 6: STICS/iMSD
%%% 7: FLIM

global MIAData UserValues
Fig = gcf; 
%%% This speeds display up
if strcmp(Fig.Tag,'Mia') 
    h = guidata(Fig);
else
    h = guidata(findobj('Tag','Mia'));
end
Save_MIA_UserValues(h)

if isempty(MIAData.Data)
    % no data loaded
    return
else
    % data loaded
    h.Mia_Progress_Text.String = 'Updating plots';
    h.Mia_Progress_Axes.Color=[1 0 0];
    drawnow;
end

%% Image info update
if h.Mia_Image.Settings.kHz.Value == 1
    cnts = 'Mean Countrate [kHz]:';
    divd = str2double(h.Mia_Image.Settings.Image_Pixel.String)/1e3;
else
    cnts = 'Mean Counts:';
    divd = 1;
end

% jelle: is this the count rate in the AR? or in the total image?
% this has to be adapted to display all channels, even if >2
if size(MIAData.Data,1)==1 && size(MIAData.Data,2)==2
    h.Mia_Image.Settings.Image_Mean_CR.String = {cnts;...
                            [num2str(mean2(MIAData.Data{1,2})/divd) ' / -']};
elseif size(MIAData.Data,1)>1 && size(MIAData.Data,2)==2
    h.Mia_Image.Settings.Image_Mean_CR.String = {cnts;...
                            [num2str(mean2(MIAData.Data{1,2})/divd) ' / '...
                             num2str(mean2(MIAData.Data{2,2})/divd)]};
else 
    h.Mia_Image.Settings.Image_Mean_CR.String = [cnts ' - / -'];
end

if size(MIAData.Data,1)>1 && ~strcmp(h.Mia_Image.Settings.Channel_PIE(2).String{h.Mia_Image.Settings.Channel_PIE(2).Value},'Nothing')
    channel = 1:2;
else
    channel = 1;
end

%% Plots intensity images
if any(mode==1)   
    for i = channel
        % Depending on how many imaging channels the user opens (1, 2,
        % 3...?) the dropdown contains a number of actual imaging channels.
        % After that, for channel two, the next option is 'nothing'.
        % After that, possible calculated images are stored.
        d = h.Mia_Image.Settings.Channel_PIE(i).Value;
        if isempty(find(strcmp(h.Mia_Image.Settings.Channel_PIE(i).String{d}, [UserValues.MIA.Maps {'Nothing'}])))
            plotframe = 1;
        else
            plotframe = 0;
        end
        %% Selects colormap
        switch h.Mia_Image.Settings.Channel_Colormap(i).Value
            case 1 %%% Gray
                h.Mia_Image.Axes(i,2).Color = [1 0 0];
                h.Mia_Image.Settings.Channel_Colormap(i).BackgroundColor = UserValues.Look.Control;
                AlphaRatio = 3;
            case 2 %%% Jet
                h.Mia_Image.Axes(i,2).Color = [0 0 0];
                h.Mia_Image.Settings.Channel_Colormap(i).BackgroundColor = UserValues.Look.Control;
                AlphaRatio = 0.25;
            case 3 %%% Hot
                h.Mia_Image.Axes(i,2).Color = [0 1 0];
                h.Mia_Image.Settings.Channel_Colormap(i).BackgroundColor = UserValues.Look.Control;
                AlphaRatio = 2;
            case 4 %%% HSV
                h.Mia_Image.Axes(i,2).Color = [0 0 0];
                h.Mia_Image.Settings.Channel_Colormap(i).BackgroundColor = UserValues.Look.Control;
                AlphaRatio = 0.5;
            case 5 %%% Custom
                h.Mia_Image.Axes(i,2).Color = 1 - h.Mia_Image.Settings.Channel_Colormap(i).UserData;
                h.Mia_Image.Settings.Channel_Colormap(i).BackgroundColor = h.Mia_Image.Settings.Channel_Colormap(i).UserData;
                AlphaRatio = 3;
            case 6 %%% HiLo
                h.Mia_Image.Axes(i,2).Color = [0 1 0];
                h.Mia_Image.Settings.Channel_Colormap(i).BackgroundColor = UserValues.Look.Control;
                AlphaRatio = 3;
            case 7 %%% Inverse Gray
                h.Mia_Image.Axes(i,2).Color = [1 0 0];
                h.Mia_Image.Settings.Channel_Colormap(i).BackgroundColor = UserValues.Look.Control;
                AlphaRatio = 3;
            case 8 %%% Parula
                h.Mia_Image.Axes(i,2).Color = [0 0 0];
                h.Mia_Image.Settings.Channel_Colormap(i).BackgroundColor = UserValues.Look.Control;
                AlphaRatio = 0.25;
        end
        
        % read colormap from UserValues and apply to axis and colorbar
        ImageColor = Image_Colormap([],[],i);
        h.Mia_Image.Axes(i,1).Colormap = ImageColor;

        %% Plots left image
        if size(MIAData.Data,1)>=i
            if plotframe
                % Display the actual frame data
                Frame=round(h.Mia_Image.Settings.Channel_Frame_Slider(i).Value);
                if Frame>0 %%% Extracts data of current frame
                    try
                        Imag = MIAData.Data{d,1}(:,:,Frame);
                    end
                elseif Frame == 0 %%% Extracts data of all selected frames, if Frame==0
                    Frames = str2num(h.Mia_Image.Settings.ROI_Frames.String); %#ok<ST2NM>
                    Imag = mean(MIAData.Data{d,1}(:,:,Frames),3);
                end
                maxdata = nanmax(MIAData.Data{d,1}(:));
                mindata = nanmin(MIAData.Data{d,1}(:));
            else
                % Display a calculated image (map or other)
                Imag = MIAData.MapData{i,d};
            end

            %%% Adjusts Scale of image
            switch h.Mia_Image.Settings.AutoScale.Value
                case 1 
                    %h.Mia_Image.Axes(i,1).CLimMode = 'auto';
                    h.Mia_Image.Settings.Scale(i,1).Visible = 'off';
                    h.Mia_Image.Settings.Scale(i,2).Visible = 'off';
                    h.Mia_Image.Settings.Scale(i,3).Visible = 'off';
                    h.Mia_Image.Settings.Scale(i,4).Visible = 'off';
                    h.Mia_Image.Settings.Scale_Text.Visible = 'off';
                    % switch to autoscale when you switch image
                    % behind the scenes, set the values for the min and max
                    h.Mia_Image.Settings.Scale(i,1).String = num2str(min(min(Imag))); %min left image
                    h.Mia_Image.Settings.Scale(i,2).String = num2str(max(max(Imag))); %max left image
                    %h.Mia_Image.Settings.Scale(i,3).String = num2str(min(min(Image))); %min right image
                    %h.Mia_Image.Settings.Scale(i,4).String = num2str(min(min(Image))); %max max image

                case 2
                    %Min = nanmin(MIAData.Data{i,1}(:));
                    %Max = nanmax(MIAData.Data{i,1}(:));
                    %h.Mia_Image.Axes(i,1).CLim = [Min Max];
                    Imag(Imag>maxdata)=maxdata;
                    Imag(Imag<mindata)=mindata;
                    %Image(end)=nanmax(MIAData.Data{i,1}(:));
                    h.Mia_Image.Settings.Scale(i,1).Visible = 'off';
                    h.Mia_Image.Settings.Scale(i,2).Visible = 'off';
                    h.Mia_Image.Settings.Scale(i,3).Visible = 'off';
                    h.Mia_Image.Settings.Scale(i,4).Visible = 'off';
                    h.Mia_Image.Settings.Scale_Text.Visible = 'off';
                case 3
      
                    %h.Mia_Image.Axes(i,1).CLim = [str2double(h.Mia_Image.Settings.Scale(i,1).String) str2double(h.Mia_Image.Settings.Scale(i,2).String)];
                    Imag(Imag>str2double(h.Mia_Image.Settings.Scale(i,2).String))=str2double(h.Mia_Image.Settings.Scale(i,2).String);
                    Imag(Imag<str2double(h.Mia_Image.Settings.Scale(i,1).String))=str2double(h.Mia_Image.Settings.Scale(i,1).String);
                    %Imag(end)=str2double(h.Mia_Image.Settings.Scale(i,2).String);
                    h.Mia_Image.Settings.Scale(i,1).Visible = 'on';
                    h.Mia_Image.Settings.Scale(i,2).Visible = 'on';
                    h.Mia_Image.Settings.Scale(i,3).Visible = 'on';
                    h.Mia_Image.Settings.Scale(i,4).Visible = 'on';
                    h.Mia_Image.Settings.Scale_Text.Visible = 'on';
            end
            Image2 = single(Imag);
            %%% Transforms intensity image to 64 bits
            Imag=round(rescale(Imag, 1, 128));
            %%% Applies colormap & Updates image and axis
            h.Plots.Image(i,1).CData = reshape(ImageColor(Imag(:),:),size(Imag,1),size(Imag,2),3);
            if ~plotframe
                % scale the brightness of the map 
                mapname = h.Mia_Image.Settings.Channel_PIE(i).String(h.Mia_Image.Settings.Channel_PIE(i).Value);
                if strcmp(mapname,'Interaction map') || strcmp(mapname,'Co-diffusion map')
                    chl = 3;
                else
                    chl = i;
                end
                switch mapname{1}
                    case 'Diffusion map'
                        ind = 1;
                    case 'Concentration map'
                        ind = 2;
                    case 'Brightness map'
                        ind = 3;
                    case 'Intensity map'
                        ind = 4;
                    case 'Signal-to-noise map'
                        ind = 5;
                    case 'Interaction map'
                        ind = 6;
                    case 'Image ratio'
                        ind = 7;
                    case 'Co-diffusion map'
                        ind = 8;
                end
                mapscaling = MIAData.MapScaling{chl,1,ind};
                for m = 1:3
                    h.Plots.Image(i,1).CData(:,:,m) = h.Plots.Image(i,1).CData(:,:,m).*mapscaling;
                end
            end
            %%% Store the image globally for later reference
            MIAData.Image{i,d} = h.Plots.Image(i,1).CData;

            if plotframe
                %%% recalculate the colorbar tick labels to get the correct units
                % count rescaling
                Image2 = Image2./str2double(h.Mia_Image.Settings.Image_Pixel.String).*1000;
            end
            if min(Image2, [], "all") ~= max(Image2, [], "all")
                MapColorbar(Image2, i, h.Mia_Image.Settings.Channel_PIE(i).String{d})
            end
            % Imin = min(min(Image2));
            % Imax = max(max(Image2));
            % n = size(h.Mia_Image.Colorbar(i).TickLabels,1)-1;
            % BinSize = (Imax-Imin)/n;
            % Bins = Imin:BinSize:Imax;
            % for j = 1:size(h.Mia_Image.Colorbar(i).TickLabels,1)
            %     h.Mia_Image.Colorbar(i).TickLabels{j} = num2str(Bins(j));
            % end
            % if i == 1
            %     h.Mia_Image.Colorbar(i).Title.String = '[kHz]';
            % end
            h.Mia_Image.Axes(i,1).XLim=[0 size(Imag,2)]+0.5;
            h.Mia_Image.Axes(i,1).YLim=[0 size(Imag,1)]+0.5;
        end

        if plotframe
            % user is plotting actual frame data
            %% Plots right image
            if size(MIAData.Data,1)>=i && size(MIAData.Data,2)>=2
                Frame=round(h.Mia_Image.Settings.Channel_Frame_Slider(i).Value);
                From= h.Plots.ROI(i).Position(1:2)+0.5;
                To=From+h.Plots.ROI(i).Position(3:4)-1;
                switch h.Mia_Image.Settings.Channel_Second(i).Value
                    case 1 %%% Uses ROI of original image
                        if Frame>0
                            Imag = MIAData.Data{d,1}(From(2):To(2),From(1):To(1),Frame);
                        elseif Frame==0
                            Frames = str2num(h.Mia_Image.Settings.ROI_Frames.String); %#ok<ST2NM>
                            Imag = mean(MIAData.Data{d,1}(From(2):To(2),From(1):To(1),Frames),3);
                        end
                    case 2 %%% Uses ROI of corrected image (=> dynamic species)
                        if Frame>0
                            Imag = MIAData.Data{d,2}(:,:,Frame);
                        elseif Frame==0
                            Frames = str2num(h.Mia_Image.Settings.ROI_Frames.String); %#ok<ST2NM>
                            Imag = mean(MIAData.Data{d,2}(:,:,Frames),3);
                        end
                    case 3 %%% Uses ROI of corrected image (=> static species that is subtracted)
                        if Frame>0
                            Imag = single(MIAData.Data{d,1}(From(2):To(2),From(1):To(1),Frame))-MIAData.Data{d,2}(:,:,Frame);
                        elseif Frame==0
                            Frames = str2num(h.Mia_Image.Settings.ROI_Frames.String); %#ok<ST2NM>
                            Imag = mean(single(MIAData.Data{d,1}(From(2):To(2),From(1):To(1),Frames))-MIAData.Data{d,2}(:,:,Frames),3);
                        end
                end
                %%% Adjusts Scale of image
                switch h.Mia_Image.Settings.AutoScale.Value
                    case 1
                        h.Mia_Image.Settings.Scale(i,3).Visible = 'off';
                        h.Mia_Image.Settings.Scale(i,4).Visible = 'off';
                    case 2
                        Imag(Imag>nanmax(MIAData.Data{d,2}(:)))=nanmax(MIAData.Data{d,2}(:));
                        Imag(Imag<nanmin(MIAData.Data{d,2}(:)))=nanmin(MIAData.Data{d,2}(:));
                        h.Mia_Image.Settings.Scale(i,3).Visible = 'off';
                        h.Mia_Image.Settings.Scale(i,4).Visible = 'off';
                    case 3
                        Imag(Imag>str2double(h.Mia_Image.Settings.Scale(i,4).String))=str2double(h.Mia_Image.Settings.Scale(i,4).String);
                        Imag(Imag<str2double(h.Mia_Image.Settings.Scale(i,3).String))=str2double(h.Mia_Image.Settings.Scale(i,3).String);
                        h.Mia_Image.Settings.Scale(i,3).Visible = 'on';
                        h.Mia_Image.Settings.Scale(i,4).Visible = 'on';
                end
                %%% Transforms intensity image to 64 bits
                Imag=round(rescale(Imag, 1, 128));
                %%% Applies colormap & Updates image
                h.Plots.Image(i,2).CData = reshape(ImageColor(Imag(:),:),size(Imag,1),size(Imag,2),3);
                %%% Adjusts Scale of image
                % switch h.Mia_Image.Settings.AutoScale.Value
                %     case 1
                %         h.Mia_Image.Axes(i,2).CLimMode = 'auto';
                %     case 2
                %         Min = nanmin(MIAData.Data{i,2}(:));
                %         Max = nanmax(MIAData.Data{i,2}(:));
                %         h.Mia_Image.Axes(i,2).CLim = [Min Max];
                %     case 3
                %         h.Mia_Image.Axes(i,2).CLim = [str2double(h.Mia_Image.Settings.Scale(i,3).String) str2double(h.Mia_Image.Settings.Scale(i,4).String)];
                % end

                %%% Sets transparency of NaN pixels to 100%;
                %%% Also sets AlphaData to right size
                h.Plots.Image(i,2).AlphaData = ~isnan(Imag);
                if Frame>0 %%% For one frame, use manual selection and arbitrary region
                    if ~isempty(MIAData.AR) && ~isempty(MIAData.AR{d,1})
                        h.Plots.Image(i,2).AlphaData = ((MIAData.AR{d,1}(:,:,Frame) & MIAData.MS{d})+AlphaRatio)/(1+AlphaRatio);
                    elseif ~all(all(MIAData.MS{1}))
                        % AROI was imported without further ado
                        h.Plots.Image(i,2).AlphaData = (MIAData.MS{d}+AlphaRatio)/(1+AlphaRatio);
                    else
                        h.Plots.Image(i,2).AlphaData = 1;
                    end
                else %%% For all frames, only use manual selection
                    if ~isempty(MIAData.AR) && ~isempty(MIAData.AR{d,1})
                        h.Plots.Image(i,2).AlphaData = ((MIAData.AR{d,2}(:,:) & MIAData.MS{d})+AlphaRatio)/(1+AlphaRatio);
                    elseif ~all(all(MIAData.MS{1}))
                        % AROI was imported without further ado
                        h.Plots.Image(i,2).AlphaData = (MIAData.MS{i}+AlphaRatio)/(1+AlphaRatio);
                    else
                        h.Plots.Image(i,2).AlphaData = 1;
                    end
                end
                %%% Updates axis
                h.Mia_Image.Axes(i,2).XLim=[0 size(Imag,2)]+0.5;
                h.Mia_Image.Axes(i,2).YLim=[0 size(Imag,1)]+0.5;
            end

        end
        %drawnow
        % generate a scale bar in pixels
        ScaleBar(i)
    end
    if strcmp(h.Mia_Image.Settings.Channel_PIE(2).String{h.Mia_Image.Settings.Channel_PIE(2).Value},'Nothing')
        % Channel 2 no channel selected
        h.Mia_Image.Settings.Channel_Link.Value = 0;
        h.Mia_Image.Settings.Channel_Link.Visible = 'off';
        h.Mia_Image.Settings.Channel_Frame(2).Visible = 'off';
        h.Mia_Image.Settings.Channel_Frame_Slider(2).Visible = 'off';
        h.Mia_Image.Axes(2,1).Visible = 'off';
        h.Mia_Image.Axes(2,2).Visible = 'off';
        h.Plots.Image(2,1).Visible = 'off';
        h.Plots.Image(2,2).Visible = 'off';
        h.Plots.ROI(2).Visible = 'off';
        h.Mia_Image.Colorbar(2).Visible = 'off';
        %%% Clears images for display channel 2
        h.Plots.Image(2,1).CData=zeros(1,1,3);
        h.Mia_Image.Axes(2,1).XLim=[0 1]+0.5;
        h.Mia_Image.Axes(2,1).YLim=[0 1]+0.5;
        h.Plots.Image(2,2).CData=zeros(1,1,3);
        h.Mia_Image.Axes(2,2).XLim=[0 1]+0.5;
        h.Mia_Image.Axes(2,2).YLim=[0 1]+0.5;
        h.Mia_Image.Settings.Channel_Colormap(2).Visible = 'off';
        h.Mia_Image.Settings.Channel_Second(2).Visible = 'off';
    else
        % Channel 2 no channel selected
        h.Mia_Image.Settings.Channel_Link.Visible = 'on';
        h.Mia_Image.Settings.Channel_Frame(2).Visible = 'on';
        h.Mia_Image.Settings.Channel_Frame_Slider(2).Visible = 'on';
        h.Mia_Image.Axes(2,1).Visible = 'on';
        h.Mia_Image.Axes(2,2).Visible = 'on';
        h.Plots.Image(2,1).Visible = 'on';
        h.Plots.Image(2,2).Visible = 'on';
        h.Plots.ROI(2).Visible = 'on';
        h.Mia_Image.Colorbar(2).Visible = 'on';
        h.Mia_Image.Settings.Channel_Colormap(2).Visible = 'on';
        h.Mia_Image.Settings.Channel_Second(2).Visible = 'on';
    end

end

%% Plots ICS data
if any(mode==2)
    %%% Selects colormap
    switch h.Mia_ICS.Cor_Colormap.Value
        case 1
            Colormap=gray(64);
            h.Mia_ICS.Cor_Colormap.BackgroundColor = UserValues.Look.Control;
        case 2
            Colormap = jet(64);
            h.Mia_ICS.Cor_Colormap.BackgroundColor=UserValues.Look.Control;
        case 3
            Colormap=hot(64);
            h.Mia_ICS.Cor_Colormap.BackgroundColor=UserValues.Look.Control;
        case 4
            Colormap=hsv(64);
            h.Mia_ICS.Cor_Colormap.BackgroundColor=UserValues.Look.Control;
        case 5
            Colormap=gray(64).*repmat(h.Mia_ICS.Cor_Colormap.UserData,[64,1]);
            h.Mia_ICS.Cor_Colormap.BackgroundColor=h.Mia_ICS.Cor_Colormap.UserData;
    end
    %%% Determins frame to plot            
    Frame=round(h.Mia_ICS.Frame_Slider.Value); 
    %%% Determins correlation size to plot
    Size=str2double(h.Mia_ICS.Size.String);    
    %%% Updates correlationplots 
    for i = 1:3
        if ~isempty(MIAData.Cor{i,1})
            %%% Forces Size into bounds
            if Size>size(MIAData.Cor{i},1) || Size>size(MIAData.Cor{i},2)
                Size=min([size(MIAData.Cor{i},1),size(MIAData.Cor{i},2)]);
                h.Mia_ICS.Size.String=num2str(Size);
            end
            %%% Determines center of correlation
            X(1)=ceil(floor(size(MIAData.Cor{i,1},1)/2)-Size/2)+1;
            X(2)=ceil(floor(size(MIAData.Cor{i,1},1)/2)+Size/2);
            Y(1)=ceil(floor(size(MIAData.Cor{i,1},2)/2)-Size/2)+1;
            Y(2)=ceil(floor(size(MIAData.Cor{i,1},2)/2)+Size/2);
            %%% Plots average correlation, if frame 0 was selected
            if Frame==0
                Frames=str2num(h.Mia_ICS.Frames2Use.String); %#ok<ST2NM>
                Frames=Frames((Frames>0) & (Frames <= size(MIAData.Cor{i,1},3)));
                Imag=mean(MIAData.Cor{i,1}(X(1):X(2),Y(1):Y(2),Frames),3);
            else
                Imag=MIAData.Cor{i,1}(X(1):X(2),Y(1):Y(2),Frame);  
            end
            %%% Plots surface plot of correlation
            h.Plots.Cor(i,2).ZData=Imag;
            %%% Resizes correlation and plots it as RGB
            Imag=round(63*(Imag-min(min(Imag)))/(max(max(Imag))-min(min(Imag))))+1;
            Imag(isnan(Imag))=1;
            Imag=reshape(Colormap(Imag,:),[size(Imag,1),size(Imag,2),3]);
            h.Plots.Cor(i,1).CData=Imag;
            h.Mia_ICS.Axes(i,1).XLim=[0 size(Imag,2)]+0.5;
            h.Mia_ICS.Axes(i,1).YLim=[0 size(Imag,1)]+0.5;
            
            %%% Calculate average of verteces surrounding the face in surf plot
            Image_Surf=h.Plots.Cor(i,2).ZData;
            Image_Surf=Image_Surf...
                +circshift(Image_Surf,[-1  0 0])...
                +circshift(Image_Surf,[-1 -1 0])...
                +circshift(Image_Surf,[ 0 -1 0]);
            %%% Resizes face intenity and plots it as RGB
            Image_Surf=round(63*(Image_Surf-min(min(Image_Surf)))/(max(max(Image_Surf))-min(min(Image_Surf))))+1;
            Image_Surf(isnan(Image_Surf))=1;
            Image_Surf=reshape(Colormap(Image_Surf,:),[size(Image_Surf,1),size(Image_Surf,2),3]);
            h.Plots.Cor(i,2).CData=Image_Surf;

            if ~isvar('fast') %Do not calculate the fit function if you don't need it
                %%% Calculates fit from table values
                Fit=reshape(Calc_RICS_Fit(i),[Size,Size]);
                %%% Changes fit axes visibility
                h.Mia_ICS.Axes(i,3).Visible='on';
                h.Plots.Cor(i,3).Visible='on';
                h.Mia_ICS.Axes(i,4).Visible='off';
                h.Plots.Cor(i,4).Visible='off';
                h.Plots.Cor(i,5).Visible='off';
                h.Plots.Cor(i,6).Visible='off';
                h.Plots.Cor(i,7).Visible='off';
                %%% Determins fit plot type
                switch h.Mia_ICS.Fit_Type.Value
                    case 1 %%% Plots fit surf plot
                        %%% Calculate average of verteces surrounding the face in fit surf plot
                        Fit_Faces=(Fit+circshift(Fit,[-1 0 0])+circshift(Fit,[-1 -1 0])+circshift(Fit,[0 -1 0]))/4;
                        %%% Resizes fit face intenity and applies colormap
                        Fit_Faces=round(63*(Fit_Faces-min(min(Fit_Faces)))/(max(max(Fit_Faces))-min(min(Fit_Faces))))+1;
                        Fit_Faces(isnan(Fit_Faces))=1;
                        Fit_Faces=reshape(Colormap(Fit_Faces,:),[size(Fit_Faces,1),size(Fit_Faces,2),3]);
                        %%% Links fit z-axes to data axes
                        h.Mia_ICS.Axes(i,3).ZLim=h.Mia_ICS.Axes(i,2).ZLim;
                    case 2 %%% Plots fit residual surf plot
                        %%% Calculates standard error of mean or uses one
                        if Frame==0
                            SEM=std(MIAData.Cor{i,1}(X(1):X(2),Y(1):Y(2),Frames),0,3)/sqrt(numel(Frames));
                        else
                            SEM=ones(size(Fit,1),size(Fit,2));
                        end


                        %%% Calculates weighted residuals
                        Fit=(h.Plots.Cor(i,2).ZData-Fit)./SEM;
                        %%% Calculate average of verteces surrounding the face in residual surf plot
                        Fit_Faces=(Fit+circshift(Fit,[-1 0 0])+circshift(Fit,[-1 -1 0])+circshift(Fit,[0 -1 0]))/4;
                        %%% Resizes residual face intenity and applies colormap
                        Fit_Faces=round(63*(Fit_Faces-min(min(Fit_Faces)))/(max(max(Fit_Faces))-min(min(Fit_Faces))))+1;

                        Fit_Faces=reshape(Colormap(Fit_Faces,:),[size(Fit_Faces,1),size(Fit_Faces,2),3]);
                        %%% Autosets z axes
                        h.Mia_ICS.Axes(i,3).ZLimMode='auto';
                    case 3 %%% Plots fit surf plot with residuals as blue (neg), red(pos) and gray(neutal)
                        %%% Calculates standard error of mean or uses one
                        if Frame==0
                            SEM=std(MIAData.Cor{i,1}(X(1):X(2),Y(1):Y(2),Frames),0,3)/sqrt(numel(Frames));
                        else
                            SEM=ones(size(Fit,1),size(Fit,2));
                        end
                        if any(any(SEM==0))
                            SEM=1;
                        end
                        %%% Calculates weighted residuals
                        Residuals=(h.Plots.Cor(i,2).ZData-Fit)./SEM;
                        %%% Calculate average of verteces surrounding the face in fit/residual surf plot
                        Residuals=(Residuals+circshift(Residuals,[-1 0 0])+circshift(Residuals,[-1 -1 0])+circshift(Residuals,[0 -1 0]))/4;
                        %%% Generates colormap blue-gray-red
                        Errormap=zeros(64,3);
                        Errormap(:,1)=[linspace(0,0.8,32), repmat(0.8,[1,32])];
                        Errormap(:,2)=[linspace(0,0.8,32), linspace(0.8,0,32)];
                        Errormap(:,3)=[repmat(0.8,[1,32]), linspace(0.8,0,32)];
                        %%% Applies colormap to residuals from -3 to +3
                        Residuals=round((Residuals+3)/6*64);
                        Residuals(Residuals<1)=1;
                        Residuals(Residuals>64)=64;
                        Fit_Faces=reshape(Errormap(Residuals(:),:),[size(Fit,1),size(Fit,2),3]);
                        %%% Links fit z-axes to data axes
                        h.Mia_ICS.Axes(i,3).ZLim=h.Mia_ICS.Axes(i,2).ZLim;
                    case 4 %%% Plots x and y axes of the fit and the data
                        h.Mia_ICS.Axes(i,3).Visible='off';
                        h.Plots.Cor(i,3).Visible='off';
                        h.Mia_ICS.Axes(i,4).Visible='on';
                        h.Plots.Cor(i,4).XData=(1:size(Fit,2))-round(size(Fit,2)/2);
                        h.Plots.Cor(i,4).YData=h.Plots.Cor(i,2).ZData(round(size(Fit,2)/2),:);
                        h.Plots.Cor(i,4).Visible='on';
                        h.Plots.Cor(i,5).XData=(1:size(Fit,2))-round(size(Fit,2)/2);
                        h.Plots.Cor(i,5).YData=Fit(round(size(Fit,2)/2),:);
                        h.Plots.Cor(i,5).Visible='on';
                        h.Plots.Cor(i,6).XData=(1:size(Fit,1))-round(size(Fit,1)/2);
                        h.Plots.Cor(i,6).YData=h.Plots.Cor(i,2).ZData(:,round(size(Fit,1)/2));
                        h.Plots.Cor(i,6).Visible='on';
                        h.Plots.Cor(i,7).XData=(1:size(Fit,1))-round(size(Fit,1)/2);
                        h.Plots.Cor(i,7).YData=Fit(:,round(size(Fit,1)/2));
                        h.Plots.Cor(i,7).Visible='on';

                        Fit_Faces=Fit;
                end
                %%% Plots fit and applies face color
                h.Plots.Cor(i,3).ZData=Fit;
                h.Plots.Cor(i,3).CData=Fit_Faces;
            end
        end
    end

end

%% Plots N&B data
if any(mode==3) && isfield(MIAData.NB,'PCH')
    %%% Selects channel to plot
    i=h.Mia_NB.Image.Channel.Value;
    %%% Selects nonempty channel
    if size(MIAData.NB.PCH,2)<i || isempty(MIAData.NB.PCH{i})
        k=1;
        while isempty(MIAData.NB.PCH{k}); k=k+1;end
        i=k;
        h.Mia_NB.Image.Channel.Value=i;
    end
    %% Updates intensity, number and brightness plots
    %%% Images intensity in kHz in borders determined by threshold
    h.Plots.NB(1).CData=MIAData.NB.Int{i}/str2double(h.Mia_NB.Image.Pixel.String)*10^3;
    h.Mia_NB.Axes(1).CLim=[str2double(h.Mia_NB.Image.Hist(1,1).String),str2double(h.Mia_NB.Image.Hist(2,1).String)];
    h.Mia_NB.Axes(1).XLim=[0 size(MIAData.NB.Int{i},2)]+0.5;
    h.Mia_NB.Axes(1).YLim=[0 size(MIAData.NB.Int{i},1)]+0.5;    
    %%% Images number in borders determined by threshold
    h.Plots.NB(2).CData=MIAData.NB.Num{i};
    h.Mia_NB.Axes(2).CLim=[str2double(h.Mia_NB.Image.Hist(1,2).String),str2double(h.Mia_NB.Image.Hist(2,2).String)];
    h.Mia_NB.Axes(2).XLim=[0 size(MIAData.NB.Int{i},2)]+0.5;
    h.Mia_NB.Axes(2).YLim=[0 size(MIAData.NB.Int{i},1)]+0.5;
    %%% Images brightness in kHz in borders determined by threshold
    h.Plots.NB(3).CData=MIAData.NB.Eps{i}/str2double(h.Mia_NB.Image.Pixel.String)*10^3;
    h.Mia_NB.Axes(3).CLim=[str2double(h.Mia_NB.Image.Hist(1,3).String),str2double(h.Mia_NB.Image.Hist(2,3).String)];
    h.Mia_NB.Axes(3).XLim=[0 size(MIAData.NB.Int{i},2)]+0.5;
    h.Mia_NB.Axes(3).YLim=[0 size(MIAData.NB.Int{i},1)]+0.5;

    %% Plots 1D histogram of selected parameter
    %%% Removes pixel determined by thresholds
    MIAData.NB.Use=((MIAData.NB.Int{i}/str2double(h.Mia_NB.Image.Pixel.String)*10^3>=str2double(h.Mia_NB.Image.Hist(1,1).String) &... %%% Lower int TH
        MIAData.NB.Int{i}/str2double(h.Mia_NB.Image.Pixel.String)*10^3<=str2double(h.Mia_NB.Image.Hist(2,1).String)) |... %%% Upper in TH
        repmat(~h.Mia_NB.Image.UseTH(1).Value,size(MIAData.NB.Int{i}))) &... %%% Do not apply if checked
        ((MIAData.NB.Num{i}>=str2double(h.Mia_NB.Image.Hist(1,2).String) &... %%% Lower number TH
        MIAData.NB.Num{i}<=str2double(h.Mia_NB.Image.Hist(2,2).String)) |... %%% Upper number TH
        repmat(~h.Mia_NB.Image.UseTH(2).Value,size(MIAData.NB.Int{i}))) &...%%% Do not apply if checked
        ((MIAData.NB.Eps{i}/str2double(h.Mia_NB.Image.Pixel.String)*10^3>=str2double(h.Mia_NB.Image.Hist(1,3).String) &... %%% Lower brightness TH
        MIAData.NB.Eps{i}/str2double(h.Mia_NB.Image.Pixel.String)*10^3<=str2double(h.Mia_NB.Image.Hist(2,3).String)) |... %%% Upper brightness TH
        repmat(~h.Mia_NB.Image.UseTH(3).Value,size(MIAData.NB.Int{i}))); %%% Do not apply if checked
    %%% Plots bar histogram
    switch h.Mia_NB.Hist1D.Value
        case 1 %%% PCH histogram; Not threshold
            h.Plots.NB(4).YData=MIAData.NB.PCH{i};
            h.Plots.NB(4).XData=0:(numel(MIAData.NB.PCH{i})-1);
            h.Mia_NB.Axes(4).XLabel.String='Counts per pixel';
            h.Mia_NB.Hist1D_Text.String='';
        case 2 %%% Intensity histogram
            h.Plots.NB(4).XData=linspace(str2double(h.Mia_NB.Image.Hist(1,1).String),str2double(h.Mia_NB.Image.Hist(2,1).String),str2double(h.Mia_NB.Image.Hist(3,1).String));
            h.Plots.NB(4).YData=histc(MIAData.NB.Int{i}(MIAData.NB.Use)/str2double(h.Mia_NB.Image.Pixel.String)*10^3,h.Plots.NB(4).XData);
            h.Mia_NB.Axes(4).XLabel.String='Intensity [kHz]';
            h.Mia_NB.Hist1D_Text.String=[num2str(mean(MIAData.NB.Int{i}(MIAData.NB.Use)/str2double(h.Mia_NB.Image.Pixel.String)*10^3),'%6.3f') '+/-' num2str(std(MIAData.NB.Int{i}(MIAData.NB.Use)/str2double(h.Mia_NB.Image.Pixel.String)*10^3),'%6.3f') ' kHz'];
        case 3 %%% Number histogram
            h.Plots.NB(4).XData=linspace(str2double(h.Mia_NB.Image.Hist(1,2).String),str2double(h.Mia_NB.Image.Hist(2,2).String),str2double(h.Mia_NB.Image.Hist(3,2).String));
            h.Plots.NB(4).YData=histc(MIAData.NB.Num{i}(MIAData.NB.Use),h.Plots.NB(4).XData);
            h.Mia_NB.Axes(4).XLabel.String='Number';
            h.Mia_NB.Hist1D_Text.String=[num2str(mean(MIAData.NB.Num{i}(MIAData.NB.Use)),'%6.3f') '+/-' num2str(std(MIAData.NB.Num{i}(MIAData.NB.Use)),'%6.3f')];
        case 4 %%% Brightness histogram
            h.Plots.NB(4).XData=linspace(str2double(h.Mia_NB.Image.Hist(1,3).String),str2double(h.Mia_NB.Image.Hist(2,3).String),str2double(h.Mia_NB.Image.Hist(3,3).String));
            h.Plots.NB(4).YData=histc(MIAData.NB.Eps{i}(MIAData.NB.Use)/str2double(h.Mia_NB.Image.Pixel.String)*10^3,h.Plots.NB(4).XData);
            h.Mia_NB.Axes(4).XLabel.String='Brightness [kHz]';
            h.Mia_NB.Hist1D_Text.String=[num2str(mean(MIAData.NB.Eps{i}(MIAData.NB.Use)/str2double(h.Mia_NB.Image.Pixel.String)*10^3),'%6.3f') '+/-' num2str(std(MIAData.NB.Eps{i}(MIAData.NB.Use)/str2double(h.Mia_NB.Image.Pixel.String)*10^3),'%6.3f') ' kHz'];
    end
    h.Mia_NB.Hist1D_Text.Position(1)=0.99-h.Mia_NB.Hist1D_Text.Extent(3);
    %%% Set X-Limit; uses 1/2 of binsize to not cut first and last bar
    h.Mia_NB.Axes(4).XLim=[h.Plots.NB(4).XData(1)-diff(h.Plots.NB(4).XData(1:2)/2),...
                           h.Plots.NB(4).XData(end)+diff(h.Plots.NB(4).XData(1:2))/2];
                                              
    %% Plots 2D histogram of selected parameters
    %%% Selects Colormap
    switch h.Mia_NB.Hist2D(3).Value
        case 1
            Color=jet(64);
        case 2
            Color=hot(64);
        case 3
            Color=hsv(64);
        case 4
            Color=gray(64);
    end
    Color=[h.Mia_NB.Hist2D(3).BackgroundColor; Color];
    h.Mia_NB.Axes(5).XColor = h.Mia_NB.Hist2D(3).ForegroundColor;
    h.Mia_NB.Axes(5).YColor = h.Mia_NB.Hist2D(3).ForegroundColor;
   
    %%% Selects x axis bounds and bins
    MinX=str2double(h.Mia_NB.Image.Hist(1,h.Mia_NB.Hist2D(2).Value).String);
    MaxX=str2double(h.Mia_NB.Image.Hist(2,h.Mia_NB.Hist2D(2).Value).String);
    BinX=str2double(h.Mia_NB.Image.Hist(3,h.Mia_NB.Hist2D(2).Value).String);
    %%% Determins parameter for x axis
    switch h.Mia_NB.Hist2D(2).Value
        case 1
            X=MIAData.NB.Int{i}(MIAData.NB.Use)/str2double(h.Mia_NB.Image.Pixel.String)*10^3;
            h.Mia_NB.Axes(5).XLabel.String='Intensity [kHz]';
        case 2
            X=MIAData.NB.Num{i}(MIAData.NB.Use);
            h.Mia_NB.Axes(5).XLabel.String='Number';
        case 3
            X=MIAData.NB.Eps{i}(MIAData.NB.Use)/str2double(h.Mia_NB.Image.Pixel.String)*10^3;
            h.Mia_NB.Axes(5).XLabel.String='Brightness [kHz]';
    end
    h.Mia_NB.Axes(5).XColor = UserValues.Look.Fore;
    h.Mia_NB.Axes(5).XLabel.Color = UserValues.Look.Fore;
    %%% Scales x data into bins
    X=floor(BinX*(X-MinX)/(MaxX-MinX));
    
    %%% Selects y axis bounds and bins
    MinY=str2double(h.Mia_NB.Image.Hist(1,h.Mia_NB.Hist2D(1).Value).String);
    MaxY=str2double(h.Mia_NB.Image.Hist(2,h.Mia_NB.Hist2D(1).Value).String);
    BinY=str2double(h.Mia_NB.Image.Hist(3,h.Mia_NB.Hist2D(1).Value).String);
    %%% Determins parameter for y axis
    switch h.Mia_NB.Hist2D(1).Value
        case 1
            Y=MIAData.NB.Int{i}(MIAData.NB.Use)/str2double(h.Mia_NB.Image.Pixel.String)*10^3;
            h.Mia_NB.Axes(5).YLabel.String='Intensity [kHz]';
        case 2
            Y=MIAData.NB.Num{i}(MIAData.NB.Use);
            h.Mia_NB.Axes(5).YLabel.String='Number';
        case 3
            Y=MIAData.NB.Eps{i}(MIAData.NB.Use)/str2double(h.Mia_NB.Image.Pixel.String)*10^3;
            h.Mia_NB.Axes(5).YLabel.String='Brightness [kHz]';
    end
    h.Mia_NB.Axes(5).YColor = UserValues.Look.Fore;
    h.Mia_NB.Axes(5).YLabel.Color = UserValues.Look.Fore;
    %%% Scales y data into bins
    Y=floor(BinY*(Y-MinY)/(MaxY-MinY));
    
    %%% Removes pixels with Y<0, due to the way the histogram is calculated
    X=X(Y>0); Y=Y(Y>0);X=X(Y<BinY); Y=Y(Y<BinY);
    Y=Y(X>0); X=X(X>0);Y=Y(X<BinX); X=X(X<BinX);
    %%% Calculates 2D histogram
    if ~isempty(X)
        Imag=reshape(histc(Y+(BinY*(X-1)),1:(BinX*BinY)),[BinY,BinX]);
        if h.Mia_NB.Image.logZ.Value
            Imag = log10(Imag);
            Imag(Imag<0)=0;
            Imag(isinf(Imag))=0;
        end
        Imag=ceil(64*Imag/max(Imag(:)))+1;
        Imag=reshape(Color(Imag,:),[BinY, BinX, 3]);
        %%% Plots and scales 2D histogram
        h.Plots.NB(5).CData=Imag;
        h.Plots.NB(5).XData=linspace(MinX,MaxX,BinX);
        h.Plots.NB(5).YData=linspace(MinY,MaxY,BinY);
        h.Mia_NB.Axes(5).XLim=[MinX MaxX]-(MaxX-MinX)/(2*BinX);
        h.Mia_NB.Axes(5).YLim=[MinY MaxY]-(MaxY-MinY)/(2*BinY);        
    end
end

%% Plots frame, PCH and intensity histogram
if any(mode==4)
    if channel == 1
        % only 1 channel, so hide the other plots
        h.Plots.Int(2,1).Visible = 'off';
        h.Plots.IntHist(2,1).Visible = 'off';
        h.Plots.Int(2,2).Visible = 'off';
        h.Plots.IntHist(2,2).Visible = 'off';
    else
        h.Plots.Int(2,1).Visible = 'on';
        h.Plots.IntHist(2,1).Visible = 'on';
        h.Plots.Int(2,2).Visible = 'on';
        h.Plots.IntHist(2,2).Visible = 'on';
    end
    minint=[]; maxint=[]; mininth=[]; maxinth=[];
    for i = channel
        d = h.Mia_Image.Settings.Channel_PIE(i).Value; %(the datachannel)
        if isempty(find(strcmp(h.Mia_Image.Settings.Channel_PIE(i).String{d}, [UserValues.MIA.Maps {'Nothing'}])))
            plotframe = 1;
        else
            plotframe = 0;
        end
        if plotframe
            %% Updates Intensity trace
            %%% X axis
            if h.Mia_Image.Intensity_Axes.XLabel.UserData == 0
                % plot time
                for j = 1:2
                    h.Plots.Int(i,j).XData = (1:size(MIAData.Data{d,1},3))*str2double(h.Mia_Image.Settings.Image_Frame.String);
                    h.Mia_Image.Intensity_Axes.XLabel.String = 'Time [s]';
                end
            else % plot frame
                for j = 1:2
                    h.Plots.Int(i,j).XData = 1:size(MIAData.Data{d,1},3);
                end
                h.Mia_Image.Intensity_Axes.XLabel.String = 'Frame';
            end
            h.Mia_Image.Intensity_Axes.XLim = [h.Plots.Int(i,1).XData(1) h.Plots.Int(i,1).XData(end)+0.00001];

            %%% Y axis
            %%% plot counts
            if h.Mia_Image.Intensity_Axes.YLabel.UserData == 0
                % raw data
                h.Plots.Int(i,1).YData = mean(mean(MIAData.Data{d,1},2),1);
                % corrected data in ROI
                Data = MIAData.Data{d,2};
                if ~isempty(MIAData.AR)
                    Data(~(MIAData.AR{d,1} & repmat(MIAData.MS{d},1,1,size(MIAData.AR{d,1},3)))) = NaN;
                end
                h.Plots.Int(i,2).YData = nanmean(nanmean(Data,2),1);
                h.Mia_Image.Intensity_Axes.YLabel.String = 'Average Frame Counts (raw:- -)';
            else % plot kHz
                % raw data
                h.Plots.Int(i,1).YData = mean(mean(MIAData.Data{d,1},2),1)/str2double(h.Mia_Image.Settings.Image_Pixel.String)*1000;
                % corrected data in ROI
                Data = MIAData.Data{d,2};
                if ~isempty(MIAData.AR)
                    Data(~(MIAData.AR{d,1} & repmat(MIAData.MS{d},1,1,size(MIAData.AR{d,1},3)))) = NaN;
                end
                h.Plots.Int(i,2).YData = nanmean(nanmean(Data,2),1)/str2double(h.Mia_Image.Settings.Image_Pixel.String)*1000;
                h.Mia_Image.Intensity_Axes.YLabel.String = 'Average Frame Countrate [kHz] (raw:- -)';
            end
            minint = min([minint,min(h.Plots.Int(i,1).YData),min(h.Plots.Int(i,2).YData)]);
            maxint = max([maxint,max(h.Plots.Int(i,1).YData),max(h.Plots.Int(i,2).YData)]);

            h.Mia_Image.Intensity_Axes.YLim = [minint, maxint];

            %% Updates Intensity/PCH histogram
            if h.Mia_Image.IntHist.Value == 2 %calculate PCH histogram
                % the PCH of the uncorrected and all data
                h.Plots.IntHist(i,1).XData = double(min(MIAData.Data{d,1}(:))):double(max(MIAData.Data{d,1}(:)));
                h.Plots.IntHist(i,1).YData = histc(double(MIAData.Data{d,1}(:)), double(min(MIAData.Data{d,1}(:))):double(max(MIAData.Data{d,1}(:))));
                % the PCH of the pixels included into the AROI (both intensity
                % and freehand drawn) and that are corrected by
                % subtraction/addition
                Max = max(MIAData.Data{d,2}(:));
                Min = min(MIAData.Data{d,2}(:));
                h.Plots.IntHist(i,2).XData = Min:Max;
                if ~isempty(MIAData.AR)
                    % user did arbitrary region, so less pixels into the PCH
                    h.Plots.IntHist(i,2).YData = histc(MIAData.Data{d,2}(MIAData.AR{d,1} & repmat(MIAData.MS{d},1,1,size(MIAData.AR{d,1},3))), Min:Max);
                else
                    % user did not do arbitrary region, so all pixels in the PCH
                    h.Plots.IntHist(i,2).YData = histc(MIAData.Data{d,2}(:), Min:Max);
                end
                h.Mia_Image.IntHist_Axes.XLabel.String = 'Counts (raw:- -)';
            else %calculate mean intensity histogram
                % the raw data intensity histogram
                Imag = mean(MIAData.Data{d,1},3);
                Imag = Imag(:)/str2double(h.Mia_Image.Settings.Image_Pixel.String)*10^3;
                Bins = 100;
                h.Plots.IntHist(i,1).XData=linspace(min(Imag),max(Imag),Bins);
                h.Plots.IntHist(i,1).YData=histc(Imag,h.Plots.IntHist(i,1).XData);
                if ~isempty(MIAData.AR)
                    % the intensity histogram of the pixels included into the AROI (both AR = intensity
                    % and MS = freehand drawn) and that are corrected by
                    % subtraction/addition
                    Imag = MIAData.Data{d,2};
                    Imag(~(MIAData.AR{d,1} & repmat(MIAData.MS{d},1,1,size(MIAData.AR{d,1},3))))=NaN;
                    Imag = mean(Imag,3,'omitnan');
                else
                    Imag = mean(MIAData.Data{d,2},3);
                end
                Imag = Imag(:)/str2double(h.Mia_Image.Settings.Image_Pixel.String)*10^3;
                Bins = 100;
                h.Plots.IntHist(i,2).XData = linspace(min(Imag),max(Imag),Bins);
                h.Plots.IntHist(i,2).YData = histc(Imag,h.Plots.IntHist(i,2).XData);
                h.Mia_Image.IntHist_Axes.XLabel.String = 'Average Count rate [kHz] (raw:- -)';
            end
            if h.Mia_Image.IntHist_Axes.YLabel.UserData == 0
                h.Mia_Image.IntHist_Axes.YScale = 'Lin';
                h.Mia_Image.IntHist_Axes.YLabel.String = 'Frequency';
            else
                h.Mia_Image.IntHist_Axes.YScale = 'Log';
                h.Mia_Image.IntHist_Axes.YLabel.String = 'Frequency, log';
            end
        end
    end
end

%% Plots TICS data
if any(mode==5)
    if isfield(MIAData.TICS,'Data')
        %% Generate the proper total mask
        mask = true(size(MIAData.TICS.Int{1,1}));
        G1a = cell(3,1);
        brightnessa = cell(3,1);
        countsa = cell(3,1);
        halflifea = cell(3,1);
        samplesa = cell(3,1);
    end
    for i=1:3
        % user has previously pressed the TICS calculate at all
        if isfield(MIAData.TICS,'Data')
            %%% 1&2: ACF 1&2
            %%% 3:   CCF
            if size(MIAData.TICS.Data,2)>=i && ~isempty(MIAData.TICS.Data{i})
                % different images to be plotted
                %%% G(first lag) - G(last lag) = the amplitude of that part
                %%% of the correlation that actually decays within the
                %%% total correlation function time
                G1 = double(MIAData.TICS.Data{i}(:,:,1)-MIAData.TICS.Data{i}(:,:,end));
                %%% G(first lag)./mean(Counts)
                switch i
                    case 1 %ACF1
                        brightness = double(MIAData.TICS.Data{1}(:,:,1).*mean(MIAData.Data{1,2}(:,:,str2num(h.Mia_Image.Settings.ROI_Frames.String)),3)); %#ok<ST2NM>
                        counts = double(MIAData.TICS.Int{i,1});
                        samples = MIAData.TICS.AR_NoIncludedFrames{i};
                    case 2 %ACF2
                        brightness = double(MIAData.TICS.Data{2}(:,:,1).*mean(MIAData.Data{2,2}(:,:,str2num(h.Mia_Image.Settings.ROI_Frames.String)),3)); %#ok<ST2NM>
                        counts = double(MIAData.TICS.Int{i,2});
                        samples = MIAData.TICS.AR_NoIncludedFrames{i};
                    case 3 %CCF
                        brightness = double(MIAData.TICS.Data{3}(:,:,1).*...
                            (mean(MIAData.Data{1,2}(:,:,str2num(h.Mia_Image.Settings.ROI_Frames.String)),3)+... 
                            mean(MIAData.Data{2,2}(:,:,str2num(h.Mia_Image.Settings.ROI_Frames.String)),3))/2); 
                        counts = double((MIAData.TICS.Int{i,1}+MIAData.TICS.Int{i,2}));
                        samples = min(MIAData.TICS.AR_NoIncludedFrames{1},MIAData.TICS.AR_NoIncludedFrames{2});
                end

                %samples(samples < 50) = NaN;
                
                %%% Find G(0)/2
                firstlag = repmat(MIAData.TICS.Data{i}(:,:,1),1,1,size(MIAData.TICS.Data{i},3));
                lastlag = repmat(MIAData.TICS.Data{i}(:,:,end),1,1,size(MIAData.TICS.Data{i},3));
                halflife = double((size(MIAData.TICS.Data{i},3)-sum(cumsum((MIAData.TICS.Data{i}-lastlag)./(firstlag-lastlag)<0.5,3)~=0,3)).*...
                    str2double(h.Mia_Image.Settings.Image_Frame.String));
                
                % median filter the images 'just to make them look better'
                if h.Mia_TICS.Median.Value
                    filtsize = str2double(h.Mia_Image.Calculations.Cor_TICS_AvgSize.String); %Size: on the Correlate tabe
                    filtsize = [filtsize filtsize];
                    G1 = medfilt2(G1, filtsize, 'symmetric');
                    brightness = medfilt2(brightness, filtsize, 'symmetric');
                    counts = medfilt2(counts, filtsize, 'symmetric');
                    halflife = medfilt2(halflife, filtsize, 'symmetric');
                    %samples = medfilt2(samples, filtsize, 'symmetric');
                end
                % reset the values
                if ~isempty(obj)
                    if strcmp(obj.Tag, 'DoTICS') || strcmp(obj.Tag, 'Reset') || strcmp(obj.Tag, 'Median')
                        % store or reset the values for each correlation
                        MIAData.TICS.Thresholds{i}(1,1) = min(min(G1));
                        MIAData.TICS.Thresholds{i}(1,2) = max(max(G1));
                        MIAData.TICS.Thresholds{i}(2,1) = min(min(brightness));
                        MIAData.TICS.Thresholds{i}(2,2) = max(max(brightness));
                        MIAData.TICS.Thresholds{i}(3,1) = min(min(counts));
                        MIAData.TICS.Thresholds{i}(3,2) = max(max(counts));
                        MIAData.TICS.Thresholds{i}(4,1) = min(min(halflife));
                        MIAData.TICS.Thresholds{i}(4,2) = max(max(halflife));
                        MIAData.TICS.Thresholds{i}(5,1) = min(min(samples(samples>0)));
                        MIAData.TICS.Thresholds{i}(5,2) = max(max(samples));
                        MIAData.TICS.ThreshMask = true(size(MIAData.TICS.Int{i,1}));
                        % display the values of the popupmenu selected correlation
                        h.Mia_TICS.Threshold_G1_Min_Edit.String = num2str(MIAData.TICS.Thresholds{h.Mia_TICS.SelectCor.Value}(1,1));
                        h.Mia_TICS.Threshold_G1_Max_Edit.String = num2str(MIAData.TICS.Thresholds{h.Mia_TICS.SelectCor.Value}(1,2));
                        h.Mia_TICS.Threshold_brightness_Min_Edit.String = num2str(MIAData.TICS.Thresholds{h.Mia_TICS.SelectCor.Value}(2,1));
                        h.Mia_TICS.Threshold_brightness_Max_Edit.String = num2str(MIAData.TICS.Thresholds{h.Mia_TICS.SelectCor.Value}(2,2));
                        h.Mia_TICS.Threshold_counts_Min_Edit.String = num2str(MIAData.TICS.Thresholds{h.Mia_TICS.SelectCor.Value}(3,1));
                        h.Mia_TICS.Threshold_counts_Max_Edit.String = num2str(MIAData.TICS.Thresholds{h.Mia_TICS.SelectCor.Value}(3,2));
                        h.Mia_TICS.Threshold_halflife_Min_Edit.String = num2str(MIAData.TICS.Thresholds{h.Mia_TICS.SelectCor.Value}(4,1));
                        h.Mia_TICS.Threshold_halflife_Max_Edit.String = num2str(MIAData.TICS.Thresholds{h.Mia_TICS.SelectCor.Value}(4,2));
                        h.Mia_TICS.Threshold_samples_Min_Edit.String = num2str(MIAData.TICS.Thresholds{h.Mia_TICS.SelectCor.Value}(5,1));
                        h.Mia_TICS.Threshold_samples_Max_Edit.String = num2str(MIAData.TICS.Thresholds{h.Mia_TICS.SelectCor.Value}(5,2));
                    elseif strcmp(obj.Tag, 'SelectCor')
                        % display the values of the popupmenu selected correlation
                        h.Mia_TICS.Threshold_G1_Min_Edit.String = num2str(MIAData.TICS.Thresholds{h.Mia_TICS.SelectCor.Value}(1,1));
                        h.Mia_TICS.Threshold_G1_Max_Edit.String = num2str(MIAData.TICS.Thresholds{h.Mia_TICS.SelectCor.Value}(1,2));
                        h.Mia_TICS.Threshold_brightness_Min_Edit.String = num2str(MIAData.TICS.Thresholds{h.Mia_TICS.SelectCor.Value}(2,1));
                        h.Mia_TICS.Threshold_brightness_Max_Edit.String = num2str(MIAData.TICS.Thresholds{h.Mia_TICS.SelectCor.Value}(2,2));
                        h.Mia_TICS.Threshold_counts_Min_Edit.String = num2str(MIAData.TICS.Thresholds{h.Mia_TICS.SelectCor.Value}(3,1));
                        h.Mia_TICS.Threshold_counts_Max_Edit.String = num2str(MIAData.TICS.Thresholds{h.Mia_TICS.SelectCor.Value}(3,2));
                        h.Mia_TICS.Threshold_halflife_Min_Edit.String = num2str(MIAData.TICS.Thresholds{h.Mia_TICS.SelectCor.Value}(4,1));
                        h.Mia_TICS.Threshold_halflife_Max_Edit.String = num2str(MIAData.TICS.Thresholds{h.Mia_TICS.SelectCor.Value}(4,2));
                        h.Mia_TICS.Threshold_samples_Min_Edit.String = num2str(MIAData.TICS.Thresholds{h.Mia_TICS.SelectCor.Value}(5,1));
                        h.Mia_TICS.Threshold_samples_Max_Edit.String = num2str(MIAData.TICS.Thresholds{h.Mia_TICS.SelectCor.Value}(5,2));
                    elseif strcmp(obj.Tag, 'thresholds')
                        % user changed threshold value
                        MIAData.TICS.Thresholds{h.Mia_TICS.SelectCor.Value}(1,1) = str2num(h.Mia_TICS.Threshold_G1_Min_Edit.String);
                        MIAData.TICS.Thresholds{h.Mia_TICS.SelectCor.Value}(1,2) = str2num(h.Mia_TICS.Threshold_G1_Max_Edit.String);
                        MIAData.TICS.Thresholds{h.Mia_TICS.SelectCor.Value}(2,1) = str2num(h.Mia_TICS.Threshold_brightness_Min_Edit.String);
                        MIAData.TICS.Thresholds{h.Mia_TICS.SelectCor.Value}(2,2) = str2num(h.Mia_TICS.Threshold_brightness_Max_Edit.String);
                        MIAData.TICS.Thresholds{h.Mia_TICS.SelectCor.Value}(3,1) = str2num(h.Mia_TICS.Threshold_counts_Min_Edit.String);
                        MIAData.TICS.Thresholds{h.Mia_TICS.SelectCor.Value}(3,2) = str2num(h.Mia_TICS.Threshold_counts_Max_Edit.String);
                        MIAData.TICS.Thresholds{h.Mia_TICS.SelectCor.Value}(4,1) = str2num(h.Mia_TICS.Threshold_halflife_Min_Edit.String);
                        MIAData.TICS.Thresholds{h.Mia_TICS.SelectCor.Value}(4,2) = str2num(h.Mia_TICS.Threshold_halflife_Max_Edit.String);
                        MIAData.TICS.Thresholds{h.Mia_TICS.SelectCor.Value}(5,1) = str2num(h.Mia_TICS.Threshold_samples_Min_Edit.String);
                        MIAData.TICS.Thresholds{h.Mia_TICS.SelectCor.Value}(5,2) = str2num(h.Mia_TICS.Threshold_samples_Max_Edit.String);
                    end
                end
                
                %%% Sets unselected pixels to NaN
                if size(MIAData.TICS.FreehandMask,1)==size(MIAData.TICS.Data{i},1) && size(MIAData.TICS.FreehandMask,2)==size(MIAData.TICS.Data{i},2)...
                    && size(MIAData.TICS.ThreshMask,1)==size(MIAData.TICS.Data{i},1) && size(MIAData.TICS.ThreshMask,2)==size(MIAData.TICS.Data{i},2)
                    % take the thresholds into account in the mask
                    mask(G1 < MIAData.TICS.Thresholds{i}(1,1)) = false;
                    mask(G1 > MIAData.TICS.Thresholds{i}(1,2)) = false;
                    mask(brightness < MIAData.TICS.Thresholds{i}(2,1)) = false;
                    mask(brightness > MIAData.TICS.Thresholds{i}(2,2)) = false;
                    mask(counts < MIAData.TICS.Thresholds{i}(3,1)) = false;
                    mask(counts > MIAData.TICS.Thresholds{i}(3,2)) = false;
                    mask(halflife < MIAData.TICS.Thresholds{i}(4,1)) = false;
                    mask(halflife > MIAData.TICS.Thresholds{i}(4,2)) = false;
                    mask(samples < MIAData.TICS.Thresholds{i}(5,1)) = false;
                    mask(samples > MIAData.TICS.Thresholds{i}(5,2)) = false;
                    % mask based on thresholds for one correlation function applies to all
                    %mask = mask & MIAData.TICS.ThreshMask;
                    MIAData.TICS.ThreshMask = mask;
                    % freehand mask for one correlation function applies to all
                    mask = mask & MIAData.TICS.FreehandMask;
                else
                    % freehand mask for whatever reason has the wrong size
                    % (e.g. Mia is first loaded), initialize them
                    MIAData.TICS.ThreshMask = true(size(MIAData.TICS.Int{i,1}));
                    MIAData.TICS.FreehandMask = true(size(MIAData.TICS.Int{i,1}));
                    mask = MIAData.TICS.FreehandMask;
                end
                % median filter the mask to make the mask look more logical
%                 if h.Mia_TICS.Median.Value
%                     mask = medfilt2(mask, filtsize, 'symmetric');
%                 end
                G1a{i} = G1;
                halflifea{i} = halflife;
                countsa{i} = counts;
                brightnessa{i} = brightness;
                samplesa{i} = samples;
            end
        end
    end
    
    for i=1:3
        % user has previously pressed the TICS calculate at all
        if isfield(MIAData.TICS,'Data')
            %%% 1&2: ACF 1&2
            %%% 3:   CCF
            if size(MIAData.TICS.Data,2)>=i && ~isempty(MIAData.TICS.Data{i})

                    %%% Plots individual pixel data in images
                switch(h.Mia_TICS.SelectImage.Value)
                    case 1 %%% G(first lag)
                        plotdata = G1a{i};
                    case 2 %%% G(first lag)./mean(Counts)
                        plotdata = brightnessa{i};
                    case 3 %%% Mean counts
                        plotdata = countsa{i};
                    case 4 %%% Find G(0)/2
                        plotdata = halflifea{i};
                    case 5 %%% samples
                        plotdata = samplesa{i};
                end
                % Show the data on the image
                h.Plots.TICSImage(i).CData = plotdata;
                % Set the image scaling on the axes
                mini = min(min(plotdata(mask)));
                maxi = max(max(plotdata(mask)));
                % if AROI is not used, each pixel is sampled equally
                if maxi == mini
                    maxi = mini+1;
                end
                h.Mia_TICS.Image(i).CLim = [mini maxi];
            else %%% Hides plots, if no TICS data exists for current channel
                h.Plots.TICS(i,1).Visible = 'off';
                h.Plots.TICS(i,2).Visible = 'off';
                h.Plots.TICSImage(i).Visible = 'off';
                h.Mia_TICS.Image(i,2).Visible = 'off';
                h.Mia_TICS.Image(i,1).Visible = 'off';
            end
        end
    end
    %% Mask all TICS data    
    for i=1:3
        % user has previously pressed the TICS calculate at all
        if isfield(MIAData.TICS,'Data')
            %%% 1&2: ACF 1&2
            %%% 3:   CCF
            if size(MIAData.TICS.Data,2)>=i && ~isempty(MIAData.TICS.Data{i})                
                TICS = MIAData.TICS.Data{i};
                TICS(repmat(~mask,1,1,size(TICS,3))) = NaN;
                %set the intensity NaN outside the mask
                Int1 = MIAData.TICS.Int{i,1};
                Int2 = MIAData.TICS.Int{i,2};
                Int1(~mask)=NaN;
                Int2(~mask)=NaN;
                
                %%% Averages pixel TICS data for selected (~NaN) pixels and
                %%% plots the curve
                ydata = squeeze(nanmean(nanmean(TICS,2),1));
                if h.Mia_TICS.Normalize.Value
                    ydata = (ydata-ydata(end))./(ydata(1)-ydata(end));
                end
                h.Plots.TICS(i,1).YData = ydata;
                h.Plots.TICS(i,1).XData = (1:size(TICS,3)).*str2double(h.Mia_Image.Settings.Image_Frame.String);
                EData = double(squeeze(nanstd(nanstd(TICS,0,2),0,1))');
                EData = EData./sqrt(sum(reshape(~isnan(TICS),[],size(TICS,3)),1));
                h.Plots.TICS(i,1).UData = EData;
                h.Plots.TICS(i,1).LData = EData;
 
                %Int1 nd Int2 are always correct
                data{i}.Counts = [nanmean(nanmean(Int1,2),1) nanmean(nanmean(Int2,2),1)]/str2double(h.Mia_Image.Settings.Image_Pixel.String)*1000;
                data{i}.Valid = 1;
                data{i}.Cor_Times = (1:size(MIAData.TICS.Data{i},3))*str2double(h.Mia_Image.Settings.Image_Frame.String);
                data{i}.Cor_Average = double(squeeze(nanmean(nanmean(TICS,2),1))');
                data{i}.Cor_Array = data{i}.Cor_Average';
                data{i}.Cor_SEM = EData;
                
                %%% Updates fit curve
                Calc_TICS_Fit([],[],i);
                
                %%% Sets transparency of unselected pixels to 80%
                h.Plots.TICSImage(i).AlphaData = (any(~isnan(TICS),3)+0.25)/1.25;
                
                %%% Updates axis and shows plots
                h.Mia_TICS.Image(i,1).XLim = [0 size(TICS,2)]+0.5;
                h.Mia_TICS.Image(i,1).YLim = [0 size(TICS,1)]+0.5;
                h.Plots.TICSImage(i).Visible = 'on';
                h.Mia_TICS.Image(i,2).Visible = 'on';
                h.Mia_TICS.Image(i,1).Visible = 'on';
            end
        end
    end
    
    % Save displayed TICS data
    if ~isempty(obj)
        if strcmp(obj.Tag, 'Save')
            Save_TICS([],[],data)
        end
    end
end

%% Plots STICS/iMSD data
if any(mode==6)
    for i=1:3
        %%% 1&3: ACF 1&2
        %%% 2:   CCF
        if size(MIAData.STICS,2)>=i && ~isempty(MIAData.STICS{i})
            %% 2D STICS Images
            Size = round(str2double(h.Mia_STICS.Size.String));
            if isempty(Size) || Size<1
                Size = 31;
                h.Mia_STICS.Size.String = '31';
            elseif Size > size(MIAData.STICS{i},1) || Size > size(MIAData.STICS{i},2)
                Size = min([size(MIAData.STICS{i},2), size(MIAData.STICS{i},2)]);
                h.Mia_STICS.Size.String = num2str(Size);
            end
            X(1)=ceil(floor(size(MIAData.STICS{i},1)/2)-Size/2)+1;
            X(2)=ceil(floor(size(MIAData.STICS{i},1)/2)+Size/2);
            Y(1)=ceil(floor(size(MIAData.STICS{i},2)/2)-Size/2)+1;
            Y(2)=ceil(floor(size(MIAData.STICS{i},2)/2)+Size/2);
            
            h.Plots.STICSImage(i,1).CData = MIAData.STICS{i}(X(1):X(2),Y(1):Y(2),round(h.Mia_STICS.Lag_Slider.Value+1));
            h.Plots.STICSImage(i,1).Visible = 'on';
            h.Mia_STICS.Image(i,2).Visible = 'on';
            %% Fitted iMSD plot
            Size = str2double(h.Mia_Image.Settings.Pixel_Size.String);
            Time = (0:(numel(MIAData.iMSD{i,1})-1))*str2double(h.Mia_Image.Settings.Image_Frame.String);
            %%% Data
            h.Plots.STICS(i,1).YData = (MIAData.iMSD{i,1}.*Size/1000).^2;
            h.Plots.STICS(i,1).XData = Time;
            h.Plots.STICS(i,1).UData = (MIAData.iMSD{i,2}(:,1).^2-MIAData.iMSD{i,1}.^2).*Size.^2/10^6;
            h.Plots.STICS(i,1).LData = (MIAData.iMSD{i,1}.^2-MIAData.iMSD{i,2}(:,2).^2).*Size.^2/10^6;
            h.Plots.STICS(i,1).Visible = 'on';
            h.Plots.STICS(i,1).Visible = 'on';
            %%% Fit
            Time = linspace(0,Time(end),1000);
            P = cellfun(@str2double,h.Mia_STICS.Fit_Table.Data(1:2:end,i));
            h.Plots.STICS(i,2).YData = P(1)^2+4*P(2).*(Time.^P(3));
            h.Plots.STICS(i,2).XData = Time;
            h.Plots.STICS(i,2).Visible = 'on';
            h.Plots.STICS(i,2).Visible = 'on';
            
            h.Mia_STICS.Axes.XLim = [0 Time(end)];
            
        else
            h.Plots.STICSImage(i,1).Visible = 'off';
            h.Mia_STICS.Image(i,2).Visible = 'off';
            h.Plots.STICS(i,1).Visible = 'off';
            h.Plots.STICS(i,1).Visible = 'off';
            h.Plots.STICS(i,2).Visible = 'off';
            h.Plots.STICS(i,2).Visible = 'off';
        end
    end
end

%% Updates filename display
if numel(MIAData.FileName)==2
    h.Mia_Progress_Text.String = [MIAData.FileName{1}{1} ' / ' MIAData.FileName{2}{1}];
elseif numel(MIAData.FileName)==1
    h.Mia_Progress_Text.String = MIAData.FileName{1}{1};
else
    h.Mia_Progress_Text.String = 'Nothing loaded';
end
h.Mia_Progress_Axes.Color=UserValues.Look.Control;



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Moves through frames %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function Mia_Frame(~,e,mode,channel)
global MIAData

Fig = gcf;
%%% This speeds display up
if strcmp(Fig.Tag,'Mia') 
    h = guidata(Fig);
else
    h = guidata(findobj('Tag','Mia'));
end
if size(MIAData.Data,1)>0
    %%% Determins slider in case of listener callback
    if nargin<4
        mode=e.AffectedObject.UserData(1);
        channel=e.AffectedObject.UserData(2);
    end
    %%% Updates UIs
    switch mode
        case 1 %%% Image frames editbox changed
            Frame=str2double(h.Mia_Image.Settings.Channel_Frame(channel).String);
            %%% Forces frame into bounds
            if Frame>size(MIAData.Data{channel,1},3)
                Frame=size(MIAData.Data{channel,1},3);
                h.Mia_Image.Settings.Channel_Frame(channel).String=num2str(size(MIAData.Data{channel,1},3));
            end
            if mod(Frame,1)~=0
                Frame=round(Frame);
                h.Mia_Image.Settings.Channel_Frame(channel).String=num2str(Frame);
            end
            if Frame<0
                Frame=0;
                h.Mia_Image.Settings.Channel_Frame(channel).String='0';
            end
            h.Mia_Image.Settings.Channel_Frame_Slider(channel).Value=Frame;
        case 2 %%% Image frames slider changed
            
            h.Mia_Image.Settings.Channel_Frame_Listener(1).Enabled=0;
            h.Mia_Image.Settings.Channel_Frame_Listener(2).Enabled=0;
            Frame=h.Mia_Image.Settings.Channel_Frame_Slider(channel).Value;

            if mod(Frame,1)~=0
                Frame=round(Frame);                
                h.Mia_Image.Settings.Channel_Frame_Slider(channel).Value=Frame;             
            end
            if Frame<0
                Frame=0;
            end
            h.Mia_Image.Settings.Channel_Frame(channel).String=num2str(Frame);
            
            % unlink and link channels - matlab issue
            original = h.Mia_Image.Settings.Channel_Link.Value;
            if original
                h.Mia_Image.Settings.Channel_Link.Value = 0;
            else
                h.Mia_Image.Settings.Channel_Link.Value = 1;
            end
            h.Mia_Image.Settings.Channel_Link.Value = original;

            if h.Mia_Image.Settings.Channel_Link.Value
                h.Mia_Image.Settings.Channel_Frame(mod(2*channel,3)).String=num2str(Frame);
                h.Mia_Image.Settings.Channel_Frame_Slider(mod(2*channel,3)).Value=Frame;
                Update_Plots([],[],1);
            else
                Update_Plots([],[],1);
            end
            h.Mia_Image.Settings.Channel_Frame_Listener(1).Enabled=1;
            h.Mia_Image.Settings.Channel_Frame_Listener(2).Enabled=1;
        case 3 %%% Cor frames editbox changed
            Frame = round(str2double(h.Mia_ICS.Frame.String));
            i = find(~cellfun(@isempty,MIAData.Cor),1,'first');
            %%% Forces frame into bounds
            if Frame>size(MIAData.Cor{i},3)
                Frame=size(MIAData.Cor{i},3);
                h.Mia_ICS.Frame.String=num2str(size(MIAData.Cor{i},3));
            elseif Frame<0 || isempty(Frame)
                Frame=0;
                h.Mia_ICS.Frame.String='0';
            end
            h.Mia_ICS.Frame_Slider.Value=Frame;
        case 4 %%% Cor frames slider changed
            Frame=h.Mia_ICS.Frame_Slider.Value;
            if mod(Frame,1)~=0
                Frame=round(Frame);
                h.Mia_ICS.Frame_Slider.Value=Frame;
            end
            h.Mia_ICS.Frame.String=num2str(Frame);
            Update_Plots([],[],2);
        case 5 %%% STICS lag editbox changed
            Lag = round(str2double(h.Mia_STICS.Lag.String));
            i = find(~cellfun(@isempty,MIAData.STICS),1,'first');
            if isempty(i) %%%Stop, if no file is loaded
               return; 
            end
            %%% Forces frame into bounds
            if Lag > size(MIAData.STICS{i},3)
                Lag = size(MIAData.STICS{i},3);
                h.Mia_STICS.Lag.String = num2str(Lag);
            elseif Lag <0 || isempty(Lag)
                Lag = 0;
                h.Mia_STICS.Lag.String = '0';
            end
            %%% Updates Slider
            h.Mia_STICS.Lag_Slider.Value = Lag;
            Update_Plots([],[],6);
        case 6 %%% STICS lag slider changed
           Lag = h.Mia_STICS.Lag_Slider.Value;
           if mod(Lag,1)~=0
               Lag = round(Lag);
               h.Mia_STICS.Lag_Slider.Value = Lag;
           end
           h.Mia_STICS.Lag.String = num2str(Lag);
           Update_Plots([],[],6);
    end
    %%% Sets the frame use value
    if str2double(h.Mia_Image.Settings.Channel_Frame(1).String)>0
        % MIAData.Use is just a 1xFrames logical with the checkboxes
        h.Mia_Image.Settings.Channel_FrameUse.Value=MIAData.Use(str2double(h.Mia_Image.Settings.Channel_Frame(1).String));
    end
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Changes custom plots color %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function Mia_Color(Obj,~)
if ~isdeployed
    Color=uisetcolor;
    if Color == 0
        return
    end
elseif isdeployed %%% uisetcolor dialog does not work in compiled application
    Color = color_setter(Obj.UserData); % open dialog to input color
end
if numel(Color)
    Obj.UserData=Color;
end
Update_Plots([],[],[1, 2]);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Selects\Unselects frames via checkbox %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function Mia_CheckFrame(~,~) %mode is channel
global MIAData
h = guidata(findobj('Tag','Mia'));
MIAData.Use(str2double(h.Mia_Image.Settings.Channel_Frame(1).String))=h.Mia_Image.Settings.Channel_FrameUse.Value;
h.Mia_Image.Settings.ROI_FramesUse.Value=2;
MIA_Various([],[],3)


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Generates corrected images %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function Mia_Correct(~,~,AR,MinMax)
%AR = 0: ignore the freehand ROI while correcting for frame ROI mean or pixel mean
%AR = 1: include the freehand ROI while correcting
%AR = 2: remove the freehand ROI before correction
%MinMax is a list of Min/Max intensities per frame, passed on from Do_RICS mode 6 (intensity-based mapping)

global MIAData UserValues
h = guidata(findobj('Tag','Mia'));

if isempty(MIAData.Data)
    % no data loaded
    return
else
    % data loaded
    h.Mia_Progress_Text.String = 'Applying Correction';
    h.Mia_Progress_Axes.Color=[1 0 0];
    drawnow;
end

%%% Clears freehand drawn ROIs
if AR==2  
    Mia_Freehand([],[],3,0);
end
%%% Performs Arbitrary Region selection
if h.Mia_Image.Settings.ROI_FramesUse.Value == 3 && AR~=0
    if ~isvar('MinMax')
        MinMax = [];
    end
    Mia_Arbitrary_Region([],[],MinMax);
end

%%% Extracts ROI position
From=h.Plots.ROI(1).Position(1:2)+0.5;
To=From+h.Plots.ROI(1).Position(3:4)-1;

h.Mia_Image.Settings.Correction_Subtract_Pixel.Visible='off';
h.Mia_Image.Settings.Correction_Subtract_Pixel_Text.Visible='off';
h.Mia_Image.Settings.Correction_Subtract_Frames.Visible='off';
h.Mia_Image.Settings.Correction_Subtract_Frames_Text.Visible='off';
h.Mia_Image.Settings.Correction_Add_Pixel.Visible='off';
h.Mia_Image.Settings.Correction_Add_Pixel_Text.Visible='off';
h.Mia_Image.Settings.Correction_Add_Frames.Visible='off';
h.Mia_Image.Settings.Correction_Add_Frames_Text.Visible='off';

for i=1:size(MIAData.Data,1)
    if ~isempty(MIAData.Data{i,1})
        MIAData.Data{i,2}=single(MIAData.Data{i,1}(From(2):To(2),From(1):To(1),:));
    end
end

%% Correct by subtracting or adding
for i=1:size(MIAData.Data,1)
    if ~isempty(MIAData.Data{i,1})
        %% Adds to image
        switch h.Mia_Image.Settings.Correction_Add.Value
            case 1 %%% Do nothing
            case 2 %%% Total ROI mean
                Add=MIAData.Data{i,2};
                if h.Mia_Image.Settings.ROI_FramesUse.Value == 3
                    Add(~(repmat(MIAData.MS{1},[1 1 size(MIAData.AR{i,1},3)]) & MIAData.AR{i,1}))=NaN;
                end
                MIAData.Data{i,2}=MIAData.Data{i,2} + nanmean(Add(:));
                clear Add
            case 3 %%% Frame ROI mean
                Add=MIAData.Data{i,2};
                if AR~=0 && h.Mia_Image.Settings.ROI_FramesUse.Value == 3
                    Add(~(repmat(MIAData.MS{1},[1 1 size(MIAData.AR{i,1},3)]) & MIAData.AR{i,1}))=NaN;
                end
                MIAData.Data{i,2}=MIAData.Data{i,2} + repmat(nanmean(nanmean(Add)),[(To(2)-From(2)+1),(To(1)-From(1)+1),1]);
            case 4 %%% Pixel mean
                Add=MIAData.Data{i,2};
                if AR~=0 && h.Mia_Image.Settings.ROI_FramesUse.Value == 3
                    Add(~(repmat(MIAData.MS{1},[1 1 size(MIAData.AR{i,1},3)]) & MIAData.AR{i,1}))=NaN;
                end
                MIAData.Data{i,2}=MIAData.Data{i,2} + (repmat(nanmean(Add,3),[1,1,size(MIAData.Data{i,1},3)]));
            case 5 %%% Moving average
                h.Mia_Image.Settings.Correction_Add_Pixel.Visible='on';
                h.Mia_Image.Settings.Correction_Add_Pixel_Text.Visible='on';
                h.Mia_Image.Settings.Correction_Add_Frames.Visible='on';
                h.Mia_Image.Settings.Correction_Add_Frames_Text.Visible='on';
                Box=[str2double(h.Mia_Image.Settings.Correction_Add_Pixel.String), str2double(h.Mia_Image.Settings.Correction_Add_Pixel.String), str2double(h.Mia_Image.Settings.Correction_Add_Frames.String)];

                %%% Forces averaging sizes into bounds
                if any(Box<1) || any(Box>size(MIAData.Data{i,2}))
                    Box(Box<1)=1;
                    if Box(1)>size(MIAData.Data{i,2},1)
                        Box(1)=size(MIAData.Data{i,2},1);
                    end
                    if Box(2)>size(MIAData.Data{i,2},2)
                        Box(2)=size(MIAData.Data{i,2},2);
                    end
                    if Box(3)>size(MIAData.Data{i,2},3)
                        Box(3)=min(size(MIAData.Data{i,2},3));
                    end
                    h.Mia_Image.Settings.Correction_Add_Pixel.String=num2str(Box(1));
                    h.Mia_Image.Settings.Correction_Add_Frames.String=num2str(Box(3));
                end
                %%% Calculates Filter
                Filter=ones(Box)/prod(Box);
                MIAData.Data{i,2}=MIAData.Data{i,2} + imfilter(MIAData.Data{i,2},Filter,'replicate');
        end
        %% Subtracts from image
        switch h.Mia_Image.Settings.Correction_Subtract.Value
            case 1 %%% Do nothing
            case 2 %%% Frame ROI mean
                Sub=single(MIAData.Data{i,1}(From(2):To(2),From(1):To(1),:));
                if AR~=0 && h.Mia_Image.Settings.ROI_FramesUse.Value == 3
                    Sub(~(repmat(MIAData.MS{1},[1 1 size(MIAData.AR{i,1},3)]) & MIAData.AR{i,1}))=NaN;
                end
                MIAData.Data{i,2}=MIAData.Data{i,2} - (repmat(nanmean(nanmean(Sub)),[(To(2)-From(2)+1),(To(1)-From(1)+1),1]));
            case 3 %%% Pixel mean
                Sub=single(MIAData.Data{i,1}(From(2):To(2),From(1):To(1),:));
                if AR~=0 && h.Mia_Image.Settings.ROI_FramesUse.Value == 3
                    Sub(~(repmat(MIAData.MS{1},[1 1 size(MIAData.AR{i,1},3)]) & MIAData.AR{i,1}))=NaN;
                end
                MIAData.Data{i,2}=MIAData.Data{i,2} - (repmat(nanmean(Sub,3),[1,1,size(MIAData.Data{i,1},3)]));
            case 4 %%% Moving average
                h.Mia_Image.Settings.Correction_Subtract_Pixel.Visible='on';
                h.Mia_Image.Settings.Correction_Subtract_Pixel_Text.Visible='on';
                h.Mia_Image.Settings.Correction_Subtract_Frames.Visible='on';
                h.Mia_Image.Settings.Correction_Subtract_Frames_Text.Visible='on';
                Box=[str2double(h.Mia_Image.Settings.Correction_Subtract_Pixel.String),...
                    str2double(h.Mia_Image.Settings.Correction_Subtract_Pixel.String),...
                    str2double(h.Mia_Image.Settings.Correction_Subtract_Frames.String)];

                %%% Forces averaging sizes into bounds
                if any(Box<1) || any(Box>size(MIAData.Data{i,2}))
                    Box(Box<1)=1;
                    if Box(1)>size(MIAData.Data{i,2},1)
                        Box(1)=size(MIAData.Data{i,2},1);
                    end
                    if Box(2)>size(MIAData.Data{i,2},2)
                        Box(2)=size(MIAData.Data{i,2},2);
                    end
                    if Box(3)>size(MIAData.Data{i,2},3)
                        Box(3)=min(size(MIAData.Data{i,2},3));
                    end
                    h.Mia_Image.Settings.Correction_Subtract_Pixel.String=num2str(Box(1));
                    h.Mia_Image.Settings.Correction_Subtract_Frames.String=num2str(Box(3));
                end
                %%% Calculates Filter
                Filter=ones(Box)/prod(Box);
                MIAData.Data{i,2}=MIAData.Data{i,2} - imfilter(single(MIAData.Data{i,1}(From(2):To(2),From(1):To(1),:)),Filter,'replicate');
        end

        %%% Removes NaNs from file
        %%% Sometimes happens with filtered data
        MIAData.Data{i,2}(isnan(MIAData.Data{i,2})) = 0;
        %subtract the background stored displayed on the ROI tab
        MIAData.Data{i,2}=MIAData.Data{i,2}-UserValues.MIA.BG(i);

    end
end
Update_Plots([],[],[1,4]);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Calculates arbitrary regions %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function Mia_Arbitrary_Region(~,~,MinMax)
h = guidata(findobj('Tag','Mia'));
global MIAData UserValues
%%% Uses Intensity and Variance thresholding to remove bad pixels
%%% ROI borders
From=h.Plots.ROI(1).Position(1:2)+0.5;
To=From+h.Plots.ROI(1).Position(3:4)-1;

if size(MIAData.Data,1)==0
    return;
end

%user wants Channel i to be the one and only mask
for i = 1:size(MIAData.Data,1)
    if ~isempty(MIAData.Data{i,1})
        if UserValues.MIA.AR_Same==(i+1)
            Channel = i;
        end
    end
end

if strcmp(h.Mia_Image.Settings.ROI_AR_Same.String(h.Mia_Image.Settings.ROI_AR_Same.Value),'AND') || strcmp(h.Mia_Image.Settings.ROI_AR_Same.String(h.Mia_Image.Settings.ROI_AR_Same.Value),'OR')
    Channel = 1:size(MIAData.Data,1);
end

%%% subregion sizes
Var_Sub = UserValues.MIA.AR_Region(2); %large region
Var_SubSub = UserValues.MIA.AR_Region(1); %small region

%%% Actually calculates arbitrary regions
for i=Channel
    %%% Thresholding Parameters
    if isempty(MinMax)
        %normal RICS analysis
        Int_Max = str2double(h.Mia_Image.Settings.Image_Pixel.String)*UserValues.MIA.AR_Int(10+i)/1000; %photons per pixel
        Int_Min = str2double(h.Mia_Image.Settings.Image_Pixel.String)*UserValues.MIA.AR_Int(i)/1000;
        Int_Max = repmat(Int_Max, 1, size(MIAData.Data{i,1},3));
        Int_Min = repmat(Int_Min, 1, size(MIAData.Data{i,1},3));
    else %intensity based RICS mapping
        %d = h.Mia_Image.Settings.ROI_AR_Same.Value-1; %(the channel that defines the mask)
        Int_Max = MinMax(:,2); %photons per pixel
        Int_Min = MinMax(:,1);
    end
    Int_Fold_Max = UserValues.MIA.AR_Int_Fold(10+i);
    Int_Fold_Min = UserValues.MIA.AR_Int_Fold(i);
    Var_Fold_Max = UserValues.MIA.AR_Var_Fold(10+i);
    Var_Fold_Min = UserValues.MIA.AR_Var_Fold(i);

    %% Static region intensity thresholding for arbitrary region ICS
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %%% Because the intenities per pixel are very low, the tresholding
    %%% works on the sum of the stack
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %%% Thresholding works on the mean (frame dimension) of the uncorrected data
    Data=mean(single(MIAData.Data{i,1}(From(2):To(2),From(1):To(1),:)),3);
    %%% Logical array to determin which pixels to use
    Use=true(size(Data));
    if ~UserValues.MIA.AR_Framewise
        %%% Removes pixel below an intensity threshold set in kHz
        if Int_Min(1)>0
            Use(Data<Int_Min(1))=false;
        end
        %%% Removes pixel above an intensity threshold set in kHz
        if Int_Max(1)>Int_Min(1)
            Use(Data>Int_Max(1))=false;
        end
        if UserValues.MIA.AR_Median
            Use = medfilt2(Use,[Var_SubSub,Var_SubSub]);
        end
    end
    MIAData.AR{i,2}=Use;

    %% Sliding window thresholding for arbitrary region ICS
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %%% The variance and intensity in a small rectangular region is
    %%% calculated and compared to the variance in a bigger region
    %%% around it
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %%% Thresholding operates on uncorrected or (see below) corrected data
    Data=single(MIAData.Data{i,1}(From(2):To(2),From(1):To(1),:));
    %%% Extends aray to determine which pixels to use, because now
    %%% every frame is calculated individually
    Use=repmat(Use,[1 1 size(Data,3)]);
    if Var_SubSub>1 && Var_Sub>Var_SubSub
        Start=ceil(Var_Sub/2)-1;
        Stop=floor(Var_Sub/2)-1;
        Filter1=ones(Var_SubSub)/(Var_SubSub)^2;
        Filter2=ones(Var_Sub)/(Var_Sub^2);

        BleachFact = 1;

        switch h.Mia_Image.Calculations.Cor_TICS_SpaceAvg.Value
            case 1 %%% Do nothing
                Filter = fspecial('average',1);
            case 2 %%% square average
                Filter = fspecial('average',round(str2double(h.Mia_Image.Calculations.Cor_TICS_AvgSize.String)));
            case 3 %%% Disc average
                Filter = fspecial('disk',str2double(h.Mia_Image.Calculations.Cor_TICS_AvgSize.String)-1);
            case 4 %%% Gaussian average
                Filter = fspecial('gaussian',2*str2double(h.Mia_Image.Calculations.Cor_TICS_AvgSize.String),str2double(h.Mia_Image.Calculations.Cor_TICS_AvgSize.String)/2);
        end
        scale = 1.2;

        for j=1:size(Data,3)
            %%% Calculates mean of both subregions
            Mean1=filter2(Filter1,Data(:,:,j));
            Mean2=filter2(Filter2,Data(:,:,j));

            if isempty(MinMax)
                %%% User enters only 1 MIN/MAX value in the UI, so if bleaching
                %%% occurs this has to be taken into account
                BleachFact = mean2(Data(:,:,j))/mean2(Data(:,:,1));
            end

            %%% Calculates population variance for both subregions (sample2population var)
            Var1=(filter2(Filter1,Data(:,:,j).^2)-Mean1.^2)*(Var_SubSub^2/(Var_SubSub^2-1));
            Var2=(filter2(Filter2,Data(:,:,j).^2)-Mean2.^2)*((Var_Sub^2)/(Var_Sub^2-1));

            %%% Discards samples with too low\high variance
            if Var_Fold_Max>1
                Use(:,:,j)=Use(:,:,j) & (Var1<(Var2*Var_Fold_Max));
            end
            if Var_Fold_Min<1 && Var_Fold_Min>0
                Use(:,:,j)=Use(:,:,j) & (Var1>(Var2*Var_Fold_Min));
            end
            %%% Discards samples with too low\high relative intensities
            if Int_Fold_Max>1
                Use(:,:,j)=Use(:,:,j) & (Mean1<(Mean2*Int_Fold_Max));
            end
            if Int_Fold_Min<1 && Int_Fold_Min>0
                Use(:,:,j)=Use(:,:,j) & (Mean1>(Mean2*Int_Fold_Min));
            end
            if UserValues.MIA.AR_Framewise
                if h.Mia_Image.Settings.Channel_Frame_Slider(1).Value == 0
                    if i == numel(Channel) 
                        % only do this once all channels have their MIAData.AR initialized
                        % don't show the average frame cause the mask will not be visible
                        h.Mia_Image.Settings.Channel_Frame_Slider(1).Value = 1;
                    end
                end
                %%% Discards samples with too low\high absolute intensities
                if Int_Max(j)>Int_Min(j)
                    Use(:,:,j)=Use(:,:,j) & ((Mean1/BleachFact)<Int_Max(j));
                end
                if Int_Min(j)>0
                    Use(:,:,j)=Use(:,:,j) & ((Mean1/BleachFact)>Int_Min(j));
                end
            end
            %%% For very low frame intensities, the IntFold and VarFold calculations fail
            %%% Include regions again in the ROI that contained no signal to begin with
            Mean1_zero = false(size(Mean1));
            Mean1_zero(Mean1==0) = true;
            Use(:,:,j) = Use(:,:,j) | Mean1_zero;

            %%% Additionally threshold the corrected image
            %%% Specify the strength of the thresholding with Intensity [Fold]
            %%% Specify the size of the image averaging on the Correlate tab
            %%% Determines spatial filter
            if h.Mia_Image.Settings.ROI_AR_ThresholdCorr.Value && size(MIAData.Data,2)>1
                use = Use(:,:,j);
                tmp = filter2(Filter,MIAData.Data{i,2}(:,:,j));
                meantmp = mean2(tmp);
                use(tmp<meantmp/scale) = false;
                use(tmp>meantmp*scale) = false;
                Use(:,:,j) = use;
            end

            %%% median filter the image
            if UserValues.MIA.AR_Median
                Use(:,:,j) = medfilt2(Use(:,:,j),[Var_SubSub,Var_SubSub]);
            end
        end

        %%% Discards border pixels, where variance and intensity were not calculated
        Use(1:Start,:,:)=false; Use(:,1:Start,:)=false;
        if size(Use,1)-Stop == 0
            h.Mia_Image.Settings.ROI_SizeX.String = num2str(size(MIAData.Data{1,1},1));
            h.Mia_Image.Settings.ROI_SizeY.String = num2str(size(MIAData.Data{1,1},2));
            h.Mia_Image.Settings.ROI_PosX.String = num2str(1);
            h.Mia_Image.Settings.ROI_PosY.String = num2str(1);
            Mia_ROI(h.Mia_Image.Settings.ROI_PosY,[],1)
            return
        end

        Use(end-Stop:end,:,:)=false; Use(:,end-Stop:end,:)=false;

    end

    %%% Removes pixels, if invalid pixels were used for averaging
    % this will actually remove quite some pixels, which in turn cause RICS
    % mapping in equally sized AROIs not to work that perfectly.
    if h.Mia_Image.Settings.Correction_Add.Value==5 || h.Mia_Image.Settings.Correction_Subtract.Value==4
        % you add the moving average or subtract the moving average
        if h.Mia_Image.Settings.Correction_Add.Value==5
            % you add the moving average
            Box1=[str2double(h.Mia_Image.Settings.Correction_Add_Pixel.String), str2double(h.Mia_Image.Settings.Correction_Add_Frames.String)];
        else
            Box1=[1 1];
        end
        if h.Mia_Image.Settings.Correction_Subtract.Value==4
            % you subtract the moving average
            Box2=[str2double(h.Mia_Image.Settings.Correction_Subtract_Pixel.String), str2double(h.Mia_Image.Settings.Correction_Subtract_Frames.String)];
        else
            Box2=[1 1];
        end
        Box=max([Box1;Box2]);
        Filter=ones(Box(1),Box(1),Box(2))/(Box(1)^2*Box(2));
        Use=logical(floor(imfilter(single(Use),Filter,'replicate')));
    end

    MIAData.AR{i,1}=Use;
    clear Data;
end

switch h.Mia_Image.Settings.ROI_AR_Same.String{h.Mia_Image.Settings.ROI_AR_Same.Value}
    case 'OR' %%% mask in each channel can be different
        for i = 1:size(MIAData.Data,1)
            if ~isempty(MIAData.Data{i,1})
                MIAData.MS{i,1} = MIAData.MS{i,2};
            end
        end
    case 'AND' %%% Both channels
        for j = 1:size(MIAData.Data,1)
            for i = 1:(size(MIAData.Data,1)-1)
                if ~isempty(MIAData.Data{i,1})
                    MIAData.AR{j,1} = MIAData.AR{1,1} & MIAData.AR{i+1,1};
                    MIAData.AR{j,2} = MIAData.AR{1,2} & MIAData.AR{i+1,2};
                    MIAData.MS{j,1} = MIAData.MS{1,1} & MIAData.MS{i+1,1};
                end
            end
        end
    otherwise %%% the mask in one channel is mapped onto all channels
        channel = h.Mia_Image.Settings.ROI_AR_Same.Value-1;
        for i = 1:size(MIAData.Data,1)
            if ~isempty(MIAData.Data{i,1})
                MIAData.AR{i,1} = MIAData.AR{channel,1};
                MIAData.AR{i,2} = MIAData.AR{channel,2};
                MIAData.MS{i,1} = MIAData.MS{channel,2};
            end
        end
        MIAData.MS{channel,1} = MIAData.MS{channel,2};

end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Calculates arbitrary regions %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function Mia_Freehand(~,~,mode,Correct)
h = guidata(findobj('Tag','Mia'));
global MIAData UserValues

% mode = 1 %%% Select Region for general manual selection
% mode = 2 %%% Unselect Region for general manual selection
% mode = 3 %%% Clear Region for general manual selection
% mode = 4 %%% Select Region for TICS manual selection
% mode = 5 %%% Unselect Region for TICS manual selection
% mode = 6 %%% Clear Region for TICS manual selection
% mode = 7 %%% freehand ROI selection for FRAP module       

if isempty(MIAData.Data)
    return;
end


switch mode
    %%% General manual selection
    case 1 %%% Select Region for general manual selection
        ROI = imfreehand;
        Mask = createMask(ROI);
        delete(ROI);
        switch gca
            case h.Mia_Image.Axes(1,2)
                d = h.Mia_Image.Settings.Channel_PIE(1).Value; %(the datachannel)
                if any(~MIAData.MS{d,2}(:))
                    MIAData.MS{d,2} = MIAData.MS{d,2} | Mask;
                else
                    MIAData.MS{d,2} = Mask;
                end
            case h.Mia_Image.Axes(2,2)
                d = h.Mia_Image.Settings.Channel_PIE(2).Value; %(the datachannel)
                if any(~MIAData.MS{d,2}(:))
                    MIAData.MS{d,2} = MIAData.MS{d,2} | Mask;
                else
                    MIAData.MS{d,2} = Mask;
                end
            case h.Mia_Image.Axes(1,1)
                d = h.Mia_Image.Settings.Channel_PIE(1).Value; %(the datachannel)
                Mask = Mask(str2double(h.Mia_Image.Settings.ROI_PosY.String):str2double(h.Mia_Image.Settings.ROI_PosY.String)+str2double(h.Mia_Image.Settings.ROI_SizeY.String)-1,...
                    str2double(h.Mia_Image.Settings.ROI_PosX.String):str2double(h.Mia_Image.Settings.ROI_PosX.String)+str2double(h.Mia_Image.Settings.ROI_SizeX.String)-1);
                if any(~MIAData.MS{d,2}(:))
                    MIAData.MS{d,2} = MIAData.MS{d,2} | Mask;
                else
                    MIAData.MS{d,2} = Mask;
                end
            case h.Mia_Image.Axes(2,1)
                d = h.Mia_Image.Settings.Channel_PIE(2).Value; %(the datachannel)
                Mask = Mask(str2double(h.Mia_Image.Settings.ROI_PosY.String):str2double(h.Mia_Image.Settings.ROI_PosY.String)+str2double(h.Mia_Image.Settings.ROI_SizeY.String)-1,...
                    str2double(h.Mia_Image.Settings.ROI_PosX.String):str2double(h.Mia_Image.Settings.ROI_PosX.String)+str2double(h.Mia_Image.Settings.ROI_SizeX.String)-1);
                if any(~MIAData.MS{d,2}(:))
                    MIAData.MS{d,2} = MIAData.MS{d,2} | Mask;
                else
                    MIAData.MS{d,2} = Mask;
                end
        end

        switch h.Mia_Image.Settings.ROI_AR_Same.String{h.Mia_Image.Settings.ROI_AR_Same.Value}
            case 'OR' %%% Individual channels
                for i = 1:size(MIAData.Data,1)
                    if ~isempty(MIAData.Data{i,1})
                        MIAData.MS{i,1} = MIAData.MS{i,2};
                    end
                end
            case 'AND' %%% Both channels
                for i = 1:size(MIAData.Data,1)
                    for j = 1:(size(MIAData.Data,1)-1)
                        if ~isempty(MIAData.Data{i,1})
                            MIAData.MS{i,1} = MIAData.MS{1,1} & MIAData.MS{j+1,1};
                        end
                    end
                end
            otherwise  %%% One channel is mapped onto all others
                channel = h.Mia_Image.Settings.ROI_AR_Same.Value-1;
                for i = 1:size(MIAData.Data,1)
                    if ~isempty(MIAData.Data{i,1})
                        MIAData.MS{i,1} = MIAData.MS{channel,2};
                    end
                end
            
        end
        Mia_Correct([],[],1);
    case 2 %%% Unselect Region for general manual selection
        ROI = imfreehand;
        Mask = createMask(ROI);
        delete(ROI);

        switch gca
            case h.Mia_Image.Axes(1,2)
                d = h.Mia_Image.Settings.Channel_PIE(1).Value; %(the datachannel)
                MIAData.MS{d,2} = MIAData.MS{d,2} & ~Mask;
            case h.Mia_Image.Axes(2,2)
                d = h.Mia_Image.Settings.Channel_PIE(2).Value; %(the datachannel)
                MIAData.MS{d,2} = MIAData.MS{d,2} & ~Mask;
            case h.Mia_Image.Axes(1,1)
                d = h.Mia_Image.Settings.Channel_PIE(1).Value; %(the datachannel)
                Mask = Mask(str2double(h.Mia_Image.Settings.ROI_PosY.String):str2double(h.Mia_Image.Settings.ROI_PosY.String)+str2double(h.Mia_Image.Settings.ROI_SizeY.String)-1,...
                    str2double(h.Mia_Image.Settings.ROI_PosX.String):str2double(h.Mia_Image.Settings.ROI_PosX.String)+str2double(h.Mia_Image.Settings.ROI_SizeX.String)-1);
                MIAData.MS{d,2} = MIAData.MS{d,2} & ~Mask;
            case h.Mia_Image.Axes(2,1)
                d = h.Mia_Image.Settings.Channel_PIE(2).Value; %(the datachannel)
                Mask = Mask(str2double(h.Mia_Image.Settings.ROI_PosY.String):str2double(h.Mia_Image.Settings.ROI_PosY.String)+str2double(h.Mia_Image.Settings.ROI_SizeY.String)-1,...
                    str2double(h.Mia_Image.Settings.ROI_PosX.String):str2double(h.Mia_Image.Settings.ROI_PosX.String)+str2double(h.Mia_Image.Settings.ROI_SizeX.String)-1);
                MIAData.MS{d,2} = MIAData.MS{d,2} & ~Mask;
        end

        switch h.Mia_Image.Settings.ROI_AR_Same.String{h.Mia_Image.Settings.ROI_AR_Same.Value}
            case 'OR'
                for i = 1:size(MIAData.Data,1)
                    if ~isempty(MIAData.Data{i,1})
                        MIAData.MS{i,1} = MIAData.MS{i,2};
                    end
                end
            case 'AND'
                for i = 1:size(MIAData.Data,1)
                    for j = 1:(size(MIAData.Data,1)-1)
                        if ~isempty(MIAData.Data{i,1})
                            MIAData.MS{i,1} = MIAData.MS{1,1} & MIAData.MS{j+1,1};
                        end
                    end
                end
            otherwise
                channel = h.Mia_Image.Settings.ROI_AR_Same.Value-1;
                for i = 1:size(MIAData.Data,1)
                    if ~isempty(MIAData.Data{i,1})
                        MIAData.MS{i,1} = MIAData.MS{channel,2};
                    end
                end

        end
        Mia_Correct([],[],1);
    case 3 %%% Clear Region for general manual selection
        for i=1:size(MIAData.Data,1)
            if ~isempty(MIAData.Data{i,1})
                MIAData.MS{i,2} = true(str2double(h.Mia_Image.Settings.ROI_SizeY.String),str2double(h.Mia_Image.Settings.ROI_SizeX.String));
            end
        end
        if strcmp(h.Mia_Image.Settings.ROI_AR_Same.String{h.Mia_Image.Settings.ROI_AR_Same.Value},'AND')
            for j = 1:size(MIAData.Data,1)
                for i = 1:(size(MIAData.Data,1)-1)
                    if ~isempty(MIAData.Data{i,1})
                        MIAData.MS{j,1} = MIAData.MS{1,1} & MIAData.MS{i+1,1};
                    end
                end
            end
        end

        switch h.Mia_Image.Settings.ROI_AR_Same.String{h.Mia_Image.Settings.ROI_AR_Same.Value}
            case 'OR' %%% Individual channels
                for i = 1:size(MIAData.Data,1)
                    if ~isempty(MIAData.Data{i,1})
                        MIAData.MS{i,1} = MIAData.MS{i,2};
                    end
                end
            case 'AND' %%% Both channels
                for j = 1:size(MIAData.Data,1)
                    for i = 1:(size(MIAData.Data,1)-1)
                        if ~isempty(MIAData.Data{i,1})
                            MIAData.MS{j,1} = MIAData.MS{i,2} & MIAData.MS{i+1,2};
                        end
                    end
                end
            otherwise %%% the mask in one channel is mapped onto all channels
                channel = h.Mia_Image.Settings.ROI_AR_Same.Value-1;
                for i = 1:size(MIAData.Data,1)
                    if ~isempty(MIAData.Data{i,1})
                        MIAData.MS{i,1} = MIAData.MS{channel,2};
                    end
                end
                MIAData.MS{channel,1} = MIAData.MS{channel,2};
            
        end
        if nargin<4 || Correct~=0
            Mia_Correct([],[],0);
            %Update_Plots([],[],1,1:size(MIAData.Data,1));
        end
    %%% TICS manual selection 
    case 4 %%% Select Region for TICS manual selection
        ROI = imfreehand;
        Mask = createMask(ROI);
        delete(ROI);        
        if any(~MIAData.TICS.FreehandMask(:))
            MIAData.TICS.FreehandMask = MIAData.TICS.FreehandMask | Mask;
        else
            MIAData.TICS.FreehandMask = Mask;
        end
        Update_Plots([],[],5);
    case 5 %%% Unselect Region for TICS manual selection
        ROI = imfreehand;
        Mask = createMask(ROI);
        delete(ROI);
        if ~isempty(MIAData.TICS.FreehandMask)
            MIAData.TICS.FreehandMask = MIAData.TICS.FreehandMask & ~Mask;
        else
            MIAData.TICS.FreehandMask = ~Mask;
        end
        Update_Plots([],[],5);
    case 6 %%% Clear Region for TICS manual selection
        MIAData.TICS.FreehandMask = true(size(MIAData.TICS.Int{1,1}));
        Update_Plots([],[],5);
        
    case 7 %%% freehand ROI selection for FRAP module       
        % clears previously drawn ROI
        f = findobj(h.Mia_Image.Axes, 'Tag', 'FRAP_Freehand');
        delete(f)
        % create Freehand object
        ROI = drawfreehand(gca, 'Tag', 'FRAP_Freehand', 'Deletable', false);
        if ~isempty(ROI.Position)
            MIAData.FRAP.Freehand = copyobj(ROI, h.Mia_Image.Axes(:,2));
            MIAData.FRAP.ROI_Link = linkprop(MIAData.FRAP.Freehand, {'Position', 'Waypoints'});
        else
            return
        end
        delete(ROI)
        %%% sets the channel(s) to show
        numChannels = size(MIAData.Data, 1);
        channel = 1:numChannels == h.Mia_Image.Calculations.FRAP_Channel.Value...
            | numChannels < h.Mia_Image.Calculations.FRAP_Channel.Value;
        set(MIAData.FRAP.Freehand(~channel), 'Visible', 'off')
end
    
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Function to update ROI position and size %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function Mia_ROI(obj,e,mode)
global MIAData UserValues
h = guidata(findobj('Tag','Mia'));

h.Mia_Progress_Text.String = 'Calculating ROI';
h.Mia_Progress_Axes.Color=[1 0 0];  
drawnow;

if ~isempty(MIAData.Data)
    d = h.Mia_Image.Settings.Channel_PIE(1).Value; %(the datachannel) 
    switch mode
        case 1 %%% Editboxes were changed
            %%% Update Size
            Size=round([str2double(h.Mia_Image.Settings.ROI_SizeX.String) str2double(h.Mia_Image.Settings.ROI_SizeY.String)]);
            %%% Forces ROI size into bounds
            if Size(1)>size(MIAData.Data{d,1},2) || Size(1)<1
                Size(1)=size(MIAData.Data{d,1},2);
                h.Mia_Image.Settings.ROI_SizeX.String=num2str(Size(1));
            end
            if Size(2)>size(MIAData.Data{d,1},1) || Size(2)<1
                Size(2)=size(MIAData.Data{d,1},1);
                h.Mia_Image.Settings.ROI_SizeY.String=num2str(Size(2));
            end
            %%% Update Position
            Pos=round([str2double(h.Mia_Image.Settings.ROI_PosX.String) str2double(h.Mia_Image.Settings.ROI_PosY.String)]);
            %%% Forces ROI size into bounds
            Pos(Pos<1)=1;
            if (Pos(1)+Size(1)-1)>size(MIAData.Data{d,1},2)
                Pos(1)=(size(MIAData.Data{d,1},2)-Size(1)+1);
                h.Mia_Image.Settings.ROI_PosX.String=num2str(Pos(1));
            end
            if (Pos(2)+Size(2)-1)>size(MIAData.Data{d,1},1)
                Pos(2)=(size(MIAData.Data{d,1},1)-Size(2)+1);
                h.Mia_Image.Settings.ROI_PosY.String=num2str(Pos(2));
            end
        case 2 %%% Image was clicked
            Type=h.Mia.SelectionType;
            switch Type
                case 'normal' %%% Centers on point
                    %%% Update Size
                    Size=round([str2double(h.Mia_Image.Settings.ROI_SizeX.String) str2double(h.Mia_Image.Settings.ROI_SizeY.String)]);
                    %%% Forces ROI size into bounds
                    if Size(1)>size(MIAData.Data{d,1},2) || Size(1)<1
                        Size(1)=size(MIAData.Data{d,1},2);
                        h.Mia_Image.Settings.ROI_SizeX.String=num2str(Size(1));
                    end
                    if Size(2)>size(MIAData.Data{d,1},1) || Size(2)<1
                        Size(2)=size(MIAData.Data{d,1},1);
                        h.Mia_Image.Settings.ROI_SizeY.String=num2str(Size(2));
                    end
                    %%% Updates position
                    Pos=round(e.Source.Parent.CurrentPoint(1,1:2)-Size/2);
                    h.Mia_Image.Settings.ROI_PosX.String=num2str(Pos(1));
                    h.Mia_Image.Settings.ROI_PosY.String=num2str(Pos(2));                    
                    %%% Forces ROI size into bounds
                    Pos(Pos<1)=1;
                    if (Pos(1)+Size(1)-1)>size(MIAData.Data{d,1},2)
                        Pos(1)=(size(MIAData.Data{d,1},2)-Size(1)+1);
                        h.Mia_Image.Settings.ROI_PosX.String=num2str(Pos(1));
                    end
                    if (Pos(2)+Size(2)-1)>size(MIAData.Data{d,1},1)
                        Pos(2)=(size(MIAData.Data{d,1},1)-Size(2)+1);
                        h.Mia_Image.Settings.ROI_PosY.String=num2str(Pos(2));
                    end
                case 'alt' %%% Draw ROI
                    %%% Turns off ROI during selection
                    h.Plots.ROI(1).Visible='off';
                    h.Plots.ROI(2).Visible='off';
                    % h.Plots.ROI(3).Visible='off';
                    % h.Plots.ROI(4).Visible='off';
                    %%% Determins selected area via dinamic box
                    Start=e.Source.Parent.CurrentPoint(1,1:2);
                    rbbox;
                    Stop=e.Source.Parent.CurrentPoint(1,1:2);
                    %%% Forces edges into bounds
                    if Stop(1)<e.Source.Parent.XLim(1)
                        Stop(1)=e.Source.Parent.XLim(1);
                    end
                    if Stop(1)>e.Source.Parent.XLim(2)
                        Stop(1)=e.Source.Parent.XLim(2);
                    end
                    if Stop(2)<e.Source.Parent.YLim(1)
                        Stop(2)=e.Source.Parent.YLim(1);
                    end
                    if Stop(2)>e.Source.Parent.YLim(2)
                        Stop(2)=e.Source.Parent.YLim(2);
                    end
                    %%% Updates position and size
                    Pos=[round(min(Start(1),Stop(1))+0.5) round(min(Start(2),Stop(2))+0.5)];
                    Size=[round(abs(Start(1)-Stop(1))) round(abs(Start(2)-Stop(2)))];
                    Size(Size<1)=1;
                    h.Mia_Image.Settings.ROI_PosX.String=num2str(Pos(1));
                    h.Mia_Image.Settings.ROI_PosY.String=num2str(Pos(2));                    
                    h.Mia_Image.Settings.ROI_SizeX.String=num2str(Size(1));
                    h.Mia_Image.Settings.ROI_SizeY.String=num2str(Size(2));
                    %%% Turns ROIs back on
                    h.Plots.ROI(1).Visible='on';
                    h.Plots.ROI(2).Visible='on';
                    % h.Plots.ROI(3).Visible='on';
                    % h.Plots.ROI(4).Visible='on';
                case 'extend' %%% Export Frame
                    Mia_Export(obj,e,1);
                    %% Updates filename display
                    if numel(MIAData.FileName)==2
                        h.Mia_Progress_Text.String = [MIAData.FileName{1}{1} ' / ' MIAData.FileName{2}{1}];
                    elseif numel(MIAData.FileName)==1
                        h.Mia_Progress_Text.String = MIAData.FileName{1}{1};
                    else
                        h.Mia_Progress_Text.String = 'Nothing loaded';
                    end
                    h.Mia_Progress_Axes.Color=UserValues.Look.Control;
                    return;
                otherwise 
                    %%% Updates images
                    % remove the freehand ROI
                    Mia_Correct([],[],2);
                    return;                   
            end
    end

    %%% Updates ROI rectangles
    for i=1:2
        h.Plots.ROI(i).Position=[Pos-0.5 Size];
    end
    %%% Updates images
    Mia_Correct([],[],2);
    % remove the freehand ROI if user is drawing a normal ROI on the image
    
end


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Funtion to automatically update image properties %%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function Mia_Update_Image_Props(h, ImageDescription, ch)
global MIAData UserValues
%%% h: handle to Mia GUI object
%%% ImageDescription: string from Tiff.ImageDescription tag
%%% ch: Mia channel
if ch == 1
    % Update all fields when loading channel 1
    ImageDescriptionFields = {'FrameTime [s]', 'LineTime [ms]',...
        'PixelTime [us]', 'PixelSize [nm]', 'Frame Interval [s]',...
        'RLICS_Scale', 'RLICS_Offset', 'TACRange [ns]', 'MI_Bins'};
else
    % Only update RLICS fields when loading other channels
    ImageDescriptionFields = {'RLICS_Scale', 'RLICS_Offset'};
end

% Parse string into cell array
ImDescription = splitlines(compose(ImageDescription));

for i = 1 : numel(ImageDescriptionFields)
    % Find cells containing matching strings
    match = strfind(ImDescription, ImageDescriptionFields{i});
    match = ~cellfun(@isempty,match);

    % Update appropriate variables depending on the matched field
    if any(match)
        switch ImageDescriptionFields{i}
            case 'FrameTime [s]'
                h.Mia_Image.Settings.Image_Frame.String = extractAfter(ImDescription{match}, ': ');
            case 'LineTime [ms]'
                h.Mia_Image.Settings.Image_Line.String = extractAfter(ImDescription{match}, ': ');
                h.Mia_ICS.Fit_Table.Data(15,:) = {extractAfter(ImDescription{match}, ': ')};
            case 'PixelTime [us]'
                h.Mia_Image.Settings.Image_Pixel.String = extractAfter(ImDescription{match}, ': ');
                UserValues.MIA.FitParams.PixTime = str2double(h.Mia_Image.Settings.Image_Pixel.String);
                LSUserValues(1)
                h.Mia_ICS.Fit_Table.Data(13,:) = {extractAfter(ImDescription{match}, ': ')};
            case 'PixelSize [nm]'
                h.Mia_Image.Settings.Pixel_Size.String = extractAfter(ImDescription{match}, ': ');
                h.Mia_ICS.Fit_Table.Data(11,:) = {extractAfter(ImDescription{match}, ': ')};
            case 'Frame Interval [s]'
                h.Mia_Image.Calculations.FRAP_Frame_Interval.String = extractAfter(ImDescription{match}, ': ');
            case 'RLICS_Scale'
                MIAData.RLICS(ch,1) = str2double(extractAfter(ImDescription{match}, ': '));
            case 'RLICS_Offset'
                MIAData.RLICS(ch,2) = str2double(extractAfter(ImDescription{match}, ': '));
            case 'TACRange [ns]'
                h.Mia_Image.Calculations.TACRange.String = extractAfter(ImDescription{match}, ': ');
            case 'MI_Bins'
                h.Mia_Image.Calculations.MI_Bins.String = extractAfter(ImDescription{match}, ': ');
        end
    end
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Funtion to update ROI position and size %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function Mia_Orientation(~,~,mode)
global MIAData UserValues
h = guidata(findobj('Tag','Mia'));

if isempty(MIAData.Data)
    return;
end

switch mode
    case {1, 2, 3}
        if size(MIAData.Data,1)<2
            return;
        end
end
switch mode
    %% Rotate and flip data
    case 1
        MIAData.Data(2,:) = cellfun(@(x)fliplr(x),MIAData.Data(2,:),'UniformOutput',false);
        
    case 2
        MIAData.Data(2,:) = cellfun(@(x)flipud(x),MIAData.Data(2,:),'UniformOutput',false);
        
    case 3
        switch h.Mia_Image.Settings.Orientation_Rotate_Dir.Value
            case 1 % clockwise
                d = 1;
            case 2 % counterclockwise
                d = -1;
        end
        MIAData.Data(2,:) = cellfun(@(x)rot90(x,d),MIAData.Data(2,:),'UniformOutput',false);
    %% Convert data to photon counting when parameters change
    case 4
        % callback of the editboxes
        % read new values from user interface
        % will only work for one- or two-channel data
        ind = strfind(h.Mia_Image.Settings.S.String,';');
        for i = 1:5
            if i == 1
                S(i) = str2double(h.Mia_Image.Settings.S.String(1:ind(i)-1));
            elseif i == 5
                S(i) = str2double(h.Mia_Image.Settings.S.String(ind(i-1)+1:end));
            else
                S(i) = str2double(h.Mia_Image.Settings.S.String(ind(i-1)+1:ind(i)-1));
            end
        end
        offset = str2double(h.Mia_Image.Settings.Offset.String);
        % will only work for one- or two-channel data
        if offset~=0 || any(S~=1)
            for i = 1:size(MIAData.Data,1)
                if ~isempty(MIAData.Data{i,1})
                    %only if S and offset apply, change data type from uint16 to single
                    MIAData.Data{i,1} = single(MIAData.Data{i,1});
                end
            end

            for i = 1:size(MIAData.Data,1)
                if ~isempty(MIAData.Data{i,1})
                    % scale the displayed data to S = 1 and offset = 0
                    MIAData.Data{i,1} = (MIAData.Data{i,1}.*UserValues.MIA.Options.S(i))+UserValues.MIA.Options.Offset;
                    % scale the displayed data to the new values
                    MIAData.Data{i,1} = (MIAData.Data{i,1}-offset)./S(i);
                    %%% Updates plots
                    Mia_ROI([],[],1)
                end
            end
        end

        % store the new values in UserValues
        UserValues.MIA.Options.S = S;
        UserValues.MIA.Options.Offset = offset;
        LSUserValues(1)
    case 5
        % Convert data to photon counting upon data load
        ind = strfind(h.Mia_Image.Settings.S.String,';');
        for i = 1:5
            if i == 1
                S(i) = str2double(h.Mia_Image.Settings.S.String(1:ind(i)-1));
            elseif i == 5
                S(i) = str2double(h.Mia_Image.Settings.S.String(ind(i-1)+1:end));
            else
                S(i) = str2double(h.Mia_Image.Settings.S.String(ind(i-1)+1:ind(i)-1));
            end
        end
        offset = str2double(h.Mia_Image.Settings.Offset.String);
        if offset~=0 || any(S~=1)
            %only if S and offset apply, change data type from uint16 to single
            for i = 1:size(MIAData.Data,1)
                if ~isempty(MIAData.Data{i,1})
                    % rescale the data
                    MIAData.Data{i,1} = single(MIAData.Data{i,1});
                    MIAData.Data{i,1} = (MIAData.Data{i,1}-offset)./S(i);
                end
            end
        end
end
Update_Plots([],[],1)

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Actions when pressing the RICS button %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function Do_RICS(~,~,Path, ImFile, analysis, params)
% h.Mia_Image.Calculations.Cor_Save_ICS.Value:
% 1 % RICS, don't save
% 2 % RICS, save as .miacor
% 3 % save as TIF
% 4 % blockwise
% 5 % grid-based mapping
% 6 % intensity-based mapping

global MIAData UserValues

h = guidata(findobj('Tag','Mia'));

if ~isvar('Path')
    Path = UserValues.File.MIAPath;
    ImPath = UserValues.File.MIAPath;
    fromia=1;
else
    %location of the data
    ImPath = Path{2};
    %location to save the results
    Path = Path{1};
    fromia=0;
end

switch h.Mia_Image.Calculations.Cor_Save_ICS.Value
    case {1,2,3,4}
        Do_2D_XCor([],[]) 
        % we pass mode but only if it's 1, it's relevant
    case 5
        h.Mia.Visible = 'off';
        % do RICS in a grid 
        if h.Mia_Image.Calculations.Cor_Type.Value == 1 %ACF1
            ch = 1; %identifier in RICS fitting
        elseif h.Mia_Image.Calculations.Cor_Type.Value == 2 %ACF2
            ch = 3; % 2 = CCF
        else
            ch = 1:3; % 2 = CCF
        end
        i = 2;
        if i == 1
            % generate a non-fitted RICS map
            RICSMap
        elseif i == 2
            Progress(0,h.Mia_Progress_Axes, h.Mia_Progress_Text,'Correlating...');

            % if arbitrary ROI hasn't been selected, select it
            if h.Mia_Image.Settings.ROI_FramesUse.Value ~=3
                h.Mia_Image.Settings.ROI_FramesUse.Value = 3;
                MIA_Various([],[],3)
            end

            %% Correlate one grid element at-a-time

            % user did not change the window/offset values, put them to the standard values for grid based analysis
            if str2double(h.Mia_Image.Calculations.Cor_ICS_Window.String) == 100
                h.Mia_Image.Calculations.Cor_ICS_Window.String = '32';
                h.Mia_Image.Calculations.Cor_ICS_Offset.String = '16';
            end

            grid = str2double(h.Mia_Image.Calculations.Cor_ICS_Window.String);
            offset = str2double(h.Mia_Image.Calculations.Cor_ICS_Offset.String);

            %the map will be constructed from nx x ny ROIs
            nx = floor((size(MIAData.Data{1,2},1)-offset)/offset);
            ny = nx;

            %third dimension is the parameter
            P = zeros(size(MIAData.Data{1,1},1),size(MIAData.Data{1,1},2),numel(ch),8);
            % P(:,:,:,type)
            % type = 1: diffusion
            % type = 2: concentration
            % type = 3: brightness
            % type = 4: mean intensity
            % type = 5: SNR
            % type = 6: number of times the parameter is sampled
            % type = 7: interaction
            % 8: number (for crosscorrelation calculations)
            R = MIAData.MS{1,1};
            r = grid^2;
            i = 0;
            dummy = 0;
            fprintf('SPECTRAL_RICS: Correlation 0 percent complete...\n')
            for x = 1:nx
                for y = 1:ny
                    i = i+1;
                    rangex = 1+(x-1)*offset:1+(x-1)*offset+grid-1;
                    rangey = 1+(y-1)*offset:1+(y-1)*offset+grid-1;
                    if sum(sum(R(rangex,rangey)))>r/2
                        %% Only correlate if more than half of the pixels are in the ROI
                        Do_2D_XCor([],[],2,rangex,rangey)

                        %% Fit (one CF at a time)
                        for c = ch
                            try
                                %% Fit
                                h.Mia_ICS.Fit_Table.Data{1,c} = num2str('100'); % set N = 100 to start
                                h.Mia_ICS.Fit_Table.Data{3,c} = num2str('1'); % set D = 1 to start

                                Do_RICS_Fit([],[],c,1) %triggers RICS fitting of channel in Mia

                                %% Store fit parameters
                                %% Diffusion
                                P(rangex,rangey,c,1) = P(rangex,rangey,c,1) + str2double(h.Mia_ICS.Fit_Table.Data{3,c});

                                %% Number
                                P(rangex,rangey,c,2) = P(rangex,rangey,c,2) + str2double(h.Mia_ICS.Fit_Table.Data{1,c});

                                %% The number of times the grid element resulted in fitted parameters
                                P(rangex,rangey,c,6) = P(rangex,rangey,c,6) + 1;

                                %% Mean Intensity
                                if c ~= 2
                                    P(rangex,rangey,c,4) = P(rangex,rangey,c,4) + MIAData.MeanIntRaw(ceil(c/1.5));
                                end

                                %% SNR
                                P(rangex,rangey,c,5) = h.Plots.Cor(c,2).ZData(grid/2,grid/2)./abs(mean2(h.Plots.Cor(c,2).ZData(1:ceil(grid/7),1:ceil(grid/7))));
                            catch
                                if dummy == 0
                                    fprintf('SPECTRAL_RICS_INFO: Fitting failed for at least one grid element. Check the images for errors!\n')
                                    dummy = 1;
                                end
                            end
                        end
                    end
                end
                % update progress bar for every new iteration of x
                % Progress(x/nx,h.Mia_Progress_Axes, h.Mia_Progress_Text,'Correlating');
                % display update at command prompt
                fprintf(['SPECTRAL_RICS: Correlation ' num2str(round(i/nx/ny*100)) ' percent complete...\n'])
            end
            %% Normalize the maps with the number of times pixels are sampled
            dummy = 0;
            for c = ch
                % Average Diffusion constant per pixel
                P(:,:,c,1) = real(P(:,:,c,1)./P(:,:,c,6));
                tmp = median(P(:,:,c,1),'all');
                tmp2 = P(:,:,c,1);
                tmp2(tmp2>20*tmp)=NaN; %diff values far above the median: discard
                tmp2(tmp2<0)=NaN;
                tmp2(isnan(tmp2))=NaN;
                P(:,:,c,1) = tmp2;
                % Average Number of molecules per pixel
                P(:,:,c,2) = real(P(:,:,c,2)./P(:,:,c,6));
                tmp = median(P(:,:,c,2),'all');
                tmp2 = P(:,:,c,2);
                tmp2(tmp2>20*tmp)=NaN; %N values far above the median: discard
                tmp2(tmp2<0)=NaN;
                tmp2(isnan(tmp2))=NaN;
                P(:,:,c,8) = tmp2; %store under 8
                %Concentration
                %Confocal Volume V = (pi/2)^(3/2)*wr^2*wz
                V = (pi/2)^(3/2)*(str2double(h.Mia_ICS.Fit_Table.Data{5,c}))^2*str2double(h.Mia_ICS.Fit_Table.Data{7,c});
                %N to nM
                P(:,:,c,2) = tmp2*10^24./(V.*6.022*10^23);
                % Average Intensity per pixel
                P(:,:,c,4) = real(P(:,:,c,4)./P(:,:,c,6));
                tmp = median(P(:,:,c,4),'all');
                tmp2 = P(:,:,c,4);
                tmp2(tmp2>20*tmp)=NaN; %diff values far above the median: discard
                tmp2(tmp2<0)=NaN;
                tmp2(isnan(tmp2))=NaN;
                P(:,:,c,4) = tmp2;
                % SNR per pixel
                P(:,:,c,5) = real(P(:,:,c,5)./P(:,:,c,6));
                tmp = median(P(:,:,c,5),'all');
                tmp2 = P(:,:,c,5);
                tmp2(tmp2>10*tmp)=NaN; %SNR values far above the median: discard
                tmp2(isnan(tmp2))=NaN;
                tmp2(tmp2<0)=NaN;
                P(:,:,c,5) = tmp2;
                if any(P(:,:,c,5) < 10, 'all') && (dummy == 0)
                    fprintf('SPECTRAL_RICS_INFO: The signal-to-noise ratio for at least one analysed grid element is < 10. Check the resulting images for possibly erroneous regions!\n')
                    dummy = 1;
                end
            end

            %%%%%%%%%%%%%%%%%%%%%%%%%
            %%% Generate the maps %%%
            %%%%%%%%%%%%%%%%%%%%%%%%%
            fprintf('SPECTRAL_RICS: Preparing to display results...\n')
            % set the AROI pixels each to their corresponding diffusion constant
            % test = reshape(reshape(permute(AR,[3,1,2]),size(AR,1)*size(AR,2),[]).*D',[size(AR,1),size(AR,2),size(AR,3)]);

            % generate brightness scaling map
           % MIAData.MapScaling = cell(size(ch,2),1,8);
            % the channel list will get changed by the maps, so store here.
            chn(1) = h.Mia_Image.Settings.Channel_PIE(1).Value;
            chn(2) = h.Mia_Image.Settings.Channel_PIE(2).Value;

            for c = ch
                if ~isequal(c,2) %for all ACFs
                    for type = 1:5 % loop through the three parameters
                        if type == 1
                            map = 'Diffusion map';
                            ind = 1;
                        elseif type == 2
                            map = 'Concentration map';
                            ind = 2;
                        elseif type == 3
                            map = 'Brightness map';
                            ind = 3;
                            P(:,:,c,3) = real(P(:,:,c,4)./P(:,:,c,8)./str2double(h.Mia_Image.Settings.Image_Pixel.String)*1000);
                            tmp = median(P(:,:,c,3),'all');
                            tmp2 = P(:,:,c,3);
                            tmp2(tmp2>20*tmp)=NaN; %diff values far above the median: discard
                            tmp2(tmp2<0)=NaN;
                            tmp2(isnan(tmp2))=NaN;
                            P(:,:,c,3) = tmp2;
                        elseif type == 4
                            map = 'Intensity map';
                            ind = 4;
                            P(:,:,c,4) = P(:,:,c,4)./str2double(h.Mia_Image.Settings.Image_Pixel.String)*1000;
                        elseif type == 5
                            map = 'Signal-to-noise map';
                            ind = 5;
                        end

                        Image = P(:,:,c,type);
                        badgrids = ones(size(Image));
                        badgrids(isnan(Image))=0;
                        badgrids(isoutlier(Image))=0;
                        Image(isnan(Image)) = min(min(Image)); % do not make this 0 since the final colorbar will be affected
                        Image(isoutlier(Image)) = min(min(Image)); 

                        % create extra entries in the channel popupmenu
                        chan = ceil(c/1.5);
                        if isempty(find(strcmp(h.Mia_Image.Settings.Channel_PIE(chan).String,map)))
                             h.Mia_Image.Settings.Channel_PIE(chan).String = [h.Mia_Image.Settings.Channel_PIE(chan).String; {map}];
                        end

                        % select this particular map in the popupmenu
                        d = find(strcmp(h.Mia_Image.Settings.Channel_PIE(chan).String,map));
                        h.Mia_Image.Settings.Channel_PIE(chan).Value = d;

                        switch h.Mia_Image.Calculations.Cor_ICS_Scaling.Value
                            case 1 % do not scale the brightness of the map
                                MIAData.MapScaling{ceil(c/1.5),1,ind} = MIAData.MS{1,1}.*badgrids; %make all bad grid elements black
                            case 2 %scale the brightness of the map with the mean intensity in both channels
                                % scale the brightness of the map in Update_Plots with the number of times the pixel was actually included in analyses
                                %MIAData.MapScaling = single(MIAData.MS{1,1});%sumAR./size(Data,3);
                                % scale the brightness of the map with the mean intensity of the channel that is used
                                tmp = mean(MIAData.Data{chn(ceil(c/1.5)),1},3);
                                tmp(tmp<0)=0;
                                tmp = tmp./max(max(tmp));
                                MS = MIAData.MS{1,1};
                                tmp = tmp/1.5+0.3333333;
                                tmp(~MS) = 0;
                                tmp = tmp.*badgrids; %make all bad grid elements black
                                MIAData.MapScaling{ceil(c/1.5),1,ind} = tmp;%medfilt2(tmp,[3 3]);
                        end
                        
                        % plot the actual map
                        MIAData.MapData{chan,d} = Image;
                        Update_Plots([],[],1)

                        %% Save Figure
                        h.Mia.SelectionType = 'extend';
                        Mia_Export(h.Plots.Image(chan,1),[],1,Path)
                        
                    end
                else
                    %% Interaction map
                    map = 'Interaction map';
                    ind = 6;
                    % relative cross-correlation = mean(NG+NR)/NCC
                    tmp = real(0.5.*(P(:,:,1,8)+P(:,:,3,8))./P(:,:,2,8));
                    % in principle, the rel.CC cannot be > 1
                    tmp(tmp>2) = 0;
                    tmp(isnan(tmp))=0;
                    P(:,:,2,7) = tmp;

                    Image = P(:,:,2,7);
                    badgrids = ones(size(Image));
                    badgrids(isnan(Image))=0;
                    badgrids(isoutlier(Image))=0;
                    Image(isnan(Image))=min(min(Image)); % do not make this 0 since the final colorbar will be affected
                    Image(isoutlier(Image))=min(min(Image));

                    % create extra entry in the channel popupmenu
                    if isempty(find(strcmp(h.Mia_Image.Settings.Channel_PIE(1).String,map)))
                        h.Mia_Image.Settings.Channel_PIE(1).String = [h.Mia_Image.Settings.Channel_PIE(1).String; {map}];
                    end

                    d = find(strcmp(h.Mia_Image.Settings.Channel_PIE(1).String,map));
                    h.Mia_Image.Settings.Channel_PIE(1).Value = d;

                    switch h.Mia_Image.Calculations.Cor_ICS_Scaling.Value
                        case 1 % do not scale the brightness of the map
                            MIAData.MapScaling{3,1,ind} = MIAData.MS{1,1}.*badgrids; %make all bad grid elements black
                        case 2 %scale the brightness of the map with the mean intensity in both channels
                            tmp = mean(MIAData.Data{chn(1),1},3) + mean(MIAData.Data{chn(2),1},3);
                            tmp(tmp<0)=0;
                            tmp = tmp./max(max(tmp));
                            MS = MIAData.MS{1,1};
                            tmp = tmp/1.5+0.3333333;
                            tmp(~MS) = 0;
                            tmp = tmp.*badgrids; %make all bad grid elements black
                            MIAData.MapScaling{3,1,ind} = tmp;%medfilt2(tmp,[3 3]); 
                    end

                    % plot the actual map
                    MIAData.MapData{1,d} = Image;
                    Update_Plots([],[],1)

                    %% Save Figure
                    h.Mia.SelectionType = 'extend';
                    Mia_Export(h.Plots.Image(1,1),[],1,Path)

                    %% Co-diffusion map
                    map = 'Co-diffusion map'; 
                    ind = 8;
                    tmp = P(:,:,2,1); 
                    tmp(tmp>2) = 0;
                    tmp(isnan(tmp))=0;
                    P(:,:,2,1) = tmp;

                    Image = P(:,:,2,1);
                    badgrids = ones(size(Image));
                    badgrids(isnan(Image))=0;
                    badgrids(isoutlier(Image))=0;
                    Image(isnan(Image))=min(min(Image)); % do not make this 0 since the final colorbar will be affected
                    Image(isoutlier(Image))=min(min(Image));

                    % create extra entry in the channel popupmenu
                    if isempty(find(strcmp(h.Mia_Image.Settings.Channel_PIE(1).String,map)))
                        h.Mia_Image.Settings.Channel_PIE(1).String = [h.Mia_Image.Settings.Channel_PIE(1).String; {map}];
                    end

                    d = find(strcmp(h.Mia_Image.Settings.Channel_PIE(1).String,map));
                    h.Mia_Image.Settings.Channel_PIE(1).Value = d;

                    switch h.Mia_Image.Calculations.Cor_ICS_Scaling.Value
                        case 1 % do not scale the brightness of the map
                            MIAData.MapScaling{3,1,ind} = MIAData.MS{1,1}.*badgrids; %make all bad grid elements black
                        case 2 %scale the brightness of the map with the mean intensity in both channels
                            tmp = mean(MIAData.Data{chn(1),1},3) + mean(MIAData.Data{chn(2),1},3);
                            tmp(tmp<0)=0;
                            tmp = tmp./max(max(tmp));
                            MS = MIAData.MS{1,1};
                            tmp = tmp/1.5+0.3333333;
                            tmp(~MS) = 0;
                            tmp = tmp.*badgrids; %make all bad grid elements black
                            MIAData.MapScaling{3,1,ind} = tmp;%medfilt2(tmp,[3 3]);
                    end

                    % plot the actual map
                    MIAData.MapData{1,d} = Image;
                    Update_Plots([],[],1)

                    %% Save Figure
                    h.Mia.SelectionType = 'extend';
                    Mia_Export(h.Plots.Image(1,1),[],1,Path)
                end
            end
            %% Save multipanel heatmaps
            if numel(ch)==3
                % 2-color analysis
                Mia_Export(h.Plots.Image(1,1),[],7,Path)
            else
                % 1-color analysis
                Mia_Export(h.Plots.Image(1,1),[],6,Path)
            end
        end
        for c = ch
            % select the diffusion map in the popupmenu
            chan = ceil(c/1.5);
            d = find(strcmp(h.Mia_Image.Settings.Channel_PIE(chan).String,'Diffusion map'));
            h.Mia_Image.Settings.Channel_PIE(chan).Value = d;
            Update_Plots([],[],1)
        end
        if fromia
            % When run from Mia UI, display the UI again
            h.Mia.Visible = 'on';
        end
    case 6 %Intensity-based mapping of parameters
        try %remove an existing Mia subfolder
            rmdir(fullfile(UserValues.File.MIAPath,'Mia'),'s')
        end
        % if arbitrary ROI hasn't been selected, select it
        if h.Mia_Image.Settings.ROI_FramesUse.Value ~=3
            h.Mia_Image.Settings.ROI_FramesUse.Value = 3;
            MIA_Various([],[],3)
        end
        %%% User can have drawn a rough AROI by hand
        %%% Code then takes n ROIs inside this
        %%% Even though the intensity ranges are set such that equal
        %%% numbers of pixels are included in each ROI, all ROIs will not be equal
        %%% in size. This is because the Correction (Sub/Add) that
        %%% follows this will throw out pixels that are not included
        %%% in at least the window for moving average subtraction.

        %% the original values in Mia's UI
        Imin = str2double(h.Mia_Image.Settings.ROI_AR_Int_Min.String);
        Imax = str2double(h.Mia_Image.Settings.ROI_AR_Int_Max.String);

        %% Select which channel will be the source of the AROI
        if strcmp(h.Mia_Image.Settings.ROI_AR_Same.String(h.Mia_Image.Settings.ROI_AR_Same.Value), 'OR') || strcmp(h.Mia_Image.Settings.ROI_AR_Same.String(h.Mia_Image.Settings.ROI_AR_Same.Value), 'AND')
            % if user didn't select a specific channel, select channel 1
            h.Mia_Image.Settings.ROI_AR_Same.Value = 2; %1 = 'OR';
            UserValues.MIA.AR_Same = 2;
            LSUserValues(1)
            d = 1; %channel 1
        else
            % take the channel set in the "which masks" popupmenu
            d = h.Mia_Image.Settings.ROI_AR_Same.Value-1;
        end

        %% Define the min and max intensities for the n ROIs between the global min and max
        % min and max can be different per frame because of slow mean intensity variations (f.e. photobleaching)
        % the channel is defined by the "which masks" popupmenu

        % number of AROIs
        nROIs = str2double(h.Mia_Image.Calculations.Cor_ICS_nAROI.String);
        % calculate the AROI from channel d
        Data = single(MIAData.Data{d,1});
        IntMin = zeros(nROIs,size(Data,3));
        IntMax = zeros(nROIs,size(Data,3));
        % spatially average Data
        Filter1=ones(UserValues.MIA.AR_Region(1))/(UserValues.MIA.AR_Region(1))^2; %small region editbox;
        MS = MIAData.MS{d,1}(:);
        for f=1:size(Data,3)
            % spatially average the image
            Im = filter2(Filter1,Data(:,:,f));
            Im = Im(:);
            %work only with the pixels in the freehand ROI (draw that
            %ROI on the same channel as the one selected in the "which
            %masks" popupmenu)
            Im(~MS)=[];
            Im = sort(Im);
            p = round(numel(Im)/nROIs);
            %define the min and max boundaries for each AROI in each frame
            for n = 1:(nROIs-1)
                IntMin(n,f) = Im((n-1)*p+1) ;
                IntMax(n,f) = Im(n*p);
            end
            IntMin(end,f) = Im(n*p+1) ;
            IntMax(end,f) = Im(end); %just take the highest intensity value as the last Imax
        end
        % figure;
        % plot(IntMin(end,:),'b')
        % hold on
        % plot(IntMax(end,:),'r')
        % toc

        %% Do ICS inside each of the n AROIs
        AR = zeros(size(MIAData.AR{d,1},1),size(MIAData.AR{d,1},2),nROIs);
        % AR: third dimension = arbitrary ROI
        % AR: fourth dimensen is diffusion constant, N or brightness
        ARmean = zeros(nROIs,size(MIAData.AR{d,1},3));
        % chose to fit only ACF1 (leftmost on the RICSfit tab), only ACF2
        % (rightmost on the RICSfit tab) or all three.
        if h.Mia_Image.Calculations.Cor_Type.Value == 3
            ch = 1:3;
        else
            ch = floor(h.Mia_Image.Calculations.Cor_Type.Value*1.5);
        end

        try %to remove an existing Mia subfolder cause the fit routine will search of all files in the Mia folder
            rmdir(fullfile(ImPath,'Mia'),'s')
        end
        % do not overwrite existing files, Mia will generate 1 file per
        % aROI
        h.Mia_Image.Calculations.Overwrite.Value = 0;

        fprintf('SPECTRAL_RICS: Correlation 0 percent completed...\n')
        % at this point, the user has defined whether they want ACF or ACF+CCF
        for n = 1:nROIs %loop through the AROIs
            % We pass the min/max value for the current AROI to Mia_Correct,
            % for moving average correction. Mia_Correct also
            % calls Mia_Arbitrary_Region for actually generating the AROI.
            % Since a specific channel is chosen to be the Master ROI, the
            % called function will obey this
            Mia_Correct([],[],1,[IntMin(n,:)',IntMax(n,:)'])

            Do_2D_XCor([],[]) %triggers RICS calculation

            for c = ch
                if ~isequal(c, 3)
                    MeanInt(n,c) = MIAData.MeanInt(c);
                    MeanIntRaw(n,c) = MIAData.MeanIntRaw(c);
                end
            end

            %% which pixels are included in AROI n and freehand ROI
            % It doesn't matter which index AR is used, they all are the same
            % Sure it's the uncorrected image??
            AReg = MIAData.AR{d,1} & repmat(MIAData.MS{d},1,1,size(MIAData.AR{d,1},3));
            % number of times each pixel in the image was included in AROI n
            AR(:,:,n) = sum(AReg,3);
            % number of included pixels per frame per AROI
            ARmean(n,:) = reshape(sum(sum(AReg, 1),2),1,size(ARmean,2));
            % figure; hold on
            % for i = 1:n
            %     scatter(i,ARmean(i,:))
            % end
            % hold off
            fprintf(['SPECTRAL_RICS: Correlation ' num2str(round(n/nROIs*100)) ' percent completed...\n'])
        end

        fprintf('SPECTRAL_RICS: Fitting correlation data...\n')

        if fromia == 1
            % user pressed the RICS button in Mia while 'intensity heatmap'
            % was selected
            Path = UserValues.File.MIAPath;
            ImFile = MIAData.FileName{1}{1};
            analysis = struct;
            analysis.Type = 'Intensity-based Heatmap';
            params.linetime = str2double(h.Mia_Image.Settings.Image_Line.String);
            params.pixeltime = str2double(h.Mia_Image.Settings.Image_Pixel.String);
            params.frametime = str2double(h.Mia_Image.Settings.Image_Frame.String);
            params.pixelsize = str2double(h.Mia_Image.Settings.Pixel_Size.String);
            for i = ch
                %1 = ACF1
                %2 = ACF2
                %3 = CCF
                if i == 2
                    j = 3;
                elseif i == 3
                    j = 2;
                else
                    j = 1;
                end
                params.w0(i) = str2double(h.Mia_ICS.Fit_Table.Data{5,j});
                params.wz(i) = str2double(h.Mia_ICS.Fit_Table.Data{7,j});
            end
        end

        %pass the scan parameters and wr_wz to MIAFit, along with the path
        [~, T] = MIAFit(ImPath, ImFile, analysis , params, ch, nROIs);

        if ~isequal(T,0)
            fprintf('SPECTRAL_RICS: Preparing to display results...\n')
            %% Save Results
            %writetable(T, fullfile(Path,[ImFile(1:end-4) '_parameters.csv']), 'WriteRowNames',1)
            writetable(T, fullfile(Path,'Results.csv'), 'WriteRowNames',1)

            for n = 1:nROIs
                for c = ch
                    D(n,c) = cell2mat(T{5,(c-1)*nROIs+n});
                   %  if n == 1 %manually make a particuler ROI 0
                    %     D(n,c)=0;
                    % end
                    if isequal(D(n,c),0) %fitting failed, D = 0
                         AR(:,:,n) = AR(:,:,n).*0; %hide this D from the heatmaps
                    end
                    C(n,c) = cell2mat(T{4,(c-1)*nROIs+n}); %concentration
                    N(n,c) = cell2mat(T{3,(c-1)*nROIs+n}); %number
                end
            end

            % the total number of times each pixel the image was included in any AROI
            sumAR = sum(AR,3);

            % % set the intensity range to the original again
            % h.Mia_Image.Settings.ROI_AR_Int_Min.String = num2str(Imin);
            % h.Mia_Image.Settings.ROI_AR_Int_Max.String = num2str(Imax);
            % MIA_Various([],[],6) %this will trigger the arbitrary region to be applied

            %%%%%%%%%%%%%%%%%%%%%%%%%
            %%% Generate the maps %%%
            %%%%%%%%%%%%%%%%%%%%%%%%%
            % set the AROI pixels each to their corresponding diffusion constant
            % test = reshape(reshape(permute(AR,[3,1,2]),size(AR,1)*size(AR,2),[]).*D',[size(AR,1),size(AR,2),size(AR,3)]);
            
            % generate brightness scaling map
            switch h.Mia_Image.Calculations.Cor_ICS_Scaling.Value
                case 1 %do not scale brightness of the maps
                    tmp = MIAData.MS{1,1};
                case 2 % scale brightness with the local count rate
                    % d is the channel where the intensity-based ROI is generated
                    tmp = mean(MIAData.Data{d,1},3);
                    tmp(tmp<0)=0;
                    tmp = medfilt2(tmp./max(max(tmp)),[3 3]);
                    MS = MIAData.MS{1,1};
                    tmp = tmp/1.5+0.3333333;
                    tmp(~MS) = 0;
                case 3 % scale brightness with the number of times the pixels are sampled
                    tmp = sumAR./size(Data,3);
            end

            for c = ch
                if ~isequal(c,3) %for all ACFs
                    for type = 1:4 % loop through the four parameters
                        if type == 1
                            map = 'Diffusion map';
                            ind = 1;
                            param = D(:,c);
                        elseif type == 2
                            map = 'Concentration map';
                            ind = 2;
                            param = C(:,c);
                        elseif type == 3
                            map = 'Brightness map';
                            ind = 3;
                            for n = 1:nROIs
                                param(n) = MeanInt(n,c)./N(n,c)./str2double(h.Mia_Image.Settings.Image_Pixel.String)*1000;
                            end
                        elseif type == 4
                            map = 'Intensity map';
                            ind = 4;
                            for n = 1:nROIs
                                % if the intensity map is not made with the
                                % uncorrected intensity image, the intensity is
                                % the same everywhere.
                                param(n) = MeanIntRaw(n,c)./str2double(h.Mia_Image.Settings.Image_Pixel.String)*1000;
                            end
                        end
                        for n = 1:nROIs
                            AR2(:,:,n) = AR(:,:,n).*param(n);
                        end
                        Image = sum(AR2,3)./sumAR;
                        Image(isnan(Image))=min(min(Image)); % do not make this 0 since the final colorbar will be affected
                       
                        % create extra entries in the channel popupmenu
                        if isempty(find(strcmp(h.Mia_Image.Settings.Channel_PIE(c).String,map)))
                            h.Mia_Image.Settings.Channel_PIE(c).String = [h.Mia_Image.Settings.Channel_PIE(c).String; {map}];
                        end

                        % select this particular map in the popupmenu
                        d = find(strcmp(h.Mia_Image.Settings.Channel_PIE(c).String,map));
                        h.Mia_Image.Settings.Channel_PIE(c).Value = d;

                        % scale the brightness of the map with the number of times the pixel was actually included in analyses
                        MIAData.MapScaling{c,1,ind} = tmp;

                        % plot the actual map
                        MIAData.MapData{c,d} = Image;
                        Update_Plots([],[],1)

                        %% Save Figure
                        h.Mia.SelectionType = 'extend';
                        Mia_Export(h.Plots.Image(c,1),[],1,Path)
                    end
                else %CCF
                    %% Interaction map
                    c = 1; %plot this map on the top channel
                    map = 'Interaction map';
                    ind = 6;
                    % the normalized CCF : mean(NG+NR)/NCC
                    param = 0.5.*(N(:,1)+N(:,2))./N(:,3);
                    for n = 1:nROIs
                        AR2(:,:,n) = AR(:,:,n).*param(n);
                    end
                    Image = sum(AR2,3)./sumAR;
                    Image(isnan(Image))=min(min(Image)); % do not make this 0 since the final colorbar will be affected
                    
                    %%% create extra entry in the channel popupmenu
                    if isempty(find(strcmp(h.Mia_Image.Settings.Channel_PIE(c).String,map)))
                        h.Mia_Image.Settings.Channel_PIE(c).String = [h.Mia_Image.Settings.Channel_PIE(c).String; {map}];
                    end

                    d = find(strcmp(h.Mia_Image.Settings.Channel_PIE(c).String,map));
                    h.Mia_Image.Settings.Channel_PIE(c).Value = d;

                    MIAData.MapScaling{3,1,ind} = tmp;

                    MIAData.MapData{1,d} = Image;
                    Update_Plots([],[],1)

                    % Save individual maps
                    h.Mia.SelectionType = 'extend';
                    Mia_Export(h.Plots.Image(c,1),[],1,Path)

                    %% Co-diffusion map
                    c = 1; %plot this map on the top channel
                    map = 'Co-diffusion map';
                    ind = 8;
                    param = D(:,3);
                    for n = 1:nROIs
                        AR2(:,:,n) = AR(:,:,n).*param(n);
                    end
                    Image = sum(AR2,3)./sumAR;
                    Image(isnan(Image))=min(min(Image)); % do not make this 0 since the final colorbar will be affected
                    
                    %%% create extra entry in the channel popupmenu
                    if isempty(find(strcmp(h.Mia_Image.Settings.Channel_PIE(c).String,map)))
                        h.Mia_Image.Settings.Channel_PIE(c).String = [h.Mia_Image.Settings.Channel_PIE(c).String; {map}];
                    end

                    d = find(strcmp(h.Mia_Image.Settings.Channel_PIE(c).String,map));
                    h.Mia_Image.Settings.Channel_PIE(c).Value = d;

                    MIAData.MapScaling{3,1,ind} = tmp;

                    MIAData.MapData{1,d} = Image;
                    Update_Plots([],[],1)

                    %% Save individual maps
                    h.Mia.SelectionType = 'extend';
                    Mia_Export(h.Plots.Image(c,1),[],1,Path)
                    
                end
            end
            %% Save multipanel heatmaps
            if numel(ch)==3
                % 2-color analysis
                Mia_Export(h.Plots.Image(c,1),[],7,Path)
            else
                % 1-color analysis
                Mia_Export(h.Plots.Image(c,1),[],6,Path)
            end
            for c = ch
                if ~isequal(c,3) %only for the ACFs
                    % select the diffusion map in the popupmenu
                    d = find(strcmp(h.Mia_Image.Settings.Channel_PIE(c).String,'Diffusion map'));
                    h.Mia_Image.Settings.Channel_PIE(c).Value = d;
                    Update_Plots([],[],1)
                end
            end
        end
        %turn off AROI and turn it on again, so the image thresholding is reset
        h.Mia_Image.Settings.ROI_FramesUse.Value = 1;
        MIA_Various([],[],3)
        h.Mia_Image.Settings.ROI_FramesUse.Value = 3;
        MIA_Various([],[],3)
        if ~isequal(T,0)
            for c = ch
                if ~isequal(c,3) %only for the ACFs
                    %set the display to the image so that the actual corrected
                    %image is correctly displayed
                    h.Mia_Image.Settings.Channel_PIE(c).Value = 1;
                    Update_Plots([],[],1)

                    % select the diffusion map in the popupmenu
                    d = find(strcmp(h.Mia_Image.Settings.Channel_PIE(c).String,'Diffusion map'));
                    h.Mia_Image.Settings.Channel_PIE(c).Value = d;
                    Update_Plots([],[],1)
                end
            end
        end
        fprintf('SPECTRAL_RICS_SUCCESS: Analysis done!\n')
end


function MapColorbar(Image, channel, type)
global UserValues
h = guidata(findobj('Tag','Mia'));
%Image2 = Image;
if strcmp(type, 'Diffusion map')  
    h.Mia_Image.Colorbar(channel).Title.String = '\mum^{2}/s'; %scaling is now Diffusion constant
elseif strcmp(type, 'Concentration map')
    h.Mia_Image.Colorbar(channel).Title.String = 'nM';
elseif strcmp(type, 'Brightness map')
    h.Mia_Image.Colorbar(channel).Title.String = 'kHz/molec.';
elseif strcmp(type, 'Intensity map')
    h.Mia_Image.Colorbar(channel).Title.String = 'kHz';
elseif strcmp(type, 'Signal-to-noise map')
    h.Mia_Image.Colorbar(channel).Title.String = 'S/N';
elseif strcmp(type, 'Interaction map')
    h.Mia_Image.Colorbar(channel).Title.String = 'rel.CC';
elseif strcmp(type, 'Image ratio')
    h.Mia_Image.Colorbar(channel).Title.String = 'ratio';
elseif strcmp(type, 'Co-diffusion map')  
    h.Mia_Image.Colorbar(channel).Title.String = '\mum^{2}/s'; %scaling is now Diffusion constant
else %normal intensity image
    h.Mia_Image.Colorbar(channel).Title.String = 'kHz';
end

% % make all pixels that were not part of the analysis white
% % do this after the color bar was plotted
% Image2(isnan(Image2))=0; %recalculate since min(min(Image)) contains also kept pixels!
% for i = 1:3
%     tmp = h.Plots.Image(channel,1).CData(:,:,i);
%     tmp(Image2 == 0) = 1;
%     h.Plots.Image(channel,1).CData(:,:,i) = tmp;
% end
% recalculate the colorbar tick labels to get the correct units
Imin = min(Image, [], "all");
Imax = max(Image, [], "all");
n = size(h.Mia_Image.Colorbar(channel).TickLabels,1)-1;
BinSize = (Imax-Imin)/n;
Bins = Imin:BinSize:Imax;
t = round(numel(Bins)/2);
t = Bins(t);

if t > 100
    Bins = round(Bins,0);
elseif t > 10
    Bins = round(Bins,1);
elseif t > 0.1
    Bins = round(Bins,2);
elseif t > 0.01
    Bins = round(Bins,3);
else
    Bins = round(Bins,4);
end

for i = 1:size(h.Mia_Image.Colorbar(channel).TickLabels,1)
    h.Mia_Image.Colorbar(channel).TickLabels{i} = num2str(Bins(i));
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Funtion to calculate image correlations %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function Do_2D_XCor(~,~,mode,rangex,rangey)
% mode = 1 % Van Steensel type co-localization 
% mode = 2 % grid-based RICS

%rangex/y: the range in the corrected image that has to be correlated

global MIAData UserValues
h = guidata(findobj('Tag','Mia'));

if ~isvar('mode')
    % normal RICS
    mode = 0;
end

%%% Stops, if no data was loaded
if size(MIAData.Data,1)<1
    return;
end

%% define the x/y range in which to do correlation
if ~isvar('rangex')
    % normal RICS, use the full x and y range of the corrected image
    rx = 1:size(MIAData.Data{1,2},1); %first channel always exists anyway
    ry = 1:size(MIAData.Data{1,2},2);
else
    % grid-based RICS, location of the grid element is provided as input
    rx = rangex;
    ry = rangey;
end

% parfor containing an fft function will be slower than just for, since fft
% is already calling all available cores on the computer
%grid = 1;
% for grid based mapping, work over >1 CPU
% if grid
%     h.Mia_Progress_Text.String = 'Opening Parallel Pool ...';
%     StartParPool();
% end

h.Mia_Progress_Text.String = 'Starting Correlation';
h.Mia_Progress_Axis.Color=[1 0 0];

%%% Clears correlation data and plots
MIAData.Cor=cell(3,2);
for i=1:3
    h.Plots.Cor(i,1).CData=zeros(1,1,3);
    h.Plots.Cor(i,2).ZData=zeros(1);
    h.Plots.Cor(i,2).CData=zeros(1,1,3);    
    h.Mia_ICS.Axes(i,1).Visible='off';
    h.Mia_ICS.Axes(i,2).Visible='off';
    h.Mia_ICS.Axes(i,3).Visible='off';
    h.Mia_ICS.Axes(i,4).Visible='off';
    h.Plots.Cor(i,1).Visible='off';
    h.Plots.Cor(i,2).Visible='off';
    h.Plots.Cor(i,3).Visible='off';
    h.Plots.Cor(i,4).Visible='off';
    h.Plots.Cor(i,5).Visible='off';
    h.Plots.Cor(i,6).Visible='off';
    h.Plots.Cor(i,7).Visible='off';
end
h.Mia_ICS.Frame_Slider.Min=0;
h.Mia_ICS.Frame_Slider.Max=0;
h.Mia_ICS.Frame_Slider.SliderStep=[1 1];
h.Mia_ICS.Frame_Slider.Value=0;

if size(MIAData.Data,1)<2
    h.Mia_Image.Calculations.Cor_Type.Value=1;
end

%%% Determines, which correlations to perform
if mode~=1
    if h.Mia_Image.Calculations.Cor_Type.Value == 3
        % Calculate Auto and Cross-RICS
        Auto = 1:2; Cross = 1;
        channel=1:3;
    else % User want to do single-color RICS of channel 1 or 2
        Auto=h.Mia_Image.Calculations.Cor_Type.Value; Cross = 0;
        channel=floor(Auto*1.5);
    end
else %mode == 1 Van Steensel type colocalization
    Auto = 1:2; Cross = 1;
    channel=2; %the CCF is stored at channel 2
end

%%% Determines, which frames to correlate
Frames=str2num(h.Mia_Image.Settings.ROI_Frames.String); %don't use str2double since the string is not a scalar
%%% Uses all Frames, if input was 0
if all(Frames==0)
    Frames=1:size(MIAData.Data{1,2},3);
end
%%% Remove all Frames<1 and >Movie size
if any(Frames<0 | Frames>size(MIAData.Data{1,2},3))
    Min=max(1,min(Frames)); Min=min(Min,size(MIAData.Data{1,2},3));
    Max=min(size(MIAData.Data{1,2},3),max(Frames)); Max=max(Max,1);
    Frames=Min:Max;
    h.Mia_Image.Settings.ROI_Frames.String=[num2str(Min) ':' num2str(Max)];
end

%%% Applies arbitrary region selection
switch (h.Mia_Image.Settings.ROI_FramesUse.Value)
    case 1 %%% Use All Frames
    case 2 %%% Use selected Frames
        Active=find(MIAData.Use);
        Frames=intersect(Frames,Active);
    case 3 %%% Does arbitrary region ICS
        Active=find(MIAData.Use);
        Frames=intersect(Frames,Active);
        for i=Auto
            Use{i} = MIAData.AR{i,1}(rx,ry,Frames) & repmat(MIAData.MS{i}(rx,ry),1,1,numel(Frames));
        end
end

%%% Performs autocorrelation
for i=Auto
    % name of the selected image
    type = h.Mia_Image.Settings.Channel_PIE(i).String{h.Mia_Image.Settings.Channel_PIE(i).Value};
    if find(strcmp(type,UserValues.MIA.Maps))
        % a map is selected, change to the i-th image in the list
        h.Mia_Image.Settings.Channel_PIE(i).Value = i;
        ImageDropDown(h.Mia_Image.Settings.Channel_PIE(i),[],i)
    end
    d = h.Mia_Image.Settings.Channel_PIE(i).Value; %(the datachannel that is currently displayed)
    if ~isvar('rangex')
        Progress(0,h.Mia_Progress_Axes, h.Mia_Progress_Text,['Correlating ACF' num2str(i)]);
    end
    MIAData.Cor{floor(i*1.5)}=zeros(numel(rx),numel(ry),numel(Frames));
    TotalInt = zeros(numel(Frames),1);
    TotalPx = zeros(numel(Frames),1);
    for j=1:numel(Frames)
        Image = double(MIAData.Data{d,2}(rx,ry,Frames(j)));
        ImRaw = double(MIAData.Data{d,1}(rx,ry,Frames(j)));
        if h.Mia_Image.Settings.ROI_FramesUse.Value==3  %%% Arbitrary region ICS
            Size = [2*size(Image,1)-1, 2*size(Image,2)-1];
            %%% Calculates normalization for zero regions
            Norm=fft2(Use{i}(:,:,j),Size(1),Size(2));
            Norm=fftshift(ifft2(Norm.*conj(Norm)));
            %%% Calculates fluctutation image
            ImageFluct=Image-mean(Image(Use{i}(:,:,j)));
            %%% Applies selected region and FFT
            ImageFluct=fft2(ImageFluct.*Use{i}(:,:,j),Size(1),Size(2));
            %%% Actual correlation
            ImageCor = fftshift(real(ifft2(ImageFluct.*conj(ImageFluct))));
            %%% Corrects for shape of selected region
            ImageCor = ImageCor./Norm;
            ImageCor = ImageCor(ceil(Size(1)/4):round(Size(1)*3/4),ceil(Size(2)/4):round(Size(2)*3/4));
            MIAData.Cor{floor(i*1.5)}(:,:,j)=ImageCor./(mean2(Image(Use{i}(:,:,j)))^2);
            %%% Used to calculate total mean
            TotalInt(j)=sum(Image(Use{i}(:,:,j)));
            TotalIntRaw(j)=sum(ImRaw(Use{i}(:,:,j)));
            TotalPx(j)=numel(Image(Use{i}(:,:,j)));
        else %%% Standard ICS
            %%% Actual correlation
            Image_FFT=fft2(Image);
            %%% Used to calculate total mean
            TotalInt(j)=sum(sum((Image)));
            TotalIntRaw(j)=sum(sum((ImRaw)));
            TotalPx(j)=numel(Image);
            MIAData.Cor{floor(i*1.5)}(:,:,j)=fftshift(real(ifft2(Image_FFT.*conj(Image_FFT))))/(size(Image,1)*size(Image,2)*(mean2(Image))^2)-1;
        end
        if ~isvar('rangex')
            % show progress bar only for normal RICS, not grid-based RICS
            if mod(j,100)==0
                Progress(j/numel(Frames),h.Mia_Progress_Axes, h.Mia_Progress_Text,['Correlating ACF' num2str(i)]);
            end
        end
    end
    
    %%% Calculates mean intensity for saving
    MIAData.MeanInt(i)=sum(TotalInt)/sum(TotalPx);
    MIAData.MeanIntRaw(i)=sum(TotalIntRaw)/sum(TotalPx);
    %store it globally so it can be accessed from outside also
    clear Image ImageFluct ImageCor;
end

%%% Performs crosscorrelation
if Cross
    d1 = h.Mia_Image.Settings.Channel_PIE(1).Value; %(the datachannel displayed on top)
    d2 = h.Mia_Image.Settings.Channel_PIE(2).Value; %(the datachannel displayed below)
    if ~isvar('rangex')
        Progress(0,h.Mia_Progress_Axes, h.Mia_Progress_Text,'Correlating CCF');
    end
    MIAData.Cor{2}=zeros(numel(rx),numel(ry),numel(Frames));
    for j=1:numel(Frames)
        Image{1}=double(MIAData.Data{d1,2}(rx,ry,Frames(j)));
        Image{2}=double(MIAData.Data{d2,2}(rx,ry,Frames(j)));
        Size = [2*size(Image{1},1)-1, 2*size(Image{1},2)-1];
        if h.Mia_Image.Settings.ROI_FramesUse.Value==3  %%% Arbitrary region ICS
            %%% Calculates normalization for zero regions
            Norm=fft2(Use{1}(:,:,j).*Use{2}(:,:,j),Size(1),Size(2));
            Norm=fftshift(ifft2(Norm.*conj(Norm)));
            %%% Calculates fluctutation image
            ImageFluct{1}=Image{1}-mean(Image{1}(Use{1}(:,:,j) & Use{2}(:,:,j)));
            ImageFluct{2}=Image{2}-mean(Image{2}(Use{1}(:,:,j) & Use{2}(:,:,j)));
            %%% Applies selected region and FFT
            ImageFluct{1}=fft2(ImageFluct{1}.*(Use{1}(:,:,j) & Use{2}(:,:,j)),Size(1),Size(2));
            ImageFluct{2}=fft2(ImageFluct{2}.*(Use{1}(:,:,j) & Use{2}(:,:,j)),Size(1),Size(2));
            %%% Actual correlation
            ImageCor = fftshift(real(ifft2(ImageFluct{1}.*conj(ImageFluct{2}))));
            %%% Corrects for shape of selected region
            ImageCor = ImageCor./Norm;
            ImageCor = ImageCor(ceil(Size(1)/4):round(Size(1)*3/4),ceil(Size(2)/4):round(Size(2)*3/4));
            if mode~=1
                %normal cc(R)ICS
                MIAData.Cor{2}(:,:,j)=ImageCor/(mean(Image{1}(Use{1}(:,:,j) & Use{2}(:,:,j)))*mean(Image{2}(Use{1}(:,:,j) & Use{2}(:,:,j))));
            else %Van Steensel type colocalization, but then in 2D
                mode = mode-1+1; %just to avoid a red squigly line under mode at function heading
                MIAData.Cor{2}(:,:,j)=ImageCor/(std(Image{1}(Use{1}(:,:,j) & Use{2}(:,:,j)))*std(Image{2}(Use{1}(:,:,j) & Use{2}(:,:,j))));
            end
        else
            ImageFluct{1} = Image{1}-mean2(Image{1});
            ImageFluct{2} = Image{2}-mean2(Image{2});
            %%% Actual correlation
            MIAData.Cor{2}(:,:,j)=fftshift(real(ifft2(fft2(ImageFluct{1}).*conj(fft2(ImageFluct{2})))))/(size(Image{1},1)*size(Image{1},2));
            if mode~=1 %normal cc(R)ICS
                MIAData.Cor{2}(:,:,j)=MIAData.Cor{2}(:,:,j)/(mean2(Image{1})*mean2(Image{2}));
            else %Pearson / Van Steensel type colocalization, but then in 2D
                MIAData.Cor{2}(:,:,j)=MIAData.Cor{2}(:,:,j)/(std2(Image{1})*std2(Image{2}));
            end
        end
        if ~isvar('rangex')
            % show progress bar only for normal RICS, not grid-based RICS
            if mod(j,100)==0
                Progress(j/numel(Frames),h.Mia_Progress_Axes, h.Mia_Progress_Text,'Correlating CCF');
            end
        end
    end
end
clear Image ImageFluct ImageCor;

if mode ~= 2
    Progress(1,h.Mia_Progress_Axes, h.Mia_Progress_Text);
end

%%% Corrects the amplitude changes due to temporal moving average addition/subtraction
%%% Does NOT take into account pixel-based moving averages!
%%% The Formula assumes 2 or 3 species with different brightnesses and corrects the amplitude accordingly
if h.Mia_Image.Settings.Correction_Add.Value==5 && h.Mia_Image.Settings.Correction_Subtract.Value==4 %%% Subtracts and Adds moving average
    Sub=str2double(h.Mia_Image.Settings.Correction_Subtract_Frames.String);
    Add=str2double(h.Mia_Image.Settings.Correction_Add_Frames.String);
    if Sub~=Add %%% If Add==Sub, nothing was done        
        Correct=1/((1+1/Add-1/Sub)^2+(1/Add-1/Sub)^2*(min(Add,Sub)-1)+(1/max(Add,Sub))^2*abs(Add-Sub));
    else %%% If Add==Sub, nothing was done
        Correct=1;
    end
elseif h.Mia_Image.Settings.Correction_Add.Value==5
    Add=str2double(h.Mia_Image.Settings.Correction_Add_Frames.String); 
    Correct=1/((1+1/Add)^2+(1/Add)^2*(Add-1));
elseif h.Mia_Image.Settings.Correction_Subtract.Value==4
    Sub=str2double(h.Mia_Image.Settings.Correction_Subtract_Frames.String);
    if Sub ~= 1
        Correct=1/((1-1/Sub)^2+(1/Sub)^2*(Sub-1));
    else
        Correct = 1;
    end
else
    Correct=1;
end
%%% Applies amplitude correction
%dummy = 0;
for i=1:size(MIAData.Cor)
    if ~isempty(MIAData.Cor{i})
        MIAData.Cor{i}=MIAData.Cor{i}*Correct;
        % if dummy == 0
        %     for j = 1:size(MIAData.Cor{i},3)
        %         tmp = MIAData.Cor{i}(:,:,j);
        %         if any(isinf(tmp),'all')
        %             dummy = 1;
        %             %fprintf('SPECTRAL_RICS_INFO: Data quality too low or mask size too small in one or more frames!\n')
        %             break
        %         end
        %     end
        % end
    end  
end

if mode~=1
    % normal/grid RICS
    savedata = h.Mia_Image.Calculations.Cor_Save_ICS.Value;
else
    %Van Steensel type colocalization
    savedata = h.Mia_Image.Calculations.Save_Coloc.Value;
end

%%% Saves correlation files
if savedata ~= 1 && savedata ~= 5
    if ~isdir(fullfile(UserValues.File.MIAPath,'Mia'))
        mkdir(fullfile(UserValues.File.MIAPath,'Mia'))
    end
    
    if savedata ~= 4 %% normal saving
        Window = numel(Frames);
        Offset = numel(Frames);
        Blocks = 1;
    else  %% save blockwise .miacor
        % Window size
        if str2double(h.Mia_Image.Calculations.Cor_ICS_Window.String) == 0
            h.Mia_Image.Calculations.Cor_ICS_Window.String = num2str(floor(numel(Frames)/5));
        end
        Window = str2double(h.Mia_Image.Calculations.Cor_ICS_Window.String);
        % Offset size
        if str2double(h.Mia_Image.Calculations.Cor_ICS_Offset.String) == 0
            h.Mia_Image.Calculations.Cor_ICS_Offset.String = num2str(Window);
        end
        Offset = str2double(h.Mia_Image.Calculations.Cor_ICS_Offset.String);
        % n.o. blocks
        Blocks = floor((numel(Frames)-Window+Offset)/Offset);
        if Blocks < 1 %% reset values to make it work
            h.Mia_Image.Calculations.Cor_ICS_Window.String = num2str(floor(numel(Frames)/5));
            Window = str2double(h.Mia_Image.Calculations.Cor_ICS_Window.String);
            h.Mia_Image.Calculations.Cor_ICS_Offset.String = num2str(Window);
            Offset = str2double(h.Mia_Image.Calculations.Cor_ICS_Offset.String);
            Blocks = floor((numel(Frames)-Window+Offset)/Offset);
        end
    end

    for b = 1:Blocks
        frames = Frames(1+(b-1)*Offset:(b-1)*Offset+Window);
        DataAll=cell(3,2);
        InfoAll = struct;
        %% Gets auto correlation data to save
        for i = Auto
            %%% File name information
            InfoAll(i).File = MIAData.FileName{i};
            InfoAll(i).Path = UserValues.File.MIAPath;
            %%% ROI and TOI
            InfoAll(i).Frames = frames;
            From = h.Plots.ROI(1).Position(1:2)+0.5;
            To = From+h.Plots.ROI(1).Position(3:4)-1;
            InfoAll(i).ROI = [From To];
            %%% Pixel [us], Line [ms] and Frametime [s]
            InfoAll(i).Times = [str2double(h.Mia_Image.Settings.Image_Pixel.String) str2double(h.Mia_Image.Settings.Image_Line.String) str2double(h.Mia_Image.Settings.Image_Frame.String)];
            %%% Pixel size
            InfoAll(i).Size = str2double(h.Mia_Image.Settings.Pixel_Size.String);
            %%% Correction information
            InfoAll(i).Correction.SubType = h.Mia_Image.Settings.Correction_Subtract.String{h.Mia_Image.Settings.Correction_Subtract.Value};
            if h.Mia_Image.Settings.Correction_Subtract.Value == 4
                InfoAll(i).Correction.SubROI = [str2double(h.Mia_Image.Settings.Correction_Subtract_Pixel.String) str2double(h.Mia_Image.Settings.Correction_Subtract_Frames.String)];
            end
            InfoAll(i).Correction.AddType = h.Mia_Image.Settings.Correction_Add.String{h.Mia_Image.Settings.Correction_Add.Value};
            if h.Mia_Image.Settings.Correction_Add.Value == 5
                InfoAll(i).Correction.AddROI = [str2double(h.Mia_Image.Settings.Correction_Add_Pixel.String) str2double(h.Mia_Image.Settings.Correction_Add_Frames.String)];
            end
            %%% Correlation Type (Arbitrary region == 3)
            InfoAll(i).Type = h.Mia_Image.Settings.ROI_FramesUse.String{h.Mia_Image.Settings.ROI_FramesUse.Value};
            switch h.Mia_Image.Settings.ROI_FramesUse.Value
                case {1 2} %%% All/Selected frames
                    %%% Mean intensity [counts]
                    %InfoAll(i).Mean = mean2(double(MIAData.Data{i,2}(:,:,frames))); %Waldi
                    InfoAll(i).AR = [];
                case 3 %%% Arbitrary region
                    %%% Mean intensity of selected pixels [counts]
                    Image = double(MIAData.Data{i,2}(:,:,frames));
                    %InfoAll(i).Mean = mean(Image(Use{i}));
                    InfoAll(i).AR = Save_ARinfo([],1);
            end
            %%% Mean intensity
            InfoAll(i).Counts = MIAData.MeanInt(i); %Waldi
            InfoAll(i).CountsRaw = MIAData.MeanIntRaw(i); 
            %%% Averaged correlation
            DataAll{i,1} = mean(MIAData.Cor{floor(1.5*i),1}(:,:,(1:numel(frames))+(b-1)*Offset),3);
            %%% Error of correlation
            if size(MIAData.Cor{floor(1.5*i),1}(:,:,1:numel(frames)),3)>1
                DataAll{i,2} = std(MIAData.Cor{floor(1.5*i),1}(:,:,(1:numel(frames))+(b-1)*Offset),0,3)./sqrt(size(MIAData.Cor{floor(1.5*i),1}(:,:,(1:numel(frames))+(b-1)*Offset),3));
            else
                DataAll{i,2} = MIAData.Cor{floor(1.5*i),1}(:,:,(1:numel(frames))+(b-1)*Offset);
            end
        end
        %% Gets cross correlation data to save
        if Cross == 1
            %%% File name information
            InfoAll(3).File = MIAData.FileName{1};
            InfoAll(3).Path = UserValues.File.MIAPath;
            %%% ROI and TOI
            InfoAll(3).Frames = frames;
            From=h.Plots.ROI(1).Position(1:2)+0.5;
            To=From+h.Plots.ROI(1).Position(3:4)-1;
            InfoAll(3).ROI = [From To];
            %%% Pixel [us], Line [ms] and Frametime [s]
            InfoAll(3).Times = [str2double(h.Mia_Image.Settings.Image_Pixel.String) str2double(h.Mia_Image.Settings.Image_Line.String) str2double(h.Mia_Image.Settings.Image_Frame.String)];
            %%% Pixel size
            InfoAll(3).Size = str2double(h.Mia_Image.Settings.Pixel_Size.String);
            %%% Correction information
            InfoAll(3).Correction.SubType = h.Mia_Image.Settings.Correction_Subtract.String{h.Mia_Image.Settings.Correction_Subtract.Value};
            if h.Mia_Image.Settings.Correction_Subtract.Value == 4
                InfoAll(3).Correction.SubROI = [str2double(h.Mia_Image.Settings.Correction_Subtract_Pixel.String) str2double(h.Mia_Image.Settings.Correction_Subtract_Frames.String)];
            end
            InfoAll(3).Correction.AddType = h.Mia_Image.Settings.Correction_Add.String{h.Mia_Image.Settings.Correction_Add.Value};
            if h.Mia_Image.Settings.Correction_Add.Value == 5
                InfoAll(3).Correction.AddROI = [str2double(h.Mia_Image.Settings.Correction_Add_Pixel.String) str2double(h.Mia_Image.Settings.Correction_Add_Frames.String)];
            end
            %%% Correlation Type (Arbitrary region == 3)
            InfoAll(3).Type = h.Mia_Image.Calculations.Cor_Type.String{h.Mia_Image.Calculations.Cor_Type.Value};
            switch h.Mia_Image.Settings.ROI_FramesUse.Value
                case {1,2} %%% All/Selected frames
                    %%% Mean intensity [counts]
                    InfoAll(3).Mean = (mean2(double(MIAData.Data{1,2}(:,:,frames))) + mean2(double(MIAData.Data{2,2}(:,:,frames))))/2;
                    InfoAll(3).AR = [];
                case 3 %%% Arbitrary region
                    %%% Mean intensity of selected pixels [counts]
                    Use1 = Use{1}(frames);
                    Use2 = Use{2}(frames);
                    Image1 = double(MIAData.Data{1,2}(:,:,frames));
                    Image2 = double(MIAData.Data{2,2}(:,:,frames));
                    InfoAll(3).Mean = (mean(Image1(Use1 & Use2)) + mean(Image2(Use1 & Use2)))/2;
                    %%% Arbitrary region information
                    InfoAll(3).AR = Save_ARinfo([],1);
            end
            %%% Mean intensity
            InfoAll(3).Counts = sum(MIAData.MeanInt);
            InfoAll(3).CountsRaw = sum(MIAData.MeanIntRaw);
            %%% Averaged correlation
            DataAll{3,1} = mean(MIAData.Cor{2,1}(:,:,(1:numel(frames))+(b-1)*Offset),3);
            %%% Error of correlation
            DataAll{3,2} = std(MIAData.Cor{2,1}(:,:,1:numel(frames)),0,3)./sqrt(size(MIAData.Cor{2,1}(:,:,(1:numel(frames))+(b-1)*Offset),3));
        end
        %% Saves correlations
        switch savedata
            case {2,4,6} %%% .miacor filetype
                
                %% Creates new filename
                %%% Removes file extension
                switch MIAData.Type
                    case {1,1.5, 2}
                        FileName=MIAData.FileName{1}{1}(1:end-4);
                end
                
                if ~h.Mia_Image.Calculations.Save_Name.Value
                    %%% Manually enter a filename
                    [FileName,PathName] = uiputfile([FileName '.miacor'], 'Save correlation as', [UserValues.File.MIAPath,'Mia']);
                    if numel(FileName)>11 && (strcmp(FileName(end-11:end),'_ACF1_1.miacor') || strcmp(FileName(end-11:end),'_ACF2_1.miacor'))
                        FileName=FileName(1:end-12);
                        
                    elseif numel(FileName)>10 && strcmp(FileName(end-10:end),'_CCF_1.miacor')
                        FileName=FileName(1:end-11);
                    else
                        FileName=FileName(1:end-7);
                    end
                    Current_FileName1=fullfile(PathName,[FileName '_ACF1_1.miacor']);
                    Current_FileName2=fullfile(PathName,[FileName '_ACF2_1.miacor']);
                    Current_FileName3=fullfile(PathName,[FileName '_CCF_1.miacor']);
                    Current_FileName4=fullfile(PathName,[FileName '_Info_1.txt']);
                    
                else
                    %%% Automatically generate filename
                    Current_FileName1=fullfile(UserValues.File.MIAPath,'Mia',[FileName '_ACF1_1.miacor']);
                    Current_FileName2=fullfile(UserValues.File.MIAPath,'Mia',[FileName '_ACF2_1.miacor']);
                    Current_FileName3=fullfile(UserValues.File.MIAPath,'Mia',[FileName '_CCF_1.miacor']);
                    Current_FileName4=fullfile(UserValues.File.MIAPath,'Mia',[FileName '_Info_1.txt']);
                    if ~h.Mia_Image.Calculations.Overwrite.Value
                        %%% Checks, if file already exists and create new filename
                        if  exist(Current_FileName1,'file')  || exist(Current_FileName2,'file') || exist(Current_FileName3,'file') || exist(Current_FileName4,'file')
                            k=1;
                            %%% Adds 1 to filename
                            Current_FileName1=[Current_FileName1(1:end-9) '_' num2str(k) '.miacor'];
                            Current_FileName2=[Current_FileName2(1:end-9) '_' num2str(k) '.miacor'];
                            Current_FileName3=[Current_FileName3(1:end-9) '_' num2str(k) '.miacor'];
                            Current_FileName4=[Current_FileName4(1:end-6) '_' num2str(k) '.txt'];
                            %%% Increases counter, until no file is found
                            while exist(Current_FileName1,'file')  || exist(Current_FileName2,'file') || exist(Current_FileName3,'file') || exist(Current_FileName4,'file')
                                k=k+1;
                                Current_FileName1=[Current_FileName1(1:end-(7+numel(num2str(k-1)))) num2str(k) '.miacor'];
                                Current_FileName2=[Current_FileName2(1:end-(7+numel(num2str(k-1)))) num2str(k) '.miacor'];
                                Current_FileName3=[Current_FileName3(1:end-(7+numel(num2str(k-1)))) num2str(k) '.miacor'];
                                Current_FileName4=[Current_FileName4(1:end-(4+numel(num2str(k-1)))) num2str(k) '.txt'];
                            end
                        end
                    else

                    end
                end
                %%% Saves Auto correlations
                for i=Auto
                    Info = InfoAll(i);
                    Data = DataAll(i,:); 
                    if i==1
                        save(Current_FileName1,'Info','Data');
                    else
                        save(Current_FileName2,'Info','Data');
                    end
                end
                %%% Saves Cross correlations
                if Cross
                    Info = InfoAll(3); 
                    Data = DataAll(3,:); 
                    save(Current_FileName3,'Info','Data');
                end
                
                %% Saves info file
                FID = fopen(Current_FileName4,'w');
                if Cross
                    Info = InfoAll(3);
                else
                    Info = InfoAll(Auto);
                end
                fprintf(FID,'%s\n','Image Correlation info file');
                %%% Pixel\Line\Frame times
                fprintf(FID,'%s\t%f\n', 'Pixel time [us]:',Info.Times(1));
                fprintf(FID,'%s\t%f\n', 'Line  time [ms]:',Info.Times(2));
                fprintf(FID,'%s\t%f\n', 'Frame time [s] :',Info.Times(3));
                %%% Pixel size
                fprintf(FID,'%s\t%f\n', 'Pixel size [nm]:',Info.Size);
                %%% Region of interest
                fprintf(FID,'%s\t%u,%u,%u,%u\t%s\n', 'Region used [px]:',Info.ROI, 'X Start, Y Start, X Stop, Y Stop');
                %%% Counts per pixel
                fprintf(FID,'%s\t%f\n', 'Mean counts per pixel:',Info.Counts);
                %%% Frames used
                fprintf(FID,['%s\t',repmat('%u\t',[1 numel(Info.Frames)]) '\n'],'Frames Used:',Info.Frames);
                %%% Subtraction used
                switch h.Mia_Image.Settings.Correction_Subtract.Value
                    case 1
                        fprintf(FID,'%s\n','Nothing subtracted');
                    case 2
                        fprintf(FID,'%s\n','Frame mean subtracted');
                    case 3
                        fprintf(FID,'%s\n','Pixel mean subtracted');
                    case 4
                        fprintf(FID,'%s\t%u%s\t%u%s\n','Moving average subtracted:', InfoAll(i).Correction.SubROI(1), ' Pixel', InfoAll(i).Correction.SubROI(2),' Frames');
                end
                %%% Addition used
                switch h.Mia_Image.Settings.Correction_Add.Value
                    case 1
                        fprintf(FID,'%s\n','Nothing added');
                    case 2
                        fprintf(FID,'%s\n','Total mean added');
                    case 3
                        fprintf(FID,'%s\n','Frame mean added');
                    case 4
                        fprintf(FID,'%s\n','Pixel mean added');
                    case 5
                        fprintf(FID,'%s\t%u%s%u%s\n','Moving average added:', InfoAll(i).Correction.SubROI(1), ' Pixel', InfoAll(i).Correction.SubROI(2),' Frames');
                end
                %%% Arbitrary region
                if h.Mia_Image.Settings.ROI_FramesUse.Value==3
                    Save_ARinfo(FID, 2);                    
                end
                fclose(FID);
                
                
            case 3 %%% .tif + .txt files
                %% Creates new filename
                %%% Removes file extension
                switch MIAData.Type
                    case {1,2}
                        FileName=MIAData.FileName{1}{1}(1:end-4);
                end
                if ~h.Mia_Image.Calculations.Save_Name.Value
                    [FileName,PathName] = uiputfile([FileName '.tif'], 'Save correlation as', [UserValues.File.MIAPath,'Mia']);
                    if numel(FileName)>8 && (strcmp(FileName(end-8:end),'_ACF1.tif') || strcmp(FileName(end-8:end),'_ACF2.tif'))
                        FileName=FileName(1:end-9);
                        
                    elseif numel(FileName)>7 && strcmp(FileName(end-7:end),'_CCF.tif')
                        FileName=FileName(1:end-8);
                    else
                        FileName=FileName(1:end-4);
                    end
                    Current_FileName1=fullfile(PathName,[FileName '_ACF1.tif']);
                    Current_FileName2=fullfile(PathName,[FileName '_ACF2.tif']);
                    Current_FileName3=fullfile(PathName,[FileName '_CCF.tif']);
                    Current_FileName4=fullfile(PathName,[FileName '_Info.txt']);
                    
                else
                    
                    
                    %%% Generates filename
                    Current_FileName1=fullfile(UserValues.File.MIAPath,'Mia',[FileName '_ACF1.tif']);
                    Current_FileName2=fullfile(UserValues.File.MIAPath,'Mia',[FileName '_ACF2.tif']);
                    Current_FileName3=fullfile(UserValues.File.MIAPath,'Mia',[FileName '_CCF.tif']);
                    Current_FileName4=fullfile(UserValues.File.MIAPath,'Mia',[FileName '_Info.txt']);
                    if ~h.Mia_Image.Calculations.Overwrite.Value
                        %%% Checks, if file already exists and create new filename
                        if  exist(Current_FileName1,'file')  || exist(Current_FileName2,'file') || exist(Current_FileName3,'file') || exist(Current_FileName4,'file')
                            k=1;
                            %%% Adds 1 to filename
                            Current_FileName1=[Current_FileName1(1:end-4) '_' num2str(k) '.tif'];
                            Current_FileName2=[Current_FileName2(1:end-4) '_' num2str(k) '.tif'];
                            Current_FileName3=[Current_FileName3(1:end-4) '_' num2str(k) '.tif'];
                            Current_FileName4=[Current_FileName4(1:end-4) '_' num2str(k) '.txt'];
                            %%% Increases counter, until no file is found
                            while exist(Current_FileName1,'file')  || exist(Current_FileName2,'file') || exist(Current_FileName3,'file')
                                k=k+1;
                                Current_FileName1=[Current_FileName1(1:end-(4+numel(num2str(k-1)))) num2str(k) '.tif'];
                                Current_FileName2=[Current_FileName2(1:end-(4+numel(num2str(k-1)))) num2str(k) '.tif'];
                                Current_FileName3=[Current_FileName3(1:end-(4+numel(num2str(k-1)))) num2str(k) '.tif'];
                                Current_FileName4=[Current_FileName4(1:end-(4+numel(num2str(k-1)))) num2str(k) '.txt'];
                            end
                        end
                    end
                end
                %% Saves info file
                FID = fopen(Current_FileName4,'w');
                if Cross
                    Info = InfoAll(3);
                else
                    Info = InfoAll(Auto);
                end
                fprintf(FID,'%s\n','Image Correlation info file');
                %%% Pixel\Line\Frame times
                fprintf(FID,'%s\t%f\n', 'Pixel time [us]:',Info.Times(1));
                fprintf(FID,'%s\t%f\n', 'Line  time [ms]:',Info.Times(2));
                fprintf(FID,'%s\t%f\n', 'Frame time [s] :',Info.Times(3));
                %%% Pixel size
                fprintf(FID,'%s\t%f\n', 'Pixel size [nm]:',Info.Size);
                %%% Region of interest
                fprintf(FID,'%s\t%u,%u,%u,%u\t%s\n', 'Region used [px]:',Info.ROI, 'X Start, Y Start, X Stop, Y Stop');
                %%% Counts per pixel
                fprintf(FID,'%s\t%f\n', 'Mean counts per pixel:',Info.Counts);
                %%% Frames used
                fprintf(FID,['%s\t',repmat('%u\t',[1 numel(Info.Frames)]) '\n'],'Frames Used:',Info.Frames);
                %%% Subtraction used
                switch h.Mia_Image.Settings.Correction_Subtract.Value
                    case 1
                        fprintf(FID,'%s\n','Nothing subtracted');
                    case 2
                        fprintf(FID,'%s\n','Frame mean subtracted');
                    case 3
                        fprintf(FID,'%s\n','Pixel mean subtracted');
                    case 4
                        fprintf(FID,'%s\t%u%s\t%u%s\n','Moving average subtracted:', InfoAll(i).Correction.SubROI(1), ' Pixel', InfoAll(i).Correction.SubROI(2),' Frames');
                end
                %%% Addition used
                switch h.Mia_Image.Settings.Correction_Add.Value
                    case 1
                        fprintf(FID,'%s\n','Nothing added');
                    case 2
                        fprintf(FID,'%s\n','Total mean added');
                    case 3
                        fprintf(FID,'%s\n','Frame mean added');
                    case 4
                        fprintf(FID,'%s\n','Pixel mean added');
                    case 5
                        fprintf(FID,'%s\t%u%s%u%s\n','Moving average added:', InfoAll(i).Correction.SubROI(1), ' Pixel', InfoAll(i).Correction.SubROI(2),' Frames');
                end
                %%% Arbitrary region
                if h.Mia_Image.Settings.ROI_FramesUse.Value==3
                    Save_ARinfo(FID, 2);  
                end
                fclose(FID);
                %% Saves correlation TIFFs
                for i=Auto
                    %%% Resizes double to 16bit uint
                    Data{1} = DataAll{i,1};
                    Min(1) = min(min(Data{1}));
                    Max(1) = max(max(Data{1}));
                    Data{1} = uint16(2^16*(Data{1}-Min(1))/(Max(1)-Min(1)));
                    Data{2} = DataAll{i,2};
                    Min(2) = min(min(Data{2}));
                    Max(2) = max(max(Data{2}));
                    Data{2} = uint16(2^16*(Data{2}-Min(2))/(Max(2)-Min(2)));
                    %%% Creates header information
                    TiffStruct.ImageWidth = size(Data{1},2);
                    TiffStruct.ImageLength = size(Data{1},1);
                    TiffStruct.BitsPerSample = 16;
                    TiffStruct.Photometric = Tiff.Photometric.MinIsBlack;
                    TiffStruct.Compression = Tiff.Compression.LZW;
                    TiffStruct.PlanarConfiguration = Tiff.PlanarConfiguration.Chunky;
                    TiffStruct.ImageDescription = num2str([round(2^16/(Max(1)-Min(1))), Min(1), InfoAll(i).Counts, InfoAll(i).Times(1)]);
                    %%% Creates new TIFF file
                    if i==1
                        
                        t = Tiff(Current_FileName1,'w');
                    else
                        t = Tiff(Current_FileName2,'w');
                    end
                    %%% Saves correlation as 2 frames (Cor and Error)
                    t.setTag(TiffStruct);
                    t.write(Data{1});
                    t.writeDirectory();
                    TiffStruct.ImageDescription = num2str([round(2^16/(Max(1)-Min(1))), Min(1), InfoAll(i).Counts, InfoAll(i).Times(1)]);
                    t.setTag(TiffStruct);
                    t.write(Data{2});
                    t.close();
                end
                if Cross
                    %%% Resizes double to 16bit uint
                    Data{1} = DataAll{3,1};
                    Min(1) = min(min(Data{1}));
                    Max(1) = max(max(Data{1}));
                    Data{1} = uint16(2^16*(Data{1}-Min(1))/(Max(1)-Min(1)));
                    Data{2} = DataAll{i,2};
                    Min(2) = min(min(Data{2}));
                    Max(2) = max(max(Data{2}));
                    Data{2} = uint16(2^16*(Data{2}-Min(2))/(Max(2)-Min(2)));
                    %%% Creates header information
                    TiffStruct.ImageWidth = size(Data{1},2);
                    TiffStruct.ImageLength = size(Data{1},1);
                    TiffStruct.BitsPerSample = 16;
                    TiffStruct.Photometric = Tiff.Photometric.MinIsBlack;
                    TiffStruct.Compression = Tiff.Compression.LZW;
                    TiffStruct.PlanarConfiguration = Tiff.PlanarConfiguration.Chunky;
                    TiffStruct.ImageDescription = num2str([round(2^16/(Max(1)-Min(1))), Min(1), InfoAll(3).Counts, InfoAll(3).Times(1)]);
                    %%% Creates new TIFF file
                    t = Tiff(Current_FileName3,'w');
                    %%% Saves correlation as 2 frames (Cor and Error)
                    t.setTag(TiffStruct);
                    t.write(Data{1});
                    t.writeDirectory();
                    TiffStruct.ImageDescription = num2str([round(2^16/(Max(2)-Min(2))), Min(2)]);
                    t.setTag(TiffStruct);
                    t.write(Data{2});
                    t.close();
                end
                
            otherwise
        end
        
    end
end

for i=channel
    h.Mia_ICS.Axes(i,1).Visible='on';
    h.Mia_ICS.Axes(i,2).Visible='on';
    h.Mia_ICS.Axes(i,3).Visible='on';
    h.Plots.Cor(i,1).Visible='on';
    h.Plots.Cor(i,2).Visible='on';
    h.Plots.Cor(i,3).Visible='on';
    
    center=[floor(size(MIAData.Cor{i},1)/2)+1,floor(size(MIAData.Cor{i},2)/2)+1];
    MIAData.Cor{i}(center(1),center(2),:)=(MIAData.Cor{i}(center(1),center(2)-1,:)+MIAData.Cor{i}(center(1),center(2)+1,:))/2;
end

%%% Updates correlation frame slider
i=channel(1);
h.Mia_ICS.Frame_Slider.Min=0;
h.Mia_ICS.Frame_Slider.Max=size(MIAData.Cor{i},3);
h.Mia_ICS.Frame_Slider.SliderStep=[1./(size(MIAData.Cor{i},3)+1),10/(size(MIAData.Cor{i},3)+1)];
h.Mia_ICS.Frame_Slider.Value=0;
h.Mia_ICS.Frames2Use.String=['1:' num2str(size(MIAData.Cor{i},3))];

%%% Updates correlation plots
Update_Plots([],[],2,'fast');
% even for grid-based RICS we have to do this, since the fit data is extracted from the plot

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Funtion to calculate temporal image correlations %%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function Do_1D_XCor(obj,~)
global MIAData
h = guidata(findobj('Tag','Mia'));

% % Force normal ROI selection, not AR
% if h.Mia_Image.Settings.ROI_FramesUse.Value ~= 1
%     h.Mia_Image.Settings.ROI_FramesUse.Value = 1;
%     m = msgbox('Normal ROI selection will be applied');
%     pause(2)
%     close(m)
%     MIA_Various([],[],3);
% end


%%% Stops, if no data was loaded
if size(MIAData.Data,1)<1
    return;
end

h.Mia_Progress_Text.String = 'Correlating';
h.Mia_Progress_Axes.Color=[1 0 0];  
drawnow;
%%% Clears correlation data and plots
MIAData.TICS.Data = [];
MIAData.TICS.FreehandMask = [];
MIAData.TICS.ThreshMask = [];
MIAData.TICS.AR_NoIncludedFrames = [];

%%% Adjust for number of selected files
if size(MIAData.Data,1)<2
    h.Mia_Image.Calculations.Cor_Type.Value = 1;
end

%%% Determines, which correlations to perform
if h.Mia_Image.Calculations.Cor_Type.Value==3
    Auto = 1:2; Cross = 1;
    channel = 1:3;
else
    Auto = h.Mia_Image.Calculations.Cor_Type.Value; Cross = 0;
    channel = floor(Auto*1.5);
end
MIAData.TICS.Auto = Auto;
MIAData.TICS.Cross = Cross;

%%% Determines, which frames to correlate
Frames = sort(str2num(h.Mia_Image.Settings.ROI_Frames.String)); %#ok<ST2NM> %%% Uses str2num, because the output is not scalar
%%% Uses all Frames, if input was 0
if all(Frames == 0)
    Frames = 1:size(MIAData.Data{1,2},3);
end
%%% Remove all Frames<1 and >Movie size
if any(Frames<0 | Frames>size(MIAData.Data{1,2},3))
    Min = max(1,min(Frames)); Min = min(Min,size(MIAData.Data{1,2},3));   
    Max = min(size(MIAData.Data{1,2},3),max(Frames)); Max = max(Max,1);
    Frames = Min:Max;
    h.Mia_Image.Settings.ROI_Frames.String = [num2str(Min) ':' num2str(Max)];
end

%%% Applies arbitrary region selection
switch (h.Mia_Image.Settings.ROI_FramesUse.Value)
    case 1 %%% Use All Frames
        for i=Auto
            Use{i} = true(size(MIAData.Data{1,2},1), size(MIAData.Data{1,2},2), Frames(end));
            MIAData.TICS.AR_NoIncludedFrames{i}(1:size(MIAData.Data{1,2},1), 1:size(MIAData.Data{1,2},2)) = Frames(end);
        end
    case 2 %%% Use selected Frames
        if Cross
            Active = find(prod(MIAData.Use));
        else
            Active = find(MIAData.Use(Auto,:));
        end
        Frames = intersect(Frames,Active);
        for i=Auto
            Use{i} = true(size(MIAData.Data{1,2},1), size(MIAData.Data{1,2},2), Frames(end));
            MIAData.TICS.AR_NoIncludedFrames{i}(1:size(MIAData.Data{1,2},1), 1:size(MIAData.Data{1,2},2)) = size(Frames,2);
        end        
    case 3 %%% Does arbitrary region ICS
        if Cross
            Active = find(prod(MIAData.Use));
        else
            Active = find(MIAData.Use(Auto,:));
        end
        Frames = intersect(Frames,Active);
        for i=Auto
            Use{i} = logical(MIAData.AR{i,1}(:,:,1:Frames(end)) & repmat(MIAData.MS{i},1,1,Frames(end)));
            % 2D array with the number of included samples per pixel
            tmp = sum(Use{i},3);
            % if a pixel contains < 50 samples, the TICS correlation will look bad anyway
            tmp (tmp < 50) = 0;
            MIAData.TICS.AR_NoIncludedFrames{i} = tmp;
            clear tmp
        end   
end
MIAData.TICS.Frames = Frames;
MIAData.TICS.Use = Use;

%% determine if spatial filtering will be applied to TICS data
if str2double(h.Mia_Image.Calculations.Cor_TICS_AvgSize.String)<=1
    h.Mia_Image.Calculations.Cor_TICS_SpaceAvg.Value=1;
end
%%% Determinesspatial filter
switch h.Mia_Image.Calculations.Cor_TICS_SpaceAvg.Value
    case 1 %%% Do nothing
        Filter = fspecial('average',1);
    case 2 %%% Moving average
        Filter = fspecial('average',round(str2double(h.Mia_Image.Calculations.Cor_TICS_AvgSize.String)));
    case 3 %%% Disc average
        Filter = fspecial('disk',str2double(h.Mia_Image.Calculations.Cor_TICS_AvgSize.String)-1);
    case 4 %%% Gaussian average
        Filter = fspecial('gaussian',2*str2double(h.Mia_Image.Calculations.Cor_TICS_AvgSize.String),str2double(h.Mia_Image.Calculations.Cor_TICS_AvgSize.String)/2);
end

%% Performs TICS correlation
for i=1:3 %%%    
    if any(Auto==i) || (i==3 && Cross)       
        %% FFT based time correlation
        %%% Exctracts data and sets unused frames to 0
        Empty = setdiff(Frames(1):Frames(end),Frames)-Frames(1);
        if i<3 %%% For autocorrelation both channels are equal
            d = h.Mia_Image.Settings.Channel_PIE(i).Value; %(the datachannel that is currently displayed)
            Norm = logical(Use{i}(:,:,Frames(1):Frames(end)));
            Norm (:,:,Empty) = false;
            TICS{1} = MIAData.Data{d,2}(:,:,(Frames(1):Frames(end)));
            TICS{1}(~Norm) = NaN;
            Int{1} = nanmean(TICS{1},3);
            TICS{1} = TICS{1}-mean2(TICS{1}(Norm));
            TICS{1}(~Norm) = 0;
            Int{2} = Int{1};
        else %%% For crosscorelation, use both channels
            d1 = h.Mia_Image.Settings.Channel_PIE(1).Value; %(the datachannel that is currently displayed)
            d2 = h.Mia_Image.Settings.Channel_PIE(2).Value; %(the datachannel that is currently displayed)
            Norm = logical(Use{1}(:,:,Frames(1):Frames(end)) & Use{2}(:,:,Frames(1):Frames(end)));
            Norm (:,:,Empty) = false;
            TICS{1} = MIAData.Data{d1,2}(:,:,(Frames(1):Frames(end)));
            TICS{1}(~Norm) = NaN;
            Int{1} = nanmean(TICS{1},3);
            TICS{1} = TICS{1}-mean2(TICS{1}(Norm));
            TICS{1}(~Norm) = 0;
            TICS{2} = MIAData.Data{d2,2}(:,:,(Frames(1):Frames(end)));
            TICS{2}(~Norm) = NaN;
            Int{2} = nanmean(TICS{2},3);
            TICS{2} = TICS{2}-mean2(TICS{2}(Norm));
            TICS{2}(~Norm) = 0;
        end
        
        %%% Calculate normalization, accounting for arbitrary region and
        %%% missing frames
        Normt = Norm; clear Norm;
        for l = 1:size(Normt,1)
            Norm = Normt(l,:,:);
            Norm = single(fft(Norm,2*size(Norm,3)-1,3));
            Norm = fftshift(real(ifft(Norm.*conj(Norm),[],3)),3);
            %%% Averages forward and backward correlation
            if mod(size(Norm,3),2) == 0
                Norm = Norm(:,:,(size(Norm,3)/2):-1:2)+Norm(:,:,(size(Norm,3)/2)+2:end);
            else
                Norm = Norm(:,:,(floor(size(Norm,3)/2):-1:1))+Norm(:,:,(ceil(size(Norm,3)/2)+1:end));
            end
            %%% Removes very long lag times (1/10th of frames)
            Norm = Norm(:,:,1:ceil((Frames(end)-Frames(1))/10));

            %%% FFT based time correlation 
            TICSl{1} = fft(TICS{1}(l,:,:),2*size(TICS{1},3)-1,3);
            if i<3 %%% Saves time for autocorrelation
                TICSl{2}=TICSl{1};
            else
                TICSl{2} = fft(TICS{2}(l,:,:),2*size(TICS{2},3)-1,3);
            end
            TICSresult = fftshift(real(ifft(TICSl{1}.*conj(TICSl{2}),[],3)),3);
            % clear TICS;
            %%% Averages forward and backward correlation
            if mod(size(TICSresult,3),2) == 0
                TICSresult = TICSresult(:,:,(size(TICSresult,3)/2):-1:2)+TICSresult(:,:,(size(TICSresult,3)/2)+2:end);
            else
                TICSresult = TICSresult(:,:,(floor(size(TICSresult,3)/2):-1:1))+TICSresult(:,:,(ceil(size(TICSresult,3)/2)+1:end));
            end
            %%% Removes very long lag times (1/10th of frames)
            TICSresult = TICSresult(:,:,1:ceil((Frames(end)-Frames(1))/10));
            %%% Normalizes to different lag occurrence
            TICSresult = TICSresult./Norm;
            clear Norm;
            %%% Normalizes to pixel intensity
            % Still store only a single (cross)correlation
            MIAData.TICS.Data{i}(l,:,:) = TICSresult./repmat(Int{1}(l,:).*Int{2}(l,:),1,1,size(TICSresult,3));
        end
        
        % apply spatial averaging to TICS data
        for l = 1:size(MIAData.TICS.Data{i},3)
            MIAData.TICS.Data{i}(:,:,l) = imfilter(MIAData.TICS.Data{i}(:,:,l),Filter,'symmetric');
        end
        
        clear Normt;
        MIAData.TICS.Int{i,1} = Int{1};
        MIAData.TICS.Int{i,2} = Int{2};
        clear TICSresult;
        %%% Remove too dark pixels
        Valid = sqrt(Int{1}.*Int{2})> nanmean(nanmean(sqrt(Int{1}.*Int{2}),2),1)/10;
        MIAData.TICS.Data{i}(~repmat(Valid,1,1,size(MIAData.TICS.Data{i},3))) = NaN;
        
        %Int1 and Int2 defined already above depending on the Auto or Cross
        data{i}.Counts = [nanmean(nanmean(Int{1},2),1) nanmean(nanmean(Int{2},2),1)]/str2double(h.Mia_Image.Settings.Image_Pixel.String)*1000;
        data{i}.Valid = 1;
        data{i}.Cor_Times = (1:size(MIAData.TICS.Data{i},3))*str2double(h.Mia_Image.Settings.Image_Frame.String);
        data{i}.Cor_Average = double(squeeze(nanmean(nanmean(MIAData.TICS.Data{i},2),1))');
        data{i}.Cor_Array = data{i}.Cor_Average';
        data{i}.Cor_SEM = double(squeeze(nanstd(nanstd(MIAData.TICS.Data{i},0,2),0,1))');
        data{i}.Cor_SEM = data{i}.Cor_SEM./sqrt(sum(reshape(~isnan(MIAData.TICS.Data{i}),[],size(MIAData.TICS.Data{i},3)),1));
    end
end
%% Saves data & info file
if h.Mia_Image.Calculations.Cor_Save_TICS.Value == 2   
    Save_TICS([],[],data)
end

Update_Plots(obj,[],5);
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Funtion to calculate STICS/iMSD correlations %%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function Do_3D_XCor(~,~)
h = guidata(findobj('Tag','Mia'));
global MIAData UserValues

MIAData.STICS = [];
%%% Stops, if no data was loaded
if size(MIAData.Data,1)<1
    return;
end

if size(MIAData.Data,1)<2
    h.Mia_Image.Calculations.Cor_Type.Value=1;
end

%%% Determins, which correlations to perform
if h.Mia_Image.Calculations.Cor_Type.Value==3
    Auto=1:2; Cross=1;
else
    Auto=h.Mia_Image.Calculations.Cor_Type.Value; Cross=0;
end


%%% Determins, which frames to correlate
Frames = str2num(h.Mia_Image.Settings.ROI_Frames.String); %#ok<ST2NM> %%% Uses str2num, because the output is not scalar
%%% Uses all Frames, if input was 0
if all(Frames==0)
    Frames=1:size(MIAData.Data{1,2},3);
end
%%% Remove all Frames<1 and >Movie size
if any(Frames<0 | Frames>size(MIAData.Data{1,2},3))
    Min=max(1,min(Frames)); Min=min(Min,size(MIAData.Data{1,2},3));   
    Max=min(size(MIAData.Data{1,2},3),max(Frames)); Max=max(Max,1);
    Frames=Min:Max;
    h.Mia_Image.Settings.ROI_Frames.String=[num2str(Min) ':' num2str(Max)];
end

%%% Applies arbitrary region selection
switch (h.Mia_Image.Settings.ROI_FramesUse.Value)
    case 1 %%% Use All Frames
        for i=Auto
            Use{i} = true(size(MIAData.Data{i,2},1),size(MIAData.Data{i,2},2),numel(Frames));
        end
    case 2 %%% Use selected Frames
        if Cross
            Active = find(prod(MIAData.Use));
        else
            Active = find(MIAData.Use(Auto,:));
        end
        Frames=intersect(Frames,Active);
        for i=Auto
            Use{i} = true(size(MIAData.Data{i,2},1),size(MIAData.Data{i,2},2),numel(Frames));
        end
    case 3 %%% Does arbitrary region ICS
        if Cross
            Active=find(prod(MIAData.Use));
        else
            Active=find(MIAData.Use(Auto,:));
        end
        Frames=intersect(Frames,Active);
        for i=Auto
            Use{i} = MIAData.AR{i,1}(:,:,Frames) & repmat(MIAData.MS{i},1,1,numel(Frames));
        end
end


MaxLag = str2double(h.Mia_Image.Calculations.Cor_ICS_Window.String);
h.Mia_STICS.Lag_Slider.Max = MaxLag;
h.Mia_STICS.Lag_Slider.SliderStep = [1/MaxLag 1/MaxLag];


%% Performs spatio temporal correlation
for i = 1:3
    if any(Auto==i) || (i==3 && Cross)
        TotalInt = zeros(numel(Frames));
        TotalPx = zeros(numel(Frames));
        First = 2-abs(i-2);  First = h.Mia_Image.Settings.Channel_PIE(First).Value; %(the datachannel that is currently displayed)      
        Second = floor(i/2)+1;  Second = h.Mia_Image.Settings.Channel_PIE(Second).Value; %(the datachannel that is currently displayed)            
        MIAData.STICS{i} = zeros(size(MIAData.Data{First,2},1),size(MIAData.Data{First,2},2),MaxLag+1, 'single');
        MIAData.STICS_SEM{i} = zeros(size(MIAData.Data{First,2},1),size(MIAData.Data{First,2},2),MaxLag+1, 'single');
        STICS_Num = zeros(size(MIAData.Data{First,2},1),size(MIAData.Data{First,2},2),MaxLag+1, 'uint16');
        if i<3
            Progress(0,h.Mia_Progress_Axes, h.Mia_Progress_Text,['Correlating ACF' num2str(i)]);
        else
            Progress(0,h.Mia_Progress_Axes, h.Mia_Progress_Text,'Correlating CCF');
        end
        for j=i:numel(Frames)
            Image{1} = double(MIAData.Data{First,2}(:,:,Frames(j)));
            if i<3
                TotalInt(j)=sum(Image{1}(Use{i}(:,:,j)));
                TotalPx(j)=numel(Image{1}(Use{i}(:,:,j)));
            end
            ImageFluct{1} = Image{1}-mean(Image{1}(Use{First}(:,:,j) & Use{Second}(:,:,j)));
            Size = [2*size(Image{1},1)-1, 2*size(Image{1},2)-1];
            ImageFluct{1} = fft2(ImageFluct{1}.*(Use{First}(:,:,j) & Use{Second}(:,:,j)),Size(1),Size(2));
                      
            for k = 0:MaxLag
                if any(Frames == Frames(j)+k)
                    Lag = find(Frames == Frames(j)+k,1,'first');
                    Image{2} = double(MIAData.Data{Second,2}(:,:,Frames(Lag)));
                    %%% Calculates normalization for zero regions
                    Norm=fft2(Use{First}(:,:,j).*Use{Second}(:,:,Lag),Size(1),Size(2));
                    Norm=fftshift(ifft2(Norm.*conj(Norm)));
                    %%% Calculates fluctutation image
                    
                    ImageFluct{2} = Image{2}-mean(Image{2}(Use{First}(:,:,j) & Use{Second}(:,:,Lag)));
                    %%% Applies selected region and FFT
                    
                    ImageFluct{2} = fft2(ImageFluct{2}.*(Use{First}(:,:,j) & Use{Second}(:,:,Lag)),Size(1),Size(2));
                    %%% Actual correlation
                    ImageCor = fftshift(real(ifft2(ImageFluct{1}.*conj(ImageFluct{2}))));
                    %%% Corrects for shape of selected region
                    ImageCor = ImageCor./Norm;
                    ImageCor = ImageCor(ceil(Size(1)/4):round(Size(1)*3/4),ceil(Size(2)/4):round(Size(2)*3/4));
                    ImageCor = ImageCor/(mean(Image{1}(Use{First}(:,:,j) & Use{Second}(:,:,Lag)))*mean(Image{2}(Use{First}(:,:,j) & Use{Second}(:,:,Lag))));
                    
                    %%% Searches for pixels with entries 
                    NonZero = ~isnan(ImageCor);
                    %%% Old mean
                    Old_Mean = MIAData.STICS{i}(:,:,k+1)./double(STICS_Num(:,:,k+1));   
                    Old_Mean(isnan(Old_Mean)) = 0;
                    %%% Adds to counter
                    STICS_Num(:,:,k+1) = STICS_Num(:,:,k+1) + uint16(~isnan(ImageCor));
                    ImageCor(isnan(ImageCor)) = 0;       
                    %%% Sum of correlations
                    MIAData.STICS{i}(:,:,k+1) = MIAData.STICS{i}(:,:,k+1) + ImageCor;
                    %%% New Mean
                    New_Mean = MIAData.STICS{i}(:,:,k+1)./double(STICS_Num(:,:,k+1));
                    
                    %%% Calculates "current mean" according to online
                    %%% algorithm (see wikipedia)
                    S = MIAData.STICS_SEM{i}(:,:,k+1);
                    S(NonZero) = S(NonZero) + (ImageCor(NonZero)-Old_Mean(NonZero)).*(ImageCor(NonZero)-New_Mean(NonZero));
                    MIAData.STICS_SEM{i}(:,:,k+1) = S;
                end                
            end            
            if mod(j,100)==0
                if i<3
                    Progress(j/numel(Frames),h.Mia_Progress_Axes, h.Mia_Progress_Text,['Correlating ACF' num2str(i)]);
                else
                    Progress(j/numel(Frames),h.Mia_Progress_Axes, h.Mia_Progress_Text,'Correlating CCF');
                end
            end
        end
        if i<3
            %%% Calculates mean intensity for saving
            MeanInt(i)=sum(TotalInt)/sum(TotalPx);
        end
        MIAData.STICS{i} = MIAData.STICS{i}./single(STICS_Num);
        MIAData.STICS_SEM{i} = sqrt(MIAData.STICS_SEM{i}./single(STICS_Num-1)./single(STICS_Num));
        
        %%% Removes noise peak at G(0, 0, 0)
        MIAData.STICS{i}(ceil((size(MIAData.STICS{i},1)+1)/2),ceil((size(MIAData.STICS{i},2)+1)/2),1) =...
            (MIAData.STICS{i}(ceil((size(MIAData.STICS{i},1)+1)/2),ceil((size(MIAData.STICS{i},2)+1)/2)-1,1)+...
             MIAData.STICS{i}(ceil((size(MIAData.STICS{i},1)+1)/2),ceil((size(MIAData.STICS{i},2)+1)/2)+1,1))/2;
        MIAData.STICS{i}(isnan(MIAData.STICS{i})) = 0;
    end  
end
clear Image ImageFluct ImageCor;

%%% Switches 2nd and 3rd entry to make it conform with ICS
%%% Cross is 2nd entry
if size(MIAData.STICS,2)>1
    if size(MIAData.STICS,2)==2
        MIAData.STICS{3} = MIAData.STICS{2};
        MIAData.STICS{2} = [];
    else
        MIAData.STICS = MIAData.STICS([1 3 2]);
    end
end

%%% Creates empty iMSD entry
for i=1:3
    if size(MIAData.STICS,2)>=i && ~isempty(MIAData.STICS{i})
        MIAData.iMSD{i,1} = zeros(size(MIAData.STICS{i},3),1);
        MIAData.iMSD{i,2} = zeros(size(MIAData.STICS{i},3),2);
    end
end

Progress(1,h.Mia_Progress_Axes, h.Mia_Progress_Text);
%% Saves correlations
if ~isdir(fullfile(UserValues.File.MIAPath,'Mia')) && h.Mia_Image.Calculations.Cor_Save_STICS.Value>1
    mkdir(fullfile(UserValues.File.MIAPath,'Mia'))
end
%%% .mcor filetype (iMSD)
if any(h.Mia_Image.Calculations.Cor_Save_STICS.Value == [2 4])
    %% Creates new filename
    %%% Removes file extension
    switch MIAData.Type
        case {1,2}
            FileName = MIAData.FileName{1}{1}(1:end-4);
    end
    %%% Generates filename
    Current_FileName1=fullfile(UserValues.File.MIAPath,'Mia',[FileName '_iMSD1.mcor']);
    Current_FileName2=fullfile(UserValues.File.MIAPath,'Mia',[FileName '_cciMSD.mcor']);
    Current_FileName3=fullfile(UserValues.File.MIAPath,'Mia',[FileName '_iMSD2.mcor']);
    %%% Checks, if file already exists and create new filename
    if  exist(Current_FileName1,'file')  || exist(Current_FileName2,'file') || exist(Current_FileName3,'file')
        k=1;
        %%% Adds 1 to filename
        Current_FileName1=[Current_FileName1(1:end-5) '_' num2str(k) '.mcor'];
        Current_FileName2=[Current_FileName2(1:end-5) '_' num2str(k) '.mcor'];
        Current_FileName3=[Current_FileName3(1:end-5) '_' num2str(k) '.mcor'];
        %%% Increases counter, until no file is found
        while exist(Current_FileName1,'file')  || exist(Current_FileName2,'file') || exist(Current_FileName3,'file')
            k=k+1;
            Current_FileName1=[Current_FileName1(1:end-(6+numel(num2str(k-1)))) num2str(k) '.mcor'];
            Current_FileName2=[Current_FileName2(1:end-(6+numel(num2str(k-1)))) num2str(k) '.mcor'];
            Current_FileName3=[Current_FileName3(1:end-(6+numel(num2str(k-1)))) num2str(k) '.mcor'];
        end
    end
    for i=1:3
        if size(MIAData.iMSD,1) >= i && ~isempty(MIAData.iMSD{i,1})
            %%% Fit STICS data with gaussin
            if all(MIAData.iMSD{i,1}==0)
                Do_Gaussian
            end
            Header = 'iMSD correlation file'; %#ok<NASGU>
            Counts = [1 1];
            Valid = 1;
            Cor_Times = (0:(size(MIAData.iMSD{i,1},1)-1))'*str2double(h.Mia_Image.Settings.Image_Frame.String);
            Cor_Times(1) = 10^-10;
            Cor_Average = MIAData.iMSD{i,1}.^2;
            Cor_Array = MIAData.iMSD{i,1}.^2;
            Cor_SEM = (abs((MIAData.iMSD{i,1}.^2-MIAData.iMSD{i,2}(:,1).^2))+abs((MIAData.iMSD{i,1}.^2-MIAData.iMSD{i,2}(:,2).^2)))/2;
            save(eval(['Current_FileName' num2str(i)]),'Header','Counts','Valid','Cor_Times','Cor_Average','Cor_SEM','Cor_Array');
        end
    end
end
%%% .stcor filetype
if any(h.Mia_Image.Calculations.Cor_Save_STICS.Value == [3 4])
    DataAll = cell(3,2);
    InfoAll = struct;
    %% Gets auto correlations data to save
    for i = Auto
        d = h.Mia_Image.Settings.Channel_PIE(i).Value;
        %%% File name information
        InfoAll(i).File = MIAData.FileName{i};
        InfoAll(i).Path = UserValues.File.MIAPath;
        %%% ROI and TOI
        InfoAll(i).Frames = Frames;
        From = h.Plots.ROI(1).Position(1:2)+0.5;
        To = From+h.Plots.ROI(1).Position(3:4)-1;
        InfoAll(i).ROI = [From To];
        %%% Pixel [us], Line [ms] and Frametime [s]
        InfoAll(i).Times = [str2double(h.Mia_Image.Settings.Image_Pixel.String) str2double(h.Mia_Image.Settings.Image_Line.String) str2double(h.Mia_Image.Settings.Image_Frame.String)];
        %%% Pixel size
        InfoAll(i).Size = str2double(h.Mia_Image.Settings.Pixel_Size.String);
        %%% Correction information
        InfoAll(i).Correction.SubType = h.Mia_Image.Settings.Correction_Subtract.String{h.Mia_Image.Settings.Correction_Subtract.Value};
        if h.Mia_Image.Settings.Correction_Subtract.Value == 4
            InfoAll(i).Correction.SubROI = [str2double(h.Mia_Image.Settings.Correction_Subtract_Pixel.String) str2double(h.Mia_Image.Settings.Correction_Subtract_Frames.String)];
        end
        InfoAll(i).Correction.AddType = h.Mia_Image.Settings.Correction_Add.String{h.Mia_Image.Settings.Correction_Add.Value};
        if h.Mia_Image.Settings.Correction_Add.Value == 5
            InfoAll(i).Correction.AddROI = [str2double(h.Mia_Image.Settings.Correction_Add_Pixel.String) str2double(h.Mia_Image.Settings.Correction_Add_Frames.String)];
        end
        %%% Correlation Type (Arbitrary region == 3)
        InfoAll(i).Type = h.Mia_Image.Settings.ROI_FramesUse.String{h.Mia_Image.Settings.ROI_FramesUse.Value};
        switch h.Mia_Image.Settings.ROI_FramesUse.Value
            case {1,2} %%% All/Selected frames
                %%% Mean intensity [counts]
                %InfoAll(i).Mean = mean2(double(MIAData.Data{i,2}(:,:,Frames)));
                InfoAll(i).AR = [];
            case 3 %%% Arbitrary region
                %%% Mean intensity of selected pixels [counts]
                Image = double(MIAData.Data{d,2}(:,:,Frames));
                %InfoAll(i).Mean = mean(Image(Use{i}));
                %%% Arbitrary region information
                InfoAll(i).AR.Int_Max(1) = str2double(h.Mia_Image.Settings.ROI_AR_Int_Max(1).String);
                InfoAll(i).AR.Int_Max(2) = str2double(h.Mia_Image.Settings.ROI_AR_Int_Max(2).String);
                InfoAll(i).AR.Int_Min(1) = str2double(h.Mia_Image.Settings.ROI_AR_Int_Min(1).String);
                InfoAll(i).AR.Int_Min(2) = str2double(h.Mia_Image.Settings.ROI_AR_Int_Min(2).String);
                InfoAll(i).AR.Int_Fold_Max = str2double(h.Mia_Image.Settings.ROI_AR_Int_Fold_Max.String);
                InfoAll(i).AR.Int_Fold_Min = str2double(h.Mia_Image.Settings.ROI_AR_Int_Fold_Min.String);
                InfoAll(i).AR.Var_Fold_Max = str2double(h.Mia_Image.Settings.ROI_AR_Var_Fold_Max.String);
                InfoAll(i).AR.Var_Fold_Min = str2double(h.Mia_Image.Settings.ROI_AR_Var_Fold_Min.String);
                InfoAll(i).AR.Var_Sub=str2double(h.Mia_Image.Settings.ROI_AR_Sub2.String);
                InfoAll(i).AR.Var_SubSub=str2double(h.Mia_Image.Settings.ROI_AR_Sub1.String);
        end
        %%% Mean intensity
        InfoAll(i).Counts = MeanInt(i);
        DataAll{i,1} = double(MIAData.STICS{floor(1.5*i)});
        DataAll{i,2} = double(MIAData.STICS_SEM{floor(1.5*i)});
    end
    %% Gets cross correlation data to save
    if Cross == 1
        d1 = h.Mia_Image.Settings.Channel_PIE(1).Value;
        d2 = h.Mia_Image.Settings.Channel_PIE(2).Value;
        %%% File name information
        InfoAll(3).File = MIAData.FileName{1};
        InfoAll(3).Path = UserValues.File.MIAPath;
        %%% ROI and TOI
        InfoAll(3).Frames = Frames;
        From=h.Plots.ROI(1).Position(1:2)+0.5;
        To=From+h.Plots.ROI(1).Position(3:4)-1;
        InfoAll(3).ROI = [From To];
        %%% Pixel [us], Line [ms] and Frametime [s]
        InfoAll(3).Times = [str2double(h.Mia_Image.Settings.Image_Pixel.String) str2double(h.Mia_Image.Settings.Image_Line.String) str2double(h.Mia_Image.Settings.Image_Frame.String)];
        %%% Pixel size
        InfoAll(3).Size = str2double(h.Mia_Image.Settings.Pixel_Size.String);
        %%% Correction information
        InfoAll(3).Correction.SubType = h.Mia_Image.Settings.Correction_Subtract.String{h.Mia_Image.Settings.Correction_Subtract.Value};
        if h.Mia_Image.Settings.Correction_Subtract.Value == 4
            InfoAll(3).Correction.SubROI = [str2double(h.Mia_Image.Settings.Correction_Subtract_Pixel.String) str2double(h.Mia_Image.Settings.Correction_Subtract_Frames.String)];
        end
        InfoAll(3).Correction.AddType = h.Mia_Image.Settings.Correction_Add.String{h.Mia_Image.Settings.Correction_Add.Value};
        if h.Mia_Image.Settings.Correction_Add.Value == 5
            InfoAll(3).Correction.AddROI = [str2double(h.Mia_Image.Settings.Correction_Add_Pixel.String) str2double(h.Mia_Image.Settings.Correction_Add_Frames.String)];
        end
        %%% Correlation Type (Arbitrary region == 3)
        InfoAll(3).Type = h.Mia_Image.Calculations.Cor_Type.String{h.Mia_Image.Calculations.Cor_Type.Value};
        switch h.Mia_Image.Settings.ROI_FramesUse.Value
            case {1,2} %%% All/Selected frames
                %%% Mean intensity [counts]
                InfoAll(3).Mean = (mean2(double(MIAData.Data{d1,2}(:,:,Frames))) + mean2(double(MIAData.Data{d2,2}(:,:,Frames))))/2;
                InfoAll(3).AR = [];
            case 3 %%% Arbitrary region
                %%% Mean intensity of selected pixels [counts]
                Image1 = double(MIAData.Data{d1,2}(:,:,Frames));
                Image2 = double(MIAData.Data{d2,2}(:,:,Frames));
                InfoAll(3).Mean = (mean(Image1(Use{1} & Use{2})) + mean(Image2(Use{1} & Use{2})))/2;
                %%% Arbitrary region information
                InfoAll(3).AR = Save_ARinfo([],1);
        end
        %%% Mean intensity
        InfoAll(3).Counts = sum(MeanInt);
        DataAll{3,1} = double(MIAData.STICS{2});
        DataAll{3,2} = double(MIAData.STICS_SEM{2});
    end
    %% Creates new filename
    %%% Removes file extension
    switch MIAData.Type
        case {1,2}
            FileName = MIAData.FileName{1}{1}(1:end-4);
    end
    %%% Generates filename
    Current_FileName1=fullfile(UserValues.File.MIAPath,'Mia',[FileName '_ACF1.stcor']);
    Current_FileName2=fullfile(UserValues.File.MIAPath,'Mia',[FileName '_ACF2.stcor']);
    Current_FileName3=fullfile(UserValues.File.MIAPath,'Mia',[FileName '_CCF.stcor']);
    %%% Checks, if file already exists and create new filename
    if  exist(Current_FileName1,'file')  || exist(Current_FileName2,'file') || exist(Current_FileName3,'file')
        k=1;
        %%% Adds 1 to filename
        Current_FileName1=[Current_FileName1(1:end-6) '_' num2str(k) '.stcor'];
        Current_FileName2=[Current_FileName2(1:end-6) '_' num2str(k) '.stcor'];
        Current_FileName3=[Current_FileName3(1:end-6) '_' num2str(k) '.stcor'];
        %%% Increases counter, until no file is found
        while exist(Current_FileName1,'file')  || exist(Current_FileName2,'file') || exist(Current_FileName3,'file')
            k=k+1;
            Current_FileName1=[Current_FileName1(1:end-(6+numel(num2str(k-1)))) num2str(k) '.stcor'];
            Current_FileName2=[Current_FileName2(1:end-(6+numel(num2str(k-1)))) num2str(k) '.stcor'];
            Current_FileName3=[Current_FileName3(1:end-(6+numel(num2str(k-1)))) num2str(k) '.stcor'];
        end
    end
    %%% Saves Auto correlations
    for i=Auto
        Info = InfoAll(i); 
        Data = DataAll(i,:); 
        if i==1
            save(Current_FileName1,'Info','Data');
        else
            save(Current_FileName2,'Info','Data');
        end
    end
    %%% Saves Cross correlations
    if Cross
        Info = InfoAll(3); 
        Data = DataAll(3,:);
        save(Current_FileName3,'Info','Data');
    end
end
Update_Plots([],[],6);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Performs rics fit %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function Do_RICS_Fit(~,~,mode,noplot)
%mode = 1: ACF1
%mode = 2: CCF
%mode = 3: ACF2

global MIAData
h = guidata(findobj('Tag','Mia'));

%plot defines whether data has to be plot in Mia or not
if ~isvar('noplot')
    %normal RICS
    noplot = 0;
% else: grid based RICS
% noplot will be 1
end

h.Mia_Progress_Text.String = 'Fitting correlation';
h.Mia_Progress_Axes.Color=[1 0 0];  
drawnow;

for m = mode
    %%% Extracts parameters and data
    NotFixed=find(~cell2mat(h.Mia_ICS.Fit_Table.Data(2:2:end,m)));
    Params=cellfun(@str2double,h.Mia_ICS.Fit_Table.Data(1:2:end,m));
    Fit_Params=Params(NotFixed);
    YData=h.Plots.Cor(m,2).ZData;
    %YData(round(size(YData,1)/2),:) = YData(round(size(YData,1)/2)+1,:);
    Size=str2double(h.Mia_ICS.Size.String);
    if str2double(h.Mia_ICS.Frame.String)==0
        %%% Extracts, what frames to use
        Frames=str2num(h.Mia_ICS.Frames2Use.String);     %#ok<ST2NM>
        %%% Calculate borders
        X(1)=ceil(floor(size(MIAData.Cor{m,1},1)/2)-Size/2)+1;
        X(2)=ceil(floor(size(MIAData.Cor{m,1},1)/2)+Size/2);
        Y(1)=ceil(floor(size(MIAData.Cor{m,1},2)/2)-Size/2)+1;
        Y(2)=ceil(floor(size(MIAData.Cor{m,1},2)/2)+Size/2);
        %%% calculates SEM
        SEM=std(double(MIAData.Cor{m,1}(X(1):X(2),Y(1):Y(2),Frames)),0,3)/sqrt(numel(Frames));
    else
        SEM=ones(size(YData));
    end
    if any(any(SEM==0))
        SEM=1;
    end

    %%%
    opts=optimset('Display','off');
    %%% Performas fit
    [Fitted_Params,~,~,~]=lsqcurvefit(@Fit_RICS,Fit_Params,{Params,NotFixed,Size,SEM(:)},YData(:)./SEM(:),[],[],opts);
    %%% Updates parameters and table
    Params(NotFixed)=Fitted_Params;
    h.Mia_ICS.Fit_Table.Data(1:2:end,m)=deal(cellfun(@num2str,num2cell(Params),'UniformOutput',false));

    if ~noplot
        %%% Calculates fit function
        Calc_RICS_Fit(m);
        %%% Updates correlation plots
        Update_Plots([],[],2);
    end
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% RICS fit function %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function [OUT] = Fit_RICS(Fit_Params,Data)

Shift=floor(Data{3}/2)+1;
[X,Y]=meshgrid(1:Data{3},1:Data{3});
X=X(:); Y=Y(:);
SEM=Data{4};

P=Data{1};
P(Data{2})=Fit_Params;

OUT= P(5) + 2.^(-3./2)./P(1).... %%% Amplitude
    .*(1+4*P(2)*10^-12*(abs(X-Shift)*P(7)*10^-6+abs(Y-Shift)*P(8)*10^-3)/(P(3)*10^-6)^2).^(-1)... %%% XY Diffusion
    .*(1+4*P(2)*10^-12*(abs(X-Shift)*P(7)*10^-6+abs(Y-Shift)*P(8)*10^-3)/(P(4)*10^-6)^2).^(-0.5)... %%% Z Diffusion
    .*exp(-(P(6)*10^-9)^2*((X-Shift).^2+(Y-Shift).^2)./((P(3)*10^-6)^2+4*P(2)*10^-12*(abs(X-Shift)*P(7)*10^-6+abs(Y-Shift)*P(8)*10^-3))); %%% Scanning
OUT((Shift-1)*(Data{3}+1)+1)=(OUT((Shift-1)*(Data{3}-1))+OUT(Shift*(Data{3}+1)))/2;
OUT=OUT./SEM;
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Calculates fit function without fitting %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%       
function [OUT] = Calc_RICS_Fit(mode)
global MIAData
h = guidata(findobj('Tag','Mia'));
P=cellfun(@str2double,h.Mia_ICS.Fit_Table.Data(1:2:end,mode));
Size=str2double(h.Mia_ICS.Size.String);

Shift=floor(Size/2)+1;
[X,Y]=meshgrid(1:Size,1:Size);
X=X(:); Y=Y(:);

OUT= P(5) + 2.^(-3./2)./P(1).... %%% Amplitude
    .*(1+4*P(2)*10^-12*(abs(X-Shift)*P(7)*10^-6+abs(Y-Shift)*P(8)*10^-3)/(P(3)*10^-6)^2).^(-1)... %%% XY Diffusion
    .*(1+4*P(2)*10^-12*(abs(X-Shift)*P(7)*10^-6+abs(Y-Shift)*P(8)*10^-3)/(P(4)*10^-6)^2).^(-0.5)... %%% Z Diffusion
    .*exp(-(P(6)*10^-9)^2*((X-Shift).^2+(Y-Shift).^2)./((P(3)*10^-6)^2+4*P(2)*10^-12*(abs(X-Shift)*P(7)*10^-6+abs(Y-Shift)*P(8)*10^-3))); %%% Scanning
OUT((Shift-1)*(Size+1)+1)=(OUT((Shift-1)*(Size-1))+OUT(Shift*(Size+1)))/2;
MIAData.Cor{mode,2} = reshape(OUT,[Size,Size]);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Peforms tics fit %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function Do_TICS_Fit(~,~)
h = guidata(findobj('Tag','Mia'));
global MIAData
h.Mia_Progress_Text.String = 'Fitting correlation';
h.Mia_Progress_Axes.Color=[1 0 0];  
drawnow;


for i=1:3
    if size(MIAData.TICS.Data,2)>=i && ~isempty(MIAData.TICS.Data{i})
        %%% Extracts parameters and data
        NotFixed=find(~cell2mat(h.Mia_TICS.Fit_Table.Data(2:2:end,i)));
        Params=cellfun(@str2double,h.Mia_TICS.Fit_Table.Data(1:2:end,i));
        Fit_Params=Params(NotFixed);
        XData = h.Plots.TICS(i,1).XData;
        YData = h.Plots.TICS(i,1).YData;
        EData = h.Plots.TICS(i,1).UData;
        
        %%%
        opts=optimset('Display','off');
        %%% Performas fit
        [Fitted_Params,~,~,~] = lsqcurvefit(@Fit_TICS,Fit_Params,{Params,NotFixed,XData,EData},double(YData./EData),[],[],opts);
        %%% Updates parameters and table
        Params(NotFixed) = Fitted_Params;
        h.Mia_TICS.Fit_Table.Data(1:2:end,i) = deal(cellfun(@num2str,num2cell(Params),'UniformOutput',false));
        %%% Calculates fit function
        Calc_TICS_Fit([],[],i);
        %%% Updates correlation plots
    end
end
Update_Plots([],[],5);
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% TICS fit function %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function [OUT] = Fit_TICS(Fit_Params,Data)
P = Data{1};
P(Data{2}) = Fit_Params;
X = Data{3};
SEM = Data{4};

%%%-----------------------------FIT FUNCTION----------------------------%%%
OUT=(1/sqrt(8))*1/P(1).*(1./(1+4*(P(2)*1e-12).*X/(P(3)*1e-6)^2)).*(1./sqrt(1+4*(P(2)*1e-12).*X/(P(4)*1e-6)^2))+P(5);
OUT=OUT./SEM;
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Calculates fit function without fitting %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function [OUT] = Calc_TICS_Fit(~,~,mode)
h = guidata(findobj('Tag','Mia'));
global MIAData

for i=mode
    if size(MIAData.TICS.Data,2)>=i && ~isempty(MIAData.TICS.Data{i})
        P = cellfun(@str2double,h.Mia_TICS.Fit_Table.Data(1:2:end,i));
        X = logspace(log10(h.Plots.TICS(i,1).XData(1)),log10(h.Plots.TICS(i,1).XData(end)),1000);
        
        OUT=real((1/sqrt(8))*1/P(1).*(1./(1+4*(P(2)*1e-12).*X/(P(3)*1e-6)^2)).*(1./sqrt(1+4*(P(2)*1e-12).*X/(P(4)*1e-6)^2))+P(5));
        
        h.Plots.TICS(i,2).XData = X;
        h.Plots.TICS(i,2).YData = OUT;
        
        h.Plots.TICS(i,1).Visible = 'on';
        h.Plots.TICS(i,2).Visible = 'on';
    else
        h.Plots.TICS(i,1).Visible = 'off';
        h.Plots.TICS(i,2).Visible = 'off';
    end
end
drawnow

function Save_TICS(~,~, data)
global UserValues MIAData
h = guidata(findobj('Tag','Mia'));

if ~h.Mia_Image.Calculations.Save_Name.Value
    switch MIAData.Type
        case {1,1.5, 2}
            FileName=MIAData.FileName{1}{1}(1:end-4);
    end
    [FileName,PathName] = uiputfile([FileName '.mcor'], 'Save correlation as', [UserValues.File.MIAPath,'Mia']);
    FileName = FileName(1:end-5);
end

for i = 1:3
    if any(MIAData.TICS.Auto==i) || (i==3 && MIAData.TICS.Cross)
        if ~isdir(fullfile(UserValues.File.MIAPath,'Mia'))
            mkdir(fullfile(UserValues.File.MIAPath,'Mia'))
        end
        %%% Generates filename
        if h.Mia_Image.Calculations.Save_Name.Value
            if i == 1 %ACF1
                FileName = MIAData.FileName{i}{1}(1:end-4);
            elseif i == 2 %ACF2
                FileName = MIAData.FileName{1}{1}(1:end-4);
            elseif i == 3 %CCF
                FileName = MIAData.FileName{2}{1}(1:end-4);
            end
            if i==1
                Current_FileName = fullfile(UserValues.File.MIAPath,'Mia',[FileName '_ACF1.mcor']);
            elseif i==2
                Current_FileName = fullfile(UserValues.File.MIAPath,'Mia',[FileName '_ACF2.mcor']);
            else
                Current_FileName = fullfile(UserValues.File.MIAPath,'Mia',[FileName '_CCF.mcor']);
            end
        else
            if i==1
                Current_FileName=fullfile(PathName,[FileName '_ACF1.mcor']);
            elseif i==2
                Current_FileName=fullfile(PathName,[FileName '_ACF2.mcor']);
            else
                Current_FileName=fullfile(PathName,[FileName '_CCF.mcor']);
            end
        end
        
        k=0;
        %%% Checks, if file already exists
        if  exist(Current_FileName,'file')
            k=1;
            %%% Adds 1 to filename
            Current_FileName=[Current_FileName(1:end-5) '_' num2str(k) '.mcor'];
            %%% Increases counter, until no file is fount
            while exist(Current_FileName,'file')
                k=k+1;
                Current_FileName=[Current_FileName(1:end-(5+numel(num2str(k-1)))) num2str(k) '.mcor'];
            end
        end
        Header = 'TICS correlation file'; 
        Counts = data{i}.Counts;
        Valid = data{i}.Valid;
        Cor_Times = data{i}.Cor_Times;
        Cor_Average = data{i}.Cor_Average;
        Cor_SEM = data{i}.Cor_SEM;
        Cor_Array = data{i}.Cor_Array;
        save(Current_FileName,'Header','Counts','Valid','Cor_Times','Cor_Average','Cor_SEM','Cor_Array');
    end
end
%% Get Info structure
Info = struct;
%%% Pixel [us], Line [ms] and Frametime [s]
Info.Times = [str2double(h.Mia_Image.Settings.Image_Pixel.String) str2double(h.Mia_Image.Settings.Image_Line.String) str2double(h.Mia_Image.Settings.Image_Frame.String)];
%%% Pixel size
Info.Size = str2double(h.Mia_Image.Settings.Pixel_Size.String);
%%% ROI and TOI
Info.Frames = MIAData.TICS.Frames;
From = h.Plots.ROI(1).Position(1:2)+0.5;
To = From+h.Plots.ROI(1).Position(3:4)-1;
Info.ROI = [From To];
%%% Countrate
if MIAData.TICS.Cross % all three CFs
    Info.Counts = data{3}.Counts;
elseif MIAData.TICS.Auto == 1 %only ACF1
    Info.Counts = data{1}.Counts;
elseif MIAData.TICS.Auto == 2 %only ACF2
    Info.Counts = data{2}.Counts;
end
%%% Correction information
Info.Correction.SubType = h.Mia_Image.Settings.Correction_Subtract.String{h.Mia_Image.Settings.Correction_Subtract.Value};
if h.Mia_Image.Settings.Correction_Subtract.Value == 4
    Info.Correction.SubROI = [str2double(h.Mia_Image.Settings.Correction_Subtract_Pixel.String) str2double(h.Mia_Image.Settings.Correction_Subtract_Frames.String)];
end
Info.Correction.AddType = h.Mia_Image.Settings.Correction_Add.String{h.Mia_Image.Settings.Correction_Add.Value};
if h.Mia_Image.Settings.Correction_Add.Value == 5
    Info.Correction.AddROI = [str2double(h.Mia_Image.Settings.Correction_Add_Pixel.String) str2double(h.Mia_Image.Settings.Correction_Add_Frames.String)];
end
%%% Correlation Type (Arbitrary region == 3)
Info.Type = h.Mia_Image.Settings.ROI_FramesUse.String{h.Mia_Image.Settings.ROI_FramesUse.Value};
switch h.Mia_Image.Settings.ROI_FramesUse.Value
    case {1,2} %%% All/Selected frames
        %%% Mean intensity [counts]
        for i = 1:2
            if any(MIAData.TICS.Auto==i)
                Info.Mean(i) = mean2(double(MIAData.Data{i,2}(:,:,Info.Frames)));
            end
        end
        Info.AR = [];
    case 3 %%% Arbitrary region
        %%% Mean intensity of selected pixels [counts]
        for i = 1:2
            if any(MIAData.TICS.Auto==i)
                Image = double(MIAData.Data{i,2}(:,:,Info.Frames));
                Info.Mean(i) = mean(Image(MIAData.TICS.Use{i}));
            end
        end
        Info.AR = Save_ARinfo([],1);
end
%% Saves info file
%FileName = MIAData.FileName{1}{1}(1:end-4);
Current_FileName=fullfile(UserValues.File.MIAPath,'Mia',[FileName '_Info.txt']);
k=0;
%%% Checks, if file already exists
if exist(Current_FileName,'file')
    %%% Increases counter, until no file is fount
    while exist(Current_FileName,'file')
        k=k+1;
        Current_FileName=fullfile(UserValues.File.MIAPath,'Mia',[FileName '_Info_' num2str(k) '.txt']);
    end
end

FID = fopen(Current_FileName,'w');
fprintf(FID,'%s\n','Image Correlation info file');
%%% Pixel\Line\Frame times
fprintf(FID,'%s\t%f\n', 'Pixel time [us]:',Info.Times(1));
fprintf(FID,'%s\t%f\n', 'Line  time [ms]:',Info.Times(2));
fprintf(FID,'%s\t%f\n', 'Frame time [s] :',Info.Times(3));
%%% Pixel size
fprintf(FID,'%s\t%f\n', 'Pixel size [nm]:',Info.Size);
%%% Region of interest
fprintf(FID,'%s\t%u,%u,%u,%u\t%s\n', 'Region used [px]:',Info.ROI, 'X Start, Y Start, X Stop, Y Stop');
%%% Counts per pixel
fprintf(FID,'%s\t%f\t%f\n', 'Mean counts per pixel:',Info.Counts(1),Info.Counts(2));
%%% Frames used
fprintf(FID,['%s\t',repmat('%u\t',[1 numel(Info.Frames)]) '\n'],'Frames Used:',Info.Frames);
%%% Subtraction used
switch h.Mia_Image.Settings.Correction_Subtract.Value
    case 1
        fprintf(FID,'%s\n','Nothing subtracted');
    case 2
        fprintf(FID,'%s\n','Frame mean subtracted');
    case 3
        fprintf(FID,'%s\n','Pixel mean subtracted');
    case 4
        fprintf(FID,'%s\t%u%s\t%u%s\n','Moving average subtracted:', Info.Correction.SubROI(1), ' Pixel', Info.Correction.SubROI(2),' Frames');
end
%%% Addition used
switch h.Mia_Image.Settings.Correction_Subtract.Value
    case 1
        fprintf(FID,'%s\n','Nothing added');
    case 2
        fprintf(FID,'%s\n','Total mean added');
    case 3
        fprintf(FID,'%s\n','Frame mean added');
    case 4
        fprintf(FID,'%s\n','Pixel mean added');
    case 5
        fprintf(FID,'%s\t%u%s%u%s\n','Moving average added:', Info.Correction.SubROI(1), ' Pixel', Info.Correction.SubROI(2),' Frames');
end
%%% Arbitrary region
if h.Mia_Image.Settings.ROI_FramesUse.Value==3
    Save_ARinfo(FID, 2);    
end
fclose(FID);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Peforms Gaussian fit %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function Do_Gaussian(~,~)
h = guidata(findobj('Tag','Mia'));
global MIAData
h.Mia_Progress_Text.String = 'Fitting Gaussian';
h.Mia_Progress_Axes.Color=[1 0 0];  
drawnow;

for i=1:3

    if size(MIAData.STICS,2)>=i && ~isempty(MIAData.STICS{i})
        MIAData.iMSD{i,1} = zeros(size(MIAData.STICS{i},3),1);
        MIAData.iMSD{i,2} = zeros(size(MIAData.STICS{i},3),2);
        for j=1:size(MIAData.STICS{i},3)
            Fit_Params = [1,5,0];
            
            Size = round(str2double(h.Mia_STICS.Size.String));
            if isempty(Size) || Size<1
                Size = 31;
                h.Mia_STICS.Size.String = '31';
            elseif Size > size(MIAData.STICS{i},1) || Size > size(MIAData.STICS{i},2)
                Size = min([size(MIAData.STICS{i},2), size(MIAData.STICS{i},2)]);
                h.Mia_STICS.Size.String = num2str(Size);
            end
            X(1)=ceil(floor(size(MIAData.STICS{i},1)/2)-Size/2)+1;
            X(2)=ceil(floor(size(MIAData.STICS{i},1)/2)+Size/2);
            Y(1)=ceil(floor(size(MIAData.STICS{i},2)/2)-Size/2)+1;
            Y(2)=ceil(floor(size(MIAData.STICS{i},2)/2)+Size/2);
            YData = MIAData.STICS{i}(X(1):X(2),Y(1):Y(2),j);
            %%% Removes noise point at G(0,0,0)
            if j==1
               YData(floor(Size/2)+1,floor(Size/2)+1) = 0;
            end
            EData = MIAData.STICS_SEM{i}(X(1):X(2),Y(1):Y(2),j);
        
        opts=optimset('Display','off');
        %%% Performas fit        
        [Fitted_Params,~,weighted_residuals,~,~,~,jacobian] = lsqcurvefit(@Fit_Gaussian,Fit_Params,{Size,double(EData),j},double(YData(:)./EData(:)),[],[],opts);

        MIAData.iMSD{i,1}(j,1) = Fitted_Params(2);
        Confidence = nlparci(Fitted_Params,weighted_residuals,'jacobian',jacobian);
        MIAData.iMSD{i,2}(j,:) = Confidence(2,:);
        end
    end
end
Update_Plots([],[],6);
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Gaussian Fit function %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 
function [OUT] = Fit_Gaussian(Fit_Params,Data)
Shift = floor(Data{1}/2)+1;
[X,Y]=meshgrid(1:Data{1},1:Data{1});
X=X(:); Y=Y(:);
SEM = Data{2};

A = Fit_Params(1);
Omega = Fit_Params(2);
I0 = Fit_Params(3);

OUT = I0+A.*exp(-((X-Shift).^2+(Y-Shift).^2)./(Omega^2));
OUT((Shift-1)*(Data{1}+1)+1) = (OUT((Shift-1)*(Data{1}-1))+OUT(Shift*(Data{1}+1)))/2;
OUT = OUT./SEM(:);
%%% Removes noise point at G(0,0,0)
if Data{3}==1
   OUT(sub2ind(size(Data{2}),Shift,Shift)) = 0;
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Peforms iMSD fit %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function Do_iMSD(~,~)
h = guidata(findobj('Tag','Mia'));
global MIAData
h.Mia_Progress_Text.String = 'Fitting iMSD';
h.Mia_Progress_Axes.Color=[1 0 0];  
drawnow;

for i=1:3
    if size(MIAData.iMSD,1)>=i && ~isempty(MIAData.iMSD{i,1}) && any(MIAData.iMSD{i,1}~=0)
        %%% Extracts parameters and data
        NotFixed = ~cell2mat(h.Mia_STICS.Fit_Table.Data(2:2:end,i));
        Params = cellfun(@str2double,h.Mia_STICS.Fit_Table.Data(1:2:end,i));
        Fit_Params = Params(NotFixed);
        
        XData = h.Plots.STICS(i,1).XData;
        YData = h.Plots.STICS(i,1).YData;
        EData = (h.Plots.STICS(i,1).UData + h.Plots.STICS(i,1).LData)/2; 
        
        opts=optimset('Display','off');
        %%% Performas fit
        [Fitted_Params,~,~,~,~,~,~] = lsqcurvefit(@Fit_iMSD,Fit_Params,{Params,NotFixed,XData,EData},double(YData./EData),[],[],opts);
        %%% Updates parameters and table
        Params(NotFixed) = Fitted_Params;
        h.Mia_STICS.Fit_Table.Data(1:2:end,i) = deal(cellfun(@num2str,num2cell(Params),'UniformOutput',false));
    end
end
Update_Plots([],[],6);
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Gaussian Fit function %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 
function [OUT] = Fit_iMSD(Fit_Params,Data)
P = Data{1};
P(Data{2}) = Fit_Params;
X = Data{3};
SEM = Data{4};

%%%-----------------------------FIT FUNCTION----------------------------%%%  
OUT=P(1)^2+4*P(2)*(X.^P(3));
OUT=OUT./SEM;


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Funtion to calculate N&B %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function Do_NB(~,~)
h = guidata(findobj('Tag','Mia'));
global MIAData

h.Mia_Progress_Text.String = 'Calculating N&B';
h.Mia_Progress_Axes.Color=[1 0 0];  
drawnow;

MIAData.NB=[];

%% Determines, for which channels to calculate
if h.Mia_Image.Calculations.NB_Type.Value==3
    Auto=1:2; Cross=1;
    channel=1:3;
else
    Auto=h.Mia_Image.Calculations.NB_Type.Value; Cross=0;
    channel=floor(Auto*1.5);
end

%% Calculates N&B
for i=Auto
    d = h.Mia_Image.Settings.Channel_PIE(i).Value; %(the datachannel)
    %%% Apply Dead time correction
    MIAData.NB.DTCorr_Img{floor(i*1.5)} = (double(flipud(MIAData.Data{d,2})))./(1-double(flipud(MIAData.Data{d,2})).*(str2double(h.Mia_Image.Calculations.NB_Detector_Deadtime.String)/(str2double(h.Mia_Image.Settings.Image_Pixel.String)*1000)));
    %%% Limit for PCH
    MaxPhotons=ceil(max(max(max(MIAData.NB.DTCorr_Img{floor(i*1.5)}))));
    %%% Calculaces PCH, mean intensity, standard deviation for each pixel
    MIAData.NB.PCH{floor(i*1.5)}=histc(MIAData.NB.DTCorr_Img{floor(i*1.5)}(:),0:MaxPhotons); 
    MIAData.NB.Int{floor(i*1.5)}=nanmean(MIAData.NB.DTCorr_Img{floor(i*1.5)},3);
    MIAData.NB.Std{floor(i*1.5)}=nanstd(MIAData.NB.DTCorr_Img{floor(i*1.5)},0,3);
    %%% Applies spatial filter to intensity and standard deviation
    if str2double(h.Mia_Image.Calculations.NB_Average_Radius.String)<=1
        h.Mia_Image.Calculations.NB_Average.Value=1;
    end
    %%% Determinesspatial filter
    switch h.Mia_Image.Calculations.NB_Average.Value
        case 1 %%% Do nothing
            Filter = fspecial('average',1);
        case 2 %%% Moving average
            Filter = fspecial('average',round(str2double(h.Mia_Image.Calculations.NB_Average_Radius.String)));
        case 3 %%% Disc average
            Filter = fspecial('disk',str2double(h.Mia_Image.Calculations.NB_Average_Radius.String)-1);
        case 4 %%% Gaussian average
            Filter = fspecial('gaussian',2*str2double(h.Mia_Image.Calculations.NB_Average_Radius.String),str2double(h.Mia_Image.Calculations.NB_Average_Radius.String)/2);
    end
    %%% Applies filter
    MIAData.NB.Int{floor(i*1.5)}=imfilter(MIAData.NB.Int{floor(i*1.5)},Filter,'symmetric');
    MIAData.NB.Std{floor(i*1.5)}=imfilter(MIAData.NB.Std{floor(i*1.5)},Filter,'symmetric');
    %%% Calculates number and brightness for each pixel
    %MIAData.NB.Num{floor(i*1.5)}=MIAData.NB.Int{floor(i*1.5)}.^2./(MIAData.NB.Std{floor(i*1.5)}.^2);
    %MIAData.NB.Eps{floor(i*1.5)}=MIAData.NB.Std{floor(i*1.5)}.^2./MIAData.NB.Int{floor(i*1.5)};
    MIAData.NB.Num{floor(i*1.5)}=MIAData.NB.Int{floor(i*1.5)}.^2./(MIAData.NB.Std{floor(i*1.5)}.^2-MIAData.NB.Int{floor(i*1.5)})/sqrt(8);
    MIAData.NB.Eps{floor(i*1.5)}=(MIAData.NB.Std{floor(i*1.5)}.^2-MIAData.NB.Int{floor(i*1.5)})./MIAData.NB.Int{floor(i*1.5)}*sqrt(8);
    if h.Mia_Image.Calculations.NB_Median.Value
        MIAData.NB.Num{floor(i*1.5)}=medfilt2(MIAData.NB.Num{floor(i*1.5)},[3 3]);
        MIAData.NB.Eps{floor(i*1.5)}=medfilt2(MIAData.NB.Eps{floor(i*1.5)},[3 3]);
    end
end

%% Calculates crossN&B
if Cross
    d1 = h.Mia_Image.Settings.Channel_PIE(1).Value; %(the datachannel) 
    d2 = h.Mia_Image.Settings.Channel_PIE(2).Value; %(the datachannel) 
    MaxPhotons=ceil(max(max(max(MIAData.Data{d1,2}+MIAData.Data{d2,2}))));
    %%% Calculaces PCH, mean intensity, standard deviation for each pixel
    MIAData.NB.PCH{2}=histc(MIAData.Data{d1,2}(:)+MIAData.Data{d2,2}(:),0:MaxPhotons); %%% PCH of sum of channels
    MIAData.NB.Int{2}=sqrt(MIAData.NB.Int{1}.*MIAData.NB.Int{3}); %%% sqrt of product to be able to calculate N&B
    MIAData.NB.Std{2}=sqrt(mean((MIAData.Data{d1,2}-repmat(mean(MIAData.Data{d1,2},3),[1 1,size(MIAData.Data{d1,2},3)]))...
                                 .*(MIAData.Data{d2,2}-repmat(mean(MIAData.Data{d2,2},3),[1 1,size(MIAData.Data{d2,2},3)])),3)); %%% sqrt of co-variance
    %%% Applies spatial filter to intensity and standard deviation
    if str2double(h.Mia_Image.Calculations.NB_Average_Radius.String)<=1
        h.Mia_Image.Calculations.NB_Average.Value=1;
    end
    %%% Determinesspatial filter
    switch h.Mia_Image.Calculations.NB_Average.Value
        case 1 %%% Do nothing
            Filter = fspecial('average',1);
        case 2 %%% Moving average
            Filter = fspecial('average',round(str2double(h.Mia_Image.Calculations.NB_Average_Radius.String)));
        case 3 %%% Disc average
            Filter = fspecial('disk',str2double(h.Mia_Image.Calculations.NB_Average_Radius.String)-1);
        case 4 %%% Gaussian average
            Filter = fspecial('gaussian',2*str2double(h.Mia_Image.Calculations.NB_Average_Radius.String),str2double(h.Mia_Image.Calculations.NB_Average_Radius.String)/2);
    end
    %%% Applies filter
    MIAData.NB.Int{floor(2)}=imfilter(MIAData.NB.Int{floor(2)},Filter,'symmetric');
    MIAData.NB.Std{floor(2)}=imfilter(MIAData.NB.Std{floor(2)},Filter,'symmetric');
    %%% Calculates number and brightness for each pixel
    MIAData.NB.Num{2}=real(MIAData.NB.Int{2}.^2./(MIAData.NB.Std{2}.^2));
    MIAData.NB.Eps{2}=real((MIAData.NB.Std{2}.^2)./MIAData.NB.Int{2}); 
end

i=channel(1);

%% Uses first active channel to initiate standard threshold parameters
%%% Uses actual min/max of mean+-3sigma for intensity
h.Mia_NB.Image.Hist(1,1).String=num2str(max([min(MIAData.NB.Int{i}(:)), mean2(MIAData.NB.Int{i})-3*mean2(MIAData.NB.Std{i})])/str2double(h.Mia_NB.Image.Pixel.String)*10^3);
h.Mia_NB.Image.Hist(2,1).String=num2str(min([max(MIAData.NB.Int{i}(:)), mean2(MIAData.NB.Int{i})+3*mean2(MIAData.NB.Std{i})])/str2double(h.Mia_NB.Image.Pixel.String)*10^3);
h.Mia_NB.Image.Hist(3,1).String='50';

%%%  Removes 5% of top and bottom values to remove outliers 
NoOutliers=sort(MIAData.NB.Num{i}(:));
NoOutliers=NoOutliers(~isnan(NoOutliers));
NoOutliers=NoOutliers(round(0.05*numel(NoOutliers)):round(0.95*numel(NoOutliers)));
%%% mena+-3sigma for number
h.Mia_NB.Image.Hist(1,2).String=num2str(mean(NoOutliers)-3*std(NoOutliers));
h.Mia_NB.Image.Hist(2,2).String=num2str(mean(NoOutliers)+3*std(NoOutliers));
h.Mia_NB.Image.Hist(3,2).String='50';

%%%  Removes 5% of top and bottom values to remove outliers 
NoOutliers=sort(MIAData.NB.Eps{i}(:));
NoOutliers=NoOutliers(~isnan(NoOutliers));
NoOutliers=NoOutliers(round(0.05*numel(NoOutliers)):round(0.95*numel(NoOutliers)));
%%% mena+-3sigma for brightness
h.Mia_NB.Image.Hist(1,3).String=num2str((mean(NoOutliers)-3*std(NoOutliers))/str2double(h.Mia_NB.Image.Pixel.String)*10^3);
h.Mia_NB.Image.Hist(2,3).String=num2str((mean(NoOutliers)+3*std(NoOutliers))/str2double(h.Mia_NB.Image.Pixel.String)*10^3);
h.Mia_NB.Image.Hist(3,3).String='50';

%%% Updates N&B plots
Update_Plots([],[],3);
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Changes 2D histogram background color %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function NB_2DHist_BG(~,~)
h = guidata(findobj('Tag','Mia'));
h.Mia_NB.Hist2D(3).BackgroundColor=1-h.Mia_NB.Hist2D(3).BackgroundColor;
%h.Mia_NB.Hist2D(3).ForegroundColor=1-h.Mia_NB.Hist2D(3).ForegroundColor;
Update_Plots([],[],3)

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Funtion to calculate FRAP %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function Do_FRAP(~,~)
h = guidata(findobj('Tag','Mia'));
global MIAData UserValues

%% Determine channel(s) for analysis
numChannels = size(MIAData.Data, 1);
channel = 1:numChannels == h.Mia_Image.Calculations.FRAP_Channel.Value...
    | numChannels < h.Mia_Image.Calculations.FRAP_Channel.Value;
ch = find(channel);

%% Calculates FRAP
for i = ch
    d = h.Mia_Image.Settings.Channel_PIE(i).Value; %(the datachannel that is currently displayed)
    %% Initializes variable
    MIAData.FRAP.Intensity{i} = NaN(1, size(MIAData.Data{d,2}, 3));
    %% Generates binary mask for bleached spot
    roiType = {'FRAP_Circle', 'FRAP_Rectangle','FRAP_Freehand'}; % the order must match entries in h.Mia_Image.Calculations.FRAP_ROI_Type
    roi = findobj(h.Mia_Image.Axes(i, 2),...
        'Tag', roiType{h.Mia_Image.Calculations.FRAP_ROI_Type.Value},...
        'Visible', 'on');
    if ~isempty(roi)
        MIAData.FRAP.Mask{i} = createMask(roi);
    else
        errordlg('No valid ROI found.', 'MIA')
        return
    end
    
    %% calculate intensity and time values in ROI
    useFrame = find(MIAData.Use(d,:));
    for f = useFrame
        frame = MIAData.Data{d,2}(:,:,f);
        MIAData.FRAP.Intensity{i}(f) = mean(frame(MIAData.FRAP.Mask{i}));
    end
    MIAData.FRAP.Time{i} = (1:size(MIAData.Data{d,2},3))*str2double(h.Mia_Image.Calculations.FRAP_Frame_Interval.String);
    MIAData.FRAP.Time{i} = MIAData.FRAP.Time{i} - MIAData.FRAP.Time{i}(str2double(h.Mia_Image.Calculations.FRAP_FirstRecoveryFrame.String));
    
    %% Normalization
    prebleachStart = str2double(h.Mia_Image.Calculations.FRAP_Prebleach_Start.String);
    prebleachEnd = str2double(h.Mia_Image.Calculations.FRAP_Prebleach_End.String);
    if (prebleachStart > 0 && prebleachEnd > 0) && (prebleachStart <= prebleachEnd) %% check validity of range
        MIAData.FRAP.Intensity{i} = MIAData.FRAP.Intensity{i} / mean(MIAData.FRAP.Intensity{i}(prebleachStart:prebleachEnd));
    end
    %% Fitting
    switch h.Mia_Image.Calculations.FRAP_Fit_Model.Value
        %%% 1 comp saturating exponential
        case 2 
            %%% define fit model and options
            model = fittype('(Amax-A0)*(1-exp(-k*t))+A0',...
                'independent', 't',...
                'coefficients', {'Amax', 'A0', 'k'});
            fitStart = str2double(h.Mia_Image.Calculations.FRAP_FirstRecoveryFrame.String);
            guess_Amax = MIAData.FRAP.Intensity{i}(end);
            guess_A0 = MIAData.FRAP.Intensity{i}(fitStart);
            guess_k = 0.1;
            options = fitoptions(model);
            options.StartPoint = [guess_Amax guess_A0 guess_k];
            options.Lower = [0 0 0];
            options.Upper = [Inf Inf Inf];
            
            %%% fit data
            [MIAData.FRAP.Fit{i}, MIAData.FRAP.Fitgof{i}] =...
                fit(MIAData.FRAP.Time{i}(fitStart:end)', MIAData.FRAP.Intensity{i}(fitStart:end)', model, options);
        %%% 2 comp saturating exponential
        case 3 
            %%% define fit model and options
            model = fittype('(Amax1-A0)*(1-exp(-k1*t))+(Amax2-A0)*(1-exp(-k2*t))+A0',...
                'independent', 't',...
                'coefficients', {'A0', 'Amax1', 'k1', 'Amax2', 'k2'});
            fitStart = str2double(h.Mia_Image.Calculations.FRAP_FirstRecoveryFrame.String);
            options = fitoptions(model);
            options.StartPoint = cell2mat(h.Mia_Image.Calculations.FRAP_FitOptions.Data(:,2));
            options.Lower = cell2mat(h.Mia_Image.Calculations.FRAP_FitOptions.Data(:,3));
            options.Upper = cell2mat(h.Mia_Image.Calculations.FRAP_FitOptions.Data(:,4));
            Fixed = cell2mat(h.Mia_Image.Calculations.FRAP_FitOptions.Data(:,5));
            options.Lower(Fixed) = options.StartPoint(Fixed);
            options.Upper(Fixed) = options.StartPoint(Fixed);
            
            %%% fit data
            [MIAData.FRAP.Fit{i}, MIAData.FRAP.Fitgof{i}] =...
                fit(MIAData.FRAP.Time{i}(fitStart:end)', MIAData.FRAP.Intensity{i}(fitStart:end)', model, options);
        
        %%% 2-phase exponential decay
        case 4
            %%% define fit model and options
            model = fittype('A1*exp(-k1*t)+A2*exp(-k2*t)+A0',...
                'independent', 't',...
                'coefficients', {'A0', 'A1', 'k1', 'A2', 'k2'});
            fitStart = str2double(h.Mia_Image.Calculations.FRAP_FirstRecoveryFrame.String);
            options = fitoptions(model);
            options.StartPoint = cell2mat(h.Mia_Image.Calculations.FRAP_FitOptions.Data(:,2));
            options.Lower = cell2mat(h.Mia_Image.Calculations.FRAP_FitOptions.Data(:,3));
            options.Upper = cell2mat(h.Mia_Image.Calculations.FRAP_FitOptions.Data(:,4));
            Fixed = cell2mat(h.Mia_Image.Calculations.FRAP_FitOptions.Data(:,5));
            options.Lower(Fixed) = options.StartPoint(Fixed);
            options.Upper(Fixed) = options.StartPoint(Fixed);
            
            %%% fit data
            [MIAData.FRAP.Fit{i}, MIAData.FRAP.Fitgof{i}] =...
                fit(MIAData.FRAP.Time{i}(fitStart:end)', MIAData.FRAP.Intensity{i}(fitStart:end)', model, options);
        %%% cFRAP based on Xiong, et al 2016; https://doi.org/10.1038/ncomms12982
        case 5
            %%% check that a rectangular roi is used
            if h.Mia_Image.Calculations.FRAP_ROI_Type.Value ~= 2
                errordlg('cFRAP only works with a rectangular ROI!', 'MIA')
                return
            end
            %%% Image parameters
            Im = double(MIAData.Data{d,2});
            Im2 = Im; % stores original image for variance calculation;
            Nimages = size(Im, 3);
            pixsize = str2double(h.Mia_Image.Settings.Pixel_Size.String)/1000;  %pixel size in m
            
            %%% ROI parameters
            topleftrect = roi.Position(1:2);
            sizerect = roi.Position(3:4); % this is the position and size from user input in rFRAP_MEM_GUI
            topleftroi = topleftrect - 20; % offset from topleftrect(i.e. user input) as implemented in rFRAP_MEM_GUI
            sizeroi = sizerect + 40;  % offset from topleftrect(i.e. user input) as implemented in rFRAP_MEM_GUI
            bgroi = MIAData.AR{d,1} & repmat(MIAData.MS{d}, 1, 1, Nimages);% use arbitrary region from Mia (bg is the selected region)
            
            %%% Other Parameters from user input
            Nprebleach = prebleachEnd; %assumes prebleach starts at frame 1;
            Nbleach = str2double(h.Mia_Image.Calculations.FRAP_FirstRecoveryFrame.String) - prebleachEnd -1;
            timestep = str2double(h.Mia_Image.Calculations.FRAP_Frame_Interval.String);
            t = (1:(size(MIAData.Data{d,2},3) - Nprebleach - Nbleach))*timestep;
            tplot = [0 t];

            %%% read user input from cFRAP Options table
            cFRAPOptions = h.Mia_Image.Calculations.cFRAP_Options.Data;
            D = cFRAPOptions{1,2}; % Diffusion coefficient D (micron^2/s):
            K0 = cFRAPOptions{2,2};% Bleaching parameter K0
            k = cFRAPOptions{3,2}; % Mobile fraction k
            r2av = cFRAPOptions{4,2}; % Average squared resolution r2av (micron^2)
            
            ND = cFRAPOptions{5,2}; % Number of discretization of D-space:
            ND_rang = str2num(cFRAPOptions{6,2}); % D-space range [Dmin Dmax]
            NC= cFRAPOptions{7,2}; % Number of rings

            % fluorescence intensity variance as a function of average fluorescence intensity
            % a and b are constant parameters and they can be determined by a series of images
            % with various laser intensities in a homogeneous solution of the fluorescent species
            % with identical instrumental settings
            slope = cFRAPOptions{8,2}; % a
            intercept = cFRAPOptions{9,2}; % b

            [parameters,Ffit,profiles,averageFdata]=rFrapFit(Im,Nimages,bgroi,Nprebleach,Nbleach,topleftrect,sizerect,topleftroi,sizeroi,pixsize,t,tplot,D,K0,k,r2av);
            fprintf('Parameters from rFRAPFit:\n')
            fprintf('  D   K0  K   r2av/N')
            fprintf([parameters '\n'])

            %Background correction:
            for f = 1:Nimages
                Im_frame = Im(:,:,f);
                bg = mean(Im_frame(bgroi(:,:,f)));
                Im(:,:,f)=Im(:,:,f)/bg; %Normalisation wrt background regions.
            end

            %Normalization of the images:
            Imprebleachmean = mean(Im(:,:,1:Nprebleach),3); %Mean of pre-bleach images pixel by pixel.
            Imprebleachmean = medfilt2(Imprebleachmean, [5 5], 'symmetric'); %Median filter over mean pre-bleach image.
            for f = 1:Nimages
                Im(:,:,f)=Im(:,:,f)./Imprebleachmean(:,:); %Normalisation wrt pre-bleach images, pixel by pixel.
            end

            %Coordinates of the pixel centers:
            [x, y] = meshgrid(0.5:1:size(Im,2)-0.5, 0.5:1:size(Im,1)-0.5);
            x = x - topleftrect(1) + 1 - sizerect(1)/2; %Origin placed at center of bleached rectangle.
            y = y - topleftrect(2) + 1 - sizerect(2)/2; %Origin placed at center of bleached rectangle.

            %Extract region of interest:
            Imroi = Im(topleftroi(2):topleftroi(2)+sizeroi(2)-1, topleftroi(1):topleftroi(1)+sizeroi(1)-1,:);
            xroi = x(topleftroi(2):topleftroi(2)+sizeroi(2)-1, topleftroi(1):topleftroi(1)+sizeroi(1)-1,:);
            yroi = y(topleftroi(2):topleftroi(2)+sizeroi(2)-1, topleftroi(1):topleftroi(1)+sizeroi(1)-1,:);

            % %Extract fitted part of bleached rectangle:
            topleftplot=zeros(1,2); sizeplot=zeros(1,2);
            topleftplot(1)=max(topleftroi(1),topleftrect(1)); topleftplot(2)=max(topleftroi(2),topleftrect(2));
            sizeplot(1)=min(topleftroi(1)+sizeroi(1), topleftrect(1)+sizerect(1))-topleftplot(1); sizeplot(2)=min(topleftroi(2)+sizeroi(2), topleftrect(2)+sizerect(2))-topleftplot(2);
            Implot = Im(topleftplot(2):topleftplot(2)+sizeplot(2)-1, topleftplot(1):topleftplot(1)+sizeplot(1)-1,:);
            xplot = x(topleftplot(2):topleftplot(2)+sizeplot(2)-1, topleftplot(1):topleftplot(1)+sizeplot(1)-1,:);
            yplot = y(topleftplot(2):topleftplot(2)+sizeplot(2)-1, topleftplot(1):topleftplot(1)+sizeplot(1)-1,:);

            %Reshape the matrices to a vector:
            xytdata = zeros(3,numel(xroi)*numel(t));
            Fdata = zeros(1,numel(xroi)*numel(t));

            for j=0:numel(t)-1
                xytdata(1,j*numel(xroi)+1:(j+1)*numel(xroi)) = reshape(xroi,1,numel(xroi))*pixsize;
                xytdata(2,j*numel(xroi)+1:(j+1)*numel(xroi)) = reshape(yroi,1,numel(xroi))*pixsize;
                xytdata(3,j*numel(xroi)+1:(j+1)*numel(xroi)) = ones(1,numel(xroi))*t(j+1);
                Fdata(1,j*numel(xroi)+1:(j+1)*numel(xroi)) = reshape(Imroi(:,:,j+1+Nprebleach+Nbleach),1,numel(xroi));
            end
            xytdataplot = zeros(3,numel(xplot)*numel(tplot));
            for j=0:numel(tplot)-1
                xytdataplot(1,j*numel(xplot)+1:(j+1)*numel(xplot)) = reshape(xplot,1,numel(xplot))*pixsize;
                xytdataplot(2,j*numel(xplot)+1:(j+1)*numel(xplot)) = reshape(yplot,1,numel(xplot))*pixsize;
                xytdataplot(3,j*numel(xplot)+1:(j+1)*numel(xplot)) = ones(1,numel(xplot))*tplot(j+1);
            end


            xyt0data(1,1:numel(xroi))=reshape(xroi,1,numel(xroi))*pixsize;
            xyt0data(2,1:numel(xroi))=reshape(yroi,1,numel(xroi))*pixsize;
            xyt0data(3,1:numel(xroi))=zeros(1,numel(xroi));
            parnonfit = [sizerect(1)*pixsize sizerect(2)*pixsize 1];

            %% Start to MEM-rFRAP analysis

            % Intial parameters of D, k,K0 and r2av by the valuse calcaulated from 1
            % component least squrea fitting

            %D=parameters(1);
            %K0=parameters(2);
            %k=parameters(3);
            %r2av=parameters(4);
            parfit = parameters;

            %Intial paramters for MEM analysis such as number of D, Dmin, Dmax

            NumDdis=ND;
            Dmin=ND_rang(1);
            Dmax=ND_rang(2);
            start_N_Curve=NC;
            end_N_Curve=NC;
            set_noise=0.1;

            % Chen: not sure what this does
            counter=1;
            Time_iter(counter,:)=clock; % recording the interation time
            Nstep=NC;
            if Nstep>=1
                Nsteppix=fix(sizeroi(1)/(2*(Nstep+1)));
            else
                Nsteppix=0;
            end
            NL=Nstep+1;
            
            % Calcualte the variance for experimental images
            %slop_intercept=get(handles.expIm,'Userdata'); % check where to get this
            %slop=str2num(slop_intercept{1});
            %intercept=str2num(slop_intercept{2});
            %SI=[slop intercept];
            SI = [slope intercept];
            [sigma2, Numpix]=varianceF(Im2,Nimages,bgroi,Nprebleach,Nbleach,topleftrect,sizerect,topleftroi,sizeroi,pixsize,t,tplot,D,K0,k,r2av,Nstep,Nsteppix,SI);
            
            Pset=[NumDdis Dmin Dmax set_noise Nstep Nsteppix];

            %Try to multicompent least squares fit first to see if chi2 can be reduced to its expectation value
            [chi2, Numdatapoint, alpha, Ddis, Fexp, Fitdata, output] = FRAP_leastsquare(Imprebleachmean,topleftrect,sizerect,topleftroi,sizeroi,Fdata,Ffit,parfit,xytdata,xytdataplot,parnonfit,sigma2,Pset);
            if chi2>Numdatapoint+sqrt(2*Numdatapoint)
                %even straightforward minimisation could not reduce the chi2 to less
                %than its expectation value, so there is no point in continuing the MEM
                %below which can only increase chi2 further.
                tag = get(gcbf,'Tag');
                messagestr = ['Least squares fitting could not reduce chi^2 to less '...
                    'than its expectation value. There is no point continuing the MEM '...
                    'analysis. This could be caused by a non-optimal discretisation '...
                    'space. You can either try a different discretisation space or '...
                    'a multi-component fit instead.'];
                warndlg(messagestr, 'MEM analysis not possible', 'modal')
            else
                %% MEM fitting the data by using subroutine FRAP_MEM
                [alpha, Ddis, Fexp, Fitdata,output] = FRAP_MEM_integral_TB(Imprebleachmean,topleftrect,sizerect,topleftroi,sizeroi,Fdata,Ffit,parfit,xytdata,xytdataplot,parnonfit,sigma2,Pset);

                %% output data as csv format

                counter=counter+1;
                Time_iter(counter,:)=clock;
                Time_iter= Time_iter';
                Niter=output.iterations;
                for j=0:numel(Fexp)/numel(t)-1
                    time(j*numel(t)+1:(j+1)*numel(t),1)=t+j*max(t);
                end

                dda(1:numel(Ddis),1)=Ddis;
                dda(1:numel(alpha),2)=alpha;
                dda(1:numel(time),3)=time;
                dda(1:numel(Fexp),4)=Fexp;
                dda(1:numel(Fitdata),5)=Fitdata;
                dda(1:numel(NumDdis),6)=NumDdis;
                dda(1:numel(Dmin),7)=Dmin;
                dda(1:numel(Dmax),8)=Dmax;
                dda(1:numel(set_noise),9)=set_noise;
                dda(1:size(Time_iter,1),10)=Time_iter(:,1);
                dda(1:size(Time_iter,1),11)=Time_iter(:,2);
                dda(1:size(Niter),12)=Niter;
                dda(1:size(D),13)=D;
                dda(1:size(K0),14)=K0;
                dda(1:size(k),15)=k;
                dda(1:size(r2av),16)=r2av;
                headers(1,1)={'Ddis'};
                headers(1,2)={'alpha'};
                headers(1,3)={'time'};
                headers(1,4)={'Fexpdata'};
                headers(1,5)={'Fitdata'};
                headers(1,6)={'NumDdis'};
                headers(1,7)={'Dmin'};
                headers(1,8)={'Dmax'};
                headers(1,9)={'set_noise'};
                headers(1,10)={'Time_iter_start'};
                headers(1,11)={'Time_iter_end'};
                headers(1,12)={'Number_iter'};
                headers(1,13)={'D'};
                headers(1,14)={'K0'};
                headers(1,15)={'k'};
                headers(1,16)={'r2av'};

                filename = fullfile(UserValues.File.MIAPath, char(MIAData.FileName{i}));
                filename = [filename(1:end-4), '_cFRAP.csv'];
                savecheck=mat2csv_wheaders(filename,headers,dda,',');
                Time_iter= Time_iter';
            end
    end
    %% Plot figure
    MIAData.FRAP.Figure{i} = figure;
    plot(MIAData.FRAP.Time{i}, MIAData.FRAP.Intensity{i}, 'b')
    if h.Mia_Image.Calculations.FRAP_Fit_Model.Value == 2
        hold 'on'
        fitIntensity = feval(MIAData.FRAP.Fit{i}, MIAData.FRAP.Time{i}(fitStart:end));
        plot(MIAData.FRAP.Time{i}(fitStart:end), fitIntensity, 'k')
        fitText = evalc('MIAData.FRAP.Fit{i}');
        fitText = ['f(' extractAfter(fitText, 'ans(')];
        textPos = [0.35 0.65 0.54 0.25];
        annotation('textbox', textPos, 'String', fitText)
        hold 'off'
    elseif h.Mia_Image.Calculations.FRAP_Fit_Model.Value == 3 || h.Mia_Image.Calculations.FRAP_Fit_Model.Value == 4
        hold 'on'
        fitIntensity = feval(MIAData.FRAP.Fit{i}, MIAData.FRAP.Time{i}(fitStart:end));
        plot(MIAData.FRAP.Time{i}(fitStart:end), fitIntensity, 'k')
        fitText = evalc('MIAData.FRAP.Fit{i}');
        fitText = ['f(' extractAfter(fitText, 'ans(')];
        textPos = [0.35 0.55 0.54 0.35];
        annotation('textbox', textPos, 'String', fitText)
        hold 'off'
    end
    xlim([MIAData.FRAP.Time{i}(1), MIAData.FRAP.Time{i}(end)])
    ylim([0, 1.1*max(MIAData.FRAP.Intensity{i})])
    xlabel('Time [s]')
    ylabel('Intensity [a.u.]')
    title(MIAData.FileName{i}, 'Interpreter', 'none')
end
%% Save results
switch h.Mia_Image.Calculations.FRAP_Save.Value
    case 2 % as .mat and .fig file
        [FileName,PathName] = uiputfile('*.mat', 'Save result as', UserValues.File.MIAPath);
        if ~(isequal(FileName,0) || isequal(PathName,0))
            SavePath = fullfile(PathName, FileName);
            FRAP = MIAData.FRAP;
            warning('off', 'MATLAB:Figure:FigureSavedToMATFile') % disable warning about saving figure handle to file
            save(SavePath, 'FRAP')
            warning('on', 'MATLAB:Figure:FigureSavedToMATFile')
            for i = ch
                savefig(MIAData.FRAP.Figure{i}, [SavePath(1:end-4) '_ch' int2str(i)])
            end
        end
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Funtions to analyse FLIM data %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function Do_FLIM(src, ~, plt, side)
% plt:  defines plot to update. 1 left image, 2 histogram, 3 residuals
% side: specifies plots for the left (1)/right(2) channels
h = guidata(findobj('Tag','Mia'));
global MIAData UserValues

if isempty(MIAData.Data)
    % no data loaded
    return
else
    % data loaded
    h.Mia_Progress_Text.String = 'Fitting';
    h.Mia_Progress_Axes.Color=[1 0 0];
    drawnow;
end

for s = side
    ch = h.Mia_FLIM.Ch(s).Channel.Value;
    if ch > size(MIAData.Data,1) || (~h.Mia_FLIM.Ch(s).AutoFit.Value && all(src~=h.MIA_FLIM.Ch(s).FitBtn))
        continue;
    end
    %% Check that TCSPC Range is valid
    MI_Rebin_Factor = round(str2double(h.Mia_Image.Calculations.MI_Rebinning_Factor.String));
    MI_Bins = ceil(str2double(h.Mia_Image.Calculations.MI_Bins.String)/MI_Rebin_Factor);
    MI_Rebin_Idx = repelem(1:MI_Bins, MI_Rebin_Factor);
    MI_Rebin_Idx = MI_Rebin_Idx(1:size(MIAData.Data{ch, 1}, 3));
    FitStart = ceil(str2double(h.Mia_FLIM.Ch(s).Fit_Start.String)/MI_Rebin_Factor);
    FitEnd = floor(str2double(h.Mia_FLIM.Ch(s).Fit_End.String)/MI_Rebin_Factor);
    
    if FitStart < 1
        FitStart = 1;
        h.Mia_FLIM.Ch(s).Fit_Start.String = num2str(FitStart);
    end
    if FitEnd > MI_Bins
        FitEnd = MI_Bins;
        h.Mia_FLIM.Ch(s).Fit_End.String = MI_Bins;
    end

    %% fitting
    %%% Get decay from ROI
    Decay = Get_Decay_From_ROI(ch, s);
    Decay = accumarray(MI_Rebin_Idx(:), Decay(:)); %Decay = Decay';
    Decay = Decay(FitStart:FitEnd);

    %%% Initialize inputs for fit
    % ScatterPattern = zeros([1 FitEnd-FitStart+1]);
    % %ScatterPattern = MIAData.FLIM.Scatter{ch};
    % if ~(sum(ScatterPattern) == 0)
    %     ScatterPattern = ScatterPattern'./sum(ScatterPattern);
    % else
    %     ScatterPattern = ScatterPattern';
    % end
    if ~isfield(MIAData.FLIM, 'IRF') || size(MIAData.FLIM.IRF, 2) < s
        Update_IRF(h.Mia_Image.Calculations.IRF_Source(s),[],s);
    end
    MIAData.FLIM.Ch{s}.IRF_Length = ceil(str2double(h.Mia_FLIM.Ch(s).IRF_Length.String)/MI_Rebin_Factor);
    IRFPattern = MIAData.FLIM.IRF{s};
    IRFPattern = accumarray(MI_Rebin_Idx(:), IRFPattern(:));
    IRFPattern = IRFPattern - mean(IRFPattern(end-round(numel(IRFPattern)/10):end)); %%% additional processing of the IRF to remove constant background
    IRFPattern = IRFPattern(1:MIAData.FLIM.Ch{s}.IRF_Length);
    IRFPattern(IRFPattern<0) = 0;
    %IRFPattern = IRFPattern ./ sum(IRFPattern);

    %Length = numel(Decay);
    ignore = ceil(str2double(h.Mia_FLIM.Ch(s).Ignore_Length.String)/MI_Rebin_Factor);
    TACRange = str2double(h.Mia_Image.Calculations.TACRange.String);
    TACChannelWidth = TACRange / MI_Bins;
    ShiftParams(1) = FitStart;
    ShiftParams(2) = 0; % not used anymore
    ShiftParams(3) = FitEnd-FitStart;
    ShiftParams(4) = ceil(str2double(h.Mia_FLIM.Ch(s).IRF_Length.String)/MI_Rebin_Factor);
    switch h.Mia_FLIM.Ch(s).ResidualsType.Value
        case 1 % MLE
            FitType = 'mlfit';
            ResWeight = [];
        case 2 % Least squares - Pearson weighting
            FitType = 'lsfit';
            ResWeight = 'Pearson';
        case 3 % Least squares - Neyman weighting
            FitType = 'lsfit';
            ResWeight = 'Neyman';
        case 4 % Least squares - Equal weight
            FitType = 'lsfit';
            ResWeight = 'Equal';
    end
    OptimMethod = h.Mia_FLIM.Ch(s).OptimMethod.Value;
    MIAData.FLIM.FitType{s} = h.Mia_FLIM.Ch(s).Model_Function.String{h.Mia_FLIM.Ch(s).Model_Function.Value};

    %%% Read out parameters
    fit_table = h.Mia_FLIM.Ch(s).Fit_Table.Data;
    x0 = cell2mat(fit_table(:,1))';
    lb = cell2mat(fit_table(:,2))';
    ub = cell2mat(fit_table(:,3))';
    fixed = cell2mat(fit_table(:,4))';
    %%% Add I0 parameter (Initial fluorescence intensity)
    % I0 = max(Decay);
    % x0(end+1) = I0;
    % lb(end+1) = 0;
    % ub(end+1) = Inf;
    % fixed(end+1) = false;
    fit = ~all(fixed); %%% if all parameters fixed, instead just plot the current values
    alpha = 0.05; %95% confidence interval
    MIAData.FLIM.ConfInt = NaN(numel(x0),2);

    %%% Convert Lifetimes
    numExponentials = str2double(regexp(MIAData.FLIM.FitType{s}, '\d', 'match', 'once'));
    lifetimes = 1:numExponentials;

    %%% set up fit parameters

    switch MIAData.FLIM.FitType{s}
        case {'1-Exp [reconv.]', '2-Exp [reconv.]', '3-Exp [reconv.]'}

            %format bounds according to fit function requirements
            lb(fixed) = x0(fixed);
            ub(fixed) = x0(fixed);


            %%% Pad IRF for fit function
            %irf = shift_by_fraction(IRFPattern,x0(end-1));
            irf = IRFPattern(ShiftParams(1):ShiftParams(4));
            irf(irf < 0.01*max(irf)) = 0;
            irf = irf./sum(irf);
            irf = [irf; zeros(ShiftParams(3)-numel(irf)+1,1)];

            switch OptimMethod
                case 1
                    bounds = [lb(lifetimes)' ub(lifetimes)'];
                    bounds = [lb(end) ub(end); bounds];
                case 2
                    tau_bounds = [lb(lifetimes)' ub(lifetimes)'];
                    c_bounds = [lb(end) ub(end)];
                    offset_bounds = [lb(end-1), ub(end-1)];
                    bounds = [c_bounds; offset_bounds; tau_bounds];
                    param0 = [x0(end); x0(end-1); x0(lifetimes)'];
                    if numExponentials > 1
                        A_bounds = [lb(lifetimes+numExponentials)' ub(lifetimes+numExponentials)'];
                        A_bounds(end,:) = [0 1];
                        A = x0(lifetimes+numExponentials)';
                        A(end) = 1-sum(A(1:end-1));
                    else
                        A_bounds = [0 Inf];
                        A = 1;
                    end
                    bounds = [bounds; A_bounds];
                    param0 = [param0; A];
            end

            reconv = true;
        case {'1-Exp [tail]', '2-Exp [tail]', '3-Exp [tail]'}

            %format bounds according to fit function requirements
            lb(fixed) = x0(fixed);
            ub(fixed) = x0(fixed);

            irf = [];

            switch OptimMethod
                case 1
                    bounds = [lb(lifetimes)' ub(lifetimes)'];
                    bounds = [lb(end) ub(end); bounds];
                case 2
                    tau_bounds = [lb(lifetimes)' ub(lifetimes)'];
                    c_bounds = [lb(end) ub(end)];
                    offset_bounds = [lb(end-1), ub(end-1)];
                    bounds = [c_bounds; offset_bounds; tau_bounds];
                    param0 = [x0(end); x0(end-1); x0(lifetimes)'];
                    if numExponentials > 1
                        A_bounds = [lb(lifetimes+numExponentials)' ub(lifetimes+numExponentials)'];
                        A_bounds(end,:) = [0 1];
                        A = x0(lifetimes+numExponentials)';
                        A(end) = 1-sum(A(1:end-1));
                    else
                        A_bounds = [0 1];
                        A = 1;
                    end
                    bounds = [bounds; A_bounds];
                    param0 = [param0; A];
            end

            reconv = false;
    end

    guess = h.Mia_FLIM.Ch(s).Guess.Value;
    switch OptimMethod
        case 1
            c0 = x0(end);
            tau0 = x0(lifetimes)';
            FitFunction = @(x) fit_exp_simplex(irf, x, TACRange, TACChannelWidth, c0, tau0, bounds, guess, true, FitType, ignore, ResWeight);
        case 2
            options = optimoptions(@fmincon,'Algorithm','trust-region-reflective',...
                'SpecifyObjectiveGradient',true, 'Display', 'none');
            FitFunction = @(x) fit_exp_fmincon(irf, x, TACRange, TACChannelWidth, param0, bounds, guess, true, FitType, ignore, ResWeight, options);
    end

    %%%Perform fit
    if fit
        %guess = h.Mia_FLIM.Ch(s).Guess.Value;
        [shift, offset, A, tau, z, ~, chi2] = FitFunction(Decay);
        if numExponentials == 1
            x = [tau./TACChannelWidth, x0(end-2), offset, shift, A];
        else
            x = [tau./TACChannelWidth; A(1:(numExponentials-1))./sum(A); x0(end-2); offset; shift; sum(A)];
            x = x';
        end
        if ~reconv
            x(end-1) = [];
        end
    else% plot only
        x = {x0};
    end

    %FitFun = FitFunction(x,{ShiftParams,IRFPattern,ScatterPattern,MI_Bins,Decay,0,1,Conv_Type});
    %if reconv
    FitFun = [zeros(ignore-1,1); z];
    %else
    %    FitFun = FitFunction(x,xdata_res);
    %end

    if strcmp(FitType, 'lsfit')
        wres = (Decay-FitFun);
        switch ResWeight
            case 'Pearson'
                sigma = sqrt(FitFun);
            case 'Neyman'
                sigma = sqrt(Decay);
                sigma(sigma<1) = 1;
            case 'Equal'
                sigma = mean(Decay);
        end
        wres = wres./sigma;
    else % MLE
        wres = MLE_w_res(FitFun,Decay).*sign(Decay-FitFun);
    end
    wres(1:(ignore-1)) = 0;
    if ~fit % plot only, chi2 was not calculated yet
        chi2 = sum(wres(~isinf(wres)).^2)./(numel(wres)-sum(~fixed));
    end

    %%% Update global variables
    MIAData.FLIM.Ch{s}.FitFun = FitFun(1:end);
    MIAData.FLIM.Ch{s}.wres = wres(1:end);
    MIAData.FLIM.Ch{s}.Decay = Decay(1:end);
    MIAData.FLIM.Ch{s}.Time = (FitStart:FitEnd) * TACChannelWidth;
    %%% Convert lifetimes to nanoseconds
    x(lifetimes) = x(lifetimes) .* TACChannelWidth;

    %%% Convert Fraction from Amplitude (species) fraction to Intensity fraction
    %%% (i.e. correct for brightness)
    %%% Intensity is proportional to tau*amplitude
    amp = zeros(numExponentials, 1);
    if numExponentials > 1
        amp(1:(numExponentials-1)) = x(1:(numExponentials-1)) .* x((numExponentials+1):(2*numExponentials-1));
        amp(numExponentials) = x(numExponentials) * (1 - sum(x((numExponentials+1):(2*numExponentials-1))));
        f = amp ./ sum(amp);
        MIAData.FLIM.Ch{s}.MeanTau = x(lifetimes) * f;
    else
        MIAData.FLIM.Ch{s}.MeanTau = x(1);
    end
    %%% Update FitResult
    MIAData.FLIM.Ch{s}.FitResult = num2cell(x');
    MIAData.FLIM.Ch{s}.ConfInt(lifetimes,:) = MIAData.FLIM.ConfInt(lifetimes,:) .* TACChannelWidth;
    h.Mia_FLIM.Ch(s).Fit_Table.Data(:,1) = MIAData.FLIM.Ch{s}.FitResult(1:end-1);
    MIAData.FLIM.Ch{s}.IRF_Shift = MIAData.FLIM.Ch{s}.FitResult{end-1};
    MIAData.FLIM.Ch{s}.I0 = MIAData.FLIM.Ch{s}.FitResult{end};

    %% Update plots
    for p = plt
        switch p
            case 1 %%% update image
                %%% Extracts position of ROI box in full image
                From=h.Plots.ROI(1).Position(1:2)+0.5;
                To=From+h.Plots.ROI(1).Position(3:4)-1;
                FrStart = ceil(str2double(h.Mia_FLIM.Ch(s).Fit_Start.String));
                FrEnd = floor(str2double(h.Mia_FLIM.Ch(s).Fit_End.String));
                switch h.Mia_FLIM.Ch(s).ImageType.Value
                    case 1 %%% Intensity
                        Im = sum(MIAData.Data{ch,1}(:,:,FrStart:FrEnd),3);
                        
                        h.Mia_FLIM.Axes(p,s).Colormap = gray(128);
                        h.Mia_FLIM.Axes(p,s).Color = [1 0 0];
                        AlphaRatio = 3;
                        AlphaData = (MIAData.FLIM.Ch{s}.ROI + AlphaRatio)/(1 + AlphaRatio);
                        
                    case 2 %%% Mean Arrival Time
                        TACRange = str2double(h.Mia_Image.Calculations.TACRange.String);
                        Bin_Size = TACRange/(MI_Bins*MI_Rebin_Factor);
                        [~, ~, Bin_Time] = meshgrid(1:size(MIAData.Data{ch,1}, 1), 1:1:size(MIAData.Data{ch,1}, 2), (FrStart:FrEnd)*Bin_Size);
                        Im = sum(single(MIAData.Data{ch,1}(:,:,FrStart:FrEnd)).*Bin_Time,3)./sum(single(MIAData.Data{ch,1}(:,:,FrStart:FrEnd)),3);
                        
                        h.Mia_FLIM.Axes(p,s).Colormap = jet(128);
                        h.Mia_FLIM.Axes(p,s).Color = [0 0 0];
                        AlphaRatio = 0.5;
                        AlphaData = (MIAData.FLIM.Ch{s}.ROI + AlphaRatio)/(1 + AlphaRatio);
                    case {3, 5, 7} %%% a1, a2, a3
                        if isfield(MIAData.FLIM.Ch{s}, 'Amp') && (((h.Mia_FLIM.Ch(s).ImageType.Value-1)/2) <= size(MIAData.FLIM.Ch{s}.Amp, 3))
                            Im = MIAData.FLIM.Ch{s}.Amp(:,:,(h.Mia_FLIM.Ch(s).ImageType.Value-1)/2);
                        else
                            Im = zeros(size(MIAData.Data{ch,1}, [1 2]));
                        end
                        Int = sum(MIAData.Data{ch,1}(:,:,FrStart:FrEnd),3);
                        Int = Int./max(Int(:));
                        Int = imadjust(Int, stretchlim(Int, [0.01 0.95]));
                        
                        h.Mia_FLIM.Axes(p,s).Colormap = jet(128);
                        h.Mia_FLIM.Axes(p,s).Color = [0 0 0];
                        AlphaRatio = 1;
                        AlphaData = Int.*(MIAData.FLIM.Ch{s}.ROI + AlphaRatio)/(1 + AlphaRatio);
                    case {4, 6, 8} %%% tau1, tau2, tau3
                        if isfield(MIAData.FLIM.Ch{s}, 'Tau') && (((h.Mia_FLIM.Ch(s).ImageType.Value-2)/2) <= size(MIAData.FLIM.Ch{s}.Tau, 3))
                            Im = MIAData.FLIM.Ch{s}.Tau(:,:,(h.Mia_FLIM.Ch(s).ImageType.Value-2)/2);
                        else
                            Im = zeros(size(MIAData.Data{ch,1}, [1 2]));
                        end
                        Int = sum(MIAData.Data{ch,1}(:,:,FrStart:FrEnd),3);
                        Int = Int./max(Int(:));
                        Int = imadjust(Int, stretchlim(Int, [0.01 0.95]));

                        h.Mia_FLIM.Axes(p,s).Colormap = jet(128);
                        h.Mia_FLIM.Axes(p,s).Color = [0 0 0];
                        AlphaRatio = 1;
                        AlphaData = Int.*(MIAData.FLIM.Ch{s}.ROI + AlphaRatio)/(1 + AlphaRatio);
                    case 9 %%% chi-squared
                        if isfield(MIAData.FLIM.Ch{s}, 'Chi2') && ~isempty(MIAData.FLIM.Ch{s}.Chi2)
                            Im = MIAData.FLIM.Ch{s}.Chi2;
                        else
                            Im = zeros(size(MIAData.Data{ch,1}, [1 2]));
                        end
                        Int = sum(MIAData.Data{ch,1}(:,:,FrStart:FrEnd),3);
                        Int = Int./max(Int(:));
                        Int = imadjust(Int, stretchlim(Int, [0.01 0.95]));

                        h.Mia_FLIM.Axes(p,s).Colormap = jet(128);
                        h.Mia_FLIM.Axes(p,s).Color = [0 0 0];
                        AlphaRatio = 1;
                        AlphaData = Int.*(MIAData.FLIM.Ch{s}.ROI + AlphaRatio)/(1 + AlphaRatio);
                    case 10 %%% mean tau
                        if isfield(MIAData.FLIM.Ch{s}, 'TauM') && ~isempty(MIAData.FLIM.Ch{s}.TauM)
                            Im = MIAData.FLIM.Ch{s}.TauM;
                        else
                            Im = zeros(size(MIAData.Data{ch,1}, [1 2]));
                        end
                        Int = sum(MIAData.Data{ch,1}(:,:,FrStart:FrEnd),3);
                        Int = Int./max(Int(:));
                        Int = imadjust(Int, stretchlim(Int, [0.01 0.95]));
                       
                        h.Mia_FLIM.Axes(p,s).Colormap = jet(128);
                        h.Mia_FLIM.Axes(p,s).Color = [0 0 0];
                        AlphaRatio = 1;
                        AlphaData = Int.*(MIAData.FLIM.Ch{s}.ROI + AlphaRatio)/(1 + AlphaRatio);
                end

                %%% update plots
                h.Plots.FLIM.Image(s).CData = Im;
                autoscale = h.Mia_FLIM.Ch(s).AutoScale.Value;
                scale_range = prctile(Im, [1 99], 'all');
                if ~autoscale || (scale_range(1) == scale_range(2))
                    scale_range = [str2double(h.Mia_FLIM.Ch(s).ScaleMin.String) str2double(h.Mia_FLIM.Ch(s).ScaleMax.String)];
                end
                clim(h.Mia_FLIM.Axes(p,s), scale_range)
                h.Mia_FLIM.Ch(s).ScaleMin.String = num2str(scale_range(1));
                h.Mia_FLIM.Ch(s).ScaleMax.String = num2str(scale_range(2));
                xlim(h.Mia_FLIM.Axes(p,s), [From(1) To(1)]);
                ylim(h.Mia_FLIM.Axes(p,s), [From(2) To(2)]);
                h.Plots.FLIM.Image(s).AlphaData = AlphaData;
            case 2 %%% update histogram and fit
                %h.Plots.FLIM(p,s).XData = MIAData.FLIM.Ch{s}.Time;
                %h.Plots.FLIM(p,s).YData = MIAData.FLIM.Ch{s}.Decay;
                IRFnorm = shift_by_fraction(irf, MIAData.FLIM.Ch{s}.IRF_Shift);
                IRFnorm = IRFnorm./max(IRFnorm).*max(MIAData.FLIM.Ch{s}.Decay);
                if contains(MIAData.FLIM.FitType{s}, '[reconv.]')
                    set(h.Plots.FLIM.IRF(s), 'XData', MIAData.FLIM.Ch{s}.Time(1:MIAData.FLIM.Ch{s}.IRF_Length), 'YData', IRFnorm(1:MIAData.FLIM.Ch{s}.IRF_Length));
                else
                    set(h.Plots.FLIM.IRF(s), 'Visible', 'off');
                end
                set(h.Plots.FLIM.Decay(s), 'XData', MIAData.FLIM.Ch{s}.Time(1:end), 'YData', MIAData.FLIM.Ch{s}.Decay(1:end));
                set(h.Plots.FLIM.Fit(s), 'XData', MIAData.FLIM.Ch{s}.Time(ignore:end), 'YData', MIAData.FLIM.Ch{s}.FitFun(ignore:end));
                xlim(h.Mia_FLIM.Axes(p,s), [MIAData.FLIM.Ch{s}.Time(1) MIAData.FLIM.Ch{s}.Time(end)] )
                ylim(h.Mia_FLIM.Axes(p,s), [max([1 min(MIAData.FLIM.Ch{s}.Decay(ignore:end))]) 1.1*max([MIAData.FLIM.Ch{s}.Decay; 1])])
                
                if strcmp(FitType, 'lsfit')
                    switch ResWeight
                        case 'Pearson'
                            res_text = ['\' sprintf('chi^2_P = %.2f', chi2)];
                        case 'Neyman'
                            res_text = ['\' sprintf('chi^2_N = %.2f', chi2)];
                        case 'Equal'
                            res_text = ['\Sigma' sprintf('res^2 = %.2f', chi2)];
                    end
                    
                else
                    res_text = ['\' sprintf('chi^2_{MLE} = %.2f', chi2)];
                end
                h.Plots.FLIM.Text(s).String = [res_text newline '\' sprintf('tau_m = %.2f', MIAData.FLIM.Ch{s}.MeanTau)];
            case 3
                set(h.Plots.FLIM.WRes(s), 'XData', MIAData.FLIM.Ch{s}.Time, 'YData', MIAData.FLIM.Ch{s}.wres);
                xlim(h.Mia_FLIM.Axes(p,s), [MIAData.FLIM.Ch{s}.Time(1) MIAData.FLIM.Ch{s}.Time(end)] )
                wres_lim = 1.1*max(abs(MIAData.FLIM.Ch{s}.wres));
                ylim(h.Mia_FLIM.Axes(p,s), [min([-wres_lim -1]), max([wres_lim 1])]);
        end
    end
end
%% Updates filename display
if numel(MIAData.FileName)==2
    h.Mia_Progress_Text.String = [MIAData.FileName{1}{1} ' / ' MIAData.FileName{2}{1}];
elseif numel(MIAData.FileName)==1
    h.Mia_Progress_Text.String = MIAData.FileName{1}{1};
else
    h.Mia_Progress_Text.String = 'Nothing loaded';
end
h.Mia_Progress_Axes.Color=UserValues.Look.Control;

function a = interlace( a, x, fix )
a(:,~fix) = x;

function Update_IRF(src,~,channel)
global MIAData UserValues
h = guidata(findobj('Tag','Mia'));
switch src.String{src.Value}
    case 'Load from file'
        [filename, filepath] = uigetfile('*.irf', 'Load IRF', UserValues.File.MIAPath, 'MultiSelect', 'off');
        fileID = fopen(fullfile(filepath, filename));
        MIAData.FLIM.IRF{channel} = fscanf(fileID, '%f\n');
        MIAData.FLIM.IRF{channel} = MIAData.FLIM.IRF{channel}(1:end-3);
        fclose(fileID);
    otherwise %%% load from PIE channel in UserValues
        MIAData.FLIM.IRF{channel} = UserValues.PIE.IRF{src.Value}(:);
        h.Mia_FLIM.Ch(channel).Fit_Start.String = num2str(UserValues.PIE.From(src.Value));
        h.Mia_FLIM.Ch(channel).Fit_End.String = num2str(UserValues.PIE.To(src.Value));
end

function Update_FLIM_ModelFun(src, ~, channel)
h = guidata(findobj('Tag','Mia'));
switch src.String{src.Value}
    case '1-Exp [reconv.]'
        RowNames = {'Tau [ns]', 'Scatter', 'Background', 'IRF Shift'};
        FitData={4, 0.1, Inf, false;...
            0, 0, Inf, true;...
            0, 0, Inf, false;...
            0, -Inf, Inf, false};
        
    case '2-Exp [reconv.]'
        RowNames = {'Tau1 [ns]','Tau2 [ns]','Fraction 1','Scatter','Background','IRF Shift'};
        FitData={1, 0.1, Inf, false;...
            4, 0.1, Inf, false;...
            0.5, 0, 1, false;...
            0, 0, Inf, true;...
            0, 0, Inf, false;...
            0, -Inf, Inf, false};
    case '3-Exp [reconv.]'
        RowNames = {'Tau1 [ns]','Tau2 [ns]','Tau3 [ns]','Fraction 1','Fraction 2','Scatter','Background','IRF Shift'};
        FitData={1, 0.1, Inf, false;...
            2, 0.1, Inf, false;...
            4, 0.1, Inf, false;...
            0.3, 0, 1, false;...
            0.3, 0, 1, false;...
            0, 0, Inf, true;...
            0, 0, Inf, false;...
            0, -Inf, Inf, false};
    case '1-Exp [tail]'
        RowNames = {'Tau [ns]', 'Offset', 'I0'};
        FitData={4, 0.1, Inf, false;...
            0, 0, Inf, false;...
            1, 0, Inf, false};
    case '2-Exp [tail]'
        RowNames = {'Tau1 [ns]', 'Tau2 [ns]', 'Fraction 1', 'Offset', 'I0'};
        FitData={1, 0.1, Inf, false;...
            4, 0.1, Inf, false;...
            0.5, 0, 1, false;...
            0, 0, Inf, false;...
            1, 0, Inf, false};
    case '3-Exp [tail]'
        RowNames = {'Tau1 [ns]', 'Tau2 [ns]', 'Tau3 [ns]', 'Fraction 1', 'Fraction 2', 'Offset', 'I0'};
        FitData={1, 0.1, Inf, false;...
            2, 0.1, Inf, false;...
            4, 0.1, Inf, false;...
            0.3, 0, 1, false;...
            0.3, 0, 1, false;...
            0, 0, Inf, false;...
            1, 0, Inf, false};
end
h.Mia_FLIM.Ch(channel).Fit_Table.RowName = RowNames;
h.Mia_FLIM.Ch(channel).Fit_Table.Data = FitData;
Do_FLIM([], [], 2:3, channel);


%%% Determines ROI and extracts decay data
function Decay = Get_Decay_From_ROI(ch, s)
global MIAData
h = guidata(findobj('Tag','Mia'));

[rows, cols, frames] = size(MIAData.Data{ch,1});

%%% Extracts position of ROI box in full image
From=h.Plots.ROI(1).Position(1:2)+0.5;
To=From+h.Plots.ROI(1).Position(3:4)-1;
FOV = false(rows,cols);
FOV(From(2):To(2), From(1):To(1)) = true;

cursor = findobj(h.Mia_FLIM.Axes(1, s), 'Tag', 'FLIM_Cursor');
if size(cursor, 1) ~= 1
    delete(cursor)
    cursor = images.roi.Crosshair(h.Mia_FLIM.Axes(1, s),...
        'Position', [10 10], 'Tag', 'FLIM_Cursor');
    addlistener(cursor, "ROIMoved", @(src, evt) Do_FLIM([], [], 1:3, s));
end

ROI = false(rows, cols);

switch h.Mia_FLIM.Ch(s).ROI_Type.Value
    case 1 %%% cursor
        cursor.Visible = 'On';
        cursorPos = round(cursor.Position);
        cursor.Position = cursorPos;
        if isfield(MIAData.FLIM, 'ROI') && ~isempty(MIAData.FLIM.ROI)
            arrayfun(@(x) set(x, 'Visible','off'), MIAData.FLIM.ROI(:,s));
        end

        %%% Pixel Binning
        range = floor(str2double(h.Mia_FLIM.Ch(s).Pixel_Binning.String));
        if range < 0 %% validity checks for range and cursor position
            range = 0;
            h.Mia_FLIM.Ch(s).Pixel_Binning.String = num2str(range);
        else
            cursorPos(cursorPos <= range) = range + 1;
            maxPos = [rows - range, cols - range];
            cursorPos(cursorPos > maxPos) = maxPos(cursorPos > maxPos);
        end
        switch h.Mia_FLIM.Ch(s).Pixel_Binning_Method.Value
            case 1 %%% Square Binning
                posX = (cursorPos(1) - range) : (cursorPos(1) + range);
                posY = (cursorPos(2) - range) : (cursorPos(2) + range);
                Decay = squeeze(sum(MIAData.Data{ch,1}(posY,posX,:), [1 2], "double"))';
                ROI(posY,posX) = true;
            case 2 %%% Circle Binning
                [x, y] = meshgrid(1:cols, 1:rows);
                distances = sqrt((x - cursorPos(1)).^2 + (y - cursorPos(2)).^2);
                pixelsWithinRadius = repmat(distances <= range, [1 1 frames]);
                Decay = reshape(MIAData.Data{ch,1}(pixelsWithinRadius), [sum(distances <= range,"all"), frames]);
                Decay = squeeze(sum(Decay, 1, "double"));
                ROI(distances <= range) = true;
        end
        %cursor.Label = num2str(cursor.Position);
    case 2 %%% selected ROIs
        cursor.Visible = 'Off';
        
        ROI_List = h.Mia_FLIM.Ch(s).ROI_Table.Data;
        ROI = false(rows, cols, numel(ROI_List(:, 2)));
        
        %%% Merge all selected pixels into 1 ROI
        %Sel = find(cell2mat(ROI_List(:, 2)))';
        for rowIdx = find(cell2mat(ROI_List(:, 2)))'
            switch ROI_List{rowIdx, 1}
                case 'All'
                    %ROI(From(2):To(2),From(1):To(1), rowIdx) = true;
                case 'MIA AROI'
                    if ~isempty(MIAData.AR) && ~isempty(MIAData.AR{ch,1})
                        ROI(:,:, rowIdx) = MIAData.MS{ch,1} & any(MIAData.AR{ch,1}, 3);
                    else
                        ROI(:,:, rowIdx) = MIAData.MS{ch,1};
                    end
                otherwise
                    if contains(ROI_List{rowIdx, 1}, 'Cellpose') %%% Cellpose ROIs, can extract directly from label matrix
                        cell_idx = str2double(extractAfter(ROI_List{rowIdx, 1}, 'Cellpose '));
                        ROI(:,:, rowIdx) = MIAData.ROI.Cellpose.masks == cell_idx;
                    elseif isfield(MIAData.FLIM, 'ROI') && ~isempty(MIAData.FLIM.ROI) %%% ROI objects drawn by user
                        MIAData.FLIM.ROI(rowIdx-2, s).Visible = 'on';
                        ROI(:,:, rowIdx) = createMask(MIAData.FLIM.ROI(rowIdx-2, s));
                    end
            end
        end
        ROI = any(ROI, 3);
        ROI = ROI & FOV; % get ROI only within current FOV
        Decay = reshape(MIAData.Data{ch,1}(repmat(ROI, [1 1 frames])), [sum(ROI,"all"), frames]);
        Decay = squeeze(sum(Decay, 1, "double"));
end
%%% Update ROI mask in MIAData
MIAData.FLIM.Ch{s}.ROI = ROI;

%%% Draws ROIs in FLIM tab and add them to ROI list
function FLIM_Draw_ROI(src, ~)
global MIAData
h = guidata(findobj('Tag','Mia'));
if ~isfield(MIAData.FLIM, 'ROI')
    MIAData.FLIM.ROI = gobjects(0);
end
if ~isfield(MIAData.FLIM, 'ROI_Link')
    MIAData.FLIM.ROI_Link = gobjects(0);
end

% function for drawing the roi
ax = h.Mia_FLIM.Axes(1,1:2);
currentAx = find(ax == gca);
otherAx = find(ax ~= gca);
switch src.Label
    case 'Assisted Freehand'
        f = findobj(ax(currentAx), '-regexp', 'Tag', 'Freehand*');
        numROI = numel(f);
        roi(currentAx) = drawassisted(ax(currentAx), 'Tag', ['Freehand '  num2str(numROI+1)]); 
        linked_props = {'Position'};
    case 'Freehand'
        f = findobj(ax(currentAx), '-regexp', 'Tag', 'Freehand*');
        numROI = numel(f);
        roi(currentAx) = drawfreehand(ax(currentAx), 'Tag', ['Freehand '  num2str(numROI+1)]); 
        linked_props = {'Position'};
    case 'Circle'
        f = findobj(ax(currentAx), '-regexp', 'Tag', 'Circle*');
        numROI = numel(f);
        roi(currentAx) = drawcircle(ax(currentAx), 'Tag', ['Circle '  num2str(numROI+1)]);
        linked_props = {'Center', 'Radius'};
    case 'Ellipse'
        f = findobj(ax(currentAx), '-regexp', 'Tag', 'Ellipse*');
        numROI = numel(f);
        roi(currentAx) = drawellipse(ax(currentAx), 'Tag', ['Ellipse '  num2str(numROI+1)]);
        linked_props = {'Center', 'Radius', 'SemiAxes', 'RotationAngle'};
    case 'Rectangle'
        f = findobj(ax(currentAx), '-regexp', 'Tag', 'Rectangle*');
        numROI = numel(f);
        roi(currentAx) = drawrectangle(ax(currentAx), 'Tag', ['Rectangle '  num2str(numROI+1)],...
            'Rotatable', true);
        linked_props = {'Position','RotationAngle'};
    case 'Polygon'
        f = findobj(ax(currentAx), '-regexp', 'Tag', 'Polygon*');
        numROI = numel(f);
        roi(currentAx) = drawpolygon(ax(currentAx), 'Tag', ['Polygon '  num2str(numROI+1)]);
        linked_props = {'Position'};
    otherwise
        disp('Unknown ROI type.');
        return;
end
set(roi(currentAx), 'SelectedColor', 'green', 'FaceAlpha', 0, 'Deletable', false); % sets common properties for rois
roi(otherAx) = copyobj(roi(currentAx), ax(otherAx));

for s = 1:2
    if ax(s) ~= gca % hide roi on inactive axes
        roi(s).Visible = 'off';
    end
    addlistener(roi(s), "ROIMoved", @(src, evt) Do_FLIM([], [], 1:3, 1:2));
end

% update global variables with roi handles and link
if isempty(MIAData.FLIM.ROI)
    MIAData.FLIM.ROI = roi;
    MIAData.FLIM.ROI_Link = linkprop(roi, linked_props);
else
    MIAData.FLIM.ROI(end+1,:) = roi;
    MIAData.FLIM.ROI_Link(end+1) = linkprop(roi, linked_props);
end
FLIM_Update_ROI_Table(MIAData.FLIM.ROI(end,:),[],[]);


%%%Update ROI table in FLIM tab
function FLIM_Update_ROI_Table(src,e,s)
global MIAData
h = guidata(findobj('Tag','Mia'));

if ~isempty(e)
    tRow = e.Indices(1);
    tCol = e.Indices(2);
    if tRow == 1 %% select/delete all ROI
        switch tCol
            case 2
                src.Data(:,2) = cellfun(@(x) {e.NewData}, src.Data(:,2));
                if e.NewData && ~isempty(MIAData.FLIM.ROI)
                    arrayfun(@(x) set(x, 'Visible','on'), MIAData.FLIM.ROI(:, s));
                elseif ~isempty(MIAData.FLIM.ROI)
                    arrayfun(@(x) set(x, 'Visible','off'), MIAData.FLIM.ROI(:, s));
                end
                Do_FLIM([],[],1:3,s);
            case 3
                msg = 'Are you sure you want to delete all ROIs?';
                selection = questdlg(msg,'Confirm ROI Deletion','Yes','No','No');
                src.Data{tRow,tCol} = false; % reset selection checkbox
                switch selection
                    case 'Yes'
                        delete(MIAData.FLIM.ROI);
                        MIAData.FLIM.ROI = [];
                        MIAData.FLIM.ROI_Link = [];
                        h.Mia_FLIM.Ch(1).ROI_Table.Data = h.Mia_FLIM.Ch(1).ROI_Table.Data(1:2, :);
                        h.Mia_FLIM.Ch(2).ROI_Table.Data = h.Mia_FLIM.Ch(2).ROI_Table.Data(1:2, :);
                        Do_FLIM([],[],1:3,s);
                    otherwise
                        return
                end
        end
    elseif tRow ==2 % if change was AROI selection
        switch tCol
            case 2
                if ~e.NewData % sets All selection to false
                    src.Data{1,2} = false;
                end
                Do_FLIM([],[],1:3,s);
            case 3 %%% deletion not possible for AROI row, so reset the option
                src.Data{tRow, tCol} = false;
        end
    else% other ROIs
        switch tCol
            case 2 % select checkbox
                if e.NewData
                    MIAData.FLIM.ROI(tRow-2, s).Visible = 'on';
                else
                    src.Data{1,2} = false;
                    MIAData.FLIM.ROI(tRow-2, s).Visible = 'off';
                end
                Do_FLIM([],[],1:3,s);
            case 3 % delete checkbox
                recalc = h.Mia_FLIM.Ch(1).ROI_Table.Data{tRow,2} | h.Mia_FLIM.Ch(2).ROI_Table.Data{tRow,2};
                if e.NewData
                    delete(MIAData.FLIM.ROI(tRow-2, :));
                    MIAData.FLIM.ROI(tRow-2, :) = [];
                    MIAData.FLIM.ROI_Link(tRow-2) = [];
                    h.Mia_FLIM.Ch(1).ROI_Table.Data(tRow,:) = [];
                    h.Mia_FLIM.Ch(2).ROI_Table.Data(tRow,:) = [];
                end
                if recalc
                    Do_FLIM([],[],1:3,1:2);
                end
        end
    end
else %%% Add ROI from MIAData
    if size(src, 2) == 2
        roi_list = arrayfun(@(x) {x.Tag, x.Visible == 'on', false}, src, 'UniformOutput', false);
    else
        roi_list = arrayfun(@(x) {x.Tag, x.Visible == 'on', false}, MIAData.FLIM.ROI, 'UniformOutput', false);
    end
    for f = 1:2
        if (size(MIAData.FLIM.ROI, 1) - size(src, 1)) == (size(h.Mia_FLIM.Ch(f).ROI_Table.Data, 1) - 2)
            h.Mia_FLIM.Ch(f).ROI_Table.Data = vertcat(h.Mia_FLIM.Ch(f).ROI_Table.Data, roi_list{:,f});
        elseif ~isempty(roi_list)
            h.Mia_FLIM.Ch(f).ROI_Table.Data = vertcat(h.Mia_FLIM.Ch(f).ROI_Table.Data(1:2, :), roi_list{:,f});
        else
            h.Mia_FLIM.Ch(f).ROI_Table.Data = h.Mia_FLIM.Ch(f).ROI_Table.Data(1:2, :);
        end
    end
    Do_FLIM([],[],1:3,1:2);
end

%%%Callback for fit table changes in FLIM Tab
function FLIM_Update_Fit_Table(~,e,s)
if (e.Indices(2) == 4) && e.NewData 
    return %% skip fitting if we are just fixing values
end
Do_FLIM([],[], 2:3, s)

%%% function to calculate decay matrix
function FLIM_Decay_Matrix(~,~,side)
global MIAData UserValues
h = guidata(findobj('Tag', 'Mia'));

if isempty(MIAData.Data)
    % no data loaded
    return
end
for s = side
    ch = h.Mia_FLIM.Ch(s).Channel.Value;
    if ch > size(MIAData.Data,1)
        continue;
    end

    [rows, cols, frames] = size(MIAData.Data{ch,1});

    %%% validity checks for range
    range = floor(str2double(h.Mia_FLIM.Ch(s).Pixel_Binning.String));
    if range < 0
        range = 0;
    elseif range > rows/2 || range > cols/2
        range = min(floor([rows/2, cols/2]));
    end
    h.Mia_FLIM.Ch(s).Pixel_Binning.String = num2str(range);

    %%% Check that TCSPC Range is valid
    MI_Rebin_Factor = round(str2double(h.Mia_Image.Calculations.MI_Rebinning_Factor.String));
    MI_Bins = ceil(str2double(h.Mia_Image.Calculations.MI_Bins.String)/MI_Rebin_Factor);
    MI_Rebin_Idx = repelem(1:MI_Bins, MI_Rebin_Factor);
    MI_Rebin_Idx = MI_Rebin_Idx(1:size(MIAData.Data{ch, 1}, 3));
    FitStart = ceil(str2double(h.Mia_FLIM.Ch(s).Fit_Start.String)/MI_Rebin_Factor);
    FitEnd = floor(str2double(h.Mia_FLIM.Ch(s).Fit_End.String)/MI_Rebin_Factor);
    if FitStart < 1
        FitStart = 1;
        h.Mia_FLIM.Ch(s).Fit_Start.String = num2str(FitStart);
    end
    if FitEnd > MI_Bins
        FitEnd = MI_Bins;
        h.Mia_FLIM.Ch(s).Fit_End.String = MI_Bins;
    end

    %% get binned image
    if mod(frames, MI_Rebin_Factor) ~= 0
        padsize = MI_Rebin_Factor - mod(frames, MI_Rebin_Factor);
        BinnedIm = padarray(MIAData.Data{ch, 1}, [0 0 padsize], 0, 'post');
    else
        BinnedIm = MIAData.Data{ch, 1};
    end
    BinnedIm = reshape(BinnedIm, rows, cols, MI_Rebin_Factor, []);
    BinnedIm = squeeze(sum(BinnedIm, 3));
    BinnedIm = BinnedIm(:,:,FitStart:FitEnd);
    MIAData.FLIM.Ch{s}.Intensity = sum(BinnedIm, 3);

    %% convolve binned image with kernel
    switch h.Mia_FLIM.Ch(s).Pixel_Binning_Method.Value
        case 1 %%% square binning
            kern = ones(2*range+1);
        case 2 %%% circle binning
            [x, y] = meshgrid(-range:range, -range:range);
            distances = sqrt(x.^2 + y.^2);
            kern = distances <= range;
    end
    BinnedIm = convn(BinnedIm, kern, "same");
    

    %% Initialize inputs for fit
    % ScatterPattern = zeros([1 FitEnd-FitStart+1]);
    % %ScatterPattern = MIAData.FLIM.Scatter{ch};
    % if ~(sum(ScatterPattern) == 0)
    %     ScatterPattern = ScatterPattern'./sum(ScatterPattern);
    % else
    %     ScatterPattern = ScatterPattern';
    % end
    if ~isfield(MIAData.FLIM, 'IRF') || size(MIAData.FLIM.IRF, 2) < s
        Update_IRF(h.Mia_Image.Calculations.IRF_Source(s),[],s);
    end
    MIAData.FLIM.Ch{s}.IRF_Length = round(str2double(h.Mia_FLIM.Ch(s).IRF_Length.String)/MI_Rebin_Factor);
    IRFPattern = MIAData.FLIM.IRF{s}';
    IRFPattern = accumarray(MI_Rebin_Idx(:), IRFPattern(:));
    IRFPattern = IRFPattern - mean(IRFPattern(end-round(numel(IRFPattern)/10):end)); %%% additional processing of the IRF to remove constant background
    IRFPattern = IRFPattern(1:MIAData.FLIM.Ch{s}.IRF_Length);
    IRFPattern(IRFPattern<0) = 0;
    %IRFPattern = IRFPattern ./ sum(IRFPattern);
    %IRFPattern = [IRFPattern; zeros(FitEnd - FitStart + 1 - numel(IRFPattern), 1)];
    

    %Length = numel(Decay);
    ignore = ceil(str2double(h.Mia_FLIM.Ch(s).Ignore_Length.String)/MI_Rebin_Factor);
    TACRange = str2double(h.Mia_Image.Calculations.TACRange.String);
    TACChannelWidth = TACRange / MI_Bins;
    ShiftParams(1) = FitStart;
    ShiftParams(2) = 0; % not used anymore
    ShiftParams(3) = FitEnd-FitStart;
    ShiftParams(4) = ceil(str2double(h.Mia_FLIM.Ch(s).IRF_Length.String)/MI_Rebin_Factor);
    switch h.Mia_FLIM.Ch(s).ResidualsType.Value
        case 1 % MLE
            FitType = 'mlfit';
            ResWeight = [];
        case 2 % Least squares - Pearson weighting
            FitType = 'lsfit';
            ResWeight = 'Pearson';
        case 3 % Least squares - Neyman weighting
            FitType = 'lsfit';
            ResWeight = 'Neyman';
        case 4 % Least squares - Equal weight
            FitType = 'lsfit';
            ResWeight = 'Equal';
    end
    OptimMethod = h.Mia_FLIM.Ch(s).OptimMethod.Value;

    MIAData.FLIM.FitType{s} = h.Mia_FLIM.Ch(s).Model_Function.String{h.Mia_FLIM.Ch(s).Model_Function.Value};

    %%% Read out parameters
    fit_table = h.Mia_FLIM.Ch(s).Fit_Table.Data;
    x0 = cell2mat(fit_table(:,1))';
    lb = cell2mat(fit_table(:,2))';
    ub = cell2mat(fit_table(:,3))';
    fixed = cell2mat(fit_table(:,4))';
    %%% Add I0 parameter (Initial fluorescence intensity)
    % I0 = max(BinnedIm, [], "all");
    % x0(end+1) = I0;
    % lb(end+1) = 0;
    % ub(end+1) = Inf;
    % fixed(end+1) = false;
    %alpha = 0.05; %95% confidence interval
    MIAData.FLIM.ConfInt = NaN(numel(x0),2);

    %%% Convert Lifetimes
    numExponentials = str2double(regexp(MIAData.FLIM.FitType{s}, '\d', 'match', 'once'));
    lifetimes = 1:numExponentials;

    %%% Pad IRF for fit function
    %irf = shift_by_fraction(IRFPattern,x0(end-1));
    if contains(MIAData.FLIM.FitType{s}, '[reconv.]')
        irf = IRFPattern(ShiftParams(1):ShiftParams(4));
        irf(irf < 0.01*max(irf)) = 0;
        irf = irf./sum(irf);
        irf = [irf; zeros(ShiftParams(3)-numel(irf)+1,1)];
    else
        irf = [];
    end

    %format bounds according to fit function requirements
    lb(fixed) = x0(fixed);
    ub(fixed) = x0(fixed);
    switch OptimMethod
        case 1
            bounds = [lb(lifetimes)' ub(lifetimes)'];
            bounds = [lb(end) ub(end); bounds];
        case 2
            tau_bounds = [lb(lifetimes)' ub(lifetimes)'];
            c_bounds = [lb(end) ub(end)];
            offset_bounds = [lb(end-1), ub(end-1)];
            bounds = [c_bounds; offset_bounds; tau_bounds];
            param0 = [x0(end); x0(end-1); x0(lifetimes)'];
            if numExponentials > 1
                A_bounds = [lb(lifetimes+numExponentials)' ub(lifetimes+numExponentials)'];
                A_bounds(end,:) = [0 1];
                A = x0(lifetimes+numExponentials)';
                A(end) = 1-sum(A(1:end-1));

                bounds = [bounds; A_bounds];
                param0 = [param0; A];
            end
    end

    %%% Initialize variables
    Shift = nan(rows, cols);
    Offset = nan(rows, cols);
    Tau = nan(rows,cols,numExponentials);
    Amp = nan(rows,cols,numExponentials);
    Chi2 = nan(rows, cols);

    
    %%% makes guess on pixel with highest peak
    guess = h.Mia_FLIM.Ch(s).Guess.Value;
    if guess
        [~,idx] = max(BinnedIm, [], 'all');
        [m, n, ~] = ind2sub([rows, cols, frames], idx);
        Decay = squeeze(BinnedIm(m,n,:));
        [shift, ~, ~, tau] = ...
            fit_exp_simplex(irf, Decay, TACRange, TACChannelWidth, x0(end), x0(lifetimes)', bounds, false, true, FitType, ignore, ResWeight);
        x0(end) = shift;
        x0(lifetimes) = tau;
        disp('Fitting with the initial tau values from distributed fit:')
        disp(tau')
    end

    
    switch OptimMethod
        case 1
            c0 = x0(end);
            tau0 = x0(lifetimes)';
            FitFun = @(x) fit_exp_simplex(irf, x, TACRange, TACChannelWidth, c0, tau0, bounds, false, true, FitType, ignore, ResWeight);
        case 2
            options = optimoptions(@fmincon,'Algorithm','trust-region-reflective',...
                'SpecifyObjectiveGradient',true, 'Display', 'none');
            FitFun = @(x) fit_exp_fmincon(irf, x, TACRange, TACChannelWidth, param0, bounds, false, true, FitType, ignore, ResWeight, options);
    end

    %% Iterate over all pixels and fit
    tic
    for i = 1: rows
            %%% Updates progress bar
            Progress((i-1)/rows,...
                h.Mia_Progress_Axes,...
                h.Mia_Progress_Text,...
                'Calculating Decay Matrix:');
            if all(fixed(lifetimes)) && fixed(end) && (OptimMethod == 1) % special case where all non-linear parameters are fixed
                Decay = squeeze(BinnedIm(i,:,:));
                Decay = permute(Decay, [2 1]);
                [shift, offset, A, tau,~,~,chi] = fit_exp_simplex(irf, Decay, TACRange, TACChannelWidth, c0, tau0, bounds, false, false, FitType, ignore, ResWeight);
                Shift(i,:) = shift;
                Offset(i,:) = offset;
                Tau(i,:,:) = repmat(reshape(tau, 1, 1, []), [1 size(Tau, 2) 1]);
                Amp(i,:,:) = reshape(permute(A, [2 1]), [1 size(Amp,[2 3])]);
                Chi2(i,:) = chi;
            else
                parfor j = 1: cols
                    Decay = squeeze(BinnedIm(i,j,:));
                    [shift, offset, A, tau, ~, ~, chi] = FitFun(Decay);
                    Shift(i,j) = shift;
                    Offset(i,j) = offset;
                    Tau(i,j,:) = tau;
                    Amp(i,j,:) = A;
                    Chi2(i,j) = chi;
                end
            end
    end
    toc
    Progress(1);
    figure;
    histogram(Tau(:));

    
    %%% Convert Fraction from Amplitude (species) fraction to Intensity fraction
    %%% (i.e. correct for brightness)
    %%% Intensity is proportional to tau*amplitude
    Amp = Amp ./ sum(Amp, 3);
    if numExponentials > 1
        f = Amp.*Tau;
        f = f./sum(f, 3);
        TauM = sum(Tau .* f, 3);
    else
        TauM = Tau;
    end

    MIAData.FLIM.Ch{s}.TauM = TauM;
    MIAData.FLIM.Ch{s}.Shift = Shift;
    MIAData.FLIM.Ch{s}.Offset = Offset;
    MIAData.FLIM.Ch{s}.Tau = Tau;
    MIAData.FLIM.Ch{s}.Amp = Amp;
    MIAData.FLIM.Ch{s}.Chi2 = Chi2;
end
%% Updates filename display
if numel(MIAData.FileName)==2
    h.Mia_Progress_Text.String = [MIAData.FileName{1}{1} ' / ' MIAData.FileName{2}{1}];
elseif numel(MIAData.FileName)==1
    h.Mia_Progress_Text.String = MIAData.FileName{1}{1};
else
    h.Mia_Progress_Text.String = 'Nothing loaded';
end
h.Mia_Progress_Axes.Color=UserValues.Look.Control;

%%% function to export FLIM analysis
function FLIM_Export(~,~,ext)
global MIAData UserValues
h = guidata(findobj('Tag', 'Mia'));

%if isempty(ext)
[filename, filepath, filteridx] = uiputfile({'*.mat','MAT-files (*.mat)'; ...
    '*.tif;*.tiff', 'TIFF-files (*.tif,*.tiff)'}, 'Save FLIM analysis as', UserValues.File.MIAPath);
if isequal(filename,0) || isequal(filepath,0)
    return;
end
switch filteridx
    case 1
        MIAData.FLIM.MI_Bins_Raw = str2double(h.Mia_Image.Calculations.MI_Bins.String);
        MIAData.FLIM.MI_Rebin_Factor = str2double(h.Mia_Image.Calculations.MI_Rebinning_Factor.String);
        if isfield(MIAData, 'ROI') % saves imported ROI data
            SaveData.ROI = MIAData.ROI;
        end
        SaveData.FLIM = MIAData.FLIM;
        save(fullfile(filepath, filename), 'SaveData');
        
    case 2
        for s = 1:2
            ch = h.Mia_FLIM.Ch(s).Channel.Value;
            if ch > size(MIAData.Data,1)
                continue;
            end

            channel_filename = [extractBefore(filename, '.tif') '_Ch' num2str(s) '.tif'];
            File = fullfile(filepath,channel_filename);
            
            [rows, cols, ~] = size(MIAData.Data{ch,1});

            Tagstruct.ImageLength = rows;
            Tagstruct.ImageWidth = cols;
            Tagstruct.Compression = 1; %1==None; 5==LZW
            Tagstruct.SampleFormat = Tiff.SampleFormat.IEEEFP; %floating point
            Tagstruct.Photometric = Tiff.Photometric.MinIsBlack;
            Tagstruct.BitsPerSample = 32;                        %32= float data, 16= Andor standard sampling
            Tagstruct.SamplesPerPixel = 1;
            Tagstruct.PlanarConfiguration = Tiff.PlanarConfiguration.Chunky;

            ImLabels = {'Intensity', 'TauM', 'Tau1', 'a1', 'Tau2', 'a2', 'Tau3', 'a3', 'Chi2', 'Shift'};
            numTau = size(MIAData.FLIM.Ch{s}.Tau, 3);
            match_tau = regexpPattern(['Tau[' num2str(numTau)+1 '-9]']);
            match_amp = regexpPattern(['a[' num2str(numTau)+1 '-9]']);
            ImLabels(contains(ImLabels, match_tau)|contains(ImLabels, match_amp)) = [];

            TIFF_handle = Tiff(File, 'w');

            for j=1:length(ImLabels)
                Tagstruct.ImageDescription = ['Label: ' ImLabels{j} '\n',...
                    'FrameTime [s]: ' h.Mia_Image.Settings.Image_Frame.String '\n',...
                    'LineTime [ms]: ' h.Mia_Image.Settings.Image_Line.String '\n',...
                    'PixelTime [us]: ' h.Mia_Image.Settings.Image_Pixel.String '\n',...
                    'PixelSize [nm]: ' h.Mia_Image.Settings.Pixel_Size.String '\n',...
                    'TACRange [ns]: ' h.Mia_Image.Calculations.TACRange.String '\n',...
                    'MI_Bins: ' h.Mia_Image.Calculations.MI_Bins.String];
                TIFF_handle.setTag(Tagstruct);

                switch ImLabels{j}
                    case 'Intensity'
                        Data = MIAData.FLIM.Ch{s}.Intensity;
                    case 'TauM'
                        Data = MIAData.FLIM.Ch{s}.TauM;
                    case 'Tau1'
                        Data = MIAData.FLIM.Ch{s}.Tau(:,:,1);
                    case 'a1'
                        Data = MIAData.FLIM.Ch{s}.Amp(:,:,1);
                    case 'Tau2'
                        Data = MIAData.FLIM.Ch{s}.Tau(:,:,2);
                    case 'a2'
                        Data = MIAData.FLIM.Ch{s}.Amp(:,:,2);
                    case 'Tau3'
                        Data = MIAData.FLIM.Ch{s}.Tau(:,:,3);
                    case 'a3'
                        Data = MIAData.FLIM.Ch{s}.Amp(:,:,3);
                    case 'Chi2'
                        Data = MIAData.FLIM.Ch{s}.Chi2;
                    case 'Shift'
                        Data = MIAData.FLIM.Ch{s}.Shift;
                end
                Data = single(Data);
                TIFF_handle.write(Data);

                if j<length(ImLabels)
                    TIFF_handle.writeDirectory();
                end
            end
            TIFF_handle.close()
        end
end
%end


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Export ROI as Binary Mask %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function Export_ROI(~,~,mode)
global MIAData UserValues
h = guidata(findobj('Tag','Mia'));
ch = h.Mia_NB.Image.Channel.Value;

ROI=[str2num(h.Mia_Image.Settings.ROI_SizeX.String)...
    str2num(h.Mia_Image.Settings.ROI_SizeY.String)...
    str2num(h.Mia_Image.Settings.ROI_PosX.String)...
    str2num(h.Mia_Image.Settings.ROI_PosY.String)];
switch mode
    case 1
        Mask=MIAData.NB.Use;
end

%Dialog for saving file
[FileName,Path] = uiputfile('*.mat', 'Save ROI as...', UserValues.File.MIAPath);
if ~(isequal(FileName,0) || isequal(Path,0))
   f = strcat(Path,FileName);
   save(f,'ch', 'ROI','Mask');
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Import ROI exported with Export_ROI function%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function Import_ROI(~,~)
global MIAData UserValues
h = guidata(findobj('Tag','Mia'));
% put UI to arbitrary ROI and set to values that will not apply it
h.Mia_Image.Settings.ROI_FramesUse.Value = 3;
MIA_Various([],[],3);
MIA_Various([],[],7);

[FileName,Path] = uigetfile('*.mat', 'Load ROI', UserValues.File.MIAPath);
if ~(isequal(FileName,0) || isequal(Path,0))
    info = load(strcat(Path,FileName));
    % info.ROI = [sizeX sizeY posX PosY]
    % info.Mask = ROI-sized single frame mask defined by the thresholds in N&B
    % info.ch = the channel from which the mask was calculated in N&B

    if isfield(info, 'ROI')
        %Set ROI size and position to the one exported from N&B
        h.Mia_Image.Settings.ROI_SizeX.String = num2str(info.ROI(1,1));
        h.Mia_Image.Settings.ROI_SizeY.String = num2str(info.ROI(1,2));
        h.Mia_Image.Settings.ROI_PosX.String = num2str(info.ROI(1,3));
        h.Mia_Image.Settings.ROI_PosY.String = num2str(info.ROI(1,4));
    end
    %Update ROI position
    Mia_ROI([],[],1)
    if ~isfield(info, 'Mask')
        a = struct2cell(info);
        info.Mask = a{1};
        info.ch = 1;
    end
    %Merge loaded ROI with existing (single-frame) arbitrary region
    if prod(size(MIAData.MS{1}) == size(info.Mask))&&(info.ch==1)
        MIAData.MS{1} = MIAData.MS{1} & info.Mask;
        MIAData.MS{2} = MIAData.MS{1};
    end
    if prod(size(MIAData.MS{2}) == size(info.Mask))&&(info.ch==3)     
        MIAData.MS{2} = MIAData.MS{2} & info.Mask;
    end
    %Update images
    Mia_Correct([],[],0);
    % set the ROI popupmenu to Arbitrary
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Import Cellpose output (_seg.npy) %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function Mia_Import_Cellpose(src,~)
global MIAData UserValues
h = guidata(findobj('Tag','Mia'));

[FileName,Path] = uigetfile('*.npy', 'Select Cellpose output for import', UserValues.File.MIAPath);
if isequal(FileName,0) || isequal(Path,0)
    return;
end

MIAData.ROI.Cellpose = ImportCellpose(fullfile(Path, FileName));
MIAData.ROI.Cellpose.Regions = regionprops(MIAData.ROI.Cellpose.masks, 'PixelIdxList', 'PixelList');

num_regions = max(MIAData.ROI.Cellpose.masks(:));
cell_boundaries = arrayfun(@(x) bwboundaries(MIAData.ROI.Cellpose.masks == x, 'noholes', 'CoordinateOrder', 'xy'), 1:num_regions, 'UniformOutput', false);
cell_boundaries = vertcat(cell_boundaries{:});
cell_colors = double(MIAData.ROI.Cellpose.colors)./255;
ax = h.Mia_FLIM.Axes(1,1:2);

for n = 1:num_regions
    roi(1) = drawfreehand(ax(1), 'Position', cell_boundaries{n}, 'Tag', ['Cellpose '  num2str(n)]);
    set(roi(1), 'Color', cell_colors(n,:), 'FaceAlpha', 0.1, 'Deletable', false, 'InteractionsAllowed', 'none',...
        'Label', num2str(n), 'LabelVisible', 'hover');
    roi(2) = copyobj(roi(1), ax(2));
    linked_props = {'Position'};
    % update global variables with roi handles and link
    if ~isfield(MIAData.FLIM, 'ROI') | isempty(MIAData.FLIM.ROI)
        MIAData.FLIM.ROI = roi;
        MIAData.FLIM.ROI_Link = linkprop(roi, linked_props);
    else
        MIAData.FLIM.ROI(end+1,:) = roi;
        MIAData.FLIM.ROI_Link(end+1) = linkprop(roi, linked_props);
    end
end
FLIM_Update_ROI_Table(MIAData.FLIM.ROI,[],[]);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Import FLIM analysis (.mat) %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function Mia_Import_FLIM(~,~)
global MIAData UserValues
h = guidata(findobj('Tag','Mia'));

[FileName,Path] = uigetfile('*.mat', 'Select FLIM analysis to import', UserValues.File.MIAPath);
if isequal(FileName,0) || isequal(Path,0)
    return;
end

Data = load(fullfile(Path, FileName), '-mat');

MIAData.FLIM = Data.SaveData.FLIM;
if isfield(Data.SaveData, 'ROI')
    MIAData.ROI = Data.SaveData.ROI;
end

function MIA_Various(Obj,~,mode)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Collection of small callbacks and functions %%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% 1:      Toggle logical in UserData of UI
%%% 1.5:    Toggle display of axes
%%% 2:      Updates additional parameters plots
%%% 3:      Hide\Show Arbitrary region controls
%%% 4:      Save TICS manual selection
%%% 5:      Display Count rate axes in kHz or a.u.
%%% 6:      Callback for channel-specific AROI settings
%%% 7:      Reset the AROI values to default
%%% 8:      Update Scale bar when pixel size is changed
%%% 9:      Raster scanning or not
%%% 10:     Toggle right-click context menu for left plot
%%% 11:     Do RICS popupmenu is toggled 
%%% 12:     FRAP tab
%%% 13:     Calculation tab
%%% 14:     Parallel processing menu 

h = guidata(findobj('Tag','Mia'));
global MIAData UserValues
for i=mode
    switch i
        case 1 %%% Toggle logical in UserData of UI
            %% e.g. change axes labels of different plots
            Obj.UserData = ~Obj.UserData;
        case 1.5
            %% change between photon counting and analog
            UserValues.MIA.Options.kHz = h.Mia_Image.Settings.kHz.Value;
            LSUserValues(1)
            if h.Mia_Image.Settings.kHz.Value == 1
                h.Mia_Image.Intensity_Axes.YLabel.UserData = 1;
            else
                h.Mia_Image.Intensity_Axes.YLabel.UserData = 0;
            end
            if ~isempty(MIAData.Data)
                Update_Plots([],[],4);
            end
        case 2 %%% Updates additional parameters plots
            Update_Plots([],[],4);
        case 3 %%% Hide\Show Arbitrary region controls
            switch h.Mia_Image.Settings.ROI_FramesUse.Value
                case {1,2} %%% Hide arbitrary region controls
                    Mia_Freehand([],[],3) % clear the manually drawn ROI
                    MIAData.AR = [];
                    h.Mia_Image.Settings.ROI_AR_Int_Fold_Max.Visible = 'off';
                    h.Mia_Image.Settings.ROI_AR_Int_Fold_Min.Visible = 'off';
                    h.Mia_Image.Settings.ROI_AR_Int_Min.Visible = 'off';
                    h.Mia_Image.Settings.ROI_AR_Int_Max.Visible = 'off';
                    h.Mia_Image.Settings.ROI_AR_Channel.Visible = 'off';
                    h.Mia_Image.Settings.ROI_AR_Same.Visible = 'off';
                    h.Mia_Image.Settings.ROI_AR_Sub1.Visible = 'off';
                    h.Mia_Image.Settings.ROI_AR_Sub2.Visible = 'off';
                    h.Mia_Image.Settings.ROI_AR_Var_Fold_Max.Visible = 'off';
                    h.Mia_Image.Settings.ROI_AR_Var_Fold_Min.Visible = 'off';
                    h.Mia_Image.Settings.ROI_AR_Spatial_Int.Visible = 'off';
                    h.Mia_Image.Settings.ROI_AR_median.Visible = 'off';
                    h.Mia_Image.Settings.ROI_AR_Reset.Visible = 'off';
                    h.Mia_Image.Settings.ROI_AR_LeftContextMenu.Visible = 'off';
                    h.Mia_Image.Settings.ROI_AR_ThresholdCorr.Visible = 'off';
                    for j=1:numel(h.Mia_Image.Settings.ROI_AR_Text)
                        h.Mia_Image.Settings.ROI_AR_Text{j}.Visible = 'off';
                    end
                    h.Mia_Image.Select_Manual_ROI.Visible = 'off';
                    h.Mia_Image.Unselect_Manual_ROI.Visible = 'off';
                    h.Mia_Image.Clear_Manual_ROI.Visible = 'off';
                    % right-click left image to drag normal ROI
                    h.Plots.Image(1,1).UIContextMenu = [];
                    h.Plots.Image(2,1).UIContextMenu = [];
                    h.Plots.Image(1,1).ButtonDownFcn = {@Mia_ROI,2};
                    h.Plots.Image(2,1).ButtonDownFcn = {@Mia_ROI,2};
                    Update_Plots([],[],[1,4]);
                case 3 %%% Show arbitrary region controls
                    h.Mia_Image.Settings.ROI_AR_Int_Fold_Max.Visible = 'on';
                    h.Mia_Image.Settings.ROI_AR_Int_Fold_Min.Visible = 'on';
                    h.Mia_Image.Settings.ROI_AR_Int_Min.Visible = 'on';
                    h.Mia_Image.Settings.ROI_AR_Int_Max.Visible = 'on';
                    h.Mia_Image.Settings.ROI_AR_Channel.Visible = 'on';
                    h.Mia_Image.Settings.ROI_AR_Same.Visible = 'on';
                    h.Mia_Image.Settings.ROI_AR_Sub1.Visible = 'on';
                    h.Mia_Image.Settings.ROI_AR_Sub2.Visible = 'on';
                    h.Mia_Image.Settings.ROI_AR_Var_Fold_Max.Visible = 'on';
                    h.Mia_Image.Settings.ROI_AR_Var_Fold_Min.Visible = 'on';
                    h.Mia_Image.Settings.ROI_AR_Spatial_Int.Visible = 'on';
                    h.Mia_Image.Settings.ROI_AR_median.Visible = 'on';
                    h.Mia_Image.Settings.ROI_AR_Reset.Visible = 'on';
                    h.Mia_Image.Settings.ROI_AR_LeftContextMenu.Visible = 'on';
                    h.Mia_Image.Settings.ROI_AR_ThresholdCorr.Visible = 'on';
                    for j=1:numel(h.Mia_Image.Settings.ROI_AR_Text)
                        h.Mia_Image.Settings.ROI_AR_Text{j}.Visible = 'on';
                    end
                    h.Mia_Image.Select_Manual_ROI.Visible = 'on';
                    h.Mia_Image.Unselect_Manual_ROI.Visible = 'on';
                    h.Mia_Image.Clear_Manual_ROI.Visible = 'on';
                    % right-click left image to draw AROI
                    if UserValues.MIA.Options.LeftContextMenu
                        h.Plots.Image(1,1).UIContextMenu = h.Mia_Image.Menu;
                        h.Plots.Image(2,1).UIContextMenu = h.Mia_Image.Menu;
                        h.Plots.Image(1,1).ButtonDownFcn = [];
                        h.Plots.Image(2,1).ButtonDownFcn = [];
                    else
                        h.Plots.Image(1,1).UIContextMenu = [];
                        h.Plots.Image(2,1).UIContextMenu = [];
                        h.Plots.Image(1,1).ButtonDownFcn = {@Mia_ROI,2};
                        h.Plots.Image(2,1).ButtonDownFcn = {@Mia_ROI,2};
                    end
                    Mia_Correct([],[],1);
            end
        case 4
            %% Set the background to 0
            tmp(1) = str2double(h.Mia_Image.Settings.Background(1).String);
            tmp(2) = str2double(h.Mia_Image.Settings.Background(2).String);
            h.Mia_Image.Settings.Background(1).String = '0';
            h.Mia_Image.Settings.Background(2).String = '0';
            Mia_Correct([],[],1)
            %% Get the background from the current ROI
            h.Mia_Image.Settings.Background(1).String = num2str(tmp(1));
            h.Mia_Image.Settings.Background(2).String = num2str(tmp(2));
            if size(MIAData.Data,1)==1 && size(MIAData.Data,2)==2
                h.Mia_Image.Settings.Background(1).String = num2str(mean2(MIAData.Data{1,2}));
                h.Mia_Image.Settings.Background(2).String = '0';
            elseif size(MIAData.Data,1)>1 && size(MIAData.Data,2)==2
                h.Mia_Image.Settings.Background(1).String = num2str(mean2(MIAData.Data{1,2}));
                h.Mia_Image.Settings.Background(2).String = num2str(mean2(MIAData.Data{2,2}));
            else
                h.Mia_Image.Settings.Background(1).String = '0';
                h.Mia_Image.Settings.Background(2).String = '0';
            end
            UserValues.MIA.BG(h.Mia_Image.Settings.Channel_PIE(1).Value) = str2double(h.Mia_Image.Settings.Background(1).String);
            UserValues.MIA.BG(h.Mia_Image.Settings.Channel_PIE(2).Value) = str2double(h.Mia_Image.Settings.Background(2).String);
            LSUserValues(1)
            Mia_Correct([],[],1)
        case 5
            %% Select channel for AROI
            channel = h.Mia_Image.Settings.ROI_AR_Channel.Value-1;
            if channel == 0 %All
                for ch = 1:size(MIAData.Data,1)
                    UserValues.MIA.AR_Int(ch) = str2double(h.Mia_Image.Settings.ROI_AR_Int_Min.String);
                    UserValues.MIA.AR_Int(10+ch) = str2double(h.Mia_Image.Settings.ROI_AR_Int_Max.String);
                    UserValues.MIA.AR_Int_Fold(ch) = str2double(h.Mia_Image.Settings.ROI_AR_Int_Fold_Min.String);
                    UserValues.MIA.AR_Int_Fold(10+ch) = str2double(h.Mia_Image.Settings.ROI_AR_Int_Fold_Max.String);
                    UserValues.MIA.AR_Var_Fold(ch) = str2double(h.Mia_Image.Settings.ROI_AR_Var_Fold_Min.String);
                    UserValues.MIA.AR_Var_Fold(10+ch) = str2double(h.Mia_Image.Settings.ROI_AR_Var_Fold_Max.String);
                end
            else
                h.Mia_Image.Settings.ROI_AR_Int_Min.String = num2str(UserValues.MIA.AR_Int(channel));
                h.Mia_Image.Settings.ROI_AR_Int_Max.String = num2str(UserValues.MIA.AR_Int(10+channel));
                h.Mia_Image.Settings.ROI_AR_Int_Fold_Min.String = num2str(UserValues.MIA.AR_Int_Fold(channel));
                h.Mia_Image.Settings.ROI_AR_Int_Fold_Max.String = num2str(UserValues.MIA.AR_Int_Fold(10+channel));
                h.Mia_Image.Settings.ROI_AR_Var_Fold_Min.String = num2str(UserValues.MIA.AR_Var_Fold(channel));
                h.Mia_Image.Settings.ROI_AR_Var_Fold_Max.String = num2str(UserValues.MIA.AR_Var_Fold(10+channel));
            end
        case 6
            %% Callback for channel-specific AROI settings
            UserValues.MIA.AR_Region(1) = str2double(h.Mia_Image.Settings.ROI_AR_Sub1.String);
            UserValues.MIA.AR_Region(2) = str2double(h.Mia_Image.Settings.ROI_AR_Sub2.String);
            UserValues.MIA.AR_Same = h.Mia_Image.Settings.ROI_AR_Same.Value;
            UserValues.MIA.AR_Median = h.Mia_Image.Settings.ROI_AR_median.Value;
            UserValues.MIA.AR_Framewise = h.Mia_Image.Settings.ROI_AR_Spatial_Int.Value;
            % channel-specific stuff
            if h.Mia_Image.Settings.ROI_AR_Channel.Value == 1
                channel = 1:size(MIAData.Data,1);
            else
                channel = h.Mia_Image.Settings.ROI_AR_Channel.Value-1;
            end
            for ch = channel
                UserValues.MIA.AR_Int(ch) = str2double(h.Mia_Image.Settings.ROI_AR_Int_Min.String);
                UserValues.MIA.AR_Int(10+ch) = str2double(h.Mia_Image.Settings.ROI_AR_Int_Max.String);
                UserValues.MIA.AR_Int_Fold(ch) = str2double(h.Mia_Image.Settings.ROI_AR_Int_Fold_Min.String);
                UserValues.MIA.AR_Int_Fold(10+ch) = str2double(h.Mia_Image.Settings.ROI_AR_Int_Fold_Max.String);
                UserValues.MIA.AR_Var_Fold(ch) = str2double(h.Mia_Image.Settings.ROI_AR_Var_Fold_Min.String);
                UserValues.MIA.AR_Var_Fold(10+ch) = str2double(h.Mia_Image.Settings.ROI_AR_Var_Fold_Max.String);
            end
            LSUserValues(1)
            Mia_Correct([],[],1)
        case 7
            %% reset the AROI values to default
            % put UI to arbitrary ROI and set to values that will not apply it
            UserValues.MIA.AR_Int = [0 0 0 0 0 0 0 0 0 0 inf inf inf inf inf inf inf inf inf inf];
            UserValues.MIA.AR_Int_Fold = [0.001 0.001 0.001 0.001 0.001 0.001 0.001 0.001 0.001 0.001 1000 1000 1000 1000 1000 1000 1000 1000 1000 1000];
            UserValues.MIA.AR_Var_Fold = [0.001 0.001 0.001 0.001 0.001 0.001 0.001 0.001 0.001 0.001 1000 1000 1000 1000 1000 1000 1000 1000 1000 1000];
            UserValues.MIA.AR_Region = [5 10];
            UserValues.MIA.AR_Same = 1;
            UserValues.MIA.AR_Median = 1;
            UserValues.MIA.AR_Framewise = 1;
            LSUserValues(1)
            MIA_Various([],[],5); %display the channel-specific values in the UI
            h.Mia_Image.Settings.ROI_AR_Same.Value = UserValues.MIA.AR_Same;
            h.Mia_Image.Settings.ROI_AR_median.Value = UserValues.MIA.AR_Median;
            h.Mia_Image.Settings.ROI_AR_Spatial_Int.Value = UserValues.MIA.AR_Framewise;
            Mia_Correct([],[],1)
        case 8
            %% Update Scale bar when pixel size is changed
            Update_Plots([],[],1);
            Update_Plots([],[],6);
        case 9
            %% raster scanning or not
            UserValues.MIA.Options.scanning = h.Mia_Image.Settings.scanning.Value;
            LSUserValues(1)
            if h.Mia_Image.Settings.scanning.Value == 1
                h.Mia_Image.Settings.Image_Line_Text.Visible = 'on';
                h.Mia_Image.Settings.Image_Line.Visible = 'on';
                h.Mia_Image.Settings.Image_Pixel_text.Visible = 'on';
                h.Mia_Image.Settings.Image_Pixel.String = num2str(UserValues.MIA.FitParams.PixTime);
                h.Mia_Image.Settings.Image_Pixel.Visible = 'on';
            else
                h.Mia_Image.Settings.Image_Line_Text.Visible = 'off';
                h.Mia_Image.Settings.Image_Line.Visible = 'off';
                h.Mia_Image.Settings.Image_Pixel_text.Visible = 'off';
                h.Mia_Image.Settings.Image_Pixel.String = '1000';
                h.Mia_Image.Settings.Image_Pixel.Visible = 'off';
            end
            if ~isempty(MIAData.Data)
                Update_Plots([],[],4);
            end
        case 10
            %% toggle right-click context menu for left plot
            UserValues.MIA.Options.LeftContextMenu = h.Mia_Image.Settings.ROI_AR_LeftContextMenu.Value;
            MIA_Various([],[],3)
            LSUserValues(1)
        case 11
            if strcmp(h.Mia_Image.Calculations.Cor_Save_ICS.String{h.Mia_Image.Calculations.Cor_Save_ICS.Value},'intensity-based map') 
                h.Mia_Image.Calculations.Cor_ICS_nAROI_txt.Visible = 'on';
                h.Mia_Image.Calculations.Cor_ICS_nAROI.Visible = 'on';
                h.Mia_Image.Calculations.Cor_ICS_Scaling.Value = 3;
                h.Mia_Image.Calculations.Cor_ICS_Scaling.String = {'Do not scale map brightness', 'Scale maps with counts', 'Scale maps with pixel sampling'};
                h.Mia_Image.Calculations.Cor_ICS_Scaling.Visible = 'on';
            elseif strcmp(h.Mia_Image.Calculations.Cor_Save_ICS.String{h.Mia_Image.Calculations.Cor_Save_ICS.Value},'grid-based map')
                h.Mia_Image.Calculations.Cor_ICS_nAROI_txt.Visible = 'off';
                h.Mia_Image.Calculations.Cor_ICS_nAROI.Visible = 'off';
                h.Mia_Image.Calculations.Cor_ICS_Scaling.Value = 2;
                h.Mia_Image.Calculations.Cor_ICS_Scaling.String = {'Do not scale map brightness', 'Scale maps with counts'};
                h.Mia_Image.Calculations.Cor_ICS_Scaling.Visible = 'on';
            else
                h.Mia_Image.Calculations.Cor_ICS_nAROI_txt.Visible = 'off';
                h.Mia_Image.Calculations.Cor_ICS_nAROI.Visible = 'off';
                h.Mia_Image.Calculations.Cor_ICS_Scaling.Visible = 'off';
            end
        case 12
            %% FRAP options callback
            %%% sets the channel to use
            numChannels = size(MIAData.Data, 1);
            channel = 1:numChannels == h.Mia_Image.Calculations.FRAP_Channel.Value...
                | numChannels < h.Mia_Image.Calculations.FRAP_Channel.Value;
            %%% Circle
            if h.Mia_Image.Calculations.FRAP_ROI_Type.Value == 1
                %%% enable menu items in FRAP panel
                MenuItem = findobj(h.Mia_Image.Calculations.FRAP_Panel, 'Tag', 'FRAP_Pos',...
                    '-or', 'Tag', 'FRAP_Pos_Circle');
                set(MenuItem, 'Visible', 'on')
                %%% get circle position and radius
                posX = str2double(h.Mia_Image.Calculations.FRAP_Spot_X.String);
                posY = str2double(h.Mia_Image.Calculations.FRAP_Spot_Y.String);
                radius = str2double(h.Mia_Image.Calculations.FRAP_Spot_Radius.String);
                %%% plot circle ROI on corresponding axes
                roi = findobj(h.Mia_Image.Axes(channel, 2), 'Tag', 'FRAP_Circle');
                if size(roi, 1) ~= sum(channel)
                    delete(roi)
                    for ch = 1:numChannels
                        roi(i) = images.roi.Circle(h.Mia_Image.Axes(ch, 2),...
                            'Center', [posX posY], 'Radius', radius,...
                            'InteractionsAllowed', 'none', 'Tag', 'FRAP_Circle');
                    end
                else
                    set(roi, {'Visible', 'Center', 'Radius'},...
                        {'on', [posX posY], radius});
                end
                %%% hide ROI for non-selected channels
                roi = findobj(h.Mia_Image.Axes(~channel, 2), 'Tag', 'FRAP_Circle');
                set(roi, 'Visible', 'off')
            else
                %%% disable menu items in FRAP panel
                MenuItem = findobj(h.Mia_Image.Calculations.FRAP_Panel,...
                    'Tag', 'FRAP_Pos_Circle');
                set(MenuItem, 'Visible', 'off')
                %%% hide roi objects
                roi = findobj(h.Mia_Image.Axes, 'Tag', 'FRAP_Circle');
                set(roi, 'Visible', 'off')
            end
            %%% Rectangle
            if h.Mia_Image.Calculations.FRAP_ROI_Type.Value == 2
                %%% enable menu items in FRAP panel
                MenuItem = findobj(h.Mia_Image.Calculations.FRAP_Panel, 'Tag', 'FRAP_Pos',...
                    '-or', 'Tag', 'FRAP_Pos_Rectangle');
                set(MenuItem, 'Visible', 'on')
                %%% get rectangle position and size
                posX = str2double(h.Mia_Image.Calculations.FRAP_Spot_X.String);
                posY = str2double(h.Mia_Image.Calculations.FRAP_Spot_Y.String);
                sizeX = str2double(h.Mia_Image.Calculations.FRAP_Rect_SizeX.String);
                sizeY = str2double(h.Mia_Image.Calculations.FRAP_Rect_SizeY.String);
                %%% plot circle ROI on corresponding axes
                roi = findobj(h.Mia_Image.Axes(channel, 2), 'Tag', 'FRAP_Rectangle');
                if size(roi, 1) ~= sum(channel)
                    delete(roi)
                    for ch = 1:numChannels
                        roi(i) = images.roi.Rectangle(h.Mia_Image.Axes(ch, 2),...
                            'Position', [posX, posY, sizeX, sizeY],...
                            'InteractionsAllowed', 'none', 'Tag', 'FRAP_Rectangle');
                    end
                else
                    set(roi, {'Visible', 'Position'},...
                        {'on', [posX, posY, sizeX, sizeY]});
                end
                %%% hide ROI for non-selected channels
                roi = findobj(h.Mia_Image.Axes(~channel, 2), 'Tag', 'FRAP_Rectangle');
                set(roi, 'Visible', 'off')
            else
                %%% disable menu items in FRAP panel
                MenuItem = findobj(h.Mia_Image.Calculations.FRAP_Panel,...
                    'Tag', 'FRAP_Pos_Rectangle');
                set(MenuItem, 'Visible', 'off')
                %%% hide roi objects
                roi = findobj(h.Mia_Image.Axes, 'Tag', 'FRAP_Rectangle');
                set(roi, 'Visible', 'off')
            end
            %%% Freehand ROI
            if h.Mia_Image.Calculations.FRAP_ROI_Type.Value == 3 
                %%% disable menu items in FRAP panel
                MenuItem = findobj(h.Mia_Image.Calculations.FRAP_Panel, 'Tag', 'FRAP_Pos',...
                    '-or', 'Tag', 'FRAP_Pos_Circle', '-or', 'Tag', 'FRAP_Pos_Rectangle');
                set(MenuItem, 'Visible', 'off')
                %%% toggle context menu for drawing
                h.Mia_Image.FRAP_ROI_Menu.Visible = 'on';
                %%% toggle visibility of freehand roi
                roi = findobj(h.Mia_Image.Axes(channel,2), 'Tag', 'FRAP_Freehand');
                set(roi, 'Visible', 'on')
                roi = findobj(h.Mia_Image.Axes(~channel,2), 'Tag', 'FRAP_Freehand');
                set(roi, 'Visible', 'off')
            else
                h.Mia_Image.FRAP_ROI_Menu.Visible = 'off';
                roi = findobj(h.Mia_Image.Axes, 'Tag', 'FRAP_Freehand');
                set(roi, 'Visible', 'off')
            end
            
            %%% 2 exp fit
            if h.Mia_Image.Calculations.FRAP_Fit_Model.Value == 3
                TableData_FRAP2ExpFit = {   'A0', 1, 0, Inf, false;...
                'Amax1', 0.5, 0, Inf, false;...
                'k1', 0.1, 0, Inf, false;...
                'Amax2', 0.5, 0, Inf, false;...
                'k2', 0.01, 0, Inf, false};
                h.Mia_Image.Calculations.FRAP_FitOptions.Data = TableData_FRAP2ExpFit;
                h.Mia_Image.Calculations.FRAP_FitOptions.Visible = 'on';
            elseif h.Mia_Image.Calculations.FRAP_Fit_Model.Value == 4
                TableData_FRAP2ExpFit = {   'A0', 0, 0, Inf, false;...
                'A1', 0.5, 0, Inf, false;...
                'k1', 0.1, 0, Inf, false;...
                'A2', 0.5, 0, Inf, false;...
                'k2', 0.01, 0, Inf, false};
                h.Mia_Image.Calculations.FRAP_FitOptions.Data = TableData_FRAP2ExpFit;
                h.Mia_Image.Calculations.FRAP_FitOptions.Visible = 'on';
            else
                h.Mia_Image.Calculations.FRAP_FitOptions.Visible = 'off';
            end
            
            %%% cFRAP
            if h.Mia_Image.Calculations.FRAP_Fit_Model.Value == 5
                h.Mia_Image.Calculations.cFRAP_Options.Visible = 'on';
            else
                h.Mia_Image.Calculations.cFRAP_Options.Visible = 'off';
            end
        case 13
            %% User edited the Calculate tab
            %% Calculate the ratio image
            range = str2num(h.Mia_Image.Calculations.Divide.String);
            range2 = str2num(h.Mia_Image.Calculations.DivideBy.String);

            Image = mean(MIAData.Data{1,2}(:,:,range2),3)./mean(MIAData.Data{1,2}(:,:,range),3);
            %h.Mia_Image.Calculations.Calc_Min
            % h.Mia_Image.Calculations.Calc_Max
            %vg = mean(MIAData.Data{1,2}(:,:,range),3);
            Image(isnan(Image))=min(min(Image)); % do not make this 0 since the final colorbar will be affected

            if h.Mia_Image.Calculations.MedFilt.Value
                Image = medfilt2(Image, [3 3],'symmetric');
            end
            %% Apply the ratio image as a mask
            % this is the original hand-drawn mask
            if isempty(MIAData.MS{2,1})
                MIAData.MS{2,1} = MIAData.MS{1,1};
            end
            Mask = ones(size(Image));             
            Mask(Image<str2double(h.Mia_Image.Calculations.RatioThresholdLow.String))=0;
            Mask(Image>str2double(h.Mia_Image.Calculations.RatioThresholdHigh.String))=0;
            MIAData.MS{1,1} = MIAData.MS{2,1} & Mask;
            %Update images, time trace, histograms
            h.Mia_Image.Settings.Channel_PIE(1).Value = 1;
            Update_Plots([],[],[1,4])

            %% Plot the ratio image
            %ImageColor=jet(128);
            %%% Transforms intensity image to 64 bits
            %ImageSc = round(127*(Image-min(min(Image)))/(max(max(Image))-min(min(Image))))+1;
            %%% Applies colormap & Updates image and axis
            map = 'Image ratio';
            if isempty(find(strcmp(h.Mia_Image.Settings.Channel_PIE(1).String,map)))
                h.Mia_Image.Settings.Channel_PIE(1).String = [h.Mia_Image.Settings.Channel_PIE(1).String, map];
            end
            %h.Plots.Image(1,1).CData = reshape(ImageColor(ImageSc(:),:),size(Image,1),size(Image,2),3);
            AlphaRatio = 0.25;
            h.Plots.Image(1,1).AlphaData = (MIAData.MS{1}+AlphaRatio)/(1+AlphaRatio);

            d = find(strcmp(h.Mia_Image.Settings.Channel_PIE(1).String,map));
            %MIAData.Image{1,d} = h.Plots.Image(1,1).CData;
            %h.Mia_Image.Axes(1,1).XLim=[0 size(MIAData.Image{1,d},2)]+0.5;
            %h.Mia_Image.Axes(1,1).YLim=[0 size(MIAData.Image{1,d},1)]+0.5;
            h.Mia_Image.Settings.Channel_PIE(1).Value = d;

            MIAData.MapData{1,d} = Image;
            MIAData.MapScaling = 1;
            Update_Plots([],[],[1,4])
                   AlphaRatio = 0.25;
          h.Plots.Image(1,1).AlphaData = (MIAData.MS{1}+AlphaRatio)/(1+AlphaRatio);

            %h.Mia_Image.Intensity_Axes.YLim = [-1 1]; 
        case 14
            if Obj == h.Mia_UseParfor
                %%% Sets number of workers used for parpool to 0 or Inf
                if strcmp(Obj.Checked,'on')
                    Obj.Checked = 'off';
                    UserValues.Settings.Pam.ParallelProcessing = 0;
                else
                    Obj.Checked = 'on';
                    UserValues.Settings.Pam.ParallelProcessing = Inf;
                end
            elseif Obj == h.Mia_NumberOfCores
                %%% Opens input dialog and gets value
                NumberOfCores=inputdlg('New number of cores to use:','Specify the number of cores',1,{num2str(UserValues.Settings.Pam.NumberOfCores)});
                if ~isempty(NumberOfCores) %user didn't cancel
                    if isempty(NumberOfCores{1}) %user pressed ok without entering a value
                        NumberOfCores{1} = '100';
                    end
                    NumberOfCores = round(str2double(cell2mat(NumberOfCores)));
                    %%% compare with available cores
                    maxCores = feature('numCores');
                    NumberOfCores = min([maxCores,NumberOfCores]);
                    %%% make minimum of 2
                    if NumberOfCores < 2
                        NumberOfCores = 2;
                    end
                    h.Mia_NumberOfCores.Label=['Number of Cores: ' num2str(NumberOfCores)];
                    UserValues.Settings.Pam.NumberOfCores=NumberOfCores;
                end
            end
    end
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Function to update UserValues %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function Save_MIA_UserValues(h)
global UserValues

Save = false;
%%% Colormaps
if any(UserValues.MIA.ColorMap_Main ~= [h.Mia_Image.Settings.Channel_Colormap(1).Value; h.Mia_Image.Settings.Channel_Colormap(2).Value])
    UserValues.MIA.ColorMap_Main = [h.Mia_Image.Settings.Channel_Colormap(1).Value; h.Mia_Image.Settings.Channel_Colormap(2).Value];
    Save = true;
end
if any(UserValues.MIA.CustomColor ~= [h.Mia_Image.Settings.Channel_Colormap(1).UserData; h.Mia_Image.Settings.Channel_Colormap(2).UserData])
    UserValues.MIA.CustomColor = [h.Mia_Image.Settings.Channel_Colormap(1).UserData; h.Mia_Image.Settings.Channel_Colormap(2).UserData];
    Save = true;
end
%%% Image correction setting
if any(UserValues.MIA.Correct_Type ~= [h.Mia_Image.Settings.Correction_Subtract.Value h.Mia_Image.Settings.Correction_Add.Value])
    UserValues.MIA.Correct_Type = [h.Mia_Image.Settings.Correction_Subtract.Value h.Mia_Image.Settings.Correction_Add.Value];
    Save = true;
end
if any(UserValues.MIA.Correct_Sub_Values ~= [str2double(h.Mia_Image.Settings.Correction_Subtract_Pixel.String) str2double(h.Mia_Image.Settings.Correction_Subtract_Frames.String)])
    UserValues.MIA.Correct_Sub_Values = [str2double(h.Mia_Image.Settings.Correction_Subtract_Pixel.String) str2double(h.Mia_Image.Settings.Correction_Subtract_Frames.String)];
    Save = true;
end
if any(UserValues.MIA.Correct_Add_Values ~= [str2double(h.Mia_Image.Settings.Correction_Add_Pixel.String) str2double(h.Mia_Image.Settings.Correction_Add_Frames.String)])
    UserValues.MIA.Correct_Add_Values = [str2double(h.Mia_Image.Settings.Correction_Add_Pixel.String) str2double(h.Mia_Image.Settings.Correction_Add_Frames.String)];
    Save = true;
end
if Save
    LSUserValues(1);
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Function for exporting various things %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function Mia_Export(obj,~,mode,PathName)
% mode = 1: Save image of the specific axis
% mode = 2: Dual color raw image from menu
% mode = 3: Dual color corrected image from menu
% mode = 4: Video of channel 1 from menu
% mode = 5: Video of channel 2 from menu
% mode = 6: multi-panel autocorrelation heatmap from RICS popupmenu
% mode = 7: multi-panel cross-correlation heatmap from RICS popupmenu


global UserValues MIAData
h = guidata(findobj('Tag','Mia'));

if ~isvar('PathName')
    PathName = UserValues.File.MIAPath;
end

switch mode
    case 1
        h.Mia_Progress_Text.String = 'Exporting image';
        h.Mia_Progress_Axes.Color=[1 0 0];  
        %% Right- or shift-click image
        if ~strcmp(h.Mia.SelectionType,'extend') && ~strcmp(h.Mia.SelectionType,'open')
            return;
        end
        ff = figure('Visible','off');
        ax = obj.Parent;
        channel = str2double(obj.Parent.Tag(end)); %Tag is called ImRaw1, ImCorr1, ImRaw2 or ImCorr2
        obj = copyobj([ax, ax.Colorbar],ff); %copy axes and colorbar
        if size(obj,1) > 1 %colorbar exists
            obj(2).Color = [1 1 1];
            obj(2).FontSize = 12; %colorbar text color black
            obj(2).Title.Color = [1 1 1];
        end
        obj(1).Colormap = ax.Colormap; %set the axis colormap and the colorbar will follow
        obj(1).Position = [0.05 0.05 0.8 0.9]; %center image
        if numel(obj)==2
            obj(1).Children(1).Visible = 'off'; %hide the ROI rectangle
        end
        channelname = h.Mia_Image.Settings.Channel_PIE(channel).String{channel};
        ff.Position(3) = round(ff.Position(3)*0.85);
        ff.Color = [0 0 0]; %figure background
        ff.InvertHardcopy = 'off';%print the background
        %                 switch mode
        %                     case 1 %%% Exports current figure as TIFF
        %                         [FileName,PathName] = uiputfile({'*.tif'}, 'Save TIFF as', UserValues.File.ExportPath);
        %                         if FileName~=0
        %                             UserValues.File.ExportPath=PathName;
        %                             LSUserValues(1)
        %                             print(ff, '-dtiff', fullfile(PathName,FileName));
        %                             delete(ff)
        %                         end
        %                 end
        if ~h.Mia_Image.Settings.SaveImage.Value
            ff.Visible = 'on';
        else
            if ~h.Mia_Image.Calculations.Save_Name.Value
                %User doesn't want automatic filename
                [FileName,PathName] = uiputfile({'*.tif'}, 'Save TIFF as', fullfile(UserValues.File.MIAPath,[MIAData.FileName{1}{1}(1:end-4), '_', channelname, '_image.tif']));
            else
                type = find(strcmp(h.Mia_Image.Settings.Channel_PIE(channel).String{h.Mia_Image.Settings.Channel_PIE(channel).Value},UserValues.MIA.Maps));
                if type ~=0
                    annstr = [channelname ' - ' UserValues.MIA.Maps{type}, ' [', obj(2).Title.String, ']']; % annotation text
                    annpos=[0.1 0.9 0.8 0.05];
                    annotation('textbox',annpos,'string',annstr,'fontsize',16,'edgecolor','none','HorizontalAlignment','center','backgroundcolor','none', 'color', 'w','interpreter','tex');

                    name = ['_', UserValues.MIA.Maps{type}];
                else
                    name = '_correctedimage';
                end
                FileName = [MIAData.FileName{1}{1}(1:end-4), '_', channelname, name];
            end
            if FileName~=0
                UserValues.File.MIAPath=PathName;
                LSUserValues(1)
                print(ff, '-dpng', '-r300', fullfile(PathName,FileName));
                % % save a 16 bit converted copy of the heatmap
                % im = MIAData.MapData{channel,h.Mia_Image.Settings.Channel_PIE(channel).Value};
                % maxim = max(max(im));
                % im = uint16(im./maxim*65536);
                % imwrite(im,fullfile(PathName,[FileName, '_', num2str(maxim/65536), '.tif']),'tif');
                delete(ff)
            end
            return
            if any(FileName~=0)
                UserValues.File.ExportPath=PathName;
                LSUserValues(1)
                Image=single(obj.CData);
                if strcmp(obj.Tag, 'imTICS')
                    Image(isnan(Image))=0;
                end

                if size(Image,3)==3
                    Image=Image/max(Image(:))*255;
                else
                    if strcmp(obj.Tag, 'imTICS')
                        mini = obj.Parent.CLim(1);
                        maxi = obj.Parent.CLim(2);
                        Image(Image<mini)=mini;
                        Image(Image>maxi)=maxi;
                        s = 255; %TICS images have a 255-element 8bit colorbar
                        a = obj.AlphaData;
                        a(a==0.2)=0; %hide masked pixels
                    else
                        if h.Mia_Image.Settings.AutoScale.Value == 1
                            %just avoid negative values
                            mini = min(min(Image));
                            maxi = max(max(Image));
                        elseif h.Mia_Image.Settings.AutoScale.Value == 3
                            % if manual scale, first include all values within the range
                            mini = obj.Parent.CLim(1);
                            maxi = obj.Parent.CLim(2);
                            Image(Image<mini)=mini;
                            Image(Image>maxi)=maxi;
                        end
                        s = 63; %Imag tab images have a 63-element 6bit colorbar
                        a = obj.AlphaData;
                    end
                    %the manual colormap will be only 6bit so rescale the image between 64 positive gray values
                    Image=(Image-mini)/(maxi-mini)*s;

                    % The cmap is only 6 bit
                    cmap=colormap(obj.Parent);

                    r=cmap(:,1)*255; g=cmap(:,2)*255; b=cmap(:,3)*255;
                    %CData = round((Image-min(Image(:)))/(max(Image(:))-min(Image(:)))*(size(cmap,1)-1))+1;
                    CData = round(Image)+1;
                    CData(CData>s)=s;
                    CData(CData<0)=0;

                    Image(:,:,1) = reshape(r(CData),size(CData));
                    Image(:,:,2) = reshape(g(CData),size(CData));
                    Image(:,:,3) = reshape(b(CData),size(CData));

                    if numel(a)>1 %%% When transparency is used to show unselected regions
                        Image(:,:,1) = Image(:,:,1).*a + 255*(1-a)*obj.Parent.Color(1);
                        Image(:,:,2) = Image(:,:,2).*a + 255*(1-a)*obj.Parent.Color(2);
                        Image(:,:,3) = Image(:,:,3).*a + 255*(1-a)*obj.Parent.Color(3);
                    end

                    barx = str2double(h.Mia_Image.Settings.ScaleBar.String);
                    if barx~=0 && ~isnan(barx)
                        pixsize = str2double(h.Mia_Image.Settings.Pixel_Size.String)/1000; %in um
                        roix = str2double(h.Mia_Image.Settings.ROI_SizeX.String); %ROI size is always smaller than imsize so ok.
                        roiy = str2double(h.Mia_Image.Settings.ROI_SizeY.String);
                        barwidth = floor(barx/pixsize); %in pixels
                        y = floor(roiy/40);
                        x = floor(roix/40);
                        for i = 1:3
                            Image(1+3*y:1+4*y,end-barwidth-4*x:end-4*x,i) = 255;
                        end
                    end
                end
            end
            % Save Image
            Image = flipud(Image);
            imwrite(uint8(Image),fullfile(PathName,FileName));
        end
    case {6,7}
        % Multi-panel heatmaps
        %6: single color: Int, D, N, Brightness map
        %7: two color: D1, D2, DC, relCC

        h.Mia_Progress_Text.String = 'Exporting image';
        h.Mia_Progress_Axes.Color=[1 0 0];

        channel = 1; %str2double(obj.Parent.Tag(end)); %Tag is called ImRaw1, ImCorr1, ImRaw2 or ImCorr2
        ff = figure('Visible','off');
        ff.Position(3)=ff.Position(3)*1.9;
        ff.Position(4)=ff.Position(4)*2;

        for i = 1:4
            if i == 1
                if mode == 6
                    d = find(strcmp(h.Mia_Image.Settings.Channel_PIE(1).String,'Intensity map'));
                    offset = 0;
                else
                    d = find(strcmp(h.Mia_Image.Settings.Channel_PIE(1).String,'Diffusion map'));
                    offset = 0.01; 
                end
                pos = [0.01 0.51 0.43 0.43];
            elseif i == 2
                if mode == 6
                    d = find(strcmp(h.Mia_Image.Settings.Channel_PIE(1).String,'Diffusion map'));
                    offset = 0.01; 
                else
                    d = find(strcmp(h.Mia_Image.Settings.Channel_PIE(2).String,'Diffusion map'));
                    offset = 0.01; 
                    channel = 2;
                end
                pos = [0.52 0.51 0.43 0.43];
            elseif i == 3
                channel = 1;
                if mode == 6
                    d = find(strcmp(h.Mia_Image.Settings.Channel_PIE(1).String,'Concentration map'));
                    offset = 0;
                else
                    d = find(strcmp(h.Mia_Image.Settings.Channel_PIE(1).String,'Co-diffusion map'));
                    offset = 0.01; 
                end
                pos = [0.02 0.01 0.43 0.43];
            else %i = 4
                channel = 1;
                if mode == 6
                    d = find(strcmp(h.Mia_Image.Settings.Channel_PIE(1).String,'Brightness map'));
                    offset = 0;
                else
                    d = find(strcmp(h.Mia_Image.Settings.Channel_PIE(1).String,'Interaction map'));
                    offset = 0;
                end
                pos = [0.52 0.01 0.43 0.43];
            end

            h.Mia_Image.Settings.Channel_PIE(channel).Value = d;
            Update_Plots([],[],1)
            
            ax = h.Mia_Image.Axes(channel,1);%obj.Parent;
            obj = copyobj([ax, ax.Colorbar],ff); %copy axes and colorbar
            if size(obj,1) > 1 %colorbar exists
                obj(2).Color = [1 1 1];
                obj(2).FontSize = 12; %colorbar text color black
                obj(2).Title.Color = [1 1 1];
            end
            obj(1).Colormap = ax.Colormap; %set the axis colormap and the colorbar will follow
            obj(1).Position = pos; %center image
            % if numel(obj{i})==2
            %     %obj{i}(1).Children(1).Visible = 'off'; %hide the ROI rectangle
            % end
            type = find(strcmp(h.Mia_Image.Settings.Channel_PIE(channel).String{h.Mia_Image.Settings.Channel_PIE(channel).Value},UserValues.MIA.Maps));
            annstr = [UserValues.MIA.Maps{type}(1:end-4) ' [' obj(2).Title.String ']']; % annotation text`
            obj(2).Title.String = ''; %had to delete colorbar title since it was displayed wrong in the exported image
            annpos = pos + [0 0.43 0.03 -0.38];
            %all titles that have a greek 'mu' are written too low
            annpos = annpos + [0 offset 0 0]; 
            %on Windows, titles are written too low
            if ~ismac
                annpos = annpos + [0 0.01 0 0]; 
            end
            annotation('textbox',annpos,'string',annstr,'fontsize',20,'HorizontalAlignment', 'center', 'edgecolor','none','backgroundcolor','none', 'color', 'w','interpreter','tex');
        end
        %% Save multipanel figure
        %ff.Position(3) = round(ff.Position(3)*0.85);
            ff.Color = [0 0 0]; %figure background
            ff.InvertHardcopy = 'off';%print the background
        UserValues.File.MIAPath=PathName;
        LSUserValues(1)
        print(ff, '-dpng', '-r300', fullfile(PathName,'Results.png'));
        delete(ff)

    case {2,3}
        %% Dual color image from menu
        h.Mia_Progress_Text.String = 'Exporting image';
        h.Mia_Progress_Axes.Color=[1 0 0];

        switch mode
            case 2 % normal image
                k = 1;
            case 3 % corrected image
                k = 2;
        end

        % make the dual color image on axis 1
        greenRGB = h.Plots.Image(1,k).CData;
        greenRGB_original = greenRGB;
        redRGB = h.Plots.Image(2,k).CData;
        for i = 1:3
            green = greenRGB(:,:,i);
            red = redRGB(:,:,i);
            green = green + red;
            green(green>1)=1; %this changed the image non-linearly if R G B are not used but other colors
            greenRGB(:,:,i) = green;
        end
        h.Plots.Image(1,k).CData = greenRGB;

        ff = figure('Visible','off');

        % copy the second colorbar
        ax2 = h.Plots.Image(2,k).Parent;
        obj2 = copyobj([ax2, ax2.Colorbar],ff);
        if size(obj2,1) > 1 %colorbar exists
            obj2(2).Color = [1 1 1];
            obj2(2).FontSize = 12; %colorbar text color black
            obj2(2).Title.Color = [1 1 1];
        end
        obj2(1).Colormap = ax2.Colormap; %set the axis colormap and the colorbar will follow
        obj2(1).Color=[0 0 0];
        obj2(1).Position = [0.17 0.05 0.7 0.9]; %center image
        if numel(obj2)==2 %raw image
            obj2(1).Children(1).Visible = 'off'; %hide the ROI rectangle
            obj2(1).Children(2).Visible='off'; % hide the image
        else %corrected image
            obj2(1).Children(1).Visible='off'; %hide the image
        end

        ax = h.Plots.Image(1,k).Parent;
        %channel = str2double(obj.Parent.Tag(end)); %Tag is called ImRaw1, ImCorr1, ImRaw2 or ImCorr2
        % copy the first colorbar
        obj = copyobj([ax, ax.Colorbar],ff); %copy axes and colorbar
        if size(obj,1) > 1 %colorbar exists
            obj(2).Color = [1 1 1];
            obj(2).FontSize = 12; %colorbar text color black
            obj(2).Title.Color = [1 1 1];
        end
        obj(1).Colormap = ax.Colormap; %set the axis colormap and the colorbar will follow
        obj(1).Position = [0.05 0.05 0.7 0.9]; %center image
        if numel(obj)==2
            obj(1).Children(1).Visible = 'off'; %hide the ROI rectangle
        end

        

        channelname = h.Mia_Image.Settings.Channel_PIE(1).String{1};
        ff.Position(3) = round(ff.Position(3)*0.85);
        ff.Color = [0 0 0]; %figure background
        ff.InvertHardcopy = 'off';%print the background

        if ~h.Mia_Image.Settings.SaveImage.Value
            ff.Visible = 'on';
        else
            if ~h.Mia_Image.Calculations.Save_Name.Value
                %User doesn't want automatic filename
                [FileName,PathName] = uiputfile({'*.tif'}, 'Save TIFF as', fullfile(UserValues.File.MIAPath,[MIAData.FileName{1}{1}(1:end-4), '_', channelname, '_image.tif']));
            else
                type = find(strcmp(h.Mia_Image.Settings.Channel_PIE(1).String{h.Mia_Image.Settings.Channel_PIE(1).Value},UserValues.MIA.Maps));
                
                    annstr = [channelname ' - Dual_Color [', obj(2).Title.String, ']']; % annotation text
                    annpos=[0.1 0.9 0.8 0.05];
                    annotation('textbox',annpos,'string',annstr,'fontsize',16,'edgecolor','none','HorizontalAlignment','center','backgroundcolor','none', 'color', 'w','interpreter','tex');

                    name = ['_', UserValues.MIA.Maps{type}];
                if k == 2
                    name = [name '_corrected'];
                end
                FileName = [MIAData.FileName{1}{1}(1:end-4), '_', channelname, name];
            end
            if FileName~=0
                UserValues.File.MIAPath=PathName;
                LSUserValues(1)
                print(ff, '-dpng', '-r300', fullfile(PathName,FileName));
                delete(ff)
            end
        end
        h.Plots.Image(1,k).CData = greenRGB_original;
    case {4,5}
        %video
        [FileName,PathName] = uiputfile({'*.mp4'}, 'Save MP4 as', fullfile(UserValues.File.MIAPath,[MIAData.FileName{1}{1}(1:end-4), '_2cim.tif']));
        switch mode
            case 4
                i = 1;
            case 5
                i = 2;
        end
        if any(FileName~=0)
            UserValues.File.ExportPath=PathName;
            LSUserValues(1)
            % generate a x-by-y-by-RGB-by-frame array
            Image = zeros(size(MIAData.Data{1,2},1),size(MIAData.Data{1,2},2),3,size(MIAData.Data{1,2},3));
            for j = 1:size(MIAData.Data{1,1},3)
                disp(j)
                % change edit box and slider for image frame
                h.Mia_Image.Settings.Channel_Frame(i).String = num2str(j);
                h.Mia_Image.Settings.Channel_Frame_Slider(i).Value = j;
                if ~size(MIAData.Data,1)<2
                    Im = h.Plots.Image(i,2).CData;
                    if size(Im,3)==3
                        %Im=Im/max(Im(:))*255;
                    else
                        % I think this will never really apply anymore
                        if h.Mia_Image.Settings.AutoScale.Value == 1
                            %just avoid negative values
                            mini = min(min(Im));
                            maxi = max(max(Im));
                        elseif h.Mia_Image.Settings.AutoScale.Value == 3
                            % if manual scale, first include all values within the range
                            mini = h.Mia_Image.Axes(i,1).CLim(1);
                            maxi = h.Mia_Image.Axes(i,1).CLim(2);
                            Im(Im<mini)=mini;
                            Im(Im>maxi)=maxi;
                        end
                        %the manual colormap will be only 6bit so rescale the image between 64 positive gray values
                        Im=(Im-mini)/(maxi-mini)*63;

                        % The cmap is only 6 bit
                        cmap=colormap(h.Mia_Image.Axes(i,2));
                        r=cmap(:,1)*255; g=cmap(:,2)*255; b=cmap(:,3)*255;
                        %CData = round((Image-min(Image(:)))/(max(Image(:))-min(Image(:)))*(size(cmap,1)-1))+1;
                        CData = round(Im)+1;
                        CData(CData>63)=63;
                        CData(CData<0)=0;
                        Imaage(:,:,1,j) = reshape(r(CData),size(CData));
                        Imaage(:,:,2,j) = reshape(g(CData),size(CData));
                        Imaage(:,:,3,j) = reshape(b(CData),size(CData));

                        if numel(h.Plots.Image(i,2).AlphaData)>1 %%% When transparency is used to show unselected regions
                            Imaage(:,:,1,j) = Imaage(:,:,1,j).*h.Plots.Image(i,2).AlphaData + 255*(1-h.Plots.Image(i,2).AlphaData)*h.Mia_Image.Axes(i,2).Color(1);
                            Imaage(:,:,2,j) = Imaage(:,:,2,j).*h.Plots.Image(i,2).AlphaData + 255*(1-h.Plots.Image(i,2).AlphaData)*h.Mia_Image.Axes(i,2).Color(2);
                            Imaage(:,:,3,j) = Imaage(:,:,3,j).*h.Plots.Image(i,2).AlphaData + 255*(1-h.Plots.Image(i,2).AlphaData)*h.Mia_Image.Axes(i,2).Color(3);
                        end

                        barx = str2double(h.Mia_Image.Settings.ScaleBar.String);
                        if barx~=0 && ~isnan(barx)
                            pixsize = str2double(h.Mia_Image.Settings.Pixel_Size.String)/1000; %in um
                            roix = str2double(h.Mia_Image.Settings.ROI_SizeX.String); %ROI size is always smaller than imsize so ok.
                            roiy = str2double(h.Mia_Image.Settings.ROI_SizeY.String);
                            barwidth = floor(barx/pixsize); %in pixels
                            y = floor(roiy/40);
                            x = floor(roix/40);
                            for k = 1:3
                                Imaage(1+3*y:1+4*y,end-barwidth-4*x:end-4*x,k,j) = 255;
                            end
                        end
                    end
                end
                Image(:,:,:,j) = Im;
            end
            %video =
        end
        profile = 'MPEG-4';
        v = VideoWriter(fullfile(PathName, FileName), profile);
        v.FrameRate = 5;
        v.Quality = 100;
        open(v)
        %Write the matrix of data A to the video file.

        writeVideo(v,Image);%uint8(Image))
        %Close the file.

        close(v)
end



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Function for exporting various things %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function MIA_CustomFileType(obj,~,mode)
h = guidata(findobj('Tag','Mia'));
global UserValues
switch mode
    case 1 %%% MIA is initialized or selection is changed

        %%% Clears previous custom file info
        for i=numel(obj.UserData{3}):-1:1
            %%% Deletes custom settings UIs
            if isvalid(obj.UserData{3}(i))
                delete(obj.UserData{3}(i));
            end
        end
        obj.UserData = {[],[],[]};

        %%% Updates UserValues
        UserValues.File.MIA_Custom_Filetype = obj.String(obj.Value);
        LSUserValues(1);

        %%% Stops execution, if no custom file type was selected
        if obj.Value == 1
            return;
        end

        %%% Retrieves the function handle of the custom filetype
        Function = str2func(obj.String{obj.Value});
        %%% Tells function to create settings UI
        %%% Out: cell array containing:
        %%% 1: File extension
        %%% 2: File description
        %%% 3: Settings object handles
        %%% 4: Function handle
        Out = Function(1); %Run custom read-in function as mode 1
        Out{4} = Function;
        %%% Stores custom filetype info
        if isempty(h)
            obj.UserData = Out;
        else
            obj.UserData = Out;
            h.Mia_Image.Settings.Custom = Out{3};
            guidata(h.Mia,h);
        end

    case 2 %%% New data is loaded
        %%% Stops execution for no selected custom filetype
        if h.Mia_Image.Settings.FileType.Value == 1
            return;
        end
        %%% Gets function handle
        Function = h.Mia_Image.Settings.FileType.UserData{4};
        %%% Executed data loading
        Function(2);


        Progress(1);

        %%% Updates plots
        Mia_ROI([],[],1)

end

function Do_FRET(~,~)
% Function for calculating intensity based FRET
global MIAData
h = guidata(findobj('Tag','Mia'));

% what is plotted in the count rate tab solid lines,
% i.e. the AROI pixels
donor = h.Plots.Int(2,1).YData;
acceptor = h.Plots.Int(2,2).YData;

%go to a post
DIm = medfilt2(mean(MIAData.Data{1,2}(:,:,10:50),3),[3,3]);
AIm = medfilt2(mean(MIAData.Data{2,2}(:,:,10:50),3),[3,3]);
DIm(DIm<0)=0;
AIm(AIm<0)=0;
ar  = MIAData.AR{1,2};
DIm(~ar)=0;
ar  = MIAData.AR{2,2};
AIm(~ar)=0;
% range over which the normalization is calculated
normrange = eval(h.Mia_Image.Calculations.FRET_norm.String);

method = h.Mia_Image.Calculations.FRET_Type.Value;
if method == 1
    normFactor = mean(acceptor(normrange)./donor(normrange));
elseif method == 2
    normFactor = 1;
else
    return
end

frametime = str2double(h.Mia_NB.Image.Frame.String);
AoverD = (acceptor./donor)./normFactor;
time = (0:(numel(donor)-1))*frametime;
figure
hold on
plot(time, AoverD);
xlabel('time [s]');
ylabel('normalized A/D');
hold off

f = figure;
im=axes(f);
AoverDim = medfilt2(AIm./DIm./normFactor,[5,5]);
imagesc(im, flipud(AoverDim));
axis equal
im.XLim= [0,size(AoverDim,2)];
im.YLim= [0,size(AoverDim,1)];
colormap(im, jet);
colorbar(im)



function Do_Coloc(~,~)
% Function for calculating intensity based FRET
global MIAData
h = guidata(findobj('Tag','Mia'));

%if the 'get background from ROI' button on the ROI tab was pushed, or if a
%background count rate was entered in the corresponding edit boxes, images
%are background corrected, and thus the Pearson's calculation also.

if size(MIAData.Data,1) > 1
    switch h.Mia_Image.Calculations.Coloc_Type.Value
        case 1 %Pearson's correlation coefficient
            % Calculate a Pearson's correlation coefficient for the corrected images within the AROI           
            Image = h.Plots.Image(1,2).CData; % channel 1 corrected image
            if iscell(MIAData.AR)
                AROI = MIAData.AR{1,2}; %for now top right AROI
            else
                AROI = true(size(Image));
            end
            if iscell(MIAData.MS) %include manually drawn ROI if it exists
                AROI = AROI & MIAData.MS{1,2}; %for now top right AROI
            end
            Image = Image(AROI); %linear array of only the included pixels
            Image2 = h.Plots.Image(2,2).CData; % channel 2 corrected image
            Image2 = Image2(AROI); %linear array of only the included pixels
            if ~h.Mia_Image.Calculations.Coloc_weighting.Value
                %% Calculations without intensity weighting
                %% Mean Pearson's coefficient
                mean_1 = mean2(Image);  % the mean intensity of image1 in the ROI
                std_1 = std2(Image);
                mean_2 = mean2(Image2);
                std_2 = std2(Image2);
                ImageP = Image.*Image2;
                n = numel(Image); %number of included pixels
                Pearson = (sum(ImageP) - n.*mean_1.*mean_2)/((n-1).*std_1.*std_2);
                h.Mia_Image.Calculations.Coloc_Pearsons.String = ['mean Pearson`s: ',num2str(Pearson)];
                
                %% Image of the Pearson's coefficeint
                % to be developed
                % spatially average the image via gaussian filtering
                %             stdeviation = str2double(h.Mia_Image.Calculations.Coloc_avg.String);
                %             Image = imgaussfilt(Image,stdeviation,'Padding','symmetric');
                %             Image2 = imgaussfilt(Image2,stdeviation,'Padding','symmetric');
                
                %% Pearson's coefficient vs. intensity G or R
                bins = linspace(min(min(Image),min(Image2)),max(max(Image),max(Image2)),50);
                for i = 1:(numel(bins)-1)
                    %green
                    im =   Image(Image>=bins(i) & Image<bins(i+1));
                    im2 = Image2(Image>=bins(i) & Image<bins(i+1));
                    mean_1 = mean(im);  % the mean intensity of image1 in the ROI
                    std_1 = std(im);
                    mean_2 = mean(im2);
                    std_2 = std(im2);
                    ImageP = im.*im2;
                    n = numel(im); %number of included pixels
                    IntG(i) = mean(bins(i:i+1));
                    ccG(i) = (sum(ImageP) - n.*mean_1.*mean_2)/((n-1).*std_1.*std_2);
                    ErrorG(i) = sqrt(ccG(i))/sqrt(n);
                    
                    %red
                    im =   Image(Image2>=bins(i) & Image2<bins(i+1));
                    im2 = Image2(Image2>=bins(i) & Image2<bins(i+1));
                    mean_1 = mean(im);  % the mean intensity of image1 in the ROI
                    std_1 = std(im);
                    mean_2 = mean(im2);
                    std_2 = std(im2);
                    ImageP = im.*im2;
                    n = numel(im); %number of included pixels
                    IntR(i) = mean(bins(i:i+1));
                    ccR(i) = (sum(ImageP) - n.*mean_1.*mean_2)/((n-1).*std_1.*std_2);
                    ErrorR(i) = sqrt(ccR(i))/sqrt(n);
                end
                %% Pearson's coefficient vs. G/R
                GoverR = Image./Image2;
                bins = linspace(min(GoverR),max(GoverR),50);
                for i = 1:(numel(bins)-1)
                    im =   Image(GoverR>=bins(i) & GoverR<bins(i+1));
                    im2 = Image2(GoverR>=bins(i) & GoverR<bins(i+1));
                    mean_1 = mean(im);  % the mean intensity of image1 in the ROI
                    std_1 = std(im);
                    mean_2 = mean(im2);
                    std_2 = std(im2);
                    ImageP = im.*im2;
                    n = numel(im); %number of included pixels
                    GR(i) = mean(bins(i:i+1));
                    ccGR(i) = (sum(ImageP) - n.*mean_1.*mean_2)/((n-1).*std_1.*std_2);
                    ErrorGR(i) = sqrt(ccGR(i))/sqrt(n);
                end
            else
                %plak hier de corresponderende berekening als wl intensity weighting wordt gedaan
            end
            %% plotting everything
            f = figure;
            ax1 = axes(f);
            errorbar(ax1,IntG,ccG,ErrorG,'g');
            hold on
            errorbar(ax1,IntR,ccR,ErrorR,'r');
            xlabel(ax1,'Intensity (a.u.)');
            ylabel(ax1,'Correlation coefficient');
            ax1.YLim = [-0.05,1.05];
            ax2 = axes('Position',ax1.Position,'Color','none');
            errorbar(ax2, GR,ccGR,ErrorGR, 'Color','k');
            ax2.Color = 'none';
            ax2.XAxisLocation = 'top';
            ax2.YAxisLocation = 'right';
            ax2.XLim = [0,ax2.XLim(2)];
            ax2.YLim = [-0.05,1.05];
            xlabel(ax2, 'Intensity ratio G/R');
            
            %     case 2 %Manders
            %     case 3 %Costes
        case 4 %Van Steensel
            Do_2D_XCor([],[],1)
            %     case 5 %Li
            %     case 6 %object based (particle localization)
        otherwise
            msgbox('not implemented yet')
            h.Mia_Image.Calculations.Coloc_Type.Value = 1;
    end
    
    
    
else
    msgbox('load 2-color data!')
end

function out = Save_ARinfo(FID, mode)
        switch mode
            case {1,2}
                global UserValues
                %%% Arbitrary region information
                out.Int_Max(1) = UserValues.MIA.AR_Int(3);
                out.Int_Max(2) = UserValues.MIA.AR_Int(4);
                out.Int_Min(1) = UserValues.MIA.AR_Int(1);
                out.Int_Min(2) = UserValues.MIA.AR_Int(2);
                out.Int_Fold_Max(1) = UserValues.MIA.AR_Int_Fold(3);
                out.Int_Fold_Min(1) = UserValues.MIA.AR_Int_Fold(1);
                out.Int_Fold_Max(2) = UserValues.MIA.AR_Int_Fold(4);
                out.Int_Fold_Min(2) = UserValues.MIA.AR_Int_Fold(2);
                out.Var_Fold_Max(1) = UserValues.MIA.AR_Var_Fold(3);
                out.Var_Fold_Min(1) = UserValues.MIA.AR_Var_Fold(1);
                out.Var_Fold_Max(2) = UserValues.MIA.AR_Var_Fold(4);
                out.Var_Fold_Min(2) = UserValues.MIA.AR_Var_Fold(2);
                out.Var_Sub=UserValues.MIA.AR_Region(2);
                out.Var_SubSub=UserValues.MIA.AR_Region(1);
                out.Same = UserValues.MIA.AR_Same;
                out.Median = UserValues.MIA.AR_Median;
                out.Framewise = UserValues.MIA.AR_Framewise;
            case 2
                fprintf(FID,'%s\n','Arbitrary region used:');
                fprintf(FID,'%s\t%f\n','Minimal average intensity [green kHz; red kHz]:',out.Int_Min(1),'; ',out.Int_Min(2));
                fprintf(FID,'%s\t%f\n','Maximal average intensity [green kHz; red kHz]:',out.Int_Max(1),'; ',out.Int_Max(2));
                fprintf(FID,'%s\t%u,%u\n','Subregions size:',out.Var_SubSub,out.Var_Sub);
                fprintf(FID,'%s\t%f\n','Minimal\Maximal intensity fold deviation [green min\max; red min\max]:',out.Int_Fold_Min(1),'; ',out.Int_Fold_Max(1),'; ',out.Int_Fold_Min(2),'; ',out.Int_Fold_Max(2));
                fprintf(FID,'%s\t%f\n','Minimal\Maximal variance fold deviation [green min\max; red min\max]:',out.Var_Fold_Min(1),'; ',out.Var_Fold_Max(1),'; ',out.Var_Fold_Min(2),'; ',out.Var_Fold_Max(2));
                fprintf(FID,'%s\t%u,%u\n','Which Masks:',out.Same);
                fprintf(FID,'%s\t%u,%u\n','Median filtering:',out.Median);
                fprintf(FID,'%s\t%u,%u\n','Framewise:',out.Framewise);
        end
        %%
    function RICSMap
        global MIAData
        corsize = 30; %take multiples of 10
        overlap = 5;
        maxf = size(MIAData.Data{1,2},3);
        maxx = round(size(MIAData.Data{1,2},1)/5)-corsize/5;
        maxy = round(size(MIAData.Data{1,2},2)/5)-corsize/5;
        remcor = 10;
        CorImage = zeros(maxx, maxy);
        ErrImage = zeros(maxx, maxy);
        for x=1:maxx
            for y=1:maxy
                Cor = zeros(corsize,corsize, maxf);
                for f=1:maxf
                    ImageTotal=double(MIAData.Data{1,2}(:,:,f));
                    Image=ImageTotal(overlap*(x-1)+1:overlap*(x-1)+corsize,overlap*(y-1)+1:overlap*(y-1)+corsize); %calculate a corsizexcorsize correlation block that shifts 5 pixels
                    Image_FFT=fft2(Image);
                    Cor(:,:,f) = fftshift(real(ifft2(Image_FFT.*conj(Image_FFT))))/(size(Image,1)*size(Image,2)*(mean2(Image))^2)-1; %add the local correlations per frame
                end
                MeanCor = nanmean(Cor,3); %take the average correlation
                MeanCor(corsize/2+1,corsize/2+1)=NaN; %remove the central noise peak
                ErrCor = nanstd(Cor,0,3); %stdev map of the correlation
                ErrCor(corsize/2+1,corsize/2+1)=NaN; %remove the central noise peak
                Gx=nanmean(MeanCor(corsize/2+1,corsize/2-remcor+1:corsize/2+remcor)); %calculate the mean correlation in x lag around the center for remcor points.
                Gy=nanmean(MeanCor(corsize/2-remcor+1:corsize/2+remcor,corsize/2+1));
                ErrGx=nanmean(ErrCor(corsize/2+1,corsize/2-remcor+1:corsize/2+remcor)); %calculate the mean error on the correlation in x lag around the center for remcor points.
                ErrGy=nanmean(ErrCor(corsize/2-remcor+1:corsize/2+remcor,corsize/2+1));
                CorImage(x,y)=Gx/Gy;
                ErrImage(x,y)=sqrt((ErrGx/Gx)^2+(ErrGy/Gy)^2)*100; % relative error (in %)
            end
        end
        
        CorImage(CorImage < 0) = 0;
        CorImage(isnan(CorImage)) = 0;
        %ErrImage(isnan(ErrImage)) = 0;
      %  ErrImage(ErrImage>1000) = nan;
       % CorImage(isnan(ErrImage)) = nan; %omit regions where the relative error is 1000%

%         ff = figure;
%         im = axes(ff);
%         imagesc(im, flipud(CorImage))
%         colorbar(im)
        
        CorImage2 = medfilt2(CorImage,'symmetric');
        ff2 = figure;
        im2 = axes(ff2);
        imagesc(im2, flipud(CorImage2))
        colorbar(im2)
      
        
        ErrImage2 = medfilt2(ErrImage,'symmetric');
        ff3 = figure;
        im3 = axes(ff3);
        imagesc(im3, flipud(ErrImage2))
        colorbar(im3)
        a = 1;

function ImageColor = Image_Colormap(~,~,channel)
global UserValues
%% Selects colormap
switch UserValues.MIA.ColorMap_Main(channel)
    case 1 %%% Gray
        ImageColor=gray(128);
    case 2 %%% Jet
        ImageColor=jet(128);
    case 3 %%% Hot
        ImageColor=hot(128);
    case 4 %%% HSV
        ImageColor=hsv(128);
    case 5 %%% Custom
        ImageColor=gray(128).*repmat(UserValues.MIA.CustomColor(channel,:),[128,1]);
    case 6 %%% HiLo
        ImageColor=[0 0 1;gray(2^7-2);1 0 0];
    case 7 %%% Inverse Gray
        ImageColor=flipud(gray(128));
    case 8 %%% Parula
        ImageColor=parula(128);
end

function tryoutrangeslider2
Labels = {'Top Channel','Bottom Channel'};
Mins = [1 1];
Maxs = [23 23];
hF = figure;
for i=1:length(Labels)
    % more direct instantiation
    % jRS = com.jidesoft.swing.RangeSlider(0,100,20,70); %min,max,low,high
    % [jRangeSlider{i}, hRangeSlider{i}] = javacomponent(jRS,[0,0,200,80],hF);%posx,posy,width,height
    jRS = com.jidesoft.swing.RangeSlider;
    [jRangeSlider{i}, hRangeSlider{i}] = javacomponent(jRS,[],hF);
    % modify rangeslider position
    set(hRangeSlider{i},'Position',[100,11+(i-1)*80,200,80])
    % modify range slider properties
    set(jRangeSlider{i},'Maximum',Maxs(i),...
        'Minimum',Mins(i),...
        'LowValue',10,...
        'HighValue',20,...
        'Name',Labels{i},...
        'MajorTickSpacing',5,...
        'MinorTickSpacing',1, ...
        'PaintTicks',true,...
        'PaintLabels',true, ...
        'StateChangedCallback',{@jRangeSlider_Callback,i});
    % add text label
    uicontrol(hF,'Style','Text','Position',[5,53+(i-1)*80,100,15],'String',Labels{i})
end

function jRangeSlider_Callback(jRangeSlider,event,i)
disp([jRangeSlider.Name ' ,extra parameter =' num2str(i)])

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Execution of Mia from the Command line %%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function Mia_Command(PathFile)
global MIAData UserValues

% For an unbiased test from within Matlab, close Mia, clc, clear all global, rename your profiles folder to
% something else, create an empty new profiles folder. Then run Mia('file') at command

% For an unbiased test from CMD or Terminal, do not run the compiled
% version if it had been ran before on any computer. Start from a freshly
% compiled version

h = guidata(findobj('Tag','Mia'));
guidata(h.Mia,h);
%h.Mia.Visible = 'on'; %make Mia visible for now

fprintf('SPECTRAL_RICS: Transferring data to analysis routine...\n')
%%% Take the path from the File
[Path, File, Ext] = fileparts(PathFile);

%% Determine what kind of file is presented
if strcmp(Ext,'.json')
    % json file containing the analysis metadata
    [fid, message] = fopen(PathFile);
    raw = fread(fid,inf);
    str = char(raw');
    fclose(fid);
    val = jsondecode(str);

    %% Info regarding all possible json files except for Calibration data (see below)
    % val.Analysis                      % structure
    % val.Analysis.Type                 % character
    % val.Analysis.Mask                 % empty or character: file.ext
    % val.Analysis.InputOne             % boolean
    % val.Analysis.InputTwo             % 1x1 cell
    % val.Analysis.InputThree           % boolean or character
    % val.Image                         % character: file.ext
    % val.ChannelOne                    % structure, correlation channel 1
    % val.ChannelOne.Name               % character
    % val.ChannelOne.OmegaR             % number
    % val.ChannelOne.OmegaZ             % number
    % val.ChannelOne.PhotonCounting     % number 
    % val.ChannelOne.ChannelIndex       % number 
    % val.ChannelTwo                    % structure, correlation channel 2
    % val.ChannelTwo.Name               % character
    % val.ChannelTwo.OmageR             % number
    % val.ChannelTwo.OmegaZ             % number
    % val.ChannelTwo.PhotonCounting     % number 
    % val.ChannelTwo.ChannelIndex       % number 
    % val.Detrending                    % boolean, moving average correction
    % val.SpectralUnmixing              % structure
    % val.SpectralUnmixing.Filter       % RSICS filter spectra per channel
    % val.SpectralUnmixing.Scaling      % The structure of this array is explained elsewhere

    switch val.Analysis.Type
        case 'Standard'
            % val.Analysis.Mask                 % empty or path to file containing either:
            %                                   % - the masks for each time frame if Intensity Thresholding is True
            %                                   % - or one single image with the ROI if Intensity Thresholding is False.
            % val.Analysis.InputOne             % Boolean: True: intensity thresholding, False: either no mask (ones) or handdrawn.
            % val.Analysis.InputTwo             % 'Name of channel mask' In fact obsolete, i do not need to know what the origin of the mask is (1, 2 or both or whathever)
            % val.Analysis.InputThree           % Boolean indicating whether Time-dependent Analysis was set to True or False.
            % if InputThree is true, the window size should be written in val.Analysis.InputTwo = 20, we don't do this
        case 'Intensity-based Heatmap'
            % val.Analysis.Mask                 % file containing one single image with the handdrawn ROI.
            % val.Analysis.InputOne             % Value for the number of sectors
            % val.Analysis.InputTwo             % 'Channel 1 name' or 'Channel 2 name'. Important: decides from which channel Mia has to generate the intensity masks
            % val.Analysis.InputThree           % []

        case 'Grid-based Heatmap'
            % val.Analysis.Mask                 % Path to file containing one single image with the ROI.
            % val.Analysis.InputOne = 32;       % Value for the grid size: 8,16,32 or 64 %jelle: correct the ACF amplitude!
            % val.Analysis.InputTwo             % []
            % val.Analysis.InputThree           % []

        case 'Calibration'
            % val.Analysis.Dye                  % 'Rh110'
            % val.Analysis.Wavelength           % 0.0
            % val.Analysis.Temperature          % 25.0
            % val.Analysis.Diffusion            % 470
            % val.Image                         % character: file.ext
            % all other entries are missing, so we define them here
            val.ChannelOne = struct;
            val.ChannelOne.Name = [];
            val.ChannelOne.PhotonCounting = val.PhotonCounting; 
            val.ChannelOne.ChannelIndex = '1';
            val.ChannelTwo = struct;
            val.ChannelTwo.PhotonCounting = [];
            val.ChannelTwo.ChannelIndex = [];
            val.ChannelTwo.Name = []; % '' and [] are both considered empty
            val.Detrending = logical(0); %hardcoded: no detrending
            val.Analysis.Mask = [];
            val.Analysis.InputOne = logical(0);
            val.Analysis.InputTwo = [];
            val.Analysis.InputThree = [];
            val.SpectralUnmixing  = struct;  
            val.SpectralUnmixing.Filter = []; 
            val.SpectralUnmixing.Scaling = [];
            %val.ChannelOne.PhotonCounting = 1;
            %val.ChannelTwo.PhotonCounting = 1;
    end

    %% Info about photon counting correction
    % in photon counting mode:
    %       1 gray value = 1 photon
    %       data recorded is always 16bit
    % in integration mode:
    %       the max gray value corresponds to a certain number of photons.
    %       this number of photons should be written into the file.
    %       for 8 bit: max gray value is 255
    %       for 16 bit: max gray value is 65536
    %       in the json, S = (max gray value/the max number of photons) is
    %       written. Dividing the image by S then converts gray levels to
    %       photons.
    %       data recorded can be recorded 8bit (default) or 16bit, but the data
    %       quality is hardly ever reasonable for 16bit, except for really
    %       highly intense samples

    %% Extract the Path to the Image
    % image is one folder up so remove the last folder level from Path
    pathparts = strsplit(Path,filesep);
    ImPath = strjoin(pathparts(1:end-1),filesep);
    
    try %to remove an existing Mia subfolder
        rmdir(fullfile(ImPath,'Mia'),'s')
    end

    %% Extract Image Filename
    s = strfind(val.Image, '\');
    if isempty(s) %file generated on Mac
        [~, ImFile, Ext] = fileparts(val.Image);
        ImFile = [ImFile Ext];
    else
        % take what comes after the last backslash as the filename
        ImFile = val.Image(s(end)+1:end);
    end

    % %% Remediate older json versions
    % if ~isstruct(val.ChannelOne) && ~strcmp(val.Analysis.Type,'Calibration')
    %     % in older json files val.ChannelOne was just the name of the channel
    %     % 'Calibration' filetype will not have val.ChannelOne at all, even in newer json files
    %     tmp = val.ChannelOne;
    %     val.ChannelOne = struct;
    %     val.ChannelOne.Name = tmp;
    %     val.ChannelOne.OmageR = val.OmageR;
    %     val.ChannelOne.OmegaZ = val.OmegaZ;
    % end
    % if ~isstruct(val.ChannelTwo)
    %     tmp = val.ChannelTwo;
    %     val.ChannelTwo = struct;
    %     val.ChannelTwo.Name = tmp;
    % end
    % val.Analysis.InputThree = logical(0); %error in json, has to be zero for non-time-dependent
    % if iscell(val.Analysis.InputTwo)
    %     val.Analysis.InputTwo = val.Analysis.InputTwo{1};
    % end

    %% Do Custom analyses
    %val.Analysis.Type = 'Grid-based Heatmap';
    %val.ChannelTwo.Name = val.Analysis.InputTwo;
    %val.ChannelTwo.OmageR = val.OmageR;
    %val.ChannelTwo.OmegaZ = val.OmegaZ;
    %val.Analysis.Type = 'Standard';
    %val.Analysis.InputThree = logical(1);
    %val.Analysis.InputOne = 32;
    %val.ChannelOne.PhotonCounting = 1;
    %val.ChannelTwo.PhotonCounting = 1;
elseif(strcmp(Ext, '.czi'))
    % user provides a czi file at the path
    % This code has to be updated

    %% Extract Image Filename
    % val.Image = not needed
    ImFile = [File Ext];

    %% Generate analysis parameters
    mode = 1;
    % mode 1:   single color no mask
    % mode 1.5: single color no mask time-dependent analysis
    % mode 2:   two color no mask
    % mode 2.5: two color no mask time-dependent analysis
    % mode 3:   single color intensity based heatmap (channel 1 is mask)
    % mode 4:   two color intensity based heatmap (channel 2 is mask)
    % mode 5:   grid-based heat map
    % mode 6:   calibration

    val = struct;
    val.Analysis = struct;
    val.ChannelOne = struct;
    val.ChannelTwo = struct;
    val.ChannelOne.PhotonCounting = 1;
    val.ChannelTwo.PhotonCounting = 1;
    val.SpectralUnmixing = struct;
    % If user opens a czi file, spectral unmixing nor masking can be done
    val.SpectralUnmixing.Filter = [];
    val.SpectralUnmixing.Scaling = []; 
    val.Analysis.Mask = []; % normally: CZI file containing one single image with the handdrawn ROI.

    switch mode
        case {1, 1.5} % single color no mask
            val.Analysis.Type = 'Standard';
            val.Analysis.InputOne = logical(0); %1 multi-frame or 0 handdrawn mask boolean
            val.Analysis.InputTwo = ''; %Mask name (obsolete)
            if mode == 1
                val.Analysis.InputThree = logical(0);
            else % Time-dependent Analysis
                val.Analysis.InputThree = logical(1);
            end
            val.ChannelOne.Name = 'ch1';
            val.ChannelOne.OmageR = 0.2;
            val.ChannelOne.OmegaZ = 1;
            val.ChannelTwo.Name = [];
            val.ChannelTwo.OmageR = [];
            val.ChannelTwo.OmegaZ = [];
            val.Detrending = logical(1);
        case {2, 2.5} % two color no mask
            val.Analysis.Type = 'Standard';
            val.Analysis.InputOne = logical(0); %1 multi-frame or 0 handdrawn mask boolean
            val.Analysis.InputTwo = ''; %Mask name (obsolete)
            if mode == 2
                val.Analysis.InputThree = logical(0);
            else % Time-dependent Analysis
                val.Analysis.InputThree = logical(1);
            end
            val.ChannelOne.Name = 'ch1';
            val.ChannelOne.OmageR = 0.2;
            val.ChannelOne.OmegaZ = 1;
            val.ChannelTwo.Name = 'ch2';
            val.ChannelTwo.OmageR = 0.2;
            val.ChannelTwo.OmegaZ = 1;
            val.Detrending = logical(1);
        case 3 %single color intensity based heatmap (channel 1 is mask)
            val.Analysis.Type = 'Intensity-based Heatmap';
            val.Analysis.InputOne = 4; %number of sectors %Jelle: reduce if fit problems !
            val.Analysis.InputTwo = 'ch1'; %Important: decides from which channel Mia has to generate the intensity masks
            val.Analysis.InputThree = ''; %obsolete, ignore.
            val.ChannelOne.Name = 'ch1';
            val.ChannelOne.OmageR = 0.2;
            val.ChannelOne.OmegaZ = 1;
            val.ChannelTwo.Name = [];
            val.ChannelTwo.OmageR = [];
            val.ChannelTwo.OmegaZ = [];
            val.Detrending = logical(1);
        case 4 %two color intensity based heatmap (channel 2 is mask)
            val.Analysis.Type = 'Intensity-based Heatmap';
            val.Analysis.InputOne = 4; %number of sectors %Jelle: reduce if fit problems !
            val.Analysis.InputTwo = 'ch2'; %Important: decides from which channel Mia has to generate the intensity masks
            val.Analysis.InputThree = ''; %obsolete, ignore.
            val.ChannelOne.Name = 'ch1';
            val.ChannelOne.OmageR = 0.2;
            val.ChannelOne.OmegaZ = 1;
            val.ChannelTwo.Name = 'ch2';
            val.ChannelTwo.OmageR = 0.2;
            val.ChannelTwo.OmegaZ = 1;
            val.Detrending = logical(1);
        case 5 %grid-based heat map
            val.Analysis.Type = 'Grid-based Heatmap';
            val.Analysis.InputOne = 4; %Value for the grid size: 8,16,32 or 64 %jelle: correct the ACF amplitude!
            val.Analysis.InputTwo = ''; %obsolete, ignore.
            val.Analysis.InputThree = ''; %obsolete, ignore.
            val.ChannelOne.Name = 'ch1';
            val.ChannelOne.OmageR = 0.2;
            val.ChannelOne.OmegaZ = 1;
            val.ChannelTwo.Name = [];
            val.ChannelTwo.OmageR = [];
            val.ChannelTwo.OmegaZ = [];
            val.Detrending = logical(1);
        case 6 %calibration
            val.Analysis.Type = 'Calibration';
            val.Analysis.Dye = 'Rh110';
            val.Analysis.Wavelength = 0.0;
            val.Analysis.Temperature = 25.0;
            val.Analysis.Diffusion = 470;
    end
else
    fprintf('SPECTRAL_RICS_ERROR: A problem occurred while transferring data to analysis routine. Analysis aborted!\n')
    return
end

%% General MIA settings that have to be set correctly
% Data is always photon counting, since we convert it to photon counting if it's not
h.Mia_Image.Settings.kHz.Value = 1;
% Data is always raster scanning
h.Mia_Image.Settings.scanning.Value = 1;
UserValues.MIA.Options.scanning = 1;
UserValues.MIA.Options.kHz = 1;
% check the save image checkbox on the options tab
h.Mia_Image.Settings.SaveImage.Value = 1;
% automatic filenaming
h.Mia_Image.Calculations.Save_Name.Value = 1;
% overwrite existing files
h.Mia_Image.Calculations.Overwrite.Value = 1;
% reset the AR Subregion 1 (for when user previously did grid-based analysis)
h.Mia_Image.Settings.ROI_AR_Sub1.String = '5';
UserValues.MIA.AR_Region(1) = 5;
% reset the RICS fit size to its default value
h.Mia_ICS.Size.String = '31'; 
% set the colormaps to 'jet'
for i = 1:2
    h.Mia_Image.Settings.Channel_Colormap(i).Value = find(strcmp(h.Mia_Image.Settings.Channel_Colormap(i).String,'Jet'));
end
LSUserValues(1)


%% Settings on RICS tab: which channels to correlate
%% Settings on Options tab: which channels to load
% channel range in CZI file for first imaging channel in Mia
h.Mia_Image.Settings.Custom(2).String = num2str(val.ChannelOne.ChannelIndex); 
UserValues.MIA.Custom.Zeiss_CZI{1} = num2str(val.ChannelOne.ChannelIndex);
% empty the spectral RICS shift and scale array
MIAData.RLICS = [];
%val.ChannelOne and ChannelTwo are the correlation channels
if isempty(val.ChannelTwo.Name) % User wants to do 'single channel' autocorrelation analysis
    h.Mia_Image.Calculations.Cor_Type.Value = 1;
    channels = 1;
    %% Set channel names, for example for later export filenames
    h.Mia_Image.Settings.Channel_PIE(1).String{1} = val.ChannelOne.Name;
    if strcmp(val.Analysis.InputTwo, val.ChannelOne.Name) && strcmp(val.Analysis.Type, 'Intensity-based Heatmap')
        % user wants to mask channel 1 via intensity thresholding of channel 1
        % no need to load channel 2
        h.Mia_Image.Settings.Channel_PIE(2).String{1} = '';
        h.Mia_Image.Settings.Custom(3).String = '';
        UserValues.MIA.Custom.Zeiss_CZI{2} = '';
    elseif ~strcmp(val.Analysis.InputTwo, val.ChannelOne.Name) && strcmp(val.Analysis.Type, 'Intensity-based Heatmap')
        % user wants to mask channel 1 via intensity thresholding of another channel
        % we need to load another channel even if we're not correlating it 
        % Jelle: could it be that Analysis.InputTwo is empty?
        val.ChannelTwo.Name = val.Analysis.InputTwo{1};
        %h.Mia_Image.Settings.Channel_PIE(1).String{2} = val.ChannelTwo.Name;
        h.Mia_Image.Settings.Channel_PIE(2).String{1} = val.ChannelOne.Name; %obsolete but ok
        h.Mia_Image.Settings.Channel_PIE(2).String{2} = val.ChannelTwo.Name;
        %h.Mia_Image.Settings.Channel_PIE(2).String = val.Analysis.InputTwo;
        h.Mia_Image.Settings.Custom(3).String = num2str(val.ChannelTwo.ChannelIndex);
        UserValues.MIA.Custom.Zeiss_CZI{2} = num2str(val.ChannelTwo.ChannelIndex);
    else % all other types of analyses
        % I don't need to know where a mask comes from, user wants to
        % correlate channel 1 and only channel 1. Load 1 channel.
        h.Mia_Image.Settings.Channel_PIE(2).String{1} = '';
        h.Mia_Image.Settings.Custom(3).String = '';
        UserValues.MIA.Custom.Zeiss_CZI{2} = '';
    end
else % User wants to do any type of (also Intensity-based heatmap) two-channel auto/crosscorrelation analysis
    h.Mia_Image.Settings.Channel_PIE(2).String = val.ChannelTwo.Name;
    % calculate ACFs and CCF
    h.Mia_Image.Calculations.Cor_Type.Value = 3;
    channels = 1:3;
    h.Mia_Image.Settings.Custom(3).String = num2str(val.ChannelTwo.ChannelIndex); %channel 2 on the Options tab
    UserValues.MIA.Custom.Zeiss_CZI{2} = num2str(val.ChannelTwo.ChannelIndex);    
end

%% Spectral RICS image scaling and shifting
if ~isempty(val.SpectralUnmixing.Scaling)
    % 'RLICS_Min-Max': store in MIAData.RLICS(ch,1)
    % 'RLICS_Min': store in MIAData.RLICS(ch,2)
    MIAData.RLICS = val.SpectralUnmixing.Scaling;
    if size(val.SpectralUnmixing.Scaling,1)==1
        MIAData.RLICS(2,:) = [1,0];
    end
end

for c = 3:5
    h.Mia_Image.Settings.Custom(c).String = ''; %channel 3, 4,5 on the Options tab
    UserValues.MIA.Custom.Zeiss_CZI{c} = '';
end
h.Mia_Image.Settings.Custom(7).Value=0; % don't show spectrum
UserValues.MIA.Custom.Zeiss_CZI{6} = 0;
h.Mia_Image.Settings.Custom(8).String='1'; % just a single z-plane
UserValues.MIA.Custom.Zeiss_CZI{7} = '1';
LSUserValues(1);
% Pass the Custom filetype popupmenu as argument to the function, as if
% user changed the popupmenu themselves
h.Mia_Image.Settings.FileType.Value = find(strcmp(h.Mia_Image.Settings.FileType.String,'Zeiss_CZI'));

MIA_CustomFileType(h.Mia_Image.Settings.FileType,[],1);
%%% Load CZI file using the custom filetype readin routine
% I have to store the Path and Image as a global variable to access it
MIAData.ImPath = ImPath;
MIAData.ImFile = ImFile;
%MIA_CustomFileType([],[],3);

%% Load data
Zeiss_CZI(4) %doesn't display it yet

fprintf('SPECTRAL_RICS: Preparing to correlate data...\n')

%% Place Mia's ROI
%%% Set the ROI to cover the whole image
h.Mia_Image.Settings.ROI_SizeX.String = num2str(size(MIAData.Data{1,1},1));
h.Mia_Image.Settings.ROI_SizeY.String = num2str(size(MIAData.Data{1,1},2));
h.Mia_Image.Settings.ROI_PosX.String = '1';
h.Mia_Image.Settings.ROI_PosY.String = '1';
%%% Turn off AROI 
%h.Mia_Image.Settings.ROI_FramesUse.Value = 1; %no AROI
%MIA_Various(h.Mia_Image.Settings.ROI_FramesUse,[],3); %Hide\Show Arbitrary region controls
%MIA_Various(h.Mia_Image.Settings.ROI_AR_Reset,[],7); %Reset the AROI values to default
%h.Mia_Image.Settings.ROI_AR_Same.Value = 1; %'OR'
%h.Mia_Image.Settings.ROI_AR_Channel.Value = 2; %channel one's settings can be edited
%%% Updates plots
Mia_ROI([],[],1)

% %%% Set the AROI settings to default
% h.Mia_Image.Settings.ROI_FramesUse.Value = 3; %arbitrary ROI
% MIA_Various(h.Mia_Image.Settings.ROI_FramesUse,[],3); %Hide\Show Arbitrary region controls
% MIA_Various(h.Mia_Image.Settings.ROI_AR_Reset,[],7); %Reset the AROI values to default
% h.Mia_Image.Settings.ROI_AR_Same.Value = 1; %'OR'
% h.Mia_Image.Settings.ROI_AR_Channel.Value = 2; %channel one's settings can be edited
% %%% Updates plots
% Mia_ROI([],[],1)

%% Moving average correction
if val.Detrending
    h.Mia_Image.Settings.Correction_Add.Value = 2; %add total ROI mean
    h.Mia_Image.Settings.Correction_Subtract.Value = 4; %subtract moving average
    h.Mia_Image.Settings.Correction_Subtract_Pixel.String = '1';
    h.Mia_Image.Settings.Correction_Subtract_Frames.String = '3';
else
    %work with raw data
    h.Mia_Image.Settings.Correction_Add.Value = 1; %add nothing
    h.Mia_Image.Settings.Correction_Subtract.Value = 1; %subtract nothing
end


%% Apply the S correction to convert analog to photon counting
if isempty(val.ChannelOne.PhotonCounting) 
    % PCF is not defined for channel 1 
    % read PCF.json from file in C:\Users\user\Documents\Carl Zeiss\ZEN\Documents\RICS\Calibration Logs\PCF.json
    % read voltage, pix time and bit depth from czi file and pick the correct one
    val.ChannelOne.PhotonCounting = 1; %jelle change to actual value
end

if ~isempty(val.ChannelTwo.Name) && isempty(val.ChannelTwo.PhotonCounting)
    % Channel two needs to be used and PCF is not defined for channel 2
    % read PCF.json from file in C:\Users\user\Documents\Carl Zeiss\ZEN\Documents\RICS\Calibration Logs\PCF.json
    % read voltage, pix time and bit depth from czi file and pick the correct one
    val.ChannelTwo.PhotonCounting = 1; %jelle change to actual value
elseif isempty(val.ChannelTwo.Name) % channel 2 is not used at all, not even for a mask
    val.ChannelTwo.PhotonCounting = 1; %just make it one, it's not used
end
    

if (val.ChannelOne.PhotonCounting ~= 1) || (val.ChannelTwo.PhotonCounting ~= 1)
    %data was not recorded using photoncounting
    h.Mia_Image.Settings.S.String = [num2str(val.ChannelOne.PhotonCounting) ';' num2str(val.ChannelTwo.PhotonCounting) ';1;1;1'];
    %h.Mia_Image.Settings.Offset.String = always 0
else
    %data was recorded using photoncounting
    h.Mia_Image.Settings.S.String = '1;1;1;1;1';
    %h.Mia_Image.Settings.Offset.String = always 0
end

%update the image
Mia_Orientation([],[],4)

%% Format the fit parameter structure for feeding into MIAFit
if ~strcmp(val.Analysis.Type,'Calibration')
    % PSF values from the json file
    % ACF1
    params.w0 = val.ChannelOne.OmegaR;
    params.wz = val.ChannelOne.OmegaZ;
    if h.Mia_Image.Calculations.Cor_Type.Value ~= 1
        % ACF2
        params.w0(2) = val.ChannelTwo.OmegaR;
        params.wz(2) = val.ChannelTwo.OmegaZ;
        % CCF : root-mean square of the ACF1/2 parameters
        params.w0(3) = sqrt(0.5.*(val.ChannelOne.OmegaR^2+val.ChannelTwo.OmegaR^2));
        params.wz(3) = sqrt(0.5.*(val.ChannelOne.OmegaZ^2+val.ChannelTwo.OmegaZ^2));
    end
end
% Scan  settings are loaded from the czi file and displayed in Mia.
params.linetime = str2double(h.Mia_Image.Settings.Image_Line.String);
params.pixeltime = str2double(h.Mia_Image.Settings.Image_Pixel.String);
params.frametime = str2double(h.Mia_Image.Settings.Image_Frame.String);
params.pixelsize = str2double(h.Mia_Image.Settings.Pixel_Size.String);

%% Time-dependent analysis settings
if val.Analysis.InputThree %&& ~strcmp(val.Analysis.Type, 'Calibration') %Jelle change this once Fernanda updates the json file
    val.Analysis.Type = 'Time-dep';
    %% MIA Settings
    % set the dropdown menu next to the RICS button to 'blockwise'
    h.Mia_Image.Calculations.Cor_Save_ICS.Value = 4;
    % do not overwrite existing files, Mia will generate 1 file per block
    h.Mia_Image.Calculations.Overwrite.Value = 0;
    % n.o. frames per window
    % should be the default value, not included in json file
    h.Mia_Image.Calculations.Cor_ICS_Window.String = num2str(20);
    % difference between the start of each window (if Window = Offset then the windows are adjacent)
    h.Mia_Image.Calculations.Cor_ICS_Offset.String = num2str(10); % can be part of JSON file
    val.Analysis.InputTwo = 20;

    Offset = str2double(h.Mia_Image.Calculations.Cor_ICS_Offset.String);
    blocks = floor((size(MIAData.Data{1,2},3)-str2double(h.Mia_Image.Calculations.Cor_ICS_Window.String)+Offset)/Offset);
else
    % set the dropdown menu next to the RICS button to 'save as .miacor'
    h.Mia_Image.Calculations.Cor_Save_ICS.Value = 2;
    % always overwrite existing files, Mia will generate 1 file per block
    h.Mia_Image.Calculations.Overwrite.Value = 1;
    blocks = 1;
end

%% Add scale bar 
h.Mia_Image.Settings.ScaleBar.String = '2';


%% Correlation, analysis and saving results
if strcmp(val.Analysis.Type, 'Standard') || strcmp(val.Analysis.Type, 'Calibration') || strcmp(val.Analysis.Type, 'Time-dep')
    % Standard RICS analysis (calibration, experiments, time-dependent...) whether or not with segmentation or spectral unmixing
    %% Load and apply the masks
    if isempty(val.Analysis.Mask)
        % Standard RICS without segmentation
        h.Mia_Image.Settings.ROI_FramesUse.Value = 1; %normal RICS
        MIA_Various([],[],3)
    else
        h.Mia_Image.Settings.ROI_FramesUse.Value = 3; %arbitrary ROI RICS
        h.Mia_Image.Settings.ROI_AR_Same.Value = 1; %'OR' option for mask selection, which is ok as masks in both channels are the same anyway
        MIA_Various([],[],[3, 6])
        %%% Extract Mask FileName
        s = strfind(val.Analysis.Mask, '\');
        if isempty(s) %file generated on Mac
            [~, MaskFile, Ext] = fileparts(val.Analysis.Mask);
            MaskFile = [MaskFile Ext];
        else
            % take what comes after the last backslash as the filename
            MaskFile = val.Analysis.Mask(s(end)+1:end);
        end
        if ~exist(fullfile(Path,MaskFile),'file')
            fprintf('SPECTRAL_RICS_ERROR: Required mask file missing!\n')
            error('mask file not present')
            return
        else
            Mask = bfopen(fullfile(Path,MaskFile)); %4x1 cell
        end
        %MaskInfo = Mask{1,2};
        Mask = Mask(:,1); %framesx1 cell
        Mask = Mask{1}(:,1);
        Mask = reshape(Mask,1,1,size(Mask,1));
        Mask = logical(cell2mat(Mask));
        for j=1:size(Mask,3)
            Mask(:,:,j)=flipud(Mask(:,:,j));
        end
        %%% STILL CHECK:
        % median filter, framewise, and all the AROI settings
        if ~val.Analysis.InputOne
            %Maximize the usable area
            h.Mia_Image.Settings.ROI_AR_Sub1.String = '1';
            UserValues.MIA.AR_Region(1) = 1;
            % Standard RICS with a hand-drawn ROI
            MIAData.MS{1,1} = Mask(:,:,1); %raw image
            MIAData.MS{1,2} = Mask(:,:,1); %corrected image
            if ~isempty(val.ChannelTwo)
                MIAData.MS{2,1} = Mask(:,:,1); %raw image
                MIAData.MS{2,2} = Mask(:,:,1); %corrected image
            end
        else
            h.Mia_Image.Settings.ROI_AR_Sub1.String = '5';
            UserValues.MIA.AR_Region(1) = 5;
            % Standard RICS with per-frame segmentation via intensity
            % thresholding (and possibly an additional hand-drawn ROI)
            MIAData.AR{1,1} = Mask;
            if ~isempty(val.ChannelTwo)
                MIAData.AR{2,1} = Mask;
            end
        end
        LSUserValues(1)
        Mia_Correct([],[],0);
    end


    %% Correlate data
    fprintf('SPECTRAL_RICS: Correlating data...\n')
    %h.Mia_Image.Settings.ROI_Frames.String = '1:30';
    Do_2D_XCor([],[]) %simple RICS

    %% Fit data
    f = 2;
    if f == 1
        % Use Mia's built-in fit function
        [F, T] = Mia_Command_Fit_fast(val.Analysis, params, channels);
    else
        %pass the scan parameters and wr_wz to MIAFit, along with the path
        [F, T] = MIAFit(ImPath, ImFile, val.Analysis, params, channels, blocks);
        % F is a figure (of correlation functions or time-dependent graphs)
        % T is a table with fit parameters
    end

    %% Save Results
    if ~isequal(F,0)
        fprintf('SPECTRAL_RICS: Preparing to display results...\n')
        writetable(T, fullfile(Path,'Results.csv'), 'WriteRowNames',1)
        %saveas(F,fullfile(Path,[ImFile(1:end-4) '_fit.tif']))
        print(F,fullfile(Path,'Results.png'),'-dpng','-r300')
        print(F,fullfile(Path,[ImFile(1:end-4), '_', val.Analysis.Type, '_fit.svg']),'-dsvg')
        fprintf('SPECTRAL_RICS_SUCCESS: Analysis done!\n')
    end

elseif strcmp(val.Analysis.Type, 'Intensity-based Heatmap')
    %% Adjust settings on the RICS and ROI tabs
    %%% Select the intensity based mapping on the (R)ICS tab
    h.Mia_Image.Calculations.Cor_Save_ICS.Value = 6;
    h.Mia_Image.Calculations.Cor_ICS_Scaling.Value = 3; %scale maps with times pixels are sampled
    MIA_Various([],[],11) %update the MIA_UI

    %%% Set the number of AROIs in Mia
    h.Mia_Image.Calculations.Cor_ICS_nAROI.String = num2str(val.Analysis.InputOne);
    LSUserValues(1)

    %% Load and apply freehand mask
    h.Mia_Image.Settings.ROI_FramesUse.Value = 3; %arbitrary ROI RICS
    MIA_Various([],[],[3,6]) %Show AR controls and update UserValues (probably obsolete)
    if ~isempty(val.Analysis.Mask)
        %%% Extract Mask FileName
        % Read the freehand mask from Zen from the file
        s = strfind(val.Analysis.Mask, '\');
        if isempty(s) %file generated on Mac
            [~, MaskFile, Ext] = fileparts(val.Analysis.Mask);
            MaskFile = [MaskFile Ext];
        else
            % take what comes after the last backslash as the filename
            MaskFile = val.Analysis.Mask(s(end)+1:end);
        end
        if ~exist(fullfile(Path,MaskFile),'file')
            fprintf('SPECTRAL_RICS_ERROR: Required mask file missing!\n')
            error('mask file not present')
            return
        else
            Mask = bfopen(fullfile(Path,MaskFile)); %4x1 cell
        end
        %MaskInfo = Mask{1,2};
        Mask = Mask(:,1); %framesx1 cell
        Mask = Mask{1}(:,1);
        Mask = reshape(Mask,1,1,size(Mask,1));
        Mask = logical(cell2mat(Mask));
        for j=1:size(Mask,3)
            Mask(:,:,j)=flipud(Mask(:,:,j));
        end
        % Assign the hand-drawn ROI from Zen to MIA
        for i = 1:size(MIAData.Data,1)
            if ~isempty(MIAData.Data{i,1})
                MIAData.MS{i,1} = Mask(:,:,1); %raw image
                MIAData.MS{i,2} = Mask(:,:,1); %corrected image
            end
        end
    else
        %% Assign ones to MIA
        % even if the user did not define an AROI before starting the
        % script, Mia has to be configured for AROI analysis, since the
        % automatic intensity based segmentation is done in AROIs.
        % Therefore we have to hardcode at least MIAData.MS as ones since it has to exist. 
        for i = 1:size(MIAData.Data,1)
            if ~isempty(MIAData.Data{i,1})
                MIAData.MS{i,1} = ones(size(MIAData.Data{i,1}(:,:,1)));
                MIAData.MS{i,2} = ones(size(MIAData.Data{i,1}(:,:,1)));
            end
        end
    end
    Mia_Correct([],[],0) %doesn't matter if I write 1 or 0 here

    %% Select if and how to do framewise intensity thresholding
    % check which channel to use for the intensity thresholding
    % this will only work if the CZI contains at max 2 channels
    % for grid-based Analysis.InputTwo is []
    if strcmp(val.Analysis.InputTwo, val.ChannelOne.Name)
        % user wants to mask channel 1 via intensity thresholding of channel 1
        UserValues.MIA.AR_Same = 2; %this is the first channel, as 1 = 'OR'
        h.Mia_Image.Settings.ROI_AR_Same.Value = 2;
        h.Mia_Image.Settings.ROI_AR_Channel.Value = 2; %just for consistency, also select 'channel 1' in the dropdown list in Mia
    else
        % user wants to mask channel 1 via intensity thresholding of channel 2
        UserValues.MIA.AR_Same = 3; %this is the second channel, as 1 = 'OR'
        h.Mia_Image.Settings.ROI_AR_Same.Value = 3;
        h.Mia_Image.Settings.ROI_AR_Channel.Value = 3; %just for consistency, also select 'channel 2' in the dropdown list in Mia
    end
    %MIA_Various([],[],6)
    LSUserValues(1)

    %% Calculate and Fit correlations
    %% Render heatmaps and save them
    fprintf('SPECTRAL_RICS: Correlating data...\n')
    Do_RICS([],[], {Path, ImPath}, ImFile, val.Analysis, params)

else %Grid-based Heatmap
    %% Adjust settings on the RICS and ROI tabs
    % maximize the usable area in the image since we're not doing intensity thresholding anyway
    h.Mia_Image.Settings.ROI_AR_Sub1.String = '1';
    UserValues.MIA.AR_Region(1) = 1;
    %%% Select the grid based mapping on the (R)ICS tab
    h.Mia_Image.Calculations.Cor_Save_ICS.Value = 5;
    h.Mia_Image.Calculations.Cor_ICS_Scaling.Value = 2; %scale with intensity
    MIA_Various([],[],11) %Update MIA UI

    %%% Set the grid resolution
    h.Mia_Image.Calculations.Cor_ICS_Window.String = num2str(val.Analysis.InputOne); % 8,16,32 or 64
    %%% Difference between the start of each grid unit (if Window = Offset then the windows are adjacent)
    if val.Analysis.InputOne > 31
        h.Mia_Image.Calculations.Cor_ICS_Offset.String = num2str(val.Analysis.InputOne/2);
    else
        h.Mia_Image.Calculations.Cor_ICS_Offset.String = num2str(val.Analysis.InputOne);
    end
    %%% Make the RICS fit size an odd number so there are as many
    %%% points before and after the max amplitude
    h.Mia_ICS.Size.String = num2str(val.Analysis.InputOne-1);
    LSUserValues(1)

    %% Load and apply freehand mask
    if ~isempty(val.Analysis.Mask)
        h.Mia_Image.Settings.ROI_FramesUse.Value = 3; %arbitrary ROI RICS
        MIA_Various([],[],[3,6]) %Show AR controls and update UserValues (probably obsolete)
        %%% Extract Mask FileName
        % Read the freehand mask from Zen from the file
        s = strfind(val.Analysis.Mask, '\');
        if isempty(s) %file generated on Mac
            [~, MaskFile, Ext] = fileparts(val.Analysis.Mask);
            MaskFile = [MaskFile Ext];
        else
            % take what comes after the last backslash as the filename
            MaskFile = val.Analysis.Mask(s(end)+1:end);
        end
        if ~exist(fullfile(Path,MaskFile),'file')
            fprintf('SPECTRAL_RICS_ERROR: Required mask file missing!\n')
            error('mask file not present')
            return
        else
            Mask = bfopen(fullfile(Path,MaskFile)); %4x1 cell
        end
        %MaskInfo = Mask{1,2};
        Mask = Mask(:,1); %framesx1 cell
        Mask = Mask{1}(:,1);
        Mask = reshape(Mask,1,1,size(Mask,1));
        Mask = logical(cell2mat(Mask));
        for j=1:size(Mask,3)
            Mask(:,:,j)=flipud(Mask(:,:,j));
        end
        % Assign the hand-drawn ROI from Zen to MIA
        for i = 1:size(MIAData.Data,1)
            if ~isempty(MIAData.Data{i,1})
                MIAData.MS{i,1} = Mask(:,:,1); %raw image
                MIAData.MS{i,2} = Mask(:,:,1); %corrected image
            end
        end
    else
        h.Mia_Image.Settings.ROI_FramesUse.Value = 1; %normal RICS, much faster
        MIA_Various([],[],3) %Hide AR controls and update UserValues
    end
    Mia_Correct([],[],0) %doesn't matter if I write 1 or 0 here

    %% Use default settings for the 'which masks' popup
    UserValues.MIA.AR_Same = 1; % = 'OR'
    h.Mia_Image.Settings.ROI_AR_Same.Value = 1;
    LSUserValues(1)

    %% Calculate and Fit correlations
    %% Render heatmaps and save them
    fprintf('SPECTRAL_RICS: Correlating data...\n')
    %no need to specify ImPath, but do it anyway
    Do_RICS([],[], {Path, ImPath}, ImFile, val.Analysis, params)
    fprintf('SPECTRAL_RICS_SUCCESS: Analysis done!\n')
end

%% Delete the Mia folder that was generated and that contained the Matlab files used for fitting the data
try
    rmdir(fullfile(ImPath,'Mia'),'s')
end

function [F, T] = Mia_Command_Fit_fast(analysis, params, channels)
% obsolete
% function that allowed to fit the RICS data directly in Mia

% F is a figure with the result data
% T is a table with the fit parameters

% In Mia, fit channel 2 is the CCF, so switch ACF2 and CCF
%params.w0(n) %(1) is ACF1, (2) is CCF, (3) is ACF2
w0acf2 = params.w0(2);
wzacf2 = params.wz(2);
params.w0(2) = params.w0(3);
params.wz(2) = params.wz(3);
params.w0(3) = w0acf2;
params.wz(3) = wzacf2;

h = guidata(findobj('Tag','Mia'));

% Set fit table initial parameters
if ~strcmp(analysis.Type, 'Calibration')
    for ch = channels
        h.Mia_ICS.Fit_Table.Data{5,ch} = num2str(params.w0(ch));
        h.Mia_ICS.Fit_Table.Data{7,ch} = num2str(params.wz(ch));
        h.Mia_ICS.Fit_Table.Data{1,ch} = num2str('100'); % set N = 100 to start
        h.Mia_ICS.Fit_Table.Data{3,ch} = num2str('1'); % set D = 1 to start
    end
else
    dye = analysis.Dye;
    wavelength = analysis.Wavelength;
    v_25 = 1.002; %viscosity of Water at 25degC in centiPoise
    D_25_dye = analysis.Diffusion;

    % % just as a test
    % % Formulas from the Peter Kapusta application note on correct
    % % diffusion constants
    % for T = 1:50
    %     if T<20.5
    %         v(T) = 10.^(1301./(998.333+8.1855*(T-20)+0.00585.*(T-20).^2)-1.30233);
    %     else
    %         v(T) = v_25.*10^((1.3272.*(20-T)-0.001053.*(T-20)^2)./(T+105));
    %     end
    %     D(T) = D_25_dye.*(T+273.15)./(v(T)/1000)*2.985.*10^(-6);
    %     OmageR_range(T) = sqrt(4*30/10^6*D(T))*1000;
    % end
    % % the percentage error on w0 if T is wrong
    % PctErrorOmageR = OmageR_range./OmageR_range(25)*100-100;
    % figure; plot(1:50,D); xlabel('diffusion constant (um^s/s)');
    % figure; plot(1:50, PctErrorOmageR); xlabel('error on w_0 (%)');

    T = analysis.Temperature;
    if T<20.5
        v_T = 10.^(1301./(998.333+8.1855*(T-20)+0.00585.*(T-20).^2)-1.30233);
    else
        v_T = v_25.*10^((1.3272.*(20-T)-0.001053.*(T-20)^2)./(T+105));
    end
    D_T_dye = D_25_dye.*(T+273.15)./(v_T/1000)*2.985.*10^(-6);
    h.Mia_ICS.Fit_Table.Data{3,1} = num2str(D_T_dye);
    h.Mia_ICS.Fit_Table.Data{4,1} = logical(1); %fix D
    h.Mia_ICS.Fit_Table.Data{5,1} = num2str(0.2);
    h.Mia_ICS.Fit_Table.Data{6,1} = logical(0); %free OmageR
    h.Mia_ICS.Fit_Table.Data{7,1} = num2str(1);
    h.Mia_ICS.Fit_Table.Data{8,1} = logical(0); %free OmegaZ
end

Do_RICS_Fit([],[],channels) %triggers RICS fitting of channel in Mia

% write csv file with the fit table
T = cell2table(h.Mia_ICS.Fit_Table.Data);
T.Properties.VariableNames = h.Mia_ICS.Fit_Table.ColumnName;
RowCell = cell(16,1);
for i = 1:8
    RowCell(2*i-1) = h.Mia_ICS.Fit_Table.RowName(2*i-1);
    RowCell(2*i) = {[h.Mia_ICS.Fit_Table.RowName{2*i-1} '_' h.Mia_ICS.Fit_Table.RowName{2*i}]};
end
T.Properties.RowNames = RowCell;
if channels == 1
    T = T(:,1);
end

% export an image of the correlation function
F = figure('Visible','off');
h.Mia_ICS.Fit_Type.Value = 3; %Fit/Residuals surface plot
Update_Plots([],[],2)
if channels == 1
    % ACF
    for i=1:3 % The first three types of plots
        subplot(2,2,i,copyfig(h.Mia_ICS.Axes(1,i)))
    end
    h.Mia_ICS.Fit_Type.Value = 4; %On Axes plot
    Update_Plots([],[],2)
    subplot(2,2,4,copyfig(h.Mia_ICS.Axes(1,4))) %On Axes plot
else
    % ACFs + CCF
    for i=1:3 % The first 3 types of plots
        for j=1:3 % The three CFs
            subplot(4,3,j+3*(i-1),copyfig(h.Mia_ICS.Axes(j,i)))
        end
    end
    h.Mia_ICS.Fit_Type.Value = 4; %On Axes plot
    Update_Plots([],[],2)
    for j=1:3 % Loop through the 3 CFs
        subplot(4,3,9+j,copyfig(h.Mia_ICS.Axes(j,4))) %On Axes Plot
    end
end
set(F,'Color','none')

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Choose the channel or image to display %%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function ImageDropDown(obj,~,ch1or2)
global MIAData UserValues
h = guidata(findobj('Tag','Mia'));

h.Mia_Image.Settings.AutoScale.Value = 1;
Update_Plots([],[],[1,4])

% the order of the strings in 'maps' has to match the order in MapColorBar
% name of the image
type = h.Mia_Image.Settings.Channel_PIE(ch1or2).String{h.Mia_Image.Settings.Channel_PIE(ch1or2).Value};
% number in the list
type2 = find(strcmp(type,UserValues.MIA.Maps));
pie = strcmp(h.Mia_Image.Settings.Channel_PIE(ch1or2).String{h.Mia_Image.Settings.Channel_PIE(ch1or2).Value}(1:3),'PIE');

if type2~=0 %plot some sort of map
    h.Plots.Image(ch1or2,1).CData = MIAData.Image{ch1or2,obj.Value};
    h.Mia_Image.Axes(ch1or2,1).XLim=[0 size(MIAData.Image{ch1or2,obj.Value},2)]+0.5;
    h.Mia_Image.Axes(ch1or2,1).YLim=[0 size(MIAData.Image{ch1or2,obj.Value},1)]+0.5;
    MapColorbar(MIAData.MapData{ch1or2,h.Mia_Image.Settings.Channel_PIE(ch1or2).Value}, ch1or2,type)
elseif pie
    % selected channel starts with 'PIE Channel:', meaning either that user changed the channel
    % without loading data to begin with, or that user wants to transfer
    % data from PAM to Mia
    % we just return here since the user has to load the data via the menu
    return
    fprintf('Load data from the menu!\n')
elseif ~strcmp(h.Mia_Image.Settings.Channel_PIE(ch1or2).String{h.Mia_Image.Settings.Channel_PIE(ch1or2).Value},'Nothing')
    Update_Plots([],[],[1,4])
    %user wants to display something in channel 2
    h.Plots.Image(ch1or2,1).CData = MIAData.Image{ch1or2,obj.Value};
else
    Update_Plots([],[],[1,4])
    %user does not want to display something in channel 2
end
% display the background that corresponds to the specific displayed channel
% in the editbox on the Correction tab.
h.Mia_Image.Settings.Background(ch1or2).String = num2str(UserValues.MIA.BG(h.Mia_Image.Settings.Channel_PIE(ch1or2).Value));

function ScaleBar(channel)
global MIAData
h = guidata(findobj('Tag','Mia'));
barx = str2double(h.Mia_Image.Settings.ScaleBar.String);
if barx~=0 && ~isnan(barx)
    pixsize = str2double(h.Mia_Image.Settings.Pixel_Size.String)/1000; %in um
    roix = str2double(h.Mia_Image.Settings.ROI_SizeX.String); %ROI size is always smaller than imsize so ok.
    roiy = str2double(h.Mia_Image.Settings.ROI_SizeY.String);
    if  barx > 0.7*roix*pixsize
        % scale bar too large, make scale bar 1/10th of the image size
        barx = round(roix*pixsize/10);
        h.Mia_Image.Settings.ScaleBar.String = num2str(barx);
    end
    barwidth = floor(barx/pixsize); %in pixels
    y = floor(roiy/40);
    x = floor(roix/40);
    for j = 1:2
        h.Plots.Image(channel,j).CData(1+3*y:1+4*y,end-barwidth-4*x:end-4*x,:) = 1; %max(max(h.Plots.Image(i,j).CData));
    end
    if ~isempty(MIAData.AR) || ~all(all(MIAData.MS{1}))
        h.Plots.Image(channel,2).AlphaData(1+3*y:1+4*y,end-barwidth-4*x:end-4*x) = 1;
    end
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Function to Load and Analyse a calibration CZI file %%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function Calibration(~,~)
global MIAData UserValues
h = guidata(findobj('Tag','Mia'));

prompt={'Channel:','Diffusion constant at 25degC [um2/s]:','Temperature [degC]'};
name='PSF Waist Calibration';
numlines=1;
defaultanswer={'1','470','23'};

answer=inputdlg(prompt,name,numlines,defaultanswer);

Analysis = struct;
Analysis.Dye = '';
Analysis.Wavelength = [];
Analysis.Diffusion = str2double(answer{2});
Analysis.Temperature = str2double(answer{3});

%% General MIA settings that have to be set correctly
% Data is always photon counting, since we convert it to photon counting if it's not
h.Mia_Image.Settings.kHz.Value = 1;
% Data is always raster scanning
h.Mia_Image.Settings.scanning.Value = 1;
UserValues.MIA.Options.scanning = 1;
UserValues.MIA.Options.kHz = 1;
% check the save image checkbox on the options tab
h.Mia_Image.Settings.SaveImage.Value = 1;
% automatic filenaming
h.Mia_Image.Calculations.Save_Name.Value = 1;
% overwrite existing files
h.Mia_Image.Calculations.Overwrite.Value = 1;
% Maximize the usable area
h.Mia_Image.Settings.ROI_AR_Sub1.String = '1';
UserValues.MIA.AR_Region(1) = 1;
LSUserValues(1)

%% Settings on RICS tab: which channels to correlate
%% Settings on Options tab: which channels to load
%channel range in CZI file for first imaging channel in Mia
h.Mia_Image.Settings.Custom(2).String = '1';
UserValues.MIA.Custom.Zeiss_CZI{1} = '1';
%h.Mia_Image.Settings.Channel_PIE(1).String = ['channel ' answer(1)];
% User wants to do channel 1 correlation analysis
h.Mia_Image.Calculations.Cor_Type.Value = 1;
channels = 1;
%h.Mia_Image.Settings.Channel_PIE(2).String = '';
for c = 2:5
    h.Mia_Image.Settings.Custom(c).String = ''; %channel 3, 4,5 on the Options tab
    UserValues.MIA.Custom.Zeiss_CZI{c} = '';
end
h.Mia_Image.Settings.Custom(7).Value=0; % don't show spectrum
UserValues.MIA.Custom.Zeiss_CZI{6} = 0;
h.Mia_Image.Settings.Custom(8).String='1'; % just a single z-plane
UserValues.MIA.Custom.Zeiss_CZI{7} = '1';
LSUserValues(1);
% Pass the Custom filetype popupmenu as argument to the function, as if
% user changed the popupmenu themselves
h.Mia_Image.Settings.FileType.Value = 3; %Zeiss_CZI
MIA_CustomFileType(h.Mia_Image.Settings.FileType,[],1);
%%% Load CZI file using the custom filetype readin routine
% I have to store the Path and Image as a global variable to access it
% User loads data from Mia
%%% Extracts information for data loading
Extension = h.Mia_Image.Settings.FileType.UserData{1};
Info = h.Mia_Image.Settings.FileType.UserData{2};
[FileName,Path] = uigetfile({Extension}, Info, UserValues.File.MIAPath, 'MultiSelect', 'on');
MIAData.ImPath = Path;
MIAData.ImFile = FileName;
MIAData.RLICS = [];
%% Load data
Zeiss_CZI(4) %doesn't display it yet
%% Place Mia's ROI
%%% Set the ROI to cover the whole image
h.Mia_Image.Settings.ROI_SizeX.String = num2str(size(MIAData.Data{1,1},1));
h.Mia_Image.Settings.ROI_SizeY.String = num2str(size(MIAData.Data{1,1},2));
h.Mia_Image.Settings.ROI_PosX.String = '1';
h.Mia_Image.Settings.ROI_PosY.String = '1';
Mia_ROI([],[],1)
%work with raw data
h.Mia_Image.Settings.Correction_Add.Value = 1; %add nothing
h.Mia_Image.Settings.Correction_Subtract.Value = 1; %subtract nothing

% Scan  settings are loaded from the czi file and displayed in Mia.
params.linetime = str2double(h.Mia_Image.Settings.Image_Line.String);
params.pixeltime = str2double(h.Mia_Image.Settings.Image_Pixel.String);
params.pixelsize = str2double(h.Mia_Image.Settings.Pixel_Size.String);

% set the dropdown menu next to the RICS button to 'save as .miacor'
h.Mia_Image.Calculations.Cor_Save_ICS.Value = 2;
% always overwrite existing files, Mia will generate 1 file per block
h.Mia_Image.Calculations.Overwrite.Value = 1;
blocks = 1;

% Standard RICS without segmentation
h.Mia_Image.Settings.ROI_FramesUse.Value = 1; %normal RICS
MIA_Various([],[],3)

%% Correlate data
Do_2D_XCor([],[]) %simple RICS

Analysis.Type = 'Calibration';

%pass the scan parameters and wr_wz to MIAFit, along with the path
[F, T] = MIAFit(ImPath, FileName, Analysis, params, channels, blocks);

%store the w_r/w_z parameters on the RICS tab
h.Mia_ICS.Fit_Table.Data{5,floor(1.5*str2double(answer{1}))} = num2str(T{5,1}{1});
h.Mia_ICS.Fit_Table.Data{7,floor(1.5*str2double(answer{1}))} = num2str(T{6,1}{1});

%recalculate w_r/w_z parameters for the CCF 
h.Mia_ICS.Fit_Table.Data{5,2} = num2str(sqrt(0.5.*(str2double(h.Mia_ICS.Fit_Table.Data{5,1})^2+str2double(h.Mia_ICS.Fit_Table.Data{5,3})^2)));
h.Mia_ICS.Fit_Table.Data{7,2} = num2str(sqrt(0.5.*(str2double(h.Mia_ICS.Fit_Table.Data{7,1})^2+str2double(h.Mia_ICS.Fit_Table.Data{7,3})^2)));

UserValues.MIA.FitParams.wr(str2double(answer{1})) = T{5,1}{1};
UserValues.MIA.FitParams.wz(str2double(answer{1})) = T{6,1}{1};
LSUserValues(1)

%% Save Results
writetable(T, fullfile(Path,[FileName(1:end-4) '_parameters.csv']), 'WriteRowNames',1)
%saveas(F,fullfile(Path,[ImFile(1:end-4) '_fit.tif']))

print(F,fullfile(Path,[FileName(1:end-4) '_fit.png']),'-dpng','-r300')
%print(F,fullfile(Path,[ImFile(1:end-4) '_fit.svg']),'-dsvg')

F.Visible='on';
disp(T)

function Open_Doc(~,~)
global PathToApp
% if isunix
%     path = fullfile(PathToApp,'doc/build/html/index.html');
% elseif ispc
%     path = fullfile(PathToApp,'doc\build\html\index.html');
% end
path = 'https://pam.readthedocs.io/en/latest/mia.html';
if ~isdeployed
    web(path);
else
    %%% use system call to browser
    %if isunix
    %    % fix spaces in path
    %    path = strrep(path,' ','\ ');
    %end
    web(path,'-browser');
end

function Open_Notepad(~,~)
%%% Check whether notepad is open
notepad = findobj('Tag','PAM_Notepad');
if isempty(notepad)
    Notepad('PAM');
else
    figure(notepad);
end

function MIA_Drift(~,~)
%% Correct the image series for linear drift
% assumption is linear x/y drift, no rotation, no stretching/shrinking

global MIAData
h = guidata(findobj('Tag','Mia'));


%%% Extracts ROI position
From=h.Plots.ROI(1).Position(1:2)+0.5;
To=From+h.Plots.ROI(1).Position(3:4)-1;

for i = 1:size(MIAData.Data,1)
    if ~isempty(MIAData.Data{i,1})
        f = 1:(size(MIAData.Data{i,1},3)-1);
        % computes drift from channel 1
        if i == 1
            % computes the drift in the selected ROI
            Im1 = double(MIAData.Data{i,1}(From(2):To(2),From(1):To(1),1));
            y = zeros(size(f));
            x = zeros(size(f));
            y0=round(size(Im1,1)/2);
            x0=round(size(Im1,2)/2);
            for j = f
                Im2=double(MIAData.Data{i,1}(From(2):To(2),From(1):To(1),j+1));
                CrCorr=fftshift(real(ifft2(fft2(Im1).*conj(fft2(Im2)))))/(size(Im1,1)*size(Im1,2));
                CrCorr=imgaussfilt(CrCorr,2); %Gaussian filtering to avoid errors when the correlation has 2 maximums
                [y(j),x(j)] = find(CrCorr == max(CrCorr(:)));
                y(j)=y(j)-y0-1;
                x(j)=x(j)-x0-1;
            end
            MIAData.Drift = vertcat(f, x, y);
        end

        % Shifts the raw data
        for j = f
            MIAData.Data{i,1}(:,:,j+1) = circshift(MIAData.Data{i,1}(:,:,j+1),[y(j) x(j)]);
        end
    end
end


%% plot drift correction
if h.Mia_Image.Settings.Correction_Drift.Value
    figure
    hold on
    plot(MIAData.Drift(1,:), MIAData.Drift(2,:),'b') %x
    plot(MIAData.Drift(1,:), MIAData.Drift(3,:),'r') %y
    legend('x', 'y')
    hold off
end
Update_Plots([],[],[1,4])
Mia_Correct([],[],1)
%%