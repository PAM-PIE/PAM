function w_res_MLE = MLE_w_res(model,data)
%%% Returns the weighted residuals based on Poissonian counting statistics,
%%% as described in Laurence TA, Chromy BA (2010) Efficient maximum likelihood estimator fitting of histograms. Nat Meth 7(5):338?339.
%%%
%%% The sum of the weighted residuals is then analogous to a chi2 goodness-of-fit estimator.

valid = true(size(data));

% filter zero bins in data to avoid divsion by zero and in model to avoid log(0)
valid = valid & (data ~= 0) & (model ~= 0);

% compute MLE residuals:
%
% chi2_MLE = 2 sum(data-model) -2*sum(data*log(model/data))
%
% For invalid bins, only compute the first summand.

log_summand = zeros(size(data));
log_summand(valid) = data(valid).*log(model(valid)./data(valid));
w_res_MLE = 2*(model - data) - 2*log_summand; %squared residuals
% avoid complex numbers
w_res_MLE(w_res_MLE < 0) = 0;
w_res_MLE = sqrt(w_res_MLE);