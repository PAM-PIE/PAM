function [c, offset, A, tau, z, t, chi, dc, dtau] = fit_exp_simplex(irf, y, p, dt, c, tau, lim, init, fit, fittype, ignore, weight_method)
% The function performs a fit of a multi-exponential decay curve.
% It is called by: 
% [c, offset, A, tau, dc, doffset, dtau, irs, z, t, chi] = fluofit(irf, y, p, dt, c, tau, limits, init, fitfun, conv_type).
% The function arguments are:
% irf 	= 	Instrumental Response Function, if empty, convolution is skipped
% y 	= 	Fluorescence decay data
% p 	= 	Time between laser exciation pulses (in nanoseconds)
% dt 	= 	Time width of one TCSPC channel (in nanoseconds)
% tau 	= 	Initial guess times
% lim   = 	limits for the shift and lifetimes as a 2-column matrix
% init  =   Whether to use initial guess routine
% fit	=	Whether to fit or not. When set to false, only computes the amplitude and decay based on initial parameters 
% fittype = 'lsfit' for least squares fit and 'mlfit' for maximum likelihood estimation
% ignore = number of bins to ignore for fitting
% weight_method =  'Pearson', 'Neyman', or 'Equal'. Use [] for MLE
%
% The return parameters are:
% c	=	Color Shift (time shift of the IRF with respect to the fluorescence curve)
% offset	=	Offset
% A	    =   Amplitudes of the different decay components
% tau	=	Decay times of the different decay components
% dc	=	Color shift error
% doffset	= 	Offset error
% dtau	=	Decay times error
% irs	=	IRF, shifted by the value of the colorshift
% z	    =   Fitted fluorecence component curve
% t     =   time axis
% chi   =   chi2 value
% 
% Based on Enderlein, J. & Erdmann, R. Fast fitting of multi-exponential decay curves. Opt Commun 134, 371-378 (1997)

if ~isempty(irf)
    irf = irf(:);
    reconv = true;
else
    reconv = false;
end

y_fit = y(ignore:end,:);
%y_fit = y_fit(:);

switch fittype
    case 'lsfit'
        fitfun = @lsfit;
    case 'mlfit'
        fitfun = @mlfit;
end

%% Initial guess routine
if init
    sh_min = max([floor(c-5) lim(1,1)]);
    sh_max = min([ceil(c+5) lim(1,2)]);
    [a_guess, t_guess, ~, c, ~, ~, ~] = DistFit(irf, y, p, dt, [sh_min sh_max]);

    %%% limit guesses to number of lifetime components in fit
    n_guess = length(t_guess);
    n_tau = length(tau);
    if n_guess > n_tau
        idx = kmeans(log10(t_guess), n_tau);
        t_guess = accumarray(idx, t_guess.*a_guess);
        t_guess = t_guess ./ accumarray(idx, a_guess);
        tau = sort(t_guess);
    elseif n_guess == n_tau
        tau = t_guess;
    elseif n_guess < n_tau
        % NYI
    end
end

%% fit
p = p/dt;
tau = tau(:)'/dt; 
lim(2:end,:) = lim(2:end,:)./dt;
t = (1:size(y,1))';
param = [c; tau'];
if fit
    % Decay times and Offset are assumed to be positive.
    paramin = lim(:,1);
    paramax = lim(:,2);
    [param, dparam] = Simplex(fitfun, param, paramin, paramax, [], [], irf, y_fit, p, ignore, weight_method);
    c = param(1);
    dc = dparam(1);
    tau = param(2:length(param))';
    dtau = dparam(2:length(param));
else
    dtau = 0;
    dc = 0;
end

%% compute fitted curve and output parameters
[err, A, z] = fitfun(param, irf, y_fit, p, ignore, weight_method);

n = size(y_fit, 1);
m = size(tau, 1);
chi = err./(n-m);
t = dt.*t;
tau = dt.*tau';
dtau = dt.*dtau';
%c = dt*c;
%dc = dt*dc;
offset = A(1,:); 
A(1,:) = [];
