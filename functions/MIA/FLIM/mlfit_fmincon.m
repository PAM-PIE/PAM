function [err, derr] = mlfit_fmincon(param_fit, param, paramfix, irf, y, p, ignore)
%	MLFIT(param, irf, y, p) returns the weighted residuals between the data y 
%	and the computed values based on Poissonian counting statistics,
%   as described in Laurence TA, Chromy BA (2010) Efficient maximum likelihood
%   estimator fitting of histograms. Nat Meth 7(5):338?339.
%	
%   MLFIT assumes a function of the form:
%
%	  y =  yoffset + A(1)*convol(irf,exp(-t/tau(1)/(1-exp(-p/tau(1)))) + ...
%
%	param(1) is the color shift value between irf and y.
%	param(2) is the irf offset.
%	param(3:...) are the decay times.
%	irf is the measured Instrumental Response Function.
%	y is the measured fluorescence decay curve.
%	p is the time between to laser excitations (in number of TCSPC channels).

param(~paramfix) = param_fit;

tp = (1:p)' + ignore - 1;
c = param(1);
offset = param(2);
param(1:2) = [];
n_tau = length(param)/2;
tau = param(1: n_tau); tau = tau(:)';
A = param((n_tau+1):end); A = A(:)';
x = offset + exp(-(tp-1)*(1./tau))*(A./(1-exp(-p./tau)))';
if ~isempty(irf)
    irs = shift_by_fraction(irf, c);
    z = convol(irs, x);
    z = z(ignore:end);
else
    irs = [];
    z = shift_by_fraction(x, ignore-1);
    z = z(ignore:end);
    z = z(1:length(y));
end

err = chisq_mle(z, y);

if nargout > 1
    derr = dchisq_mle(z, y, tau, A, irs, tp, p, ignore);
    derr = derr(~paramfix);
end



function chisq = chisq_mle(model,data)
%%% Returns the weighted residuals based on Poissonian counting statistics,
%%% as described in Laurence TA, Chromy BA (2010) Efficient maximum likelihood estimator fitting of histograms. Nat Meth 7(5):338?339.
%%%
%%% The sum of the weighted residuals is then analogous to a chi2 goodness-of-fit estimator.

% filter zero bins in data to avoid divsion by zero and in model to avoid log(0)
valid = (data ~= 0) & (model ~= 0);

% compute MLE residuals:
%
% chi2_MLE = 2 sum(data-model) -2*sum(data*log(model/data))
%
% For invalid bins, only compute the first summand.

log_summand = zeros(size(data));
log_summand(valid) = data(valid).*log(model(valid)./data(valid));

chisq = 2*sum(model - data) - 2*sum(log_summand); %squared residuals

function dchisq = dchisq_mle(model, data, tau, A, irf, tp, p, ignore)

exponent1 = exp(-(tp-1)*(1./tau));
exponent2 = exp(-p./tau);
factor1 = p.*exponent2./(1-exponent2);
factor2 = (1-exponent2).*(tau.^2);

dfda = exponent1*diag(1./(1-exponent2));
dfdt = (exponent1.*(tp-1))*diag(A./factor2) + ...
      exponent1*diag(A.*factor1./factor2) ;
if ~isempty(irf)
    df = convol(irf, [dfdt dfda]);
    df = df(ignore:end, :);
else
    df = shift_by_fraction([dfdt dfda], ignore-1);
    df = df(ignore:end, :);
    df = df(1:length(model),:);
end
df = [-gradient(model) ones(size(model)) df];

dchisq = 2*sum((1-data./model).*df);
dchisq = dchisq';