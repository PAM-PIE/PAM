function [cx, tau, offset, csh, z, t, err] = DistFit(irf, y, p, dt, shift, ntau, tau_lim, ignore)
% The function performs a fit of a distributed decay curve.
% It is called by: 
% [cx, tau, offset, csh, z, t, err] = DistFit(irf, y, p, dt, shift, ntau, tau_lim).
% The function arguments are:
% irf 	= 	Instrumental Response Function
% y 	= 	Fluorescence decay data
% p 	= 	Time between laser exciation pulses (in nanoseconds)
% dt 	= 	Time width of one TCSPC channel (in nanoseconds)
% shift	=	boundaries of colorshift in channels
% ntau  =   Number of tau values to sample
% tau_lim = lower and upper bounds for the tau values
%
% The return parameters are:
% cx	    =	lifetime distribution
% tau       =   used lifetimes
% offset    =	Offset
% csh       =   Color Shift
% z 	    =	Fitted fluorecence curve
% t         =   time axis
% err       =   chi2 value
% 
% Based on Enderlein, J. & Erdmann, R. Fast fitting of multi-exponential decay curves. Opt Commun 134, 371-378 (1997)
% and DistFluofit.m by Enderlein J

if nargin<6 || isempty(ntau)
    ntau = 100;
end
if nargin<7 || isempty(tau_lim)
    tau_lim = [p/100, p];
end
if nargin<8 || isempty(ignore)
    ignore = 1;
end

if isempty(irf)
    irf = zeros(size(y));
    irf(1) = 1;
end
irf = irf(:);
y = y(:);
y = y(ignore:end);
n = length(irf); 
p = p/dt;
tp = (1:p)';
t = (1:n)';

if nargin>4 && ~isempty(shift)
    sh_min = shift(1);
    sh_max = shift(2);
else
    sh_min = -3;
    sh_max = 3;
end

%% Generate decays
tau = (logspace(log10(tau_lim(1)), log10(tau_lim(2)), ntau))/dt; % distribution of decay times
x = exp(-(tp-1)*(1./tau))*diag(1./(1-exp(-p./tau)));
if ~isempty(irf)
    x = convol(irf, x);
    x = x(ignore:end,:);
else
    x = shift_by_fraction(x, ignore-1);
    x = x(ignore:end,:);
    x = x(1:length(y));
end
M0 = [ones(size(x, 1), 1) x];
%M0 = M0./(ones(n,1)*sum(M0));
%err = [];

%% Fit shift
if sh_max-sh_min>0
    csh = sh_min:0.1:sh_max;
    err = zeros(size(csh));
    for i = 1:length(csh)
        M = shift_by_fraction(M0, csh(i));
        cx = lsqnonneg(M,y);
        z = M*cx;
        
        err(i) = sum((z-y).^2./abs(z));
        %err(end)
    end
    [~, idx] = min(err);
    csh = csh(idx);
else
    csh = sh_min;
end

%% Compute linear coefficients
M = shift_by_fraction(M0, csh);
cx = lsqnonneg(M,y);
z = M*cx;
err = sum((z-y).^2./abs(z))/n;

tau = dt*tau';
offset = cx(1);
cx(1) = [];
tau = tau(cx>0);
cx = cx(cx>0);