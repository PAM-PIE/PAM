function [c, offset, A, tau, z, t, chi, exitflag, output] = fit_exp_fmincon(irf, y, p, dt, x0, lim, init, fit, fittype, ignore, weight_method, options)
% The function performs a fit of a multi-exponential decay curve using fmincon (trust-region-reflective)
% It is called by: 
% [c, offset, A, tau, dc, dtau, z, t, chi] = fit_exp_tr(irf, y, p, dt, c, x0, limits, init, fitfun, ignore, weight_method).
% The function arguments are:
% irf 	= 	Instrumental Response Function, if empty, convolution is skipped
% y 	= 	Fluorescence decay data
% p 	= 	Time between laser exciation pulses (in nanoseconds)
% dt 	= 	Time width of one TCSPC channel (in nanoseconds)
% x0 	= 	Initial parameter guesses as a column vector [c; offset; tau; A]
% lim   = 	Lower and upper bounds parameters as a 2-column matrix
% init  =   Whether to use initial guess routine
% fit	=	Whether to fit or not. When set to false, only computes the amplitude and decay based on initial parameters 
% fittype = 'lsfit' for least squares fit and 'mlfit' for maximum likelihood estimation
% ignore = number of bins to ignore for fitting
% weight_method =  'Pearson', 'Neyman', or 'Equal'. Ignored/use [] for MLE
% options = fit options for fmincon
%
% The return parameters are:
% c	=	Color Shift (time shift of the IRF with respect to the fluorescence curve)
% offset	=	Offset
% A	    =   Amplitudes of the different decay components
% tau	=	Decay times of the different decay components
% dc	=	Color shift error
% doffset	= 	Offset error
% dtau	=	Decay times error
% irs	=	IRF, shifted by the value of the colorshift
% z	    =   Fitted fluorecence component curve
% t     =   time axis
% chi   =   chi2 value
% 
% Based on Enderlein, J. & Erdmann, R. Fast fitting of multi-exponential decay curves. Opt Commun 134, 371-378 (1997)

if ~isempty(irf)
    irf = irf(:);
    reconv = true;
else
    reconv = false;
end

y_fit = y(ignore:end);
y_fit = y_fit(:);

n_tau = (length(x0) - 2)/2;
t_idx = (1:n_tau)+2;

%% Initial guess routine
if init
    c = x0(1);
    sh_min = max([floor(c-5) lim(1,1)]);
    sh_max = min([ceil(c+5) lim(1,2)]);
    [a_guess, t_guess, offset, c, ~, ~, ~] = DistFit(irf, y, p, dt, [sh_min sh_max]);

    %%% limit guesses to number of lifetime components in fit
    n_guess = length(t_guess);
    tau = x0(t_idx);
    A = x0(t_idx+n_tau);
    if n_guess > n_tau
        idx = kmeans(log10(t_guess), n_tau);
        t_guess = accumarray(idx, t_guess.*a_guess);
        a_guess = accumarray(idx, a_guess);
        t_guess = t_guess ./ a_guess;
        [tau, tau_idx] = sort(t_guess);
        A = a_guess(tau_idx);
    elseif n_guess == n_tau
        [tau, tau_idx] = sort(t_guess);
        A = a_guess(tau_idx);
    elseif n_guess < n_tau
        tau(1:n_guess) = t_guess;
        A(1:n_guess) = a_guess;
    end
    x0 = [c; offset; tau; A];
else
    [~,A] = mlfit([x0(1); x0(t_idx)./dt], irf, y_fit, p/dt, ignore, []);
    offset = A(1); A(1) = [];
    x0(t_idx+n_tau) = A;
end

%% fit
p = p/dt;
%tp = (1:p)';

x0(t_idx) = x0(t_idx)./dt;
lim(t_idx,:) = lim(t_idx,:)./dt;
lim(t_idx+n_tau, :) = lim(t_idx+n_tau, :).*(sum(A)+offset);

paramin = lim(:,1);
paramax = lim(:,2);
x_fix = paramin == paramax;
param_fit = x0(~x_fix);
paramin = paramin(~x_fix);
paramax = paramax(~x_fix);

switch fittype
    case 'lsfit'
        fitfun = @(x) lsfit_fmincon(x, x0, x_fix, irf, y_fit, p, ignore, weight_method);
    case 'mlfit'
        fitfun = @(x) mlfit_fmincon(x, x0, x_fix, irf, y_fit, p, ignore);
end
if isempty(options)
    options = optimoptions(@fmincon,'Algorithm','trust-region-reflective',...
        'SpecifyObjectiveGradient',true, 'Display', 'none');
end

if fit
    [param_fit, err, exitflag, output] = fmincon(fitfun, param_fit, [],[],[],[], paramin, paramax, [], options);
end

x0(~x_fix) = param_fit;
c = x0(1);
offset = x0(2);
tau = x0(t_idx);
A = x0(t_idx+n_tau);

%% compute fitted function
tp = (1:p)' + ignore - 1;
x = offset + exp(-(tp-1)*(1./tau'))*(A./(1-exp(-p./tau)));
if ~isempty(irf)
    irs = shift_by_fraction(irf, c);
    z = convol(irs, x);
    z = z(ignore:end);
else
    z = shift_by_fraction(x, ignore-1);
    z = z(ignore:end);
    z = z(1:length(y_fit));
end

n = length(y_fit);
m = sum(paramin~=paramax);

tau = tau.*dt;
chi = err/(n-m);

t = 1:length(y);
t = dt*t;