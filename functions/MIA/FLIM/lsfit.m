function [err, A, z] = lsfit(param, irf, y, p, ignore, weight_method)
%	LSFIT(param, irf, y, p) returns the Least-Squares deviation between the data y 
%	and the computed values. 
%	LSFIT assumes a function of the form:
%
%	  y =  yoffset + A(1)*convol(irf,exp(-t/tau(1)/(1-exp(-p/tau(1)))) + ...
%
%	param(1) is the color shift value between irf and y.
%	param(2) is the irf offset.
%	param(3:...) are the decay times.
%	irf is the measured Instrumental Response Function.
%	y is the measured fluorescence decay curve.
%	p is the time between to laser excitations (in number of TCSPC channels).


%n = length(irf);
%t = 1:n;
tp = (1:p)';
c = param(1);
tau = param(2:length(param)); tau = tau(:)';
x = exp(-(tp-1)*(1./tau))*diag(1./(1-exp(-p./tau)));
if ~isempty(irf)
    irs = shift_by_fraction(irf, c);
    z = convol(irs, x);
    z = z(ignore:end,:);
else
    z = shift_by_fraction(x, ignore-1);
    z = z(ignore:end,:);
    z = z(1:size(y, 1),:);
end

z = [ones(size(z,1),1) z];
A = z\y;
for idx = find(any(A < 0))
    A(:,idx) = lsqnonneg(z, y(:,idx));
end

z = z*A;
% if 1
%     semilogy(t, irs/max(irs)*max(y), t, y, 'bo', t, z);
% 	drawnow
% end
switch weight_method
    case 'Pearson'
        sigma = abs(z);
    case 'Neyman'
        sigma = y;
        sigma(sigma < 1) = 1;
    case 'Equal'
        sigma = 1;
end
err = sum((z-y).^2./sigma);

