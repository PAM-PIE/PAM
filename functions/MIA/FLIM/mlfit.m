function [err, A, z] = mlfit(param, irf, y, p, ignore, ~)
%	MLFIT(param, irf, y, p) returns the weighted residuals between the data y 
%	and the computed values based on Poissonian counting statistics,
%   as described in Laurence TA, Chromy BA (2010) Efficient maximum likelihood
%   estimator fitting of histograms. Nat Meth 7(5):338?339.
%	
%   MLFIT assumes a function of the form:
%
%	  y =  yoffset + A(1)*convol(irf,exp(-t/tau(1)/(1-exp(-p/tau(1)))) + ...
%
%	param(1) is the color shift value between irf and y.
%	param(2) is the irf offset.
%	param(3:...) are the decay times.
%	irf is the measured Instrumental Response Function.
%	y is the measured fluorescence decay curve.
%	p is the time between to laser excitations (in number of TCSPC channels).

%n = length(irf);
%t = 1:n;
tp = (1:p)' + ignore - 1;
c = param(1);
tau = param(2:length(param)); tau = tau(:)';
x = exp(-(tp-1)*(1./tau))*diag(1./(1-exp(-p./tau)));
if ~isempty(irf)
    irs = shift_by_fraction(irf, c);
    z = convol(irs, x);
    z = z(ignore:end,:);
else
    z = shift_by_fraction(x, ignore-1);
    z = z(ignore:end,:);
    z = z(1:size(y, 1),:);
end

z = [ones(size(z,1),1) z];
A = z\y;
for idx = find(any(A < 0))
    A(:,idx) = lsqnonneg(z, y(:,idx));
end
z = z*A;

err = chisq_mle(z, y);

function chisq = chisq_mle(model,data)
%%% Returns the weighted residuals based on Poissonian counting statistics,
%%% as described in Laurence TA, Chromy BA (2010) Efficient maximum likelihood estimator fitting of histograms. Nat Meth 7(5):338?339.
%%%
%%% The sum of the weighted residuals is then analogous to a chi2 goodness-of-fit estimator.

% filter zero bins in data to avoid divsion by zero and in model to avoid log(0)
valid = (data ~= 0) & (model ~= 0);

% compute MLE residuals:
%
% chi2_MLE = 2 sum(data-model) -2*sum(data*log(model/data))
%
% For invalid bins, only compute the first summand.

log_summand = zeros(size(data));
log_summand(valid) = data(valid).*log(model(valid)./data(valid));

chisq = 2*sum(model - data) - 2*sum(log_summand); %squared residuals




