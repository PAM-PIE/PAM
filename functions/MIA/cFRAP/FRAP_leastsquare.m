function [chi2 Numdatapoint alpha Ddis Fexp Fitdata output]=FRAP_leastsquare(Imprebleachmean,topleftrect,sizerect,topleftroi,sizeroi,Fdata,Ffit,parfit,xytdata,xytdataplot,parnonfit,sigma2,Pset)
% function [alpha Ddis Fexp Fitdata output] = FRAP_MEM(Imprebleachmean,topleftrect,sizerect,topleftroi,sizeroi,Fdata,parfit,xytdata,xytdataplot,parnonfit,sigma2,Pset)

D_fit=parfit(1); K0=parfit(2);r2av=parfit(4); % ranhua adds
% D_fit=parfit(1); K0=0.4;r2av=9.0;
Lx = parnonfit(1); Ly = parnonfit(2); f0 = parnonfit(3); % ranhua adds
% x = xytdataplot(1,:); y = xytdataplot(2,:); t = xytdataplot(3,:);  % ranhua adds
x = xytdata(1,:); y = xytdata(2,:); t = xytdata(3,:);  % ranhua adds

NumFdata = numel(Fdata); %number of F data
% Numdatapoint=numel(sigma2);
Numdatapoint=NumFdata;%number of datapoints
NumDdis =Pset(1); %numer of discretization points in D-space
Dmin=Pset(2);  % ranhua adds
Dmax=Pset(3);   % ranhua adds
set_noise=Pset(4);
Nstep=Pset(5);
Nsteppix=Pset(6);
Ddis=logspace(log10(Dmin),log10(Dmax),NumDdis);


%The function fmincon can be used to minimize the negative entropy (which
%is the same as maximizing the entropy), on condition that the chi^2 should
%fall within the expectance region N-sqrt(2N)<chi^2<N+sqrt(2N).

%A constant function has maximum entropy, so this should be our initial
%guess under the constraint that the summ of all fractions should be 1:
% Di_0=ones(NumDdis,1)/NumDdis;

K0i_0=ones(NumDdis,1)*K0/NumDdis;
r2i_0=ones(NumDdis,1)*r2av;
alpha_0 = [K0i_0 r2i_0];
alpha_0 = reshape(alpha_0,2*NumDdis,1);

% alpha_0 = [Di_0 K0i_0 r2i_0];
% alpha_0 = [Di_0 K0i_0];
% alpha_0 = reshape(alpha_0,3*NumDdis,1);

%The constraint that the sum of all fractions should be 1 should be
%included as Aeq*alpha=1, where Aeq is a unit row vector [1 1 ... 1]:

% AeqDi = ones(1,NumDdis);
AeqK0i= zeros(1,NumDdis);
Aeqr2i= zeros(1,NumDdis);
Aeq=[AeqK0i Aeqr2i];
beq = K0;

Auneq1=ones(1,NumDdis);
Auneq2=zeros(1,NumDdis);
Auneq=[Auneq1 Auneq2];
buneq=1-1e-3/NumDdis;

%Furthermore it should be specified that the alpha-values cannot be smaller
%than 0, nor larger than 1:

% lbDi = zeros(NumDdis,1)+1/NumFdata;
% ubDi = ones(NumDdis,1);


lbK0i=zeros(NumDdis,1)+1/NumFdata;
ubK0i=zeros(NumDdis,1)+1.0;
lbr2i= zeros(NumDdis,1)+1e-3/NumDdis;
ubr2i= zeros(NumDdis,1)+100;
lb=[lbK0i lbr2i];
ub=[ubK0i ubr2i];


lb=reshape(lb,2*NumDdis,1);
ub=reshape(ub,2*NumDdis,1);
%%%calculate the Fexp value with different sizes%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%% 1. reshape the Fdata into 3-D Nx*Ny*Nt and the fcal into 4-D Nx*Ny*Nt*Numdis


N=NumFdata; Nroix= sizeroi(1); Nroiy=sizeroi(2); N_imroi=Nroix*Nroiy;
Nt=N/N_imroi;


%%1.2 reshape one-D Fdata(1,Nx*Ny*Nt) into 3-D Imroi(Nx,Ny,Nt)

Imroi=reshape(Fdata,Nroix,Nroiy,Nt);
Imroi_r=ones(Nstep+1,sizeroi(1)*sizeroi(2),Nt);

for i=0:Nstep

    Xmin=i*Nsteppix+1;
    Ymin=i*Nsteppix+1;
    Width=sizeroi(1)-i*Nsteppix*2;
    Height=sizeroi(2)-i*Nsteppix*2;
    W(i+1)=Width;
    H(i+1)=Height;
    m=1;
    for j=Xmin:(Xmin+Width-1)
        for k=Ymin:(Ymin+Height-1)
            if (Xmin+Nsteppix)<=j && j<=(Xmin+Width-1-Nsteppix) && (Ymin+Nsteppix)<=k && k<=(Ymin+Height-1-Nsteppix)
                Imroi_s=Imroi(j,k);
            else
                Imroi_r(i+1,m,:)=Imroi(j,k,:);
            end

            m=m+1;
        end
    end
end

%filter bacterial
%         Nones=ones(1,size(Imroi_r,1));
%         for i=1:size(Imroi_r,1)
%              count=0;
%              for m=1:size(Imroi_r,2)
%                  if Imroi_r(i,m,1)==1
%                      count=count+1;
%                  end
%              end
%              Nones(i)=count;
%         end

sum_ring=sum(Imroi_r,2);
sum_ring=reshape(sum_ring,size(sum_ring,1)*size(sum_ring,2),size(sum_ring,3));

Npix_r=ones(1,size(Imroi_r,1))*size(Imroi_r,2);

%          for i=1:Nt
%              mean_ring(:,i)=(sum_ring(:,i)-Nones')./(Npix_r'-Nones');
%          end
for i=1:Nt
    mean_ring(:,i)=sum_ring(:,i)./Npix_r';
end

mean_ring2=mean_ring;
Fexp=reshape(mean_ring2',1,size(mean_ring,1)*size(mean_ring,2));
Fexpdata=Fexp;

%%%calculate the initial Fcal value with different ring sizes%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

K0i=K0i_0; r2i=r2i_0;

for i=1:NumDdis

    Nb = sqrt(4*Ddis(1,i)*t + r2i(i));
    x_factor = erf((x+Lx/2)./Nb) - erf((x-Lx/2)./Nb);
    y_factor = erf((y+Ly/2)./Nb) - erf((y-Ly/2)./Nb);
    %           fcal = f0 - 0.25 * K0i(i) * f0 * x_factor .* y_factor;
    fcal(i,:) =  - 0.25 * K0i(i) * x_factor .* y_factor;
    %%1.1 reshape two-D fcal(Numdis,Nx*Ny*Nt) into 4-D Imcal(Nx,Ny,Nt,Numdis)
    %           Imcal(:,:,:,i)=reshape(fcal,Nroix,Nroiy,Nt);

end %for i
fcal=f0*(1-sum(fcal));

Imcal=reshape(fcal,Nroix,Nroiy,Nt);

%%1.1 reshape two-D fcal(Numdis,Nx*Ny*Nt) into 4-D Imcal(Nx,Ny,Nt,Numdis)




%      for i=1:NumDdis
for j=0:Nstep

    Xmin=j*Nsteppix+1;
    Ymin=j*Nsteppix+1;
    Width=sizeroi(1)-j*Nsteppix*2;
    Height=sizeroi(2)-j*Nsteppix*2;

    for k=1:Nt
        Imcalavroi(k,j+1)=mean2(Imcal(Xmin:Xmin+Width-1,Ymin:Ymin+Height-1,k));
    end

    Imcalsumroi(:,j+1)=Imcalavroi(:,j+1)*Width*Height;



end

%      end


if Nstep >= 1

    %          for i=1:NumDdis
    for j=1:Nstep
        %                   meancalRing(:,j,i)=(Imcalsumroi(:,j,i)-Imcalsumroi(:,j+1,i))./(H(j)*W(j)-W(j+1)*H(j+1));
        Imcalavroi(:,j)=(Imcalsumroi(:,j)-Imcalsumroi(:,j+1))./(H(j)*W(j)-W(j+1)*H(j+1));
    end
    %          end


end

% reshape back to 1-D data stracture

%       for i=1:NumDdis
%           Fcaldata(i,:)=reshape(Imcalavroi(:,:,i),1,size(Imcalavroi,1)*size(Imcalavroi,2));
%           Fcal(i,:)= Fcaldata(i,:)*alpha(i);
Fcal=reshape(Imcalavroi,1,size(Imcalavroi,1)*size(Imcalavroi,2));
%       end
Fcaldata=Fcal;


sigma2=reshape(sigma2,1,numel(sigma2));
%%%%%%%%%%%%%%%%%%%%performing fmincon for least square fitting%%%%%%%%%%%%%%%%%%%%%%
options = optimset('Display','iter','PlotFcns',@optimplotx,'MaxFunEvals',NumDdis*2000,'MaxIter',30,'TolFun',1E-5,'TolX',1E-5,'TolCon',NumFdata/1e3);
% [alpha,fval,exitflag,output] = fmincon(@maxentropy, alpha_0,[] ,[] , Aeq,beq , lb, ub, @mycon, options);
[alpha,fval,exitflag,output] = fmincon(@mycon, alpha_0,Auneq,buneq ,[],[],lb, ub,[],options);

%calculate chi2 based on the fitting alpha

Fitdata=Fcalav;

r2i=alpha(NumDdis+1:2*NumDdis);

for i=1:NumDdis

    Nb = sqrt(4*Ddis(1,i)*t + r2i(i));
    x_factor = erf((x+Lx/2)./Nb) - erf((x-Lx/2)./Nb);
    y_factor = erf((y+Ly/2)./Nb) - erf((y-Ly/2)./Nb);
    %           fcal = f0 - 0.25 * K0i(i) * f0 * x_factor .* y_factor;
    fcal (i,:) =  f0/NumDdis- 0.25 *f0* alpha(i) * x_factor .* y_factor;
    %%1.1 reshape two-D fcal(Numdis,Nx*Ny*Nt) into 4-D Imcal(Nx,Ny,Nt,Numdis)
    %           Imcal(:,:,:,i)=reshape(fcal,Nroix,Nroiy,Nt);

end %for i

fcal=sum(fcal);

Imcal=reshape(fcal,Nroix,Nroiy,Nt);

%%1.1 reshape two-D fcal(Numdis,Nx*Ny*Nt) into 4-D Imcal(Nx,Ny,Nt,Numdis)
%          for i=1:NumDdis
%               Imcal(:,:,:,i)=reshape(fcal(i,:),Nroix,Nroiy,Nt);
%           end

%       for i=1:NumDdis
%           fdata(:,:,i)=Imavroi;
%       end



%      for i=1:NumDdis
for j=0:Nstep

    Xmin=j*Nsteppix+1;
    Ymin=j*Nsteppix+1;
    Width=sizeroi(1)-j*Nsteppix*2;
    Height=sizeroi(2)-j*Nsteppix*2;

    for k=1:Nt
        Imcalavroi(k,j+1)=mean2(Imcal(Xmin:Xmin+Width-1,Ymin:Ymin+Height-1,k));
    end

    Imcalsumroi(:,j+1)=Imcalavroi(:,j+1)*Width*Height;



end

%      end


if Nstep >= 1

    %          for i=1:NumDdis
    for j=1:Nstep
        %                   meancalRing(:,j,i)=(Imcalsumroi(:,j,i)-Imcalsumroi(:,j+1,i))./(H(j)*W(j)-W(j+1)*H(j+1));
        Imcalavroi(:,j)=(Imcalsumroi(:,j)-Imcalsumroi(:,j+1))./(H(j)*W(j)-W(j+1)*H(j+1));
    end
    %          end


end

% reshape back to 1-D data stracture

%       for i=1:NumDdis
%           Fcaldata(i,:)=reshape(Imcalavroi(:,:,i),1,size(Imcalavroi,1)*size(Imcalavroi,2));
%           Fcal(i,:)= Fcaldata(i,:)*alpha(i);
Fcal=reshape(Imcalavroi,1,size(Imcalavroi,1)*size(Imcalavroi,2));
%       end


Fcalav=Fcal;

%       chi2=sum((Fexpdata-Fcalav).^2);
chi2=sum(((Fexpdata-Fcalav).^2)./sigma2);

Ddis=Ddis';


    function chi2 = mycon(alpha)

        %Nested function to calculate the chi square constraint: chi^2=N

        for i=1:NumDdis
            if alpha(i)<=0
                alpha(i)=lb(i);
            end
            %             if alpha(NumDdis+i)<=0.1
            %                 alpha(NumDdis+i)=lb(NumDdis+i);
            %             end
            %             if alpha(NumDdis*2+i)~=r2av
            %                 alpha(NumDdis*2+i)=lb(NumDdis*2+i);
            %             end
        end

        %         [chi2 Fcalav] = chisqure(alpha);
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% Calculate fcal %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        %%%calculate the Fexp value with different sizes%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        %          Di=alpha(1:NumDdis);

        %      r2i=alpha(NumDdis*2+1:3*NumDdis);
        %   if  K0i ~=  alpha(NumDdis+1:2*NumDdis)

        %       K0i=alpha(NumDdis+1:2*NumDdis);
        %       r2i=alpha(NumDdis*2+1:3*NumDdis);
        r2i=alpha(NumDdis+1:2*NumDdis);

        for i=1:NumDdis

            Nb = sqrt(4*Ddis(1,i)*t + r2i(i));
            x_factor = erf((x+Lx/2)./Nb) - erf((x-Lx/2)./Nb);
            y_factor = erf((y+Ly/2)./Nb) - erf((y-Ly/2)./Nb);
            %           fcal = f0 - 0.25 * K0i(i) * f0 * x_factor .* y_factor;
            fcal(i,:) =  f0/NumDdis- 0.25 *f0* alpha(i) * x_factor .* y_factor;
            %%1.1 reshape two-D fcal(Numdis,Nx*Ny*Nt) into 4-D Imcal(Nx,Ny,Nt,Numdis)
            %           Imcal(:,:,:,i)=reshape(fcal,Nroix,Nroiy,Nt);

        end %for i

        fcal=sum(fcal);

        Imcal=reshape(fcal,Nroix,Nroiy,Nt);

        %%1.1 reshape two-D fcal(Numdis,Nx*Ny*Nt) into 4-D Imcal(Nx,Ny,Nt,Numdis)
        %          for i=1:NumDdis
        %               Imcal(:,:,:,i)=reshape(fcal(i,:),Nroix,Nroiy,Nt);
        %           end

        %       for i=1:NumDdis
        %           fdata(:,:,i)=Imavroi;
        %       end



        %      for i=1:NumDdis
        for j=0:Nstep

            Xmin=j*Nsteppix+1;
            Ymin=j*Nsteppix+1;
            Width=sizeroi(1)-j*Nsteppix*2;
            Height=sizeroi(2)-j*Nsteppix*2;

            for k=1:Nt
                Imcalavroi(k,j+1)=mean2(Imcal(Xmin:Xmin+Width-1,Ymin:Ymin+Height-1,k));
            end

            Imcalsumroi(:,j+1)=Imcalavroi(:,j+1)*Width*Height;



        end

        %      end


        if Nstep >= 1

            %          for i=1:NumDdis
            for j=1:Nstep
                %                   meancalRing(:,j,i)=(Imcalsumroi(:,j,i)-Imcalsumroi(:,j+1,i))./(H(j)*W(j)-W(j+1)*H(j+1));
                Imcalavroi(:,j)=(Imcalsumroi(:,j)-Imcalsumroi(:,j+1))./(H(j)*W(j)-W(j+1)*H(j+1));
            end
            %          end


        end

        % reshape back to 1-D data stracture

        %       for i=1:NumDdis
        %           Fcaldata(i,:)=reshape(Imcalavroi(:,:,i),1,size(Imcalavroi,1)*size(Imcalavroi,2));
        %           Fcal(i,:)= Fcaldata(i,:)*alpha(i);
        Fcal=reshape(Imcalavroi,1,size(Imcalavroi,1)*size(Imcalavroi,2));
        %       end


        Fcalav=Fcal;

        chi2=sum(((Fexpdata-Fcalav).^2)./sigma2);



    end %X

%         ceq = [];
%
%             N=Numdatapoint;
% %             N=Nt;
% %           N=NumDdis;
%         c = [N-sqrt(2*N)-chi2; chi2-N-sqrt(2*N)]; %the chi^2 goodness of fit equality constraint
end



%     function [chi2 Fcalav]  = chisqure(alpha)
%
%  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% Calculate fcal %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%  %%%calculate the Fexp value with different sizes%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% %          Di=alpha(1:NumDdis);
%
% %      r2i=alpha(NumDdis*2+1:3*NumDdis);
% %   if  K0i ~=  alpha(NumDdis+1:2*NumDdis)
%
% %       K0i=alpha(NumDdis+1:2*NumDdis);
% %       r2i=alpha(NumDdis*2+1:3*NumDdis);
%         r2i=alpha(NumDdis+1:2*NumDdis);
%
%      for i=1:NumDdis
%
%           Nb = sqrt(4*Ddis(1,i)*t + r2i(i));
%           x_factor = erf((x+Lx/2)./Nb) - erf((x-Lx/2)./Nb);
%           y_factor = erf((y+Ly/2)./Nb) - erf((y-Ly/2)./Nb);
% %           fcal = f0 - 0.25 * K0i(i) * f0 * x_factor .* y_factor;
%           fcal (i,:) =  f0/NumDdis- 0.25 *f0* alpha(i) * x_factor .* y_factor;
%         %%1.1 reshape two-D fcal(Numdis,Nx*Ny*Nt) into 4-D Imcal(Nx,Ny,Nt,Numdis)
% %           Imcal(:,:,:,i)=reshape(fcal,Nroix,Nroiy,Nt);
%
%      end %for i
%
%            fcal=sum(fcal);
%
%           Imcal=reshape(fcal,Nroix,Nroiy,Nt);
%
%   %%1.1 reshape two-D fcal(Numdis,Nx*Ny*Nt) into 4-D Imcal(Nx,Ny,Nt,Numdis)
% %          for i=1:NumDdis
% %               Imcal(:,:,:,i)=reshape(fcal(i,:),Nroix,Nroiy,Nt);
% %           end
%
% %       for i=1:NumDdis
% %           fdata(:,:,i)=Imavroi;
% %       end
%
%
%
% %      for i=1:NumDdis
%          for j=0:Nstep
%
%              Xmin=j*Nsteppix+1;
%              Ymin=j*Nsteppix+1;
%              Width=sizeroi(1)-j*Nsteppix*2;
%              Height=sizeroi(2)-j*Nsteppix*2;
%
%              for k=1:Nt
%               Imcalavroi(k,j+1)=mean2(Imcal(Xmin:Xmin+Width-1,Ymin:Ymin+Height-1,k));
%              end
%
%              Imcalsumroi(:,j+1)=Imcalavroi(:,j+1)*Width*Height;
%
%
%
%          end
%
% %      end
%
%
%      if Nstep >= 1
%
% %          for i=1:NumDdis
%               for j=1:Nstep
% %                   meancalRing(:,j,i)=(Imcalsumroi(:,j,i)-Imcalsumroi(:,j+1,i))./(H(j)*W(j)-W(j+1)*H(j+1));
%                     Imcalavroi(:,j)=(Imcalsumroi(:,j)-Imcalsumroi(:,j+1))./(H(j)*W(j)-W(j+1)*H(j+1));
%               end
% %          end
%
%
%      end
%
%      % reshape back to 1-D data stracture
%
% %       for i=1:NumDdis
% %           Fcaldata(i,:)=reshape(Imcalavroi(:,:,i),1,size(Imcalavroi,1)*size(Imcalavroi,2));
% %           Fcal(i,:)= Fcaldata(i,:)*alpha(i);
%             Fcal=reshape(Imcalavroi,1,size(Imcalavroi,1)*size(Imcalavroi,2));
% %       end
%
%
%       Fcalav=Fcal;
%
%       chi2=sum(((Fexpdata-Fcalav).^2)./sigma2);
%
%
%
%     end %X






% end

