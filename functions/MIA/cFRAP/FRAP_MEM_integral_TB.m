function [alpha, Ddis, Fexp, Fitdata,output] = FRAP_MEM_integral_TB(Imprebleachmean,topleftrect,sizerect,topleftroi,sizeroi,Fdata,Ffit,parfit,xytdata,xytdataplot,parnonfit,sigma2,Pset)

D_fit=parfit(1);
K0=parfit(2);
r2av=parfit(4);

Lx = parnonfit(1);
Ly = parnonfit(2);
f0 = parnonfit(3);
pixsize = Lx/sizerect(1);
x = xytdata(1,:);
y = xytdata(2,:);
t = xytdata(3,:);

NumFdata = numel(Fdata); %number of F data
Numdatapoint=numel(sigma2); %number of datapoints used for the cFRAP fitting
NumDdis =Pset(1); %numer of discretization points in D-space
Dmin=Pset(2);
Dmax=Pset(3);
set_noise=Pset(4);
Nstep=Pset(5);
Nsteppix=Pset(6);
Ddis=logspace(log10(Dmin),log10(Dmax),NumDdis);


%The function fmincon can be used to minimize the negative entropy (which
%is the same as maximizing the entropy), on condition that the chi^2 should
%fall within the expectance region N-sqrt(2N)<chi^2<N+sqrt(2N).

%A constant function has maximum entropy, so this should be our initial
%guess under the constraint that the summ of all fractions should be 1:

K0i_0=ones(NumDdis,1)*K0/NumDdis;
r2i_0=ones(NumDdis,1)*r2av;
alpha_0 = [K0i_0 r2i_0];
alpha_0 = reshape(alpha_0,2*NumDdis,1);

%The constraint that the sum of all fractions should be 1 should be
%included as Aeq*alpha=1, where Aeq is a unit row vector [1 1 ... 1]:

AeqK0i= zeros(1,NumDdis);
Aeqr2i= zeros(1,NumDdis);
Aeq=[AeqK0i Aeqr2i];
beq = K0;

%Furthermore it should be specified that the K0-values cannot be smaller
%than 0, nor larger than 1; the rAv-values should be between 0 and 1000

lbK0i=zeros(NumDdis,1)+1/(Numdatapoint*10);
ubK0i=zeros(NumDdis,1)+1.0;
lbr2i= zeros(NumDdis,1)+1e-3/NumDdis;
ubr2i= zeros(NumDdis,1)+1000;
lb=[lbK0i lbr2i];
ub=[ubK0i ubr2i];

% lb=[lbDi lbK0i lbr2i];
% ub=[ubDi ubK0i ubr2i];
% lb=[lbDi lbK0i];
% ub=[ubDi ubK0i];

lb=reshape(lb,2*NumDdis,1);
ub=reshape(ub,2*NumDdis,1);



%%%calculate the Fexp value with different sizes%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%% The extraction of the ROI out of the Fdata or Fcal happens
%%%% with a matrix multiplication weighed_extrator*data, where data is an
%%%% (Nx*Ny) x Nt reshaped matrix of the Fdata/Fcalc and weighed_extractor is
%%%% a Nstep x (Nx*Ny) matrix, selecting the elements from data that
%%%% correspond with the different circles

%%%% 1. Define extractor and weighed_extractor
Nroix= sizeroi(1);
Nroiy=sizeroi(2);
N_imroi=Nroix*Nroiy;
N=NumFdata;
Nt=N/N_imroi;
standard_roi=zeros(Nroix,Nroiy);
extractor=zeros(Nstep+1,N_imroi);

for i=0:Nstep
    Xmin=i*Nsteppix+1;
    Ymin=i*Nsteppix+1;
    Width=sizeroi(1)-i*Nsteppix*2;
    Height=sizeroi(2)-i*Nsteppix*2;
    W(i+1)=Width;
    H(i+1)=Height;
    current_roi=standard_roi;
    current_roi(Xmin:Xmin+Width-1,Ymin:Ymin+Height-1)=1;
    extractor(i+1,:)=reshape(current_roi,[1,N_imroi]);
end

extractor=[abs(diff(extractor));extractor(end,:)];
weighed_extractor=diag(1./sum(extractor,2))*extractor;
numpixs=repmat(permute(sum(extractor,2),[3 2 1]),[NumDdis,Nt,1]);
%%%% 1. reshape the Fdata into (Nx*Ny) x Nt matrix

Imroi=reshape(Fdata,Nroix*Nroiy,Nt);

%%1.3 use mask to calculate different sizes of average value of Fdata

Imavroi=weighed_extractor*Imroi;

% reshape back to 1-D data structure

Fexpdata=reshape(Imavroi',1,size(Imavroi,1)*size(Imavroi,2));
Fexp=Fexpdata;

%%%calculate the initial Fcal value with different ring sizes%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

K0i=K0i_0;
r2i=r2i_0;

% timefactor=4*Ddis'*t;
% X=repmat(x,[NumDdis,1]);
% Y=repmat(y,[NumDdis,1]);
% K0irep=repmat(K0i,[1,size(X,2)]);
% r2irep=repmat(r2i,[1,size(X,2)]);
sigma2=reshape(sigma2,1,numel(sigma2));

Xa=repmat(permute(W/2*pixsize,[1 3 2]),[NumDdis,Nt,1]);
Ya=repmat(permute(H/2*pixsize,[1 3 2]),[NumDdis,Nt,1]);

xaeqya=~sum(W~=H);
timefactor_unique=repmat(4*Ddis'*unique(t),[1,1,Nstep+1]);

% Nb = sqrt(timefactor + r2irep);
% x_factor = erf((X+Lx/2)./Nb) - erf((X-Lx/2)./Nb);
% y_factor = erf((Y+Ly/2)./Nb) - erf((Y-Ly/2)./Nb);
% fcal =  f0/NumDdis- 0.25 *f0* K0irep .* x_factor .* y_factor;
% fcal=sum(fcal);
%
%
% Imcal=reshape(fcal,Nroix*Nroiy,Nt);
% Fcal=reshape((weighed_extractor*Imcal)',1,(Nstep+1)*Nt);
% Fcaldata=Fcal;

Fcalav = [];



%%%%%%%%%%%%%%%%%%%%performing fmincon%%%%%%%%%%%%%%%%%%%%%%
options = optimset('Algorithm', 'active-set','Display','iter','PlotFcns',@optimplotx,'MaxFunEvals',NumDdis*2000,'MaxIter',200,'TolFun',1E-3,'TolX',1E-3,'TolCon',NumFdata/1e2,'OutputFcn',@outfun);
% options = optimset('Display','iter','PlotFcns',@optimplotx,'MaxFunEvals',NumDdis*2000,'MaxIter',200,'TolFun',1E-3,'TolX',1E-3,'TolCon',NumFdata/1e2);
[alpha,fval,exitflag,output] = fmincon(@maxentropy, alpha_0, [], [], [], [], lb, ub, @mycon, options);
Fitdata=Fcalav;
Ddis=Ddis';


%%%%%%%%%%%%%%%%%%%%%%%%%end fmincon%%%%%%%%%%%%%%%%%%%%%%%%
    function S = maxentropy(alpha)
        %Nested function to calculate the (negative) entropy

        alpha(alpha<=0)=lb(alpha<=0);
        S = sum(alpha(1:NumDdis).*log(alpha(1:NumDdis)));

    end

    function [c, ceq] = mycon(alpha)
        %Nested function to calculate the chi square constraint: chi^2=N

        alpha(alpha<=0)=lb(alpha<=0);
        [chi2, Fcalav] = chisqure(alpha);
        ceq = [];
        N=Numdatapoint;
        c = [N-sqrt(2*N)-chi2; chi2-N-sqrt(2*N)]; %the chi^2 goodness of fit equality constraint
    end



    function [res, Fcalav]  = chisqure(alpha)

        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% Calculate fcal %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        %%%calculate the Fexp value with different sizes%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

        K0i=alpha(1:NumDdis);
        r2i=alpha(NumDdis+1:2*NumDdis);
        K0irep=repmat(K0i,[1,size(timefactor_unique,2),Nstep+1]);
        r2irep=repmat(r2i,[1,size(timefactor_unique,2),Nstep+1]);


        Nb = sqrt(timefactor_unique + r2irep);

        x_factor = integral_factor(Xa,Nb,Lx);
        if xaeqya
            y_factor=x_factor;
        else
            y_factor = integral_factor(Ya,Nb,Ly);
        end

        integral= (4*f0/NumDdis*Xa.*Ya-0.25*f0*K0irep .* x_factor .* y_factor)/pixsize^2;
        integral2=cat(3,integral(:,:,2:end),zeros(size(integral(:,:,1))));

        fcal=integral-integral2;
        Fcal=reshape(sum(fcal,1)./repmat(permute(sum(extractor,2),[3 2 1]),[1,Nt,1]),[1,(Nstep+1)*Nt]);

        Fcalav=Fcal;

        res=sum(((Fexpdata-Fcalav).^2)./sigma2);

    end
end


function integral=integral_factor(Xa,N_b,l_x)
integral=erf(-Xa./N_b+(1/2)*l_x./N_b).*(2*Xa-l_x)+erf(Xa./N_b+(1/2)*l_x./N_b).*(2*Xa+l_x)+2*exp(-(1/4)*l_x.^2./N_b.^2).*N_b.*(exp(-l_x*Xa./N_b.^2)-exp(l_x*Xa./N_b.^2))./(sqrt(pi)*exp(Xa.^2./N_b.^2));

end

function stop = outfun(x, optimValues, state)

%This subfunction is used to check for 'Hessian not updated' during the
%fmincon procedure. If it happens then we will top fmincon, because else it
%will proceed until the max number of evaluations while nothing changes
%anymore. This message is in the field 'procedure' of the optimValues
%structure.

stop = false;
test = optimValues.procedure;
test = strtrim(test); %to remove leading and trailing blanks
test2 = optimValues.iteration;
if test2>=55
    dummy=1;
end
switch test
    case 'Hessian not updated'
        stop = true;
end

end %funtion outfun