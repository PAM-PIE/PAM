function [alpha Ddis Fexp Fitdata output] = FRAP_MEM(Imprebleachmean,topleftrect,sizerect,topleftroi,sizeroi,Fdata,Ffit,parfit,xytdata,xytdataplot,parnonfit,sigma2,Pset)
% function [alpha Ddis Fexp Fitdata output] = FRAP_MEM(Imprebleachmean,topleftrect,sizerect,topleftroi,sizeroi,Fdata,parfit,xytdata,xytdataplot,parnonfit,sigma2,Pset)
%parfit = [D K0 k r2av]
%xytdata = [x; y; t]
%parnonfit = [Lx Ly F0]
% Fdata are the experimental pixel values be fitted.
% 
D_fit=parfit(1); K0=parfit(2);r2av=parfit(4); % ranhua adds 
% D_fit=parfit(1); K0=0.4;r2av=9.0;
Lx = parnonfit(1); Ly = parnonfit(2); f0 = parnonfit(3); % ranhua adds
% x = xytdataplot(1,:); y = xytdataplot(2,:); t = xytdataplot(3,:);  % ranhua adds 
x = xytdata(1,:); y = xytdata(2,:); t = xytdata(3,:);  % ranhua adds 

NumFdata = numel(Fdata); %number of F data
Numdatapoint=numel(sigma2); %number of datapoints
% Numdatapoint=NumFdata;
NumDdis =Pset(1); %numer of discretization points in D-space
Dmin=Pset(2);  % ranhua adds
Dmax=Pset(3);   % ranhua adds
set_noise=Pset(4);
Nstep=Pset(5);
Nsteppix=Pset(6);
Ddis=logspace(log10(Dmin),log10(Dmax),NumDdis);
 

%The function fmincon can be used to minimize the negative entropy (which
%is the same as maximizing the entropy), on condition that the chi^2 should
%fall within the expectance region N-sqrt(2N)<chi^2<N+sqrt(2N).

%A constant function has maximum entropy, so this should be our initial
%guess under the constraint that the summ of all fractions should be 1:
% Di_0=ones(NumDdis,1)/NumDdis;

K0i_0=ones(NumDdis,1)*K0/NumDdis;
r2i_0=ones(NumDdis,1)*r2av;
alpha_0 = [K0i_0 r2i_0];
alpha_0 = reshape(alpha_0,2*NumDdis,1);

% alpha_0 = [Di_0 K0i_0 r2i_0];
% alpha_0 = [Di_0 K0i_0];
% alpha_0 = reshape(alpha_0,3*NumDdis,1);

%The constraint that the sum of all fractions should be 1 should be
%included as Aeq*alpha=1, where Aeq is a unit row vector [1 1 ... 1]:

% AeqDi = ones(1,NumDdis);
AeqK0i= zeros(1,NumDdis);
Aeqr2i= zeros(1,NumDdis);
Aeq=[AeqK0i Aeqr2i];
beq = K0;

%Furthermore it should be specified that the alpha-values cannot be smaller
%than 0, nor larger than 1:

% lbDi = zeros(NumDdis,1)+1/NumFdata;
% ubDi = ones(NumDdis,1);

% lbK0i1= zeros(fix(NumDdis/2),1)+0.2;
% ubK0i1= zeros(fix(NumDdis/2),1)+0.5;
% lbK0i2= zeros(NumDdis-fix(NumDdis/2),1)+0.2;
% ubK0i2= zeros(NumDdis-fix(NumDdis/2),1)+0.5;
% lbK0i=[lbK0i1' lbK0i2'];
% ubK0i=[ubK0i1' ubK0i2'];
% lbK0i=zeros(NumDdis,1)+0.1;
% ubK0i=zeros(NumDdis,1)+1.0;
 lbK0i=zeros(NumDdis,1)+1/(Numdatapoint*10);
 ubK0i=zeros(NumDdis,1)+1.0;
 lbr2i= zeros(NumDdis,1)+1e-3/NumDdis;
 ubr2i= zeros(NumDdis,1)+1000;
 lb=[lbK0i lbr2i];
 ub=[ubK0i ubr2i];

% lb=[lbDi lbK0i lbr2i];
% ub=[ubDi ubK0i ubr2i];
% lb=[lbDi lbK0i];
% ub=[ubDi ubK0i];

lb=reshape(lb,2*NumDdis,1);
ub=reshape(ub,2*NumDdis,1);



 %%%calculate the Fexp value with different sizes%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
 
   %%%% 1. reshape the Fdata into 3-D Nx*Ny*Nt and the fcal into 4-D Nx*Ny*Nt*Numdis
   
     
             N=NumFdata; Nroix= sizeroi(1); Nroiy=sizeroi(2); N_imroi=Nroix*Nroiy;
             Nt=N/N_imroi;
          
         
     %%1.2 reshape one-D Fdata(1,Nx*Ny*Nt) into 3-D Imroi(Nx,Ny,Nt)
          
          Imroi=reshape(Fdata,Nroix,Nroiy,Nt);
          
     %%1.3 use mask to calculate different sizes of average value of Fdata
          
%            Imvroi1=zeros(Nroix,Nroiy,Nt,NumDdis);
           
        for i=0:Nstep
            
            Xmin=i*Nsteppix+1;
            Ymin=i*Nsteppix+1;
            Width=sizeroi(1)-i*Nsteppix*2;
            Height=sizeroi(2)-i*Nsteppix*2;
            W(i+1)=Width;
            H(i+1)=Height;
            
           for j=1:Nt
               
                Imavroi(j,i+1)=mean2(Imroi(Xmin:Xmin+Width-1,Ymin:Ymin+Height-1,j));
                Imsumroi(j,i+1)=Imavroi(j,i+1)*Width*Height;
                
           end
            
        end
        
        if Nstep >= 1
            for i=1:Nstep
                meanRing(:,i)=(Imsumroi(:,i)-Imsumroi(:,i+1))./(H(i)*W(i)-W(i+1)*H(i+1));
                Imavroi(:,i)=meanRing(:,i);
            end
     
        
            for i=1:Nstep
                 Imavroi(:,i)=meanRing(:,i);
            end
        
        end
         % reshape back to 1-D data stracture
   
      Fexpdata=reshape(Imavroi,1,size(Imavroi,1)*size(Imavroi,2));
      Fexp=Fexpdata;
      
  %%%calculate the initial Fcal value with different ring sizes%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%      
%      K0i = sym('K0i%d', [NumDdis 1])
        K0i=K0i_0; r2i=r2i_0;
        
         for i=1:NumDdis
          
          Nb = sqrt(4*Ddis(1,i)*t + r2i(i));
          x_factor = erf((x+Lx/2)./Nb) - erf((x-Lx/2)./Nb);
          y_factor = erf((y+Ly/2)./Nb) - erf((y-Ly/2)./Nb);
%           fcal = f0 - 0.25 * K0i(i) * f0 * x_factor .* y_factor;
          fcal(i,:) =  - 0.25 * K0i(i) * x_factor .* y_factor;
        %%1.1 reshape two-D fcal(Numdis,Nx*Ny*Nt) into 4-D Imcal(Nx,Ny,Nt,Numdis)
%           Imcal(:,:,:,i)=reshape(fcal,Nroix,Nroiy,Nt);
          
     end %for i
           fcal=f0*(1-sum(fcal));
       
          Imcal=reshape(fcal,Nroix,Nroiy,Nt);
     
  %%1.1 reshape two-D fcal(Numdis,Nx*Ny*Nt) into 4-D Imcal(Nx,Ny,Nt,Numdis)   
%          for i=1:NumDdis
%               Imcal(:,:,:,i)=reshape(fcal(i,:),Nroix,Nroiy,Nt);
%           end
          
%       for i=1:NumDdis
%           fdata(:,:,i)=Imavroi;
%       end
      
      

%      for i=1:NumDdis
         for j=0:Nstep
             
             Xmin=j*Nsteppix+1;
             Ymin=j*Nsteppix+1;
             Width=sizeroi(1)-j*Nsteppix*2;
             Height=sizeroi(2)-j*Nsteppix*2;
            
             for k=1:Nt
              Imcalavroi(k,j+1)=mean2(Imcal(Xmin:Xmin+Width-1,Ymin:Ymin+Height-1,k)); 
             end
             
             Imcalsumroi(:,j+1)=Imcalavroi(:,j+1)*Width*Height;
             
             
             
         end
         
%      end
     
     
     if Nstep >= 1
         
%          for i=1:NumDdis
              for j=1:Nstep
%                   meancalRing(:,j,i)=(Imcalsumroi(:,j,i)-Imcalsumroi(:,j+1,i))./(H(j)*W(j)-W(j+1)*H(j+1));
                    Imcalavroi(:,j)=(Imcalsumroi(:,j)-Imcalsumroi(:,j+1))./(H(j)*W(j)-W(j+1)*H(j+1));
              end
%          end
        
       
     end

     % reshape back to 1-D data stracture
     
%       for i=1:NumDdis
%           Fcaldata(i,:)=reshape(Imcalavroi(:,:,i),1,size(Imcalavroi,1)*size(Imcalavroi,2));
%           Fcal(i,:)= Fcaldata(i,:)*alpha(i);
            Fcal=reshape(Imcalavroi,1,size(Imcalavroi,1)*size(Imcalavroi,2));
%       end
             Fcaldata=Fcal;
   
%      for i=1:NumDdis        
%           Nb = sqrt(4*Ddis(1,i)*t + r2i(i));
%           x_factor = erf((x+Lx/2)./Nb) - erf((x-Lx/2)./Nb);
%           y_factor = erf((y+Ly/2)./Nb) - erf((y-Ly/2)./Nb);         
%           fcal(i,:) = f0 - 0.25 * K0i(i) * f0 * x_factor .* y_factor;
%           
%       end %for i
%       
%       
%       
%       
%   %%1.1 reshape two-D fcal(Numdis,Nx*Ny*Nt) into 4-D Imcal(Nx,Ny,Nt,Numdis)   
%          for i=1:NumDdis
%               Imcal(:,:,:,i)=reshape(fcal(i,:),Nroix,Nroiy,Nt);
%           end
%           
% %       for i=1:NumDdis
% %           fdata(:,:,i)=Imavroi;
% %       end
%       
%       
%         
%    %%1.4 use mask to calculate different size of average value of fcal
%      for i=1:NumDdis
%          for j=0:Nstep
%              Xmin=j*Nsteppix+1;
%              Ymin=j*Nsteppix+1;
%              Width=sizeroi(1)-j*Nsteppix*2;
%              Height=sizeroi(2)-j*Nsteppix*2;
%             
%             for k=1:Nt
%               Imcalavroi(k,j+1,i)=mean2(Imcal(Xmin:Xmin+Width-1,Ymin:Ymin+Height-1,k,i));
%               Imcalsumroi(k,j+1,i)=Imcalavroi(k,j+1,i)*Width*Height;
%             end
%             
%          end
%          
%      end
%      
%      
%      if Nstep >= 1
%          
%          for i=1:NumDdis
%               for j=1:Nstep
%                   meancalRing(:,j,i)=(Imcalsumroi(:,j,i)-Imcalsumroi(:,j+1,i))./(H(j)*W(j)-W(j+1)*H(j+1));
%               end
%          end
%  
%      
%        for i=1:NumDdis
%            
%          for j=1:Nstep
%              
%             Imcalavroi(:,j,i)=meancalRing(:,j,i);
%             
%          end
%          
%        end
%        
%        
%      end
% 
%      % reshape back to 1-D data stracture
%       for i=1:NumDdis
%           Fcaldata(i,:)=reshape(Imcalavroi(:,:,i),1,size(Imcalavroi,1)*size(Imcalavroi,2));
%       end
%       
    
     sigma2=reshape(sigma2,1,numel(sigma2));   
 
%%%%%%%%%%%%%%%%%%%%performing fmincon%%%%%%%%%%%%%%%%%%%%%%
options = optimset('Display','iter','PlotFcns',@optimplotx,'MaxFunEvals',NumDdis*2000,'MaxIter',200,'TolFun',1E-2,'TolX',1E-2,'TolCon',NumFdata/1e3);    
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Iter_time=zeros(200,6);
% count=1;

% [alpha,fval,exitflag,output] = fmincon(@maxentropy, alpha_0,[] ,[] , Aeq,beq , lb, ub, @mycon, options);
  [alpha,fval,exitflag,output] = fmincon(@maxentropy, alpha_0,[] ,[] , [],[] , lb, ub, @mycon, options);

Fitdata=Fcalav;

Ddis=Ddis';


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%end fmincon%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    function S = maxentropy(alpha)
        %Nested function to calculate the (negative) entropy
%         Iter_time(count,:)=clock;
        
        for i=1:NumDdis
            if alpha(i)<=0
                alpha(i)=lb(i);
            end
%             if alpha(NumDdis+i)<=0.1
%                 alpha(NumDdis+i)=lb(NumDdis+i);
%             end
%             if alpha(NumDdis*2+i)~=r2av
%                 alpha(NumDdis*2+i)=lb(NumDdis*2+i);
%             end
        end
        
        
        S = sum(alpha(1:NumDdis).*log(alpha(1:NumDdis)));
%           S = sum(alpha.*log(alpha));
%         
%         count=count+1;
    end %function S

   function [c, ceq] = mycon(alpha)
        %Nested function to calculate the chi square constraint: chi^2=N
        
        for i=1:NumDdis
            if alpha(i)<=0
                alpha(i)=lb(i);
            end
%             if alpha(NumDdis+i)<=0.1
%                 alpha(NumDdis+i)=lb(NumDdis+i);
%             end
%             if alpha(NumDdis*2+i)~=r2av
%                 alpha(NumDdis*2+i)=lb(NumDdis*2+i);
%             end
        end
        
        [chi2 Fcalav] = chisqure(alpha);
        ceq = [];
        
            N=Numdatapoint;
%             N=Nt;
%           N=NumDdis;
        c = [N-sqrt(2*N)-chi2; chi2-N-sqrt(2*N)]; %the chi^2 goodness of fit equality constraint
   end



    function [X Fcalav]  = chisqure(alpha)
       
 %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% Calculate fcal %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
 %%%calculate the Fexp value with different sizes%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%          Di=alpha(1:NumDdis);
     
%      r2i=alpha(NumDdis*2+1:3*NumDdis);
%   if  K0i ~=  alpha(NumDdis+1:2*NumDdis)
      
%       K0i=alpha(NumDdis+1:2*NumDdis);
%       r2i=alpha(NumDdis*2+1:3*NumDdis);
        r2i=alpha(NumDdis+1:2*NumDdis);
      
     for i=1:NumDdis
          
          Nb = sqrt(4*Ddis(1,i)*t + r2i(i));
          x_factor = erf((x+Lx/2)./Nb) - erf((x-Lx/2)./Nb);
          y_factor = erf((y+Ly/2)./Nb) - erf((y-Ly/2)./Nb);
%           fcal = f0 - 0.25 * K0i(i) * f0 * x_factor .* y_factor;
          fcal (i,:) =  f0/NumDdis- 0.25 *f0* alpha(i) * x_factor .* y_factor;
        %%1.1 reshape two-D fcal(Numdis,Nx*Ny*Nt) into 4-D Imcal(Nx,Ny,Nt,Numdis)
%           Imcal(:,:,:,i)=reshape(fcal,Nroix,Nroiy,Nt);
          
     end %for i
     
           fcal=sum(fcal);
       
          Imcal=reshape(fcal,Nroix,Nroiy,Nt);
     
  %%1.1 reshape two-D fcal(Numdis,Nx*Ny*Nt) into 4-D Imcal(Nx,Ny,Nt,Numdis)   
%          for i=1:NumDdis
%               Imcal(:,:,:,i)=reshape(fcal(i,:),Nroix,Nroiy,Nt);
%           end
          
%       for i=1:NumDdis
%           fdata(:,:,i)=Imavroi;
%       end
      
      

%      for i=1:NumDdis
         for j=0:Nstep
             
             Xmin=j*Nsteppix+1;
             Ymin=j*Nsteppix+1;
             Width=sizeroi(1)-j*Nsteppix*2;
             Height=sizeroi(2)-j*Nsteppix*2;
            
             for k=1:Nt
              Imcalavroi(k,j+1)=mean2(Imcal(Xmin:Xmin+Width-1,Ymin:Ymin+Height-1,k)); 
             end
             
             Imcalsumroi(:,j+1)=Imcalavroi(:,j+1)*Width*Height;
             
             
             
         end
         
%      end
     
     
     if Nstep >= 1
         
%          for i=1:NumDdis
              for j=1:Nstep
%                   meancalRing(:,j,i)=(Imcalsumroi(:,j,i)-Imcalsumroi(:,j+1,i))./(H(j)*W(j)-W(j+1)*H(j+1));
                    Imcalavroi(:,j)=(Imcalsumroi(:,j)-Imcalsumroi(:,j+1))./(H(j)*W(j)-W(j+1)*H(j+1));
              end
%          end
        
       
     end

     % reshape back to 1-D data stracture
     
%       for i=1:NumDdis
%           Fcaldata(i,:)=reshape(Imcalavroi(:,:,i),1,size(Imcalavroi,1)*size(Imcalavroi,2));
%           Fcal(i,:)= Fcaldata(i,:)*alpha(i);
            Fcal=reshape(Imcalavroi,1,size(Imcalavroi,1)*size(Imcalavroi,2));
%       end
   
      
      Fcalav=Fcal;
      
      X=sum(((Fexpdata-Fcalav).^2)./sigma2);
      
      
     
    end %X

   
      
        


end