function [parameters,Ffit,profiles,averageFdata] = rFrapFit(Im,Nimages,bgroi,Nprebleach,Nbleach,topleftrect,sizerect,topleftroi,sizeroi,pixsize,t,tplot,D,K0,k,r2av,varargin)

%Background correction:
%Nbg = size(sizebg,1); bg = zeros(1,Nbg); %Background regions.
for i = 1:Nimages
    Im_frame = Im(:,:,i);
    bg = mean(Im_frame(bgroi(:,:,i)));
    Im(:,:,i)=Im(:,:,i)/bg; %Normalisation wrt background regions.
end

%Normalization of the images:
Imprebleachmean = mean(Im(:,:,1:Nprebleach),3); %Mean of pre-bleach images pixel by pixel.
Imprebleachmean = medfilt2(Imprebleachmean, [5 5], 'symmetric'); %Median filter over mean pre-bleach image.
%Imprebleachmean=mean(mean(mean(Im(:,:,1:Nprebleach))));
for i = 1:Nimages
    Im(:,:,i)=Im(:,:,i)./Imprebleachmean; %Normalisation wrt pre-bleach images, pixel by pixel.
end

%Coordinates of the pixel centers:
[x, y] = meshgrid(0.5:1:size(Im,2)-0.5, 0.5:1:size(Im,1)-0.5);
x = x - topleftrect(1) + 1 - sizerect(1)/2; %Origin placed at center of bleached rectangle.
y = y - topleftrect(2) + 1 - sizerect(2)/2; %Origin placed at center of bleached rectangle.

%Extract region of interest:
Imroi = Im(topleftroi(2):topleftroi(2)+sizeroi(2)-1, topleftroi(1):topleftroi(1)+sizeroi(1)-1,:);
xroi = x(topleftroi(2):topleftroi(2)+sizeroi(2)-1, topleftroi(1):topleftroi(1)+sizeroi(1)-1,:);
yroi = y(topleftroi(2):topleftroi(2)+sizeroi(2)-1, topleftroi(1):topleftroi(1)+sizeroi(1)-1,:);

%Extract fitted part of bleached rectangle:
topleftplot=zeros(1,2); sizeplot=zeros(1,2);
topleftplot(1)=max(topleftroi(1),topleftrect(1)); topleftplot(2)=max(topleftroi(2),topleftrect(2));
sizeplot(1)=min(topleftroi(1)+sizeroi(1), topleftrect(1)+sizerect(1))-topleftplot(1); sizeplot(2)=min(topleftroi(2)+sizeroi(2), topleftrect(2)+sizerect(2))-topleftplot(2);
Implot = Im(topleftplot(2):topleftplot(2)+sizeplot(2)-1, topleftplot(1):topleftplot(1)+sizeplot(1)-1,:);
xplot = x(topleftplot(2):topleftplot(2)+sizeplot(2)-1, topleftplot(1):topleftplot(1)+sizeplot(1)-1,:);
yplot = y(topleftplot(2):topleftplot(2)+sizeplot(2)-1, topleftplot(1):topleftplot(1)+sizeplot(1)-1,:);

%Reshape the matrices:
xytdata = zeros(3,numel(xroi)*numel(t));
Fdata = zeros(1,numel(xroi)*numel(t));
for j=0:numel(t)-1
    xytdata(1,j*numel(xroi)+1:(j+1)*numel(xroi)) = reshape(xroi,1,numel(xroi))*pixsize;
    xytdata(2,j*numel(xroi)+1:(j+1)*numel(xroi)) = reshape(yroi,1,numel(xroi))*pixsize;
    xytdata(3,j*numel(xroi)+1:(j+1)*numel(xroi)) = ones(1,numel(xroi))*t(j+1);
    Fdata(1,j*numel(xroi)+1:(j+1)*numel(xroi)) = reshape(Imroi(:,:,j+1+Nprebleach+Nbleach),1,numel(xroi));
end
xytdataplot = zeros(3,numel(xplot)*numel(tplot));
for j=0:numel(tplot)-1
    xytdataplot(1,j*numel(xplot)+1:(j+1)*numel(xplot)) = reshape(xplot,1,numel(xplot))*pixsize;
    xytdataplot(2,j*numel(xplot)+1:(j+1)*numel(xplot)) = reshape(yplot,1,numel(xplot))*pixsize;
    xytdataplot(3,j*numel(xplot)+1:(j+1)*numel(xplot)) = ones(1,numel(xplot))*tplot(j+1);
end
xyt0data=zeros(3,numel(xroi));
xyt0data(1,1:numel(xroi))=reshape(xroi,1,numel(xroi))*pixsize;
xyt0data(2,1:numel(xroi))=reshape(yroi,1,numel(xroi))*pixsize;
xyt0data(3,1:numel(xroi))=zeros(1,numel(xroi));

%Perform fitting procedure:
if isempty(varargin)
    %Define the initial fitting parameters:
    parfit_init = [D K0 k r2av];
    %Parameters excluded from fit (rectangle size in �m and initial
    %normalised fluorescence):
    parnonfit = [sizerect(1)*pixsize sizerect(2)*pixsize 1];
    %Define bounds
    LB = [0 0 0 0]; UB = [Inf Inf 1 Inf]; %Define bounds.
    %Perform fitting procedure:
    options = optimset('LargeScale','on','GradObj','on','TolFun',1e-8,'TolX',1e-8);
    %options=optimset('LargeScale','off','Algorithm','Levenberg-Marquardt','Display','off');
    [parfit, resnorm, residual, exitflag, output, lambda, jacobian] = lsqcurvefit('rFRAP_formula_1comp', parfit_init, xytdata, Fdata, LB, UB, options, parnonfit);
    Ffit = rFRAP_formula_1comp(parfit, xytdata, parnonfit);
    Ffitplot = rFRAP_formula_1comp(parfit, xytdataplot, parnonfit);
    Ffitt0=rFRAP_formula_1comp(parfit,xyt0data,parnonfit);
    parameters=[parfit(1),parfit(2),parfit(3),parfit(4)];
    %calculate errors:
    %err=nlparci(parfit,residual,'jacobian',jacobian);
    %err=(err(:,2)-err(:,1))/2;
    %parameters={parfit(1),parfit(2),parfit(3),parfit(4),err(1), err(2), err(3), err(4)};
else
    %Define the initial fitting parameters (D1=D, D2=varargin{1}, k1=k):
    parfit_init=[D varargin{1} K0 k r2av];
    %Parameters excluded from fit (rectangle size in �m and initial
    %normalised fluorescence):
    parnonfit = [sizerect(1)*pixsize sizerect(2)*pixsize 1];
    %Define bounds:
    LB = [0 0 0 0 0]; UB = [];
    %Perform fitting procedures:
    options = optimset('LargeScale','on','GradObj','on','TolFun',1e-8,'TolX',1e-8);
    [parfit, resnorm, residual, exitflag] = lsqcurvefit('rFRAP_formula_2comp', parfit_init, xytdata, Fdata, LB, UB, options, parnonfit);
    Ffit = rFRAP_formula_2comp(parfit, xytdata, parnonfit);
    Ffitplot = rFRAP_formula_2comp(parfit, xytdataplot, parnonfit);
    Ffitt0=rFRAP_formula_2comp(parfit,xyt0data,parnonfit);
    parameters=[parfit(1),parfit(2),parfit(3),parfit(4),parfit(5)];
end

%Reshape the fit fluorescence matrices:
Ffitresh = zeros(size(xroi,1),size(xroi,2),numel(t));
for j=0:numel(t)-1
    Ffitresh(:,:,j+1) = reshape(Ffit(j*numel(xroi)+1:(j+1)*numel(xroi)),size(xroi,1),size(xroi,2));
end
Ffitplotresh = zeros(size(xplot,1),size(xplot,2),numel(tplot));
for j=0:numel(tplot)-1
    Ffitplotresh(:,:,j+1)= reshape(Ffitplot(j*numel(xplot)+1:(j+1)*numel(xplot)),size(xplot,1),size(xplot,2));
end
Ffitt0resh=zeros(size(xroi,1),size(xroi,2));
Ffitt0resh=reshape(Ffitt0,size(xroi,1),size(xroi,2));

%Save recovery curve over bleached region:
averageFdata = zeros(1,numel(t)+1); averageFfit = zeros(1,numel(t)+1);
for j=1:numel(tplot)
    if j == 1
        averageFdata(j) = 1;
        averageFfit(j) = mean(mean(Ffitplotresh(:,:,j)));
    else
        averageFdata(j) = mean(mean(Implot(:,:,Nprebleach+Nbleach+j-1)));
        averageFfit(j) = mean(mean(Ffitplotresh(:,:,j)));
    end
end
recovery={averageFdata,averageFfit};

%Save profiles in x- and y-direction through rectangle center.
xCenter=topleftplot(1)+round(sizeplot(1)/2)-topleftplot(1)+1;
yCenter=topleftplot(2)+round(sizeplot(2)/2)-topleftplot(2)+1;
xCoord=xroi(yCenter,:);
yCoord=yroi(:,xCenter);
xProfileExp=zeros(sizeroi(1),numel(t));
xProfileFit=zeros(sizeroi(1),1+numel(t));
yProfileExp=zeros(sizeroi(2),numel(t));
yProfileFit=zeros(sizeroi(2),1+numel(t));
xProfileFit(:,1)=mean(Ffitt0resh(yCenter-2:yCenter+2,:))';
yProfileFit(:,1)=mean(Ffitt0resh(:,xCenter-2:xCenter+2),2);
for j=1:length(t)
    xProfileExp(:,j)=mean(Imroi(yCenter-2:yCenter+2,:,Nprebleach+Nbleach+j))';
    xProfileFit(:,j+1)=mean(Ffitresh(yCenter-2:yCenter+2,:,j))';
    yProfileExp(:,j)=mean(Imroi(:,xCenter-2:xCenter+2,Nprebleach+Nbleach+j),2);
    yProfileFit(:,j+1)=mean(Ffitresh(:,xCenter-2:xCenter+2,j),2);
end
profiles={xCoord*pixsize,yCoord*pixsize,xProfileExp,xProfileFit,yProfileExp,yProfileFit};

end