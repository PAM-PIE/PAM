function Fm = rFRAP_formula_1comp(par, xytdata, parnonfit)

%par = [D K0 k r2av]
%xytdata = [x; y; t]
%parnonfit = [Lx Ly F0]

D = par(1); K0 = par(2); k = par(3); r2av = par(4);
Lx = parnonfit(1); Ly = parnonfit(2); F0 = parnonfit(3);
x = xytdata(1,:); y = xytdata(2,:); t = xytdata(3,:);

N = sqrt(r2av);
x_factor = erf((x+Lx/2)/N) - erf((x-Lx/2)/N);
y_factor = erf((y+Ly/2)/N) - erf((y-Ly/2)/N);
Ft0 = F0 - 0.25 * K0 * F0 * x_factor .* y_factor;

N = sqrt(4*D*t + r2av);
x_factor = erf((x+Lx/2)./N) - erf((x-Lx/2)./N);
y_factor = erf((y+Ly/2)./N) - erf((y-Ly/2)./N);
F = F0 - 0.25 * K0 * F0 * x_factor .* y_factor;

Fm = Ft0 + k * ( F - Ft0 );

end