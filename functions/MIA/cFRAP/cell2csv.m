function datei = cell2csv(datName,cellArray,trennZeichen)
% Writes Cell-Array content into csv.
% 
% datName = Name of the file to save. [ i.e. 'text.csv' ]
% cellarray = Name of the Cell Array where the data is in
% trennZeichen = seperating sign, normally:',' it's default

% by Sylvain Fiedler, KA, 2004
%
% Modified by Kevin braeckmans, Ghent University, 2005


if trennZeichen == ''
    trennZeichen = ',';
end

datei = fopen(datName,'w');
if datei==-1
    %file could not be opened
    return
end


for z=1:size(cellArray,1)
    for s=1:size(cellArray,2)
        
        var = eval(['cellArray{z,s}']);
        
        if size(var,1) == 0
            var = '';
        end
        
        if isnumeric(var) == 1
            var = num2str(var);
        end
        
        fprintf(datei,var);
        
        if s ~= size(cellArray,2)
            fprintf(datei,trennZeichen);%fprintf(datei,',');
        end
    end
    fprintf(datei,'\n');
end
fclose(datei);
