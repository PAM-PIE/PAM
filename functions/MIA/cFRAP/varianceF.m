function [sigma2 Numpix]=varianceF(Im2,Nimages,bgroi,Nprebleach,Nbleach,topleftrect,sizerect,topleftroi,sizeroi,pixsize,t,tplot,D,K0,k,r2av,Nstep,Nsteppix,SI)
% HV=80;
a=SI(1);   %slop of line of average and variance
b=SI(2);   %intercept of line of average and variance
% % HV=60
% a=0.64722;   %slop of line of average and variance
% b=0.4303;   %intercept of line of average and variance

Nt=Nimages;

%Background correction:
%Nbg = size(sizebg,1); bg = zeros(1,Nbg); %Background regions.
for i = 1:Nimages
    Im_frame = Im2(:,:,i);
    bg = mean(Im_frame(bgroi(:,:,i)));
    ImFr1(:,:,i)=Im2(:,:,i)/bg; %Normalisation wrt background regions.
    Ref(i)=bg;
end

%Coordinates of the pixel centers:
[x, y] = meshgrid(0.5:1:size(Im2,2)-0.5, 0.5:1:size(Im2,1)-0.5);
x = x - topleftrect(1) + 1 - sizerect(1)/2; %Origin placed at center of bleached rectangle.
y = y - topleftrect(2) + 1 - sizerect(2)/2; %Origin placed at center of bleached rectangle.

%Extract region of interest:
Imroi = Im2(topleftroi(2):topleftroi(2)+sizeroi(2)-1, topleftroi(1):topleftroi(1)+sizeroi(1)-1,:);
ImFr1roi = ImFr1(topleftroi(2):topleftroi(2)+sizeroi(2)-1, topleftroi(1):topleftroi(1)+sizeroi(1)-1,:);
xroi = x(topleftroi(2):topleftroi(2)+sizeroi(2)-1, topleftroi(1):topleftroi(1)+sizeroi(1)-1,:);
yroi = y(topleftroi(2):topleftroi(2)+sizeroi(2)-1, topleftroi(1):topleftroi(1)+sizeroi(1)-1,:);

%Extract fitted part of bleached rectangle:
% 
% topleftplot=zeros(1,2); sizeplot=zeros(1,2);
% topleftplot(1)=max(topleftroi(1),topleftrect(1)); topleftplot(2)=max(topleftroi(2),topleftrect(2));
% sizeplot(1)=min(topleftroi(1)+sizeroi(1), topleftrect(1)+sizerect(1))-topleftplot(1); sizeplot(2)=min(topleftroi(2)+sizeroi(2), topleftrect(2)+sizerect(2))-topleftplot(2);
% Implot = Im(topleftplot(2):topleftplot(2)+sizeplot(2)-1, topleftplot(1):topleftplot(1)+sizeplot(1)-1,:);
% 
% ImFr1plot = ImFr1(topleftplot(2):topleftplot(2)+sizeplot(2)-1, topleftplot(1):topleftplot(1)+sizeplot(1)-1,:);
% 
% xplot = x(topleftplot(2):topleftplot(2)+sizeplot(2)-1, topleftplot(1):topleftplot(1)+sizeplot(1)-1,:);
% yplot = y(topleftplot(2):topleftplot(2)+sizeplot(2)-1, topleftplot(1):topleftplot(1)+sizeplot(1)-1,:);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

 for i=0:Nstep
            
            Xmin=i*Nsteppix+1;
            Ymin=i*Nsteppix+1;
            Width=sizeroi(1)-i*Nsteppix*2;
            Height=sizeroi(2)-i*Nsteppix*2;
            W(i+1)=Width;
            H(i+1)=Height;
            
           for j=1:Nt
               
                Fr(j,i+1)=mean2(Imroi(Xmin:Xmin+Width-1,Ymin:Ymin+Height-1,j));
                Fr1(j,i+1)=mean2(ImFr1roi(Xmin:Xmin+Width-1,Ymin:Ymin+Height-1,j));
                Frsumroi(j,i+1)=Fr(j,i+1)*Width*Height;
                Fr1sumroi(j,i+1)=Fr1(j,i+1)*Width*Height;
                
           end
            
   end
        
        if Nstep >= 1
            for i=1:Nstep
                FrmeanRing(:,i)=( Frsumroi(:,i)- Frsumroi(:,i+1))./(H(i)*W(i)-W(i+1)*H(i+1));
                Fr1meanRing(:,i)=( Fr1sumroi(:,i)- Fr1sumroi(:,i+1))./(H(i)*W(i)-W(i+1)*H(i+1));
            end
     
        
            for i=1:Nstep
                 Fr(:,i)=FrmeanRing(:,i);
                 Fr1(:,i)=Fr1meanRing(:,i);
            end
        
        end
        
       if Nstep >= 1
        for i=1:Nstep
            Numpix(i)=H(i)*W(i)-W(i+1)*H(i+1);
        end
            Numpix(i+1)=W(i+1)*H(i+1);
       else
           
           Numpix=W(1)*H(1);
       end
        
                    
% Fr=mean(mean(Implot(:,:,1:Nimages)));
% Fr1=mean(mean(ImFr1plot(:,:,1:Nimages)));
% Fr=reshape(Fr,1,Nimages);
% Fr1=reshape(Fr1,1,Nimages);

Ref=Ref';
for i=1:Nstep+1
    ref(:,i)=Ref;
end

for i=1:Nstep+1
sigma2Fr(:,i)=(a*Fr(:,i)+b)/Numpix(i);
sigma2Ref(:,i)=(a*ref(:,i)+b)/Numpix(i);
sigma2Fr1(:,i)=(sigma2Fr(:,i)./(Fr(:,i).^2)+sigma2Ref(:,i)./(ref(:,i).^2)).*Fr1(:,i).^2;
end
for i=1:Nstep+1
    for j=1:Nprebleach
    F0(j,i)=Fr1(j,i);
    sigma20(j,i)=sigma2Fr1(j,i);
    end
end

% F0(1,:)=mean(F0,2);
% sigma20(1,:)=mean(sigma20,2);

for i=1:Nstep+1
   F(:,i)=Fr1(:,i)./F0(i);
   sigma21(:,i)=(sigma2Fr1(:,i)./(Fr1(:,i).^2)+sigma20(i)./(F0(i).^2)).*F(:,i).^2;
end
  i=1;
  j=i+Nprebleach+Nbleach;
for i=1:numel(t)
    sigma2(i,:)=sigma21(j,:);
    j=j+1;
end
















% Frt1=mean(mean(Imroi(:,:,1:Nimages)));
% Brt=mean(mean(Imbg(:,:,1:Nimages)));
% Rrt=Frt1(1);
% sgmFrt1=a*Frt1+b;
% Frt2=(Frt1-Brt)/(Rrt-Brt(1));
% sgmFrt2=()






