function savecheck = mat2csv_wheaders(filepath,headers,M,DLM)

%mat2csv_wheaders(filepath,headers,M,DLM)
%
%This function combines the flexibility of the function cell2csv (is able
%to write any data type to a csv file) with the speed of the Matlab
%function dlmwrite for numeric data. It is used to write a csv file with
%one or more rows of headers followed by the numerical data.
%
%FILEPATH is the full path and filename of the csv file. HEADERS is a cell
%array of containing the headers as strings. M is the matrix containing the
%numeric data. The number of columns in HEADERS must be the same as the
%number of columns of M. The number of rows in HEADERS is unlimited.

if ~iscell(headers) | size(headers,2)~=size(M,2) | ~isnumeric(M) | ~ischar(DLM)
    error('Wrong data input.')
end

savecheck = cell2csv(filepath,headers,DLM); %Writes the headers to the csv file
if savecheck~=-1
    dlmwrite(filepath,M,'-append','delimiter',DLM) %appends the numerical data to the csv file
end