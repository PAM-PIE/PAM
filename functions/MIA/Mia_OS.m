function Mia_OS
x = 'start';
while ~strcmp(x,'stop')
    if ~strcmp(x,'start')
        try
            Mia(x)
        catch
            fprintf('SPECTRAL_RICS_ERROR: Analysis not successful!\n')
        end
    end
    x = input('Complete path to file: \n');
end
if strcmp(x,'stop')
    h = guidata(findobj('Tag','Mia'));
    delete(h.Mia)
end
