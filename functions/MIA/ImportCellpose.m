function Data = ImportCellpose(filepath)
%IMPORTCELLPOSE Summary of this function goes here
%   Detailed explanation goes here
npy_data = py.numpy.load(filepath, allow_pickle=true).item();
keys = string(py.list(npy_data.keys()));

for i = 1:length(keys)
    dat = npy_data{keys(i)};
    type = class(dat);
    switch type
        case 'py.list'
            switch keys(i)
                case "chan_choose"
                    Data.(keys(i)) = uint8(dat);
                case "flows"
                    flows = cell(dat);
                    Data.(keys(i)) = cellfun(@cast_ndarray, flows, 'UniformOutput', false);
                case "manual_changes"
                    tmp = cell(dat);
                    tmp = cellfun(@cell, tmp, 'UniformOutput', false);
                    Headings = {'time', 'action', 'data'};
                    tmp = cellfun(@(x) cell2struct(x, Headings, 2), tmp);
                    for j = 1:length(tmp)
                        tmp(j).time = string(tmp(j).time);
                        tmp(j).action = string(tmp(j).action);
                        tmp(j).data = cell(tmp(j).data);
                        tmp(j).data = cellfun(@cast_ndarray, tmp(j).data, 'UniformOutput', false);
                    end
                    Data.(keys(i)) = tmp;
                otherwise
                    Data.(keys(i)) = string(dat);
            end
        case 'py.numpy.ndarray'
            Data.(keys(i)) = cast_ndarray(dat);
        case 'py.str'
            Data.(keys(i)) = string(dat);
        case 'py.dict'
            tmp = struct(dat);
            Data.(keys(i)) = structfun(@cast_dict, tmp, 'UniformOutput', false);
        otherwise
            Data.(keys(i)) = dat;
            if contains(type, 'py.')
                disp(['Unconverted python type ' type ' in field ' keys(i)])
            end
    end
end
end

function out = cast_ndarray(dat)
dtype = string(dat.dtype.name);
switch dtype
    case 'bool'
        dtype = 'logical';
    case 'float32'
        dtype = 'single';
    case 'float64'
        dtype = 'double';
end
out = cast(dat,dtype);
end

function out = cast_dict(dat)
if isa(dat, 'py.NoneType')
    out = [];
else
    out = double(dat);
end
end