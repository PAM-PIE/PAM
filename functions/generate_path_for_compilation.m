% execute this script from PAM's root folder to obtain a shell script with
% compilation instructions

% Preferred File/Foldername of the App that will be compiled
%AppName = 'PAM';
%AppName = 'Mia_UI';
AppName = 'Mia_CMD';

% Filename of the main function of the App
%MainFunction = 'Launcher.m';
%MainFunction = 'Mia.m';
MainFunction = fullfile('functions', 'MIA', 'Mia_OS.m');

% add folders to path
addpath(genpath(['.' filesep 'functions']));
% get folders
if ~ispc
    folders = strsplit(genpath('functions'),':');
else
    folders = strsplit(genpath('functions'),';');
end
% exclude C_Files folder as it causes an error
remove = find(cell2mat(cellfun(@(x) ~isempty(strfind(x,'C_Files')),folders,'UniformOutput',false)));
folders(remove) = [];

% basic command
command = ['mcc -C -o ' AppName ' -W main:' AppName ' -T link:exe -d ' AppName '_compiled -v ' MainFunction ' -a functions/Custom_Read_Ins -a functions/MIA/ReadIn -a images -a Models -a functions/bfmatlab/bioformats_package.jar'];

if ispc
    command = strrep(command,'/','\');
end
% add additonal folders
for i = 1:numel(folders)
    if ~isempty(folders{i})
        if ispc
            command = [command ' -I ''' folders{i} ''''];
        else
            command = [command ' -I ''' folders{i} ''''];
        end
    end
end

% write bash script
fid = fopen(['compile_' AppName '.sh'],'w');

if isunix
    fprintf(fid,'#!/bin/bash\n');
end
fprintf(fid,'%s',command);
fclose(fid);