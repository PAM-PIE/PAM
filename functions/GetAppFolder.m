function GetAppFolder()
%%% Find the folder where PAM.m or PAM.app/PAM.exe is located
global PathToApp
if isdeployed
    PathToApp = ctfroot;%GetExeLocation();
    [~,name,~] = fileparts(PathToApp);
    name = name(1:end-4);
    %the actual name of the deployed app is the name user wants to give _mcr, so we take just the actual name.
    if ~ismac % on UNIX and Windows, the relevant data is placed in a subfolder "PAM" in ctfroot
        PathToApp = [PathToApp filesep name]; 
    elseif ismac
        PathToApp = [PathToApp filesep name]; % 13-06-2022: It seems that on Mac, this is now also the case
    end
    fprintf(['You can find the profiles and fit models at: ' PathToApp '.\n']);
%     if ismac
%         %%% navigate out of the packaged app to folder of .app
%         for i = 1:4
%             PathToApp = fileparts(PathToApp);
%         end
%     elseif ispc
%         %%% remove filename and extension
%         PathToApp = fileparts(PathToApp);
%     end
else
    %%% if not deployed, the folder to PAM. is one up from /functions
    PathToApp = [fileparts(mfilename('fullpath')) filesep '..'];
end