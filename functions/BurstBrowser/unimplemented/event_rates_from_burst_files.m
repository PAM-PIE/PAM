function event_rates_from_burst_files(save_plots)
global BurstData
%% parameters for script
if nargin < 1
    save_plots = true;
end

% check for pore_sizes.csv file in folder
fn_csv = fullfile(fileparts(BurstData{1}.PathName),'pore_sizes.csv');
if exist(fn_csv,'file') == 2
    pore_size = readmatrix(fn_csv);
end

warning off export_fig:exportgraphics
%% get data and generate table
if isempty(BurstData)
    disp('Load some data first.');
    return;
end
fn = cell(numel(BurstData),1);
dur = zeros(numel(BurstData),1);
rates = zeros(numel(BurstData),2);
for i = 1:numel(BurstData)
    % get name
    fn{i} = BurstData{i}.FileNameSPC;
    % get measurement duration
    dur(i) = BurstData{i}.FileInfo.MeasurementTime;
    % get events detected in the different channels
    for j = 1:2
        rates(i,j) = sum(BurstData{i}.DataArray(:,strcmp(BurstData{i}.NameArray,'Detected Channel'))==j)./dur(i);
    end
end
filename = fn;
duration = dur;
rate_channel1 = rates(:,1);
rate_channel2 = rates(:,2);

%% try to infer additional metadata from filenames
concentration = zeros(numel(BurstData),1);
analyte = cell(numel(BurstData),1);
pore_number = zeros(numel(BurstData),1);
BSAafterKap = false;
for i = 1:numel(BurstData)
    % get analyte (Kap, BSA, or both)
    if contains(filename{i},'bsa','IgnoreCase',true) && contains(filename{i},'kap','IgnoreCase',true)
        if contains(filename{i},'afterkap','IgnoreCase',true)
            analyte{i} = 'BSA (after Kap)';
            BSAafterKap = true;
        else
            analyte{i} = 'Kap+BSA';
        end
    elseif contains(filename{i},'bsa','IgnoreCase',true)
        analyte{i} = 'BSA';
    elseif contains(filename{i},'kap','IgnoreCase',true)
        analyte{i} = 'Kap';
    elseif contains(filename{i},'dye','IgnoreCase',true)
        analyte{i} = 'Dyes';
    else
        analyte{i} = '';
    end
    %if contains(filename{i},'wash','IgnoreCase',true)
    %    analyte{i} = ['wash (after ' analyte{i} ')'];
    %end
    % get concentration of analyte
    % check if there is a concentration specified
    if contains(filename{i},'nm','IgnoreCase',true) % nm or nM is specified
        if contains(filename{i},'nm')
            pat = digitsPattern + 'nm';
        elseif contains(filename{i},'nM')
            pat = digitsPattern + 'nM';
        end
        c = extract(extract(filename{i},pat),digitsPattern);
        concentration(i) = str2double(c{1});
    else
        concentration(i) = NaN;
    end
    % get pore number
    if contains(filename{i},'pore','IgnoreCase',true)
        try
            pat = 'pore' + digitsPattern;
            n = extract(extract(filename{i},pat),digitsPattern);
            pore_number(i) = str2double(n{1});
        catch
            try
                pat = 'pore_' + digitsPattern;
                n = extract(extract(filename{i},pat),digitsPattern);
                pore_number(i) = str2double(n{1});
            catch
                pore_number(i) = NaN;
            end
        end

    else
        pore_number(i) = NaN;
    end
end

%% save event rates as a table
if ~exist('pore_size','var') % no size information
    data = table(filename,pore_number,analyte,concentration,rate_channel1,rate_channel2,duration);
    % sort according to concentration, analyte, pore number
    data = sortrows(data,{'pore_number','analyte','concentration'});
else
    pore_diameter = pore_size(pore_number);
    data = table(filename,pore_number,pore_diameter,analyte,concentration,rate_channel1,rate_channel2,duration);
    % sort according to concentration, analyte, pore number
    data = sortrows(data,{'pore_diameter','analyte','concentration'});
end

%% do some further analysis and plotting (fitting of event rate / nM for each pore)
foldername = [fileparts(BurstData{1}.PathName) filesep 'results_' date];
if ~exist(foldername,'dir')
    mkdir(foldername);
end
% save data (use parent folder name as file name)
[~,parent_folder] = fileparts(fileparts(BurstData{1}.PathName));
writetable(data,[foldername filesep parent_folder '.xlsx'],'Sheet','Event Rates');
%% first, plot the result as bar plot
% format labels
labels = {};
for i = 1:numel(data.analyte)
    if ~contains(data.analyte{i},'wash')   
        labels{i} = sprintf('pore %d - %s %d nM',data.pore_number(i),data.analyte{i},data.concentration(i));
    else
        labels{i} = sprintf('pore %d - %s',data.pore_number(i),data.analyte{i});
    end
    if i > 1
        % check for duplicates
        if any(strcmp(labels{i},labels(1:i-1)))
            labels{i} = [labels{i} '\_1'];
        end
        c = 2;
        label_root = labels{i};
        while any(strcmp(labels{i},labels(1:i-1)))
            labels{i} = [label_root(1:end-1),num2str(c)];
            c = c+1;
        end
    end
end
f = figure('Position',[50,50,1000,600],'Color',[1,1,1]); hold on;
bar([data.rate_channel1,data.rate_channel2],'BarWidth',1,'LineWidth',1);
set(gca,'XTick',1:numel(labels),'XTickLabel',labels);
set(gca,'TickDir','out','Box','off','XTickLabelRotation',-90,'LineWidth',1.5,'FontSize',20,'Color',[1,1,1]);
ylabel('event rate (Hz)');
% add vertical dashed lines to indicate the different pores
for i = unique(pore_number)'
    xline(find(data.pore_number==i,1,'last')+0.5,'LineStyle','--');
    ax = gca;
    xpos = find(data.pore_number==i,1,'first');    
    if exist('pore_size','var')
        % plot diameter as text        
        text(xpos,ax.YLim(2)*0.95,sprintf('\\oslash %d nm',round(pore_size(i))),'FontSize',20);
    else
        % plot pore number
        text(xpos,ax.YLim(2)*0.95,sprintf('pore %d',i),'FontSize',20);
    end
end
legend({'BSA','Kap'},'EdgeColor','none','Location','eastoutside');

% save
if save_plots
    savefig([foldername filesep 'overview.fig']);
    exportgraphics(gca,[foldername filesep 'overview.png'],'Resolution',300);
    exportgraphics(gca,[foldername filesep 'overview.pdf']);
    delete(f);
end
%%
slope_Kap = NaN(1,max(pore_number));
slope_BSA = NaN(1,max(pore_number));
if BSAafterKap
    slope_BSAafterKap = NaN(1,max(pore_number));
end
for i = unique(pore_number)'
    % estimate the slopes
    bsa_idx = contains(analyte,'BSA') & ~contains(analyte,'(after Kap)') & pore_number == i;
    if ~all(bsa_idx == 0)
        rates_BSA = rate_channel1(bsa_idx); c_BSA = concentration(bsa_idx);
        f_BSA = fit(c_BSA,rates_BSA,@(a,x) a*x,'StartPoint',1,'Lower',0,'Upper',Inf);
        slope_BSA(i) = f_BSA.a;
        try
            slope_ci_BSA(i) = diff(confint(f_BSA,0.68))/2;
        catch
            slope_ci_BSA(i) = 0;
        end
    else
        rates_BSA = NaN;
        c_BSA = NaN;
        slope_BSA(i) = NaN;
        slope_ci_BSA(i) = NaN;
        f_BSA = @(x) NaN;
    end

    kap_idx = contains(analyte,'Kap') & ~contains(analyte,'(after Kap)') & pore_number == i;
    if ~all(kap_idx == 0)
        rates_Kap = rate_channel2(kap_idx); c_Kap = concentration(kap_idx);
        f_Kap = fit(c_Kap,rates_Kap,@(a,x) a*x,'StartPoint',1,'Lower',0,'Upper',Inf);
        slope_Kap(i) = f_Kap.a;
        try
            slope_ci_Kap(i) = diff(confint(f_Kap,0.68))/2;
        catch
            slope_ci_Kap(i) = 0;
        end
    else
        rates_Kap = NaN;
        c_Kap = NaN;
        slope_Kap(i) = NaN;
        slope_ci_Kap(i) = NaN;
        f_Kap = @(x) NaN;
    end

    BSA_measured_after_Kap = any(strcmp(analyte,'BSA (after Kap)') & pore_number == i);
    if BSA_measured_after_Kap
        BSAafterKap_idx = strcmp(analyte,'BSA (after Kap)') & pore_number == i;
        rates_BSAafterKap = rate_channel1(BSAafterKap_idx);
        c_BSAafterKap = concentration(BSAafterKap_idx);
        f_BSAafterKap = fit(c_BSAafterKap,rates_BSAafterKap,@(a,x) a*x,'StartPoint',1,'Lower',0,'Upper',Inf);
        slope_BSAafterKap(i) = f_BSAafterKap.a;
        try
            slope_ci_BSAafterKap(i) = diff(confint(f_BSAafterKap,0.68))/2;
        catch
            slope_ci_BSAafterKap(i) = 0;
        end
    end
    %kap_bsa_idx = strcmp(analyte,'Kap+BSA') & pore_number == i;
    %rates_Kap_BSA = rate_channel2(kap_bsa_idx);
    %rates_BSA_Kap = rate_channel1(kap_bsa_idx);
    %c_Kap_BSA = concentration(kap_bsa_idx);
    %slope_Kap_BSA(i) = sum(c_Kap_BSA.*rates_Kap_BSA)/sum(c_Kap_BSA.^2);
    %slope_BSA_Kap(i) = sum(c_Kap_BSA.*rates_BSA_Kap)/sum(c_Kap_BSA.^2);
    

    colors = lines(2);
    c = 0:1:max(concentration)*1.1;
    if ~(all(isnan(rates_BSA)) && all(isnan(rates_Kap)))
        %%% prepare plots of each rate-concentation dependence with fit
        % BSA
        f = figure('Position',[50,50,500,400],'Color',[1,1,1]); hold on;
        % first fits
        p = [];
        p_f_BSA = plot(c,f_BSA(c)); set(p_f_BSA,'LineStyle','-','Color',colors(1,:),'LineWidth',1.5);
        p_f_Kap = plot(c,f_Kap(c)); set(p_f_Kap,'LineStyle','-','Color',colors(2,:),'LineWidth',1.5);
        if BSA_measured_after_Kap
            p_f_BSAafterKap = plot(c,f_BSAafterKap(c)); set(p_f_BSAafterKap,'LineStyle',':','Color',colors(1,:),'LineWidth',1.5);
        end
        %plot(c,c*slope_BSA_Kap(i),'--','Color',colors(1,:),'LineWidth',1.5);
        %plot(c,c*slope_Kap_BSA(i),'--','Color',colors(2,:),'LineWidth',1.5);        
        % then data
        p(end+1) = scatter(c_BSA,rates_BSA,100,'Marker','o','MarkerFaceColor',colors(1,:),'MarkerEdgeColor',[0,0,0]);
        p(end+1) = scatter(c_Kap,rates_Kap,100,'Marker','o','MarkerFaceColor',colors(2,:),'MarkerEdgeColor',[0,0,0]);
        if BSA_measured_after_Kap
            p(end+1) = scatter(c_BSAafterKap,rates_BSAafterKap,100,'Marker','o','MarkerFaceColor','none','MarkerEdgeColor',colors(1,:));
        end
        xlim([0,max([max(c_Kap),max(c_BSA)])*1.1]);
        %p(end+1) = scatter(c_Kap_BSA,rates_BSA_Kap,100,'o','MarkerFaceColor','none','MarkerEdgeColor',colors(1,:),'LineWidth',1);
        %p(end+1) = scatter(c_Kap_BSA,rates_Kap_BSA,100,'o','MarkerFaceColor','none','MarkerEdgeColor',colors(2,:),'LineWidth',1);
        
    
        set(gca,'Box','on','LineWidth',1.5,'FontSize',18,'Color',[1,1,1]);
        if exist('pore_size','var')
            title(sprintf('pore #%d, \\oslash = %d nm',i,round(pore_size(i))));
        else
            title(sprintf('pore #%d',i));
        end
        ylabel('event rate (Hz)');
        xlabel('protein concentration (nM)');
        
        if BSA_measured_after_Kap
            legend(p,{'BSA','Kap','BSA (after Kap)'...
                },'Location','northwest','EdgeColor','none','Color',[1,1,1]);
        else
            legend(p,{'BSA','Kap'...
                },'Location','northwest','EdgeColor','none','Color',[1,1,1]);
        end
        if save_plots    
            % save plots
            savefig([foldername filesep sprintf('event_rates_pore%d.fig',i)]);
            exportgraphics(gca,[foldername filesep sprintf('event_rates_pore%d.png',i)],'Resolution',300);
            exportgraphics(gca,[foldername filesep sprintf('event_rates_pore%d.pdf',i)]);
        end
        delete(f);
    end
end

%% prepare plot of rates vs pores
if ~exist('pore_size','var')
    pore_size = unique(pore_number);
end
[pore_size, idx] = sort(pore_size);

f = figure('Position',[50,50,500,400],'Color',[1,1,1]);
set(f,'defaultAxesColorOrder',[0.5,0.5,0.5;0,0,0]);
hold on;
%plot(pore_size,1000*slope_BSA(idx),'Color',colors(1,:),'LineWidth',1.5);
%plot(pore_size,1000*slope_Kap(idx),'Color',colors(2,:),'LineWidth',1.5);
%plot(pore_size,slope_BSA_Kap,'--','Color',colors(2,:),'LineWidth',1.5);
%plot(pore_size,slope_Kap_BSA,'--','Color',colors(1,:),'LineWidth',1.5);
p = [];
p(end+1) = errorbar(pore_size,1000*slope_BSA(idx),1000*slope_ci_BSA(idx)); set(p(end),'MarkerSize',10,'Marker','o','MarkerFaceColor',colors(1,:),'MarkerEdgeColor',[0,0,0],'LineStyle','none','LineWidth',1,'Color',colors(1,:));
p(end+1) = errorbar(pore_size,1000*slope_Kap(idx),1000*slope_ci_Kap(idx)); set(p(end),'MarkerSize',10,'Marker','o','MarkerFaceColor',colors(2,:),'MarkerEdgeColor',[0,0,0],'LineStyle','none','LineWidth',1,'Color',colors(2,:));
if BSAafterKap
    p(end+1) = errorbar(pore_size,1000*slope_BSAafterKap(idx),1000*slope_ci_BSAafterKap(idx)); set(p(end),'MarkerSize',10,'Marker','o','MarkerFaceColor','none','MarkerEdgeColor',colors(1,:),'LineStyle','none','LineWidth',1,'Color',colors(1,:));
end
%p(end+1) = scatter(pore_size,slope_BSA_Kap,100,'o','MarkerFaceColor','none','MarkerEdgeColor',colors(2,:),'LineWidth',1);
%p(end+1) = scatter(pore_size,slope_Kap_BSA,100,'o','MarkerFaceColor','none','MarkerEdgeColor',colors(1,:),'LineWidth',1);

set(gca,'Box','on','LineWidth',1.5,'FontSize',18,'Color',[1,1,1]);
ylabel('event rate per \muM (s^{-1}\muM^{-1})');
if ~all(diff(pore_size)==1) %nargin > 0
    xlabel('pore diameter (nm)');
else
    xlabel('pore #');
end

if BSAafterKap
    legend(p,{'BSA','Kap','BSA (after Kap)',...
        },'Location','northwest','EdgeColor','none','Color','none');
else
legend(p,{'BSA','Kap'...
    %,'BSA (Kap)'...,'Kap (BSA)'
    },'Location','northwest','EdgeColor','none','Color','none');
end

if ~all(diff(pore_size)==1)
    xlim([min(pore_size)-5,max(pore_size)+5]);
else
    xlim([min(pore_size)-0.5,max(pore_size)+0.5]);
end

fit_event_rate = true;
% Fit expected dependency of translocation rate on pore size
if fit_event_rate && ~all(diff(pore_size)==1)
    % define the relatibe diffusion coefficient, reduced due to pore confinement
    rel_Dp = @(Rg,R) (1+(9/8)*(Rg./R).*log(Rg./R)...
        -1.56034*(Rg./R)+0.528155*(Rg./R).^2+...
        1.91521*(Rg./R).^3-2.81903*(Rg./R).^4+...
        0.270788*(Rg./R).^5+1.10115*(Rg./R).^6-...
        0.435933*(Rg./R).^7); %Diffusion coefficient in the pore (according to eq. 11), in µm2/s
    
    % sanitize input by removing zero values
    data_Kap = 1000*slope_Kap(idx)'; 
    valid_Kap = data_Kap > 0;
    data_BSA = 1000*slope_BSA(idx)';
    valid_BSA = data_BSA > 0;
    fit_Kap_eventrate = fit(pore_size(valid_Kap),data_Kap(valid_Kap),@(a,b,x) a.*(x/2-b).^2,'StartPoint',[1,5],'Lower',[0,0],'Upper',[Inf,Inf]);
    Rg_Kap = fit_Kap_eventrate.b;
    try
        fit_BSA_eventrate = fit(pore_size(valid_BSA),data_BSA(valid_BSA),@(a,b,x) a.*(x/2-b).^2,'StartPoint',[1,5],'Lower',[0,0],'Upper',[Inf,Inf]);
        Rg_BSA = fit_BSA_eventrate.b;
    catch
        fit_BSA_eventrate = @(x) NaN;
        Rg_BSA = NaN;
    end

    if BSAafterKap
        data_BSAafterKap = 1000*slope_BSAafterKap(idx)'; 
        valid_BSAafterKap = data_BSAafterKap > 0;
        try
            fit_BSAafterKap_eventrate = fit(pore_size(valid_BSAafterKap),data_BSAafterKap(valid_BSAafterKap),@(a,b,x) a.*(x/2-b).^2,'StartPoint',[1,5],'Lower',[0,0],'Upper',[Inf,Inf]);
            Rg_BSAafterKap = fit_BSAafterKap_eventrate.b;
        catch 
            fit_BSAafterKap_eventrate = @(x) NaN;
            Rg_BSAafterKap = NaN;
        end
    end

    xl = get(gca,'XLim'); yl = get(gca,'YLim');
    R_Kap = linspace(2*Rg_Kap,xl(2),1000);
    R_BSA = linspace(2*Rg_BSA,xl(2),1000);
    
    p_f_BSA_eventrate = plot(R_BSA,fit_BSA_eventrate(R_BSA)); set(p_f_BSA_eventrate,'LineStyle','-','Color',colors(1,:),'LineWidth',1.5);
    p_f_Kap_eventrate = plot(R_Kap,fit_Kap_eventrate(R_Kap)); set(p_f_Kap_eventrate,'LineStyle','-','Color',colors(2,:),'LineWidth',1.5);
    if BSAafterKap
        R_BSAafterKap = linspace(2*Rg_BSAafterKap,xl(2),1000);
        p_f_BSAafterKap_eventrate = plot(R_BSAafterKap,fit_BSAafterKap_eventrate(R_BSAafterKap));
        set(p_f_BSAafterKap_eventrate,'LineStyle',':','Color',colors(1,:),'LineWidth',1.5);
    end
    
    set(gca,'YLim',yl); % reset ylim
    set(gca,'XLim',xl);%[2*min([R_Kap,R_BSA]),xl(2)]); % reset xlim

    % redraw legend
    legend('off');
    if BSAafterKap
        legend(p,{sprintf('BSA (b = %.2f nm)',Rg_BSA),sprintf('Kap (b = %.2f nm)',Rg_Kap), sprintf('BSA (after Kap) (b = %.2f nm)',Rg_BSAafterKap)...
        },'Location','northwest','EdgeColor','none','Color','none');
    else
        legend(p,{sprintf('BSA (b = %.2f nm)',Rg_BSA),sprintf('Kap (b = %.2f nm)',Rg_Kap)...
        },'Location','northwest','EdgeColor','none','Color','none');
    end
end

% set lower ylim to zero
yl = get(gca,'YLim');
set(gca,'YLim',[0,yl(2)]);

if 0
    % plot dyes event rates for reference
    if any(strcmp(analyte,'Dyes'))
        % find dyes
        idx_dyes = strcmp(analyte,'Dyes');
        yyaxis right;
        hold on;
        scatter(pore_size(pore_number(idx_dyes)),rate_channel1(idx_dyes),100,'x','MarkerEdgeColor',colors(1,:),'LineWidth',1,'DisplayName','Alexa488');
        scatter(pore_size(pore_number(idx_dyes)),rate_channel2(idx_dyes),100,'x','MarkerEdgeColor',colors(2,:),'LineWidth',1,'DisplayName','Alexa647');
        ylabel('Dyes 100 nM');
    end
end
if save_plots
    % save plots
    savefig([foldername filesep 'event_rates_vs_pore_size.fig']);
    exportgraphics(gca,[foldername filesep 'event_rates_vs_pore_size.png'],'Resolution',300);
    exportgraphics(gca,[foldername filesep 'event_rates_vs_pore_size.pdf']);
    delete(f);
end

% plot the selectivity ratio versus pore size
f = figure('Position',[50,50,500,400],'Color',[1,1,1]);
set(f,'defaultAxesColorOrder',[0.5,0.5,0.5;0,0,0]);
hold on;

% calculate selectivity ratio as Kap/BSA
selectivity_ratio = slope_Kap(idx)./slope_BSA(idx);
% estimate the uncertainty based on uncertainties of the event rates
ci_selectivity_ratio = (slope_Kap(idx)./slope_BSA(idx)).*sqrt((slope_ci_Kap(idx)./slope_Kap(idx)).^2 + (slope_ci_BSA(idx)./slope_BSA(idx)).^2);

p = [];
p(end+1) = errorbar(pore_size,selectivity_ratio,ci_selectivity_ratio); set(p(end),'MarkerSize',10,'Marker','o','MarkerFaceColor',[0,0,0],'MarkerEdgeColor',[0,0,0],'LineStyle','none','LineWidth',1,'Color',[0,0,0]);
%p(end+1) = scatter(pore_size,slope_BSA_Kap,100,'o','MarkerFaceColor','none','MarkerEdgeColor',colors(2,:),'LineWidth',1);
%p(end+1) = scatter(pore_size,slope_Kap_BSA,100,'o','MarkerFaceColor','none','MarkerEdgeColor',colors(1,:),'LineWidth',1);

set(gca,'Box','on','LineWidth',1.5,'FontSize',18,'Color',[1,1,1]);
ylabel('selectivity ratio (Kap/BSA)');
if ~all(diff(pore_size)==1) %nargin > 0
    xlabel('pore diameter (nm)');
else
    xlabel('pore #');
end

if ~all(diff(pore_size)==1)
    xlim([min(pore_size)-5,max(pore_size)+5]);
else
    xlim([min(pore_size)-0.5,max(pore_size)+0.5]);
end

% set ylim based on data points, not errorbars
try
    ylim([0,max(selectivity_ratio(isfinite(selectivity_ratio)))*1.1])
end
% add zero selectivity line
yline(0.5,'--','Color',[0.5,0.5,0.5],'LineWidth',1.5);

if save_plots
    % save plots
    savefig([foldername filesep 'selectivity_ratio_vs_pore_size.fig']);
    exportgraphics(gca,[foldername filesep 'selectivity_ratio_vs_pore_size.png'],'Resolution',300);
    exportgraphics(gca,[foldername filesep 'selectivity_ratio_vs_pore_size.pdf']);
    delete(f);
end

% add plot of event rate Kap vs event rate BSA
f = figure('Position',[50,50,500,400],'Color',[1,1,1]);
set(f,'defaultAxesColorOrder',[0.5,0.5,0.5;0,0,0]);
hold on;

if ~all(diff(pore_size)==1)
    scatter(slope_BSA(idx),slope_Kap(idx),pore_size,pore_size,'o','filled');
    colormap(batlow);
    c = colorbar();
    c.Label.String = "pore diameter (nm)";
else
    scatter(slope_BSA(idx),slope_Kap(idx),'o','filled');
end
xylim = [min(min(slope_BSA(idx)),min(slope_Kap(idx)))/10,max(max(slope_BSA(idx)),max(slope_Kap(idx)))*10];

plot(xylim,0.5*xylim,'--k');
text(xylim(2)/7,xylim(2)/2,'y=0.5x','FontSize',14);

xlabel('Event rate BSA (Hz/µM)');
ylabel('Event rate Kap (Hz/µM)');
set(gca,'YColor',colors(2,:),'XColor',colors(1,:),'Color',[1,1,1],'Box','off','FontSize',14,'LineWidth',1.5);
legend('off');
set(gca,'XScale','log','YScale','log');

xlim(xylim);
ylim(xylim);

if save_plots
    % save plots
    savefig([foldername filesep 'event_rate_Kap_vs_BSA.fig']);
    exportgraphics(gca,[foldername filesep 'event_rate_Kap_vs_BSA.png'],'Resolution',300);
    exportgraphics(gca,[foldername filesep 'event_rate_Kap_vs_BSA.pdf']);
    delete(f);
end


%%% write the final result to the table
slope_BSA = 1000*slope_BSA'; slope_ci_BSA = 1000*slope_ci_BSA';
slope_Kap = 1000*slope_Kap'; slope_ci_Kap = 1000*slope_ci_Kap';
selectivity_ratio = selectivity_ratio'; ci_selectivity_ratio = ci_selectivity_ratio';
if ~BSAafterKap
    data_result = table(pore_size,slope_BSA,slope_ci_BSA,slope_Kap,slope_ci_Kap,selectivity_ratio,ci_selectivity_ratio);
else
    % BSA was measured after Kap
    slope_BSAafterKap = 1000*slope_BSAafterKap'; slope_ci_BSAafterKap = 1000*slope_ci_BSAafterKap';
    data_result = table(pore_size,slope_BSA,slope_ci_BSA,slope_Kap,slope_ci_Kap,slope_BSAafterKap,slope_ci_BSAafterKap,selectivity_ratio,ci_selectivity_ratio);
end
data_result = sortrows(data_result,'pore_size');
writetable(data_result,[foldername filesep parent_folder '.xlsx'],'Sheet','Results');
end

%%% Predict reduced diffusion due to constriction
%%% Outputs the relative diffusion coefficient Dp/D0
function rel_Dp = calc_rel_Dp(Rg,R)
rel_Dp =(1+(9/8)*(Rg./R).*log(Rg./R)...
        -1.56034*(Rg./R)+0.528155*(Rg./R).^2+...
        1.91521*(Rg./R).^3-2.81903*(Rg./R).^4+...
        0.270788*(Rg./R).^5+1.10115*(Rg./R).^6-...
        0.435933*(Rg./R).^7); %Diffusion coefficient in the pore (according to eq. 11), in µm2/s
end
%%% Predict expected event rate "r" based on Fick's law
%%% Accounting for reduced diffusion due to constriction
function [r,r_ideal] = event_rate(R,L,c,Rg,D)
    NA = 6.022E23; % avogadros constant, mol^-1
    
    A = pi*R.^2; % pore area in nm2
    Dp =(1+(9/8)*(Rg./R).*log(Rg./R)...
        -1.56034*(Rg./R)+0.528155*(Rg./R).^2+...
        1.91521*(Rg./R).^3-2.81903*(Rg./R).^4+...
        0.270788*(Rg./R).^5+1.10115*(Rg./R).^6-...
        0.435933*(Rg./R).^7).*D; %Diffusion coefficient in the pore (according to eq. 11), in µm2/s
    
    J = (Dp*1000000)*(c*0.000000001*1E-24)/L; % flux in mol/s/nm2
    r = J.*A.*NA;
    
    J_ideal = (D*1000000)*(c*0.000000001*1E-24)/L; % flux in mol/s/nm2
    r_ideal = J_ideal.*A.*NA;
end