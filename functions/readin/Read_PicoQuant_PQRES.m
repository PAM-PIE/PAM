function [Data, Header] = Read_PicoQuant_PQRES(FileName)
%%% Loads PicoQuant PQRES files from SymphoTime software
%%% Input parameters:
%%% Filename: Full filename
Header = struct;
Header.CurveNames = {};
Header.CountRates = {};
Data = {};

fid=fopen(FileName,'r');
fseek(fid,0,1);
filesize = ftell(fid);
fseek(fid,0,-1);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% ASCII file header processing
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% some constants
tyEmpty8      = hex2dec('FFFF0008');
tyBool8       = hex2dec('00000008');
tyInt8        = hex2dec('10000008');
tyBitSet64    = hex2dec('11000008');
tyColor8      = hex2dec('12000008');
tyFloat8      = hex2dec('20000008');
tyTDateTime   = hex2dec('21000008');
tyFloat8Array = hex2dec('2001FFFF');
tyAnsiString  = hex2dec('4001FFFF');
tyWideString  = hex2dec('4002FFFF');
tyBinaryBlob  = hex2dec('FFFFFFFF');
% RecordTypes
rtPicoHarpT3     = hex2dec('00010303');% (SubID = $00 ,RecFmt: $01) (V1), T-Mode: $03 (T3), HW: $03 (PicoHarp)
rtPicoHarpT2     = hex2dec('00010203');% (SubID = $00 ,RecFmt: $01) (V1), T-Mode: $02 (T2), HW: $03 (PicoHarp)
rtHydraHarpT3    = hex2dec('00010304');% (SubID = $00 ,RecFmt: $01) (V1), T-Mode: $03 (T3), HW: $04 (HydraHarp)
rtHydraHarpT2    = hex2dec('00010204');% (SubID = $00 ,RecFmt: $01) (V1), T-Mode: $02 (T2), HW: $04 (HydraHarp)
rtHydraHarp2T3   = hex2dec('01010304');% (SubID = $01 ,RecFmt: $01) (V2), T-Mode: $03 (T3), HW: $04 (HydraHarp)
rtHydraHarp2T2   = hex2dec('01010204');% (SubID = $01 ,RecFmt: $01) (V2), T-Mode: $02 (T2), HW: $04 (HydraHarp)
rtTimeHarp260NT3 = hex2dec('00010305');% (SubID = $00 ,RecFmt: $01) (V1), T-Mode: $03 (T3), HW: $05 (TimeHarp260N)
rtTimeHarp260NT2 = hex2dec('00010205');% (SubID = $00 ,RecFmt: $01) (V1), T-Mode: $02 (T2), HW: $05 (TimeHarp260N)
rtTimeHarp260PT3 = hex2dec('00010306');% (SubID = $00 ,RecFmt: $01) (V1), T-Mode: $03 (T3), HW: $06 (TimeHarp260P)
rtTimeHarp260PT2 = hex2dec('00010206');% (SubID = $00 ,RecFmt: $01) (V1), T-Mode: $02 (T2), HW: $06 (TimeHarp260P)
rtMultiHarpNT3   = hex2dec('00010307');% (SubID = $00 ,RecFmt: $01) (V1), T-Mode: $03 (T3), HW: $07 (MultiHarp150N)
rtMultiHarpNT2   = hex2dec('00010207');% (SubID = $00 ,RecFmt: $01) (V1), T-Mode: $02 (T2), HW: $07 (MultiHarp150N)
% Globals for subroutines
TTResultFormat_TTTRRecType = 0;
TTResult_NumberOfRecords = 0;
%Create a structure called Header to store all the outputs of Read_PTU
%function
Header.MeasDesc_Resolution = 0;
Header.MeasDesc_GlobalResolution = 0;

Magic = fread(fid, 8, '*char');
if not(strcmp(Magic(Magic~=0)','PQRESLT') | strcmp(Magic(Magic~=0)','PQRESFCS'))
    error('Magic invalid, this is not an PQRES Lifetime or FCS file.');
    return;
end
Version = fread(fid, 8, '*char');

%%% Following code reads out all values from header

% there is no repeat.. until (or do..while) construct in matlab so we use
% while 1 ... if (expr) break; end; end;
while 1
    % read Tag Head
    TagIdent = fread(fid, 32, '*char'); % TagHead.Ident
    TagIdent = (TagIdent(TagIdent ~= 0))'; % remove #0 and more more readable
    TagIdx = fread(fid, 1, 'int32');    % TagHead.Idx
    TagTyp = fread(fid, 1, 'uint32');   % TagHead.Typ
    % TagHead.Value will be read in the
    % right type function
    TagIdent = genvarname(TagIdent);
    if TagIdx > -1
        EvalName = [TagIdent '(' int2str(TagIdx + 1) ')'];
    else
        EvalName = TagIdent;
    end
    if strcmp(EvalName(1),'$');
        EvalName = EvalName(2:end);
    end
    %fprintf(1,'\n   %-40s', EvalName);
    % check Typ of Header
    switch TagTyp
        case tyEmpty8
            fread(fid, 1, 'int64');
            %fprintf(1,'<Empty>');
        case tyBool8
            TagInt = fread(fid, 1, 'int64');
            if TagInt==0
                %fprintf(1,'FALSE');
                eval([EvalName '=false;']);
            else
                %fprintf(1,'TRUE');
                eval([EvalName '=true;']);
            end
        case tyInt8
            TagInt = fread(fid, 1, 'int64');
            %fprintf(1,'%d', TagInt);
            eval([EvalName '=TagInt;']);
        case tyBitSet64
            TagInt = fread(fid, 1, 'int64');
            %fprintf(1,'%X', TagInt);
            eval([EvalName '=TagInt;']);
        case tyColor8
            TagInt = fread(fid, 1, 'int64');
            %fprintf(1,'%X', TagInt);
            eval([EvalName '=TagInt;']);
        case tyFloat8
            TagFloat = fread(fid, 1, 'double');
            %fprintf(1, '%e', TagFloat);
            eval([EvalName '=TagFloat;']);
        case tyFloat8Array
            TagInt = fread(fid, 1, 'int64');
            %fprintf(1,'<Float array with %d Entries>', TagInt / 8);
            %fseek(fid, TagInt, 'cof');
            eval([EvalName '= fread(fid, TagInt/8, "double");']);
        case tyTDateTime
            TagFloat = fread(fid, 1, 'double');
            %fprintf(1, '%s', datestr(datenum(1899,12,30)+TagFloat)); % display as Matlab Date String
            eval([EvalName '=datenum(1899,12,30)+TagFloat;']); % but keep in memory as Matlab Date Number
        case tyAnsiString
            TagInt = fread(fid, 1, 'int64');
            TagString = fread(fid, TagInt, '*char');
            TagString = (TagString(TagString ~= 0))';
            %fprintf(1, '%s', TagString);
            if TagIdx > -1
                EvalName = [TagIdent '(' int2str(TagIdx + 1) ',:)'];
            end;
            if strcmp(TagIdent,'UsrHeadName') && exist('UsrHeadName','var')
                %%% Catch case where length of TagString exceeds length of
                %%% UsrHeadName character array
                if eval(['size(' TagIdent ',2) < numel(TagString)'])
                    eval([TagIdent '(:,end:numel(TagString)) = '' '';']);
                end
            end
            try;eval([EvalName '=TagString;']);end;
        case tyWideString
            % Matlab does not support Widestrings at all, just read and
            % remove the 0's (up to current (2012))
            TagInt = fread(fid, 1, 'int64');
            TagString = fread(fid, TagInt, '*char');
            TagString = (TagString(TagString ~= 0))';
            %fprintf(1, '%s', TagString);
            if TagIdx > -1
                EvalName = [TagIdent '(' int2str(TagIdx + 1) ',:)'];
            end;
            eval([EvalName '=TagString;']);
        case tyBinaryBlob
            TagInt = fread(fid, 1, 'int64');
            %fprintf(1,'<Binary Blob with %d Bytes>', TagInt);
            fseek(fid, TagInt, 'cof');
        otherwise
            error('Illegal Type identifier found! Broken file?');
    end;
    if strcmp(TagIdent, 'Header_End')
        break
    end
end
Header.File_Comment = File_Comment;
switch VarScriptIdent 
    case 'FCSFitting' % FCS file
        Index_Count_Rate = strfind(File_Comment, 'Average:');
        % check for channels
        if exist('VarAutoFCSA','var') 
            Data{end+1} = [VarAutoFCSAX, VarAutoFCSAY,VarAutoFCSAStdDevY];
            Header.CurveNames{end+1} = VarFileName_A;
            countrate = sscanf(File_Comment(Index_Count_Rate(1) + length('Average:'):end), '%g', 1);;
            Header.CountRates{end+1} = [countrate,countrate];
        end
        if exist('VarAutoFCSB','var')
            Data{end+1} = [VarAutoFCSBX, VarAutoFCSBY,VarAutoFCSBStdDevY];
            Header.CurveNames{end+1} = VarFileName_B;
            countrate = sscanf(File_Comment(Index_Count_Rate(2) + length('Average:'):end), '%g', 1);
            Header.CountRates{end+1} = [countrate,countrate];
        end
        if exist('VarFCCSCurve','var')
            Data{end+1} = [VarFCCSCurveX, VarFCCSCurveY,VarFCSCurveStdDevY];
            Header.CurveNames{end+1} = VarFileName_B;
            Header.CountRates{end+1} = [Header.CountRates{1}(1),Header.CountRates{2}(1)];
        end
    case 'TCSPCFitting'
        % check for channels
        if exist('VarChannel1','var') 
            Data{end+1} = [VarChannel1X, VarChannel1Y];
        end
        if exist('VarChannel2','var') 
            Data{end+1} = [VarChannel2X, VarChannel2Y];
        end
end
